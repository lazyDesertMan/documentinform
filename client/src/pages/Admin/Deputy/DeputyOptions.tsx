import React from "react";
import { Button, Card, Stack} from "react-bootstrap";
import LoadListDeputy from "../../../components/Deputy/ViewListDeputy";
import '../../../css/style.css';

const DeputyOptions = () => {
    return(
        <>
        <div className="pages">
            <h1 style={{color: 'gray', minWidth: '18vw', position: 'absolute', verticalAlign: 'baseline', left: '7vw', backgroundColor: 'white', borderRadius: '5px', paddingLeft: '6px', paddingRight: '6px'}}>Заместители</h1>
            <div style={{border: '2px solid gray',  margin: '2%',padding: '20px'}}>
                <Stack direction="horizontal" gap={3} style={{ padding: '15px'}}>
                    <Card style={{minHeight: '15%', width: '33%'}}>
                        <Card.Body style={{height: '35%'}}>
                            <Card.Title>Добавить постоянного заместителя</Card.Title>
                            <Card.Text style={{textAlign: 'left'}}>
                                Необходимо:
                                <ul>
                                    <li style={{color: 'red'}}>Группа добавляемого заместителя</li>
                                    <li style={{color: 'red'}}>Должность заместителя</li>
                                </ul>
                                <br></br>
                            </Card.Text>
                            <Button style={{width: '100%'}} variant="primary"  href={process.env.REACT_APP_CLIENT_URL +"/admin/depFunc/add"}>Открыть</Button>
                        </Card.Body>
                    </Card>
                   <Card style={{minHeight: '15%', width: '33%'}}>
                        <Card.Body style={{height: '35%'}}>
                            <Card.Title>Удалить заместителя</Card.Title>
                            <Card.Text style={{textAlign: 'left'}}>
                                Необходимо:
                                <ul>
                                    <li style={{color: 'red'}}>Группа удаляемого заместителя</li>
                                    <li style={{color: 'red'}}>Удаляемый заместитель</li>
                                </ul>
                                <br></br>
                            </Card.Text>
                            <Button style={{width: '100%'}} variant="primary" href={process.env.REACT_APP_CLIENT_URL +"/admin/depFunc/delete"}>Открыть</Button>
                        </Card.Body>
                    </Card>
                    <Card style={{minHeight: '15%', width: '33%'}}>
                        <Card.Body style={{height: '35%'}}>
                            <Card.Title>Добавить временного заместителя</Card.Title>
                            <Card.Text style={{textAlign: 'left'}}>
                                Необходимо:
                                <ul>
                                    <li style={{color: 'red'}}>Группа</li>
                                    <li style={{color: 'red'}}>Заменяющий сотрудник</li>
                                    <li style={{color: 'red'}}>Дата начала</li>
                                    <li style={{color: 'red'}}>Дата завершения</li>
                                </ul>
                            </Card.Text>
                            <Button style={{width: '100%'}} variant="primary" href={process.env.REACT_APP_CLIENT_URL +"/admin/depFunc/addTempDeputy"}>Открыть</Button>
                        </Card.Body>
                    </Card>
                </Stack>
            </div>
            <br></br>
            <LoadListDeputy />
        </div>
        </>
    );
};
export default DeputyOptions;