import React from "react";
import { useParams } from "react-router-dom";
import '../../../css/style.css';
import AddPerDeputy from "../../../components/Deputy/AddPerDeputy";
import DeletePerDeputy from "../../../components/Deputy/DeleteDeputy";
import AddTempDeputy from "../../../components/Deputy/AddTempDeputy";

const DeputyFunctional = () => {
    const {path} = useParams();
    return(
        <div className="pages">    
            { 
            path === "add"? 
                <AddPerDeputy />
            : path === "addTempDeputy"?
                <AddTempDeputy />
            :
                <DeletePerDeputy />
            }
        </div>
    );
};
export default DeputyFunctional;