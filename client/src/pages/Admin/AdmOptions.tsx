import React, { useState } from "react";
import { Form} from "react-bootstrap";
import '../../css/style.css';
import AdminHard from "../../components/Admin/AdminHard";
import AdminSimplified from "../../components/Admin/AdminSimplified";

const Options = () => {
    let [isSimplified, setIsSimplified] = useState(true);
    return(
        <>
        <div className="pages">
        <h1 style={{color: 'gray', minWidth: '18vw', position: 'absolute', verticalAlign: 'baseline', left: '7vw', backgroundColor: 'white', borderRadius: '5px', paddingLeft: '6px', paddingRight: '6px'}}>Возможности администратора</h1>
            <div style={{border: '2px solid gray',  margin: '2%',padding: '20px'}}>
            <Form.Check 
            type="switch"
            id="custom-switch"
            defaultChecked
            label="Упрощенное меню"
            style={{float: 'right'}}
            onChange={e => setIsSimplified(e.target.checked)}
            />
            <br></br>
            {isSimplified?
                <AdminSimplified />
            : <AdminHard />}
        </div></div>
        </>
    );
};
export default Options;