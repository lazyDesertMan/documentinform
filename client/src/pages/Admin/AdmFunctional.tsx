import React from "react";
import AddGroup from "../../components/Groups/AddGroup";
import { useParams } from "react-router-dom";
import AddWorker from "../../components/Users/AddWorker";
import AddPosition from "../../components/Positions/AddPosition";
import ChangeGroup from "../../components/Groups/ChangeGroup";
import DeletePosition from "../../components/Positions/DeletePosition";
import '../../css/style.css';
import ChoiceDeputy from "../../components/Deputy/AddTempDeputy";
import DeleteGroup from "../../components/Groups/DeleteGroup";
import ChangePosition from "../../components/Positions/ChangePosition";
import ChangeWorker from "../../components/Users/ChangeWorker";
import DeleteWorker from "../../components/Users/DeleteWorker";

const Add = () => {
    const {path} = useParams();
    return(
        <div className="pages">
            
            { 
            // Группы
            path === "addGroup"? 
                <AddGroup/>
            : path === "updateGroup"?
                <ChangeGroup/>
            : path === "choiceDeputy"?
                <ChoiceDeputy />
            : path === "deleteGroup"?
                <DeleteGroup/>
            // Должности
            : path === "addPos"?
                <AddPosition/>
            : path === "updatePos"?
                <ChangePosition/>
            : path === "deletePos"?
                <DeletePosition/>
            // Сотрудники
            : path === "updateUser"?
                <ChangeWorker/>
            : path === "addUser"?
                <AddWorker />
            :
                <DeleteWorker />
            }
            
        </div>
    );
};
export default Add;