import React from "react";
import LoadDataForProfile from "../components/Users/ViewUserInfo";

const Profile = () => {
    return(
        <div className="pages">
            <LoadDataForProfile />
        </div>
    );
};
export default Profile;