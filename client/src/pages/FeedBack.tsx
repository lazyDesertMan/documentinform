import React, { useState } from "react";
import { Button, Card, Col, Form, Row, Stack } from "react-bootstrap";
import { SetFeedBack } from "../apiRequests/FeedbackRequests";

const FeedBack = () => {
    const [fname, setFName] = useState("");
    const [topic, setTopic] = useState("");
    const [message, setMessage] = useState("");
    const [agree, setAgree] = useState(false);

    const B_SendFeedBack = async () =>{
        if(message.length > 0){
            await SetFeedBack(message, topic, fname);
        }   
    }
    return(
        <div className="pages d-flex justify-content-center align-items-center">
            <Card style={{borderRadius: '40px', width: '75%'}} border="2px">
                <h1 className="d-flex justify-content-center align-items-center" style={{marginBottom: '5%', marginTop: '1%'}}>Обратная связь</h1>
                <div className="d-flex justify-content-center align-items-center">
                    <Form style={{width: '65%'}}>
                        <Form.Group as={Row} className="mb-3" controlId="FName">
                            <Form.Label column sm="2">
                            Имя
                            </Form.Label>
                            <Col sm="10">
                            <Form.Control type="text" placeholder="Имя" 
                                onChange={e=>setFName(e.target.value)}/>
                            </Col>
                        </Form.Group>

                        <Form.Group as={Row} className="mb-3" controlId="SName">
                            <Form.Label column sm="2">
                            Тема
                            </Form.Label>
                            <Col sm="10">
                            <Form.Control type="text" placeholder="Тема" 
                                onChange={e=>setTopic(e.target.value)}/>
                            </Col>
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="Feedback">
                            <Form.Label>Оставьте сообщение <strong style={{color: 'red'}}>*</strong></Form.Label>
                            <Form.Control as="textarea" rows={3} 
                                onChange={e=>setMessage(e.target.value)}/>
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="controlCheckbox_1">
                            <Form.Check 
                                type="checkbox" 
                                label="Я согласен с передачей указанного мной имени разработчикам" 
                                onChange={(e) => { setAgree(e.target.checked) }}/>
                        </Form.Group>
                        <div className="d-flex justify-content-center align-items-center" >
                            <Button style={{margin: '1%'}} onClick={B_SendFeedBack}
                            disabled={message.length > 0 && (agree || fname.length === 0)? false : true}>Отправить</Button>
                        </div>
                    </Form>
                    
                </div>
            </Card>
        </div>
    );
};
export default FeedBack;