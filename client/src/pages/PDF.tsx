import React from "react";
import { Button} from "react-bootstrap";
import {DOCUMENT_ROUTE } from "../utils/consts";
import '../css/style.css';
import { useNavigate, useParams } from "react-router-dom";
import { SetCompleted } from "../apiRequests/TaskRequests";

const PDF = () => {
    const {id, taskID} = useParams();
    const navigate = useNavigate();
    return(
        <div className="pages">
            <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
                <iframe src={"http://localhost:1337/api/document/read/" + id} width="100%" height="685em" title="documentDotPDF"></iframe>
            </div>
            <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
                {taskID ?
                    <Button 
                        variant={"outline-dark"}
                        onClick={() =>  {
                            SetCompleted(taskID);
                            navigate(DOCUMENT_ROUTE)
                        }}
                    >
                    Ознакомиться
                    </Button>
                    :
                    <div></div>
                }
            </div>
        </div>
    );
};
export default PDF;