import React from "react";
import '../css/style.css';
import '../css/home-styles.css';
import logo from "../assets/logo.png";
import LogIn from "../components/Users/LogIn";
import Cookies from 'js-cookie';
import AuthCallback from "../classes/AuthCallback";

const Home = (callback : AuthCallback) => {
    const usrCookie = Cookies.get("usr");
    return(
        <>
            <div className="page">
                <main className="content">
                    <div className="flex-column">
                        <div className="flex-row">
                            <div className="three-blocks"><img className="left" src={logo} alt="Логотип" width="60%"></img></div>
                        </div>
                        <div className="flex-row margin-left-right" style={{marginTop: '7%'}}>
                            <div className="two-blocks two-blocks-left-block" style={{textAlign: 'center'}}>
                                <strong >Система информирования сотрудников об изменениях в локальных документах организации</strong><br></br>
                                <small>АО "Аэропорт Астрахань"</small>
                            </div>
                            {usrCookie != null ?
                                <div className="two-blocks two-blocks-right-block poluprozrachiy"></div>
                            :
                                <div className="two-blocks two-blocks-right-block poluprozrachiy"><LogIn func = {callback.func}/></div>
                            }
                        </div>
                    </div>
                </main>
                <div className="d-flex justify-content-center align-items-center text-style" style={{color : 'black'}}>© 2022 Copyright: document_inform</div>
            </div>
        </>
    );
};
export default Home;