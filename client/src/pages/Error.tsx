import React from "react";

class ErrorParams {
    msg? : string;
}

const Error = (error : ErrorParams) => {

    return(
        <div className="pages">
            {
                error.msg ? <div className="d-grid gap-2 d-sm-flex justify-content-sm-center"
                    style={{color: 'green', fontSize: '50px', marginTop: '10%'}}>Ошибка: {error.msg}
                </div>
                : <div className="d-grid gap-2 d-sm-flex justify-content-sm-center"
                    style={{color: 'green', fontSize: '50px', marginTop: '10%'}}>Ошибка
                </div>
            }
        </div>
    );
};
export default Error;