import React from "react";
import ViewDocuments from "../components/Documents/ViewDocuments";

const Document = () => {
    return(
        <div className="pages">
            <ViewDocuments />
        </div>
    );
};
export default Document;