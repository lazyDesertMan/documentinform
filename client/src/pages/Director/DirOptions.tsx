import React from "react";
import { Button, Card, Stack} from "react-bootstrap";
import '../../css/style.css';

const Documents = () => {
    return(
        <>
        <div className="pages">
            <h1 style={{color: 'gray', minWidth: '18vw', position: 'absolute', verticalAlign: 'baseline', left: '7vw', backgroundColor: 'white', borderRadius: '5px', paddingLeft: '6px', paddingRight: '6px'}}>Возможности директора</h1>
            <div style={{border: '2px solid gray',  margin: '2%',padding: '20px'}}>
                <Stack direction="horizontal" gap={4} style={{ padding: '15px'}}>
                    <Card style={{minHeight: '15%', minWidth: '20%'}}>
                        <Card.Body>
                            <Card.Title>Добавить документ</Card.Title>
                            <Card.Text style={{textAlign: 'left'}}>
                                Необходимо:
                                <ul>
                                    <li style={{color: 'red'}}>Название</li>
                                    <li>Доп. информация</li>
                                    <li style={{color: 'red'}}>Дата вступления в силу</li>
                                    <li style={{color: 'red'}}>Загрузить документ</li>
                                </ul>
                                <p></p><br></br><br></br><br></br>
                            </Card.Text>
                            <Button style={{width: '100%'}} variant="primary"  href={process.env.REACT_APP_CLIENT_URL +"/director/functional/add"}>Открыть</Button>
                        </Card.Body>
                    </Card>
                    <Card style={{minHeight: '15%', minWidth: '20%'}}>
                        <Card.Body>
                            <Card.Title>Обновить документ</Card.Title>
                            <Card.Text style={{textAlign: 'left'}}>
                                Необходимо:
                                <ul>
                                    <li style={{color: 'red'}}>Выбрать редактируемый документ</li>
                                    <li>Ввести новое название</li>
                                    <li>Ввести описание новой версии</li>
                                    <li>Назначить дату вступления в силу</li>
                                    <li>Загрузить документ</li>
                                    <li>Выбрать старую версию</li>
                                </ul>
                                <br></br>
                            </Card.Text>
                            <Button style={{width: '100%'}} variant="primary" href={process.env.REACT_APP_CLIENT_URL +"/director/functional/update"}>Открыть</Button>
                        </Card.Body>
                    </Card>
                    <Card style={{minHeight: '15%', minWidth: '20%'}}>
                        <Card.Body>
                            <Card.Title>Отправить документ</Card.Title>
                            <Card.Text style={{textAlign: 'left'}}>
                                Необходимо:
                                <ul>
                                    <li style={{color: 'red'}}>Выбрать тип задания</li>
                                    <li style={{color: 'red'}}>Выбрать документ</li>
                                    <li style={{color: 'red'}}>Выбрать получателя</li>
                                    <li style={{color: 'red'}}>Выбрать дату начала</li>
                                    <li style={{color: 'red'}}>Выбрать дедлайн</li>
                                </ul>
                                <br></br><br></br>
                            </Card.Text>
                            <Button style={{width: '100%'}} variant="primary" href={process.env.REACT_APP_CLIENT_URL +"/director/functional/send"}>Открыть</Button>
                        </Card.Body>
                    </Card>
                </Stack>
            </div>
        </div>
        </>
    );
};
export default Documents;