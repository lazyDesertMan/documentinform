import React, { useState } from "react";
import { Form, Stack } from "react-bootstrap";
import { useParams } from "react-router-dom";
import LoadDocumentReportComp from "../../components/Reports/ViewDocumentReport";

const Reports = () => {
    const {idDoc}= useParams();

    const [startDate, setStartDate] = useState("");
    const [endDate, setEndDate] = useState("");
    return(
        <div className="pages">
            <div className="d-flex justify-content-center align-items-center">
                <Stack direction="horizontal" gap={3}>
                    <strong>Фильтрация отчета : </strong>
                    <Form.Group className="mb-3" controlId="controlInput_StartDate">
                        <Form.Label>Дата начала заданий</Form.Label>
                        <Form.Control 
                            type="date"
                            onChange={e=>setStartDate(e.target.value)} />
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="controlInput_EndDate">
                        <Form.Label>Дата завершения заданий</Form.Label>
                        <Form.Control 
                            type="date"
                            onChange={e=>setEndDate(e.target.value)} />
                    </Form.Group>
                </Stack>
            </div>
            <br></br>
            <br></br>
            <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
                <LoadDocumentReportComp idDoc = {parseInt(idDoc)} startDate = {startDate} endDate ={endDate}/>
            </div>
        </div>
    );
};
export default Reports;