import React from "react";
import AddDoc from "../../components/Documents/AddDoc"
import { useParams } from "react-router-dom";
import ChangeVersionDoc from "../../components/Documents/ChangeDoc";
import '../../css/style.css';
import AddTask from "../../components/Tasks/AddTask";

const Functional = () => {
    const {path} = useParams();
    return(
        <div className="pages">    
            { 
            path === "add"? 
                <AddDoc />
            : path === "update"?
                <ChangeVersionDoc/>
            : 
                <AddTask />
            }
        </div>
    );
};
export default Functional;