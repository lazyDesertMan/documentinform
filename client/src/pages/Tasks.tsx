import React from "react";
import '../css/style.css';
import LoadTasksComp from "../components/Tasks/ViewTasks";
import Footer from "../components/Footer";

function Tasks() {
    return (
        <div className="pages">
            <LoadTasksComp />
            <br></br>
            <Footer/>
        </div>
    );
};
export default Tasks;