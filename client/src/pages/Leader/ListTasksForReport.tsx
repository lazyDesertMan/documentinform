import React, { useState } from "react";
import { Form, Stack } from "react-bootstrap";
import LoadResendListComp from "../../components/Reports/ViewResendList";

const ListTasksForReport = () => {
    const [sDate, setStartDate] = useState(new Date(0).toString());
    const [eDate, setEndDate] = useState(new Date(8_640_000_000_000_000).toString());
    const [click, setClick] = useState(false);
    const B_ViewReport = () => {
        setClick(true);
    }
    const setStartD = (value : string)=>{
        if(typeof value === "string"){
            setStartDate(value);
            setClick(false);
        }
        else
            setStartDate("");
    }
    const setEndD = (value : string)=>{
        if(typeof value === "string"){
            setEndDate(value);
            setClick(false);
        }
        else
            setEndDate("");
    }
    return(
        <div className="pages">
            <h2 className="m-auto">Вывод списка заданий</h2>
            <br></br>
            <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
                <Stack direction="horizontal" gap={4}>
                    <Form.Group className="mb-3" controlId="controlInput_StartDate">
                        <Form.Label>Начальная дата получения заданий</Form.Label>
                        <Form.Control 
                            type="date"
                            onChange={e=>setStartD(e.target.value)} />
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="controlInput_EndDate">
                        <Form.Label>Конечная дата получения заданий</Form.Label>
                        <Form.Control 
                            type="date"
                            onChange={e=>setEndD(e.target.value)} />
                    </Form.Group>
                </Stack>
            </div>
            <br></br>
            <br></br>                
            <LoadResendListComp startDate={sDate ? sDate : new Date(0).toString()} endDate={eDate ? eDate : new Date(8_640_000_000_000_000).toString()}/>
        </div>
    );
};
export default ListTasksForReport;