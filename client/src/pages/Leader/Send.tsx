import React from "react";
import '../../css/style.css';
import { useParams } from "react-router-dom";
import SendTask from "../../components/Tasks/SendTask";

class Data{
    idDoc : number;
    nameDoc : string;
    taskID : number;
    recipient : number;
    constructor(id : number, name : string, idTask : number, recipient : number){
        this.idDoc = id;
        this.nameDoc = name;
        this.taskID = idTask;
        this.recipient = recipient;
    }
}
const Send = () => {
    const {idDoc, nameDoc, taskID, recipient}= useParams();
    var data = new Data(parseInt(idDoc), nameDoc, parseInt(taskID), parseInt(recipient));
    return(
        <div className ="pages">
            <SendTask recipient={data.recipient} idDoc = {data.idDoc} nameDoc ={data.nameDoc} taskID ={data.taskID}/>
        </div>
    );
};
export default Send;