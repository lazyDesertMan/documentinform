import React, { useEffect, useState } from "react";
import { CheckOnLeader } from "../../apiRequests/GroupRequests";
import LoadInfoAboutMyGrop_Comp from "../../components/mygroup/group_leader";

class loadStates {
    isLeader: boolean;
}

function MyGroup() {
    const [leader, setIsLeader] = useState<loadStates>({isLeader: false});
    
    useEffect(() => {
        CheckOnLeader().then(isLeader => setIsLeader({isLeader : isLeader}));
    }, []);

    return <PageAboutMyGroup isLeader={leader.isLeader} />
}

class viewProps {
    isLeader: boolean;
}
const PageAboutMyGroup = (props: viewProps) => {
    return(
        <div className="pages">
            {props.isLeader?
                <LoadInfoAboutMyGrop_Comp />
           :<div className="d-grid gap-2 d-sm-flex justify-content-sm-center"
                style={{color: 'green', fontSize: '50px', marginTop: '10%'}} >Нет доступа</div> }
        </div>
    );
};
export default MyGroup;