import { TaskStatus } from "./SelfMonitoringReport";
import { ITask } from "./Task";


/**
 * Строка отчёта о заданиях
 */
 class TaskStatusRecord {
    task   : ITask;
    status : TaskStatus;

    constructor(task : ITask, status : TaskStatus) {
        this.task = task;
        this.status = status;
    }
}

/**
 * Часть отчёта, содержащая сведения о текущем и предыдущих заданиях
 */
class TaskStatusTree {
    task    : TaskStatusRecord;             //!< Задание
    childs   : TaskStatusTree[];             //!< Предыдущие задания
}

/**
 * Отчёт по иерархии заданий по выбранному документу
 */
class DocumentReport {
    public readonly date            : Date;                  //!< Дата формирования отчёта
    public readonly report          : TaskStatusTree[];      //!< Иерархия заданий
}

export {
    DocumentReport
}