import { ITask } from "./Task";

/**
 * Перечисление возможных состояний задания
 */
enum TaskStatus {
    TSTAT_COMPLETE           = 1,   //!< Задание выполнено
    TSTAT_ACTIVE             = 2,   //!< Задание не выполнено, но сроки выполнения не нарушены
    TSTAT_DEADLINE_VIOLATION = 3    //!< Задание не выполнено и нарушены сроки выполнения задания
}

/**
 * Строка отчёта о заданиях
 */
class ReportLine {
    public readonly task   : ITask;
    public readonly state : TaskStatus;

    constructor(task : ITask, status : TaskStatus) {
        this.task = task;
        this.state = status;
    }
}

/**
 * Часть отчёта, содержащая сведения о задания по должности
 */
class PositionRecord {
    public readonly position     : string;        //!< Должность пользователя
    public readonly tasks        : ReportLine[];  //!< Состояния заданий

    constructor(position : string) {
        this.position = position;
        this.tasks = [];
    }

    public add(line : ReportLine) : void {
        this.tasks.push(line);
    }
}

/**
 * Отчёт о заданиях сотрудника
 */
class SelfMonitoringReport {
    public readonly date         : Date;              //!< Дата формирования отчёта
    public readonly userName     : string;            //!< ФИО пользователя
    public readonly records      : PositionRecord[];  //!< Сведения по должностям

    constructor(userName : string) {
        this.date = new Date();
        this.userName = userName;
        this.records = [];
    }

    public add(record : PositionRecord) : void {
        this.records.push(record);
    }
}

export {
    TaskStatus,
    ReportLine,
    PositionRecord,
    SelfMonitoringReport
}