/**
 * Тип задания
 */
 enum TaskType {
    READ_TASK_TYPE = 1,
    RESEND_TASK_TYPE = 2
}

/**
 * Интерфейс для всех типов заданий
 */
interface ITask {
    ID         : number;    //!< ID задания
    document   : string     //!< Название документа
    documentID : number;    //!< ID документа, связанного с заданием
    senderID   : number;    //!< ID должности сотрудника, выдавшего задание
    startDate  : Date;      //!< Дата выдачи задания
    deadline   : Date;      //!< Крайний срок выполнения задания
    prevTask   : number;    //!< ID предыдущего задания
    type       : TaskType;  //!< Тип задания
}
/**
 * Задание на пересылку документа
 */
 class ResendTask implements ITask {
    public ID         : number;    //!< ID задания
    public document   : string;    //!< Название документа
    public documentID : number;    //!< ID документа, связанного с заданием
    public senderID   : number;    //!< ID должности сотрудника, выдавшего задание
    public recipient  : number;    //!< ID должности, с которой должна проводиться пересылка
    public startDate  : Date;      //!< Дата выдачи задания
    public deadline   : Date;      //!< Крайний срок выполнения задания
    public prevTask   : number;    //!< ID предыдущего задания
    public type       : TaskType;  //!< Тип задания

    constructor (id : number, name : string, doc : number, sender : number, recip : number, start : Date, end : Date, prevTask : number = null) {
        this.ID = id;
        this.document = name;
        this.documentID = doc;
        this.senderID = sender;
        this.recipient = recip;
        this.startDate = start;
        this.deadline = end;
        this.prevTask = prevTask;
        this.type = TaskType.RESEND_TASK_TYPE;
    }
}
/**
 * Задание на чтение документа
 */
 class ReadTask implements ITask {
    public ID         : number;    //!< ID задания
    public document   : string;    //!< Название документа
    public documentID : number;    //!< ID документа, связанного с заданием
    public senderID   : number;    //!< ID должности сотрудника, выдавшего задание
    public recipient  : number;    //!< ID сотрудника, который должен ознакомиться с документом
    public startDate  : Date;      //!< Дата выдачи задания
    public deadline   : Date;      //!< Крайний срок выполнения задания
    public prevTask   : number;    //!< ID предыдущего задания
    public type       : TaskType;  //!< Тип задания

    constructor (id : number, name : string, doc : number, sender : number, recip : number, start : Date, end : Date, prevTask : number = null) {
        this.ID = id;
        this.document = name;
        this.documentID = doc;
        this.senderID = sender;
        this.recipient = recip;
        this.startDate = start;
        this.deadline = end;
        this.prevTask = prevTask;
        this.type = TaskType.READ_TASK_TYPE;
    }
}

export {
    ITask,
    TaskType,
    ResendTask,
    ReadTask
}