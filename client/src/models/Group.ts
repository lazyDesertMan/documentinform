class Group {
    public id            : number;   //!< ID группы
    public leaderPositionID      : number;   //!< ID руководителя группы
    public parentID      : number;   //!< ID родительской группы (если есть)
    public name          : string;   //!< Название группы

    init(id : number, leaderID : number, parentID : number,  name : string) : Group {
        this.id = id;
        this.leaderPositionID = leaderID;
        this.parentID = parentID;
        this.name = name;
        return this;
    }
}

/**
 * Иерархия групп
 */
class GroupHierarchy {
    group     : Group;             //!< Группа
    subgroups : GroupHierarchy[];  //!< Подгруппы

    init(group : Group, subgroups : GroupHierarchy[]) : GroupHierarchy {
        this.group = group;
        this.subgroups = subgroups;
        return this;
    }
}

export {
    Group,
    GroupHierarchy
}