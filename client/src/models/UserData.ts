/**
 * Перечень допустимых ролей
 */
 enum Roles {
    /** Работник */
    ROLE_WORKER   = "worker",
    /** Руководитель */
    ROLE_LEADER   = "leader",
    /** Директор */
    ROLE_DIRECTOR = "director",
    /** Админ */
    ROLE_ADMIN = "administrator"
}

/*
 * Данные о пользователе системы 
 */
class UserData {
    public id:         number;  //!< ID пользователя
    public login:      string;  //!< Логин пользователя
    public role:       string;  //!< Роль пользователя в системе
    public firstName:  string;  //!< Имя пользователя
    public secondName: string;  //!< Фамилия пользователя
    public thirdName:  string;  //!< Отчество пользователя
    public email:      string;  //!< Электронная почта
    public isAuth:     boolean;

    public constructor() {
        this.id = 0;
        this.login = "";
        this.firstName = "";
        this.secondName = "";
        this.thirdName = "";
        this.email = "";
        this.role = "";
    }

    public init(id : number, login : string, role : string, fname : string, sname : string, tname : string, email : string) : UserData {
        this.id = id;
        this.login = login.trim();
        this.role = role;
        this.firstName = fname.trim();
        this.secondName = sname.trim();
        this.thirdName = tname.trim();
        this.email = email.trim();
        return this;
    }

    setAuth(auth : boolean) {
        this.isAuth = auth;
    }
}

export {
    UserData,
    Roles
}