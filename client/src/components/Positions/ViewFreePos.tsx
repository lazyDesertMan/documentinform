import React, {useEffect, useState } from "react";
import { Button, Card, Container, Form, Modal } from "react-bootstrap";
import { GetListGroup } from "../../apiRequests/GroupRequests";
import { GetFreePosition } from "../../apiRequests/PositionRequests";
import {Group} from "../../models/Group";

class Data {
    groups : Group[];
}

function ViewFreePos() {
    const [data, setData] = useState<Data>({
        groups : []
    })

    useEffect(() => {
        GetListGroup().then(groups => setData({groups : groups}));
    }, []);
    return <ViewFreePosCard groups={data.groups} />;
}
class cardProps {
    groups : Group[];
}

class viewStates {
    show : boolean;
}

function ViewFreePosCard(props : cardProps) {
    const [name, setName] = useState("");
    const [idGroup, setGroup] = useState(-1);
    const [freePosInGroup, setFreePos] = useState([]);
    let [state, setStates] = useState<viewStates>({ show : false});
    function handleClose() {
        setStates({show : false});
    }
    function handleShow(){
        setStates({show : true});
    }
    const setGroupID=async(idx)=>{
        if(idx !== -1)
        {
            setName(props.groups[idx].name);
            setGroup(props.groups[idx].id);
            await GetFreePosition(props.groups[idx].id).then(positions => setFreePos(positions));
        }           
        else
            setGroup(-1)
    }

    return(
        <Container 
            className="d-flex justify-content-center align-items-center"
            style={{height: window.innerHeight - 54}}
        >
            <Card style={{width: 590}} className="p-5">
                <h2 className="m-auto">Свободные должности в группе</h2>
                <Form className="d-flex flex-column">
                    <select 
                        name="groups" 
                        placeholder="Выберите группу" 
                        onChange={e=>setGroupID(e.target.value)}
                        className="mt-3">
                        <option key=""value={-1}>Выберите группу</option>
                        {Array.from({ length: props.groups.length }).map((_, idx) => (
                            <option key={idx} value={idx}>{props.groups[idx].name}</option>
                        ))}
                    </select>
                    {idGroup !== -1?
                    <>
                        <br></br>
                        <Button variant="primary" onClick={handleShow}>
                            Вывести
                        </Button>
                        <Modal show={state.show} fullscreen={true} onHide={handleClose}>
                            <Modal.Header closeButton>
                                <Modal.Title>Список свободных должностей в группе [{name}]</Modal.Title>
                            </Modal.Header>
                            <Modal.Body>    
                                <Container>
                                <div className="table-responsive">
                                    <table style={{fontSize:'14px'}} className="table table-striped table-sm">
                                        <thead>
                                            <tr key={"thead"}>
                                                <th scope="col">#</th>
                                                <th scope="col">Свободные должности</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        {Array.from({ length: freePosInGroup.length }).map((_, idx) => (
                                            <tr key={"tbody" + idx}>
                                            <td>{idx + 1}</td>
                                            <td>{freePosInGroup[idx].name}</td>
                                            </tr>
                                        ))}
                                        </tbody>
                                    </table>
                                </div>
                                </Container>
                            </Modal.Body>
                            <Modal.Footer>
                                <Button variant="outline-primary" onClick={handleClose}>
                                    Закрыть
                                </Button>
                            </Modal.Footer>
                        </Modal>
                    </>
                    :<></>}
                </Form>
            </Card>
        </Container>
    );
}
export default ViewFreePos;