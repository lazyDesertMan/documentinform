import React, {useEffect, useState } from "react";
import { Button, Card, Container, Form, Modal, Stack } from "react-bootstrap";
import {Group} from "../../models/Group";
import { Position } from "../../models/Position";
import { ADMIN_OPTIONS_ROUTE } from "../../utils/consts";
import { useNavigate } from "react-router-dom";
import { UserData } from "../../models/UserData";
import Select from "react-select";
import { GetListGroup } from "../../apiRequests/GroupRequests";
import { GetListPosition, GetAllPositionsInGroup, UpdatePosition } from "../../apiRequests/PositionRequests";
import { GetListUsers } from "../../apiRequests/UserRequests";

class Data {
    groups : Group[];
    users : UserData[];
    positions : Position[];
}

function ChangePosition() {
    const [data, setData] = useState<Data>({
        groups : [], users : [], positions : []
    })

    useEffect(() => {
        GetListGroup().then(groups => GetListUsers().then(users => GetListPosition().then(positions => 
            setData({groups : groups, users : users, positions : positions}))));
    }, []);
    return <ChangePositionCard groups={data.groups} users={data.users} positions={data.positions} />;
}
class cardProps {
    groups : Group[];
    users : UserData[];
    positions : Position[];
}

class Demonstration{
    oldNameGroup : string;
    youngNameGroup : string;
    oldPosName : string;
    oldUserName : string;
    youngUserName : string;

    constructor(){
        this.oldNameGroup = "";
        this.youngNameGroup = "";
        this.oldPosName = "";
        this.oldUserName = "";
        this.youngUserName = "";
    }
}
var Demon : Demonstration;
class viewStates {
    show : boolean;
}

class Pair {
    label : string;
    value : number;
}

class GroupStates {
    idGroup : number;
    data : Pair[];
}

function ChangePositionCard(props : cardProps) {
    const navigate = useNavigate();
    const [name, setName] = useState("");
    const [idGroup, setGroup] = useState<GroupStates>({data: [], idGroup: -1});
    const [idNewGroup, setNewGroup] = useState(-1);
    const [idPos, setPos] = useState(-1);

    /// Безопасность для выбора изменяемой должности
    const [dataPos, setDataPos] = useState([]);

    const [idUser, setUser] = useState(-1);
    let [state, setStates] = useState<viewStates>({ show : false});

    // Настройки
    const optionsGroup = props.groups.map(data => ({label: data.name, value: data.id}));
    optionsGroup.push({label: "Не выбрано", value: -1});
    const optionsUser = props.users.map(data => ({label: data.secondName + " " + data.firstName + " " + data.thirdName, value: data.id}));
    optionsUser.push({label: "Не выбрано", value: -1});

    useEffect(() => {
        Demon = new Demonstration();
    }, []);
    function handleClose() {
        setStates({show : false});
    }
    function handleShow(){
        setStates({show : true});
    }
    const setGroupID=async(id : number)=>{
        if(id !== -1)
        {
            const tmp : Position[] = await GetAllPositionsInGroup(id);
            const optionsPos = tmp.map(data => ({label: data.name, value: data.id}));
            optionsPos.push({label: "Не выбрано", value: -1});
            Demon.oldNameGroup = props.groups.find(f => f.id === id).name;
            setGroup({ idGroup: id, data: optionsPos });
            setPosID(-1);
            setNewGroupID(-1);
            setName("");
            setUserID(-1);
        }           
        else
            setGroup({idGroup: -1, data: []});
    }
    const setNewGroupID=(id : number)=>{
        if(id !== -1)
        {
            setNewGroup(id);
            Demon.youngNameGroup = props.groups.find(f => f.id === id).name;
        }           
        else
            setNewGroup(-1);
    }
    const setPosID=(id : number)=>{
        if(id !== -1)
        {
            Demon.oldPosName = idGroup.data.find(f => f.value === id).label;
            const position = props.positions.find(f => f.id === id);
            if(typeof(position.workerID) !== undefined) {
                const user = props.users.find(f => f.id === position.workerID);
                Demon.oldUserName = user.secondName + " " + user.firstName + " " + user.thirdName;
            }
            setPos(id);
            const tmp : Pair[] = [];
            tmp.push({label : Demon.oldPosName, value : id});
            setDataPos(tmp);
        }           
        else{
            setPos(-1);
            setDataPos([]);
        }
            
    }
    const setUserID=(id : number)=>{
        if(id !== -1) {
            setUser(id);
            const user = props.users.find(f => f.id === id);
            Demon.youngUserName = user.secondName + " " + user.firstName + " " + user.thirdName;
        }
        else {
            setUser(-1);
        }
    }
    const B_ChangePosition = async () =>{
        setStates({show : false});
        let newGroup: number = idNewGroup;
        let newUser: number = idUser;
        let newName: string = name;
        if(newGroup === -1){
            setNewGroup(undefined);
            newGroup = undefined;
        }
            
        if(newUser === -1){
            setUser(undefined);
            newUser = undefined;
        }
           
        if(newName.trim().length === 0){
            setName(undefined);
            newName = undefined;
        }
        let flag : boolean = await UpdatePosition(idPos, newGroup, newUser, newName);
        if(flag)
            navigate(ADMIN_OPTIONS_ROUTE);
    }
    return(
        <Container>
            <Stack direction="horizontal" gap={3} style={{ padding: '15px'}}>
                <div style={{width: '30vw'}}></div>
                <Card style={{width: 550, overflowX: 'hidden', overflowY: 'auto'}} className="p-5">
                    <h2 className="m-auto">Изменение должности</h2>
                    <br></br>
                    <Form style={{minHeight: '30vh'}}>
                        <Form.Group className="mb-3" controlId="controlSelectGroup">
                            <Form.Label>Выберите группу</Form.Label>
                            <Select 
                                options={optionsGroup}
                                isSearchable={true}
                                onChange={data=>setGroupID(data.value)}/>
                        </Form.Group>
                        {idGroup.idGroup !== -1 && idGroup.data.length > 1?
                        <>
                            <Form.Group className="mb-3" controlId="controlSelectPos">
                                <Form.Label>Выберите изменяемую должность</Form.Label>
                                <Select 
                                    value={dataPos}
                                    options={idGroup.data}
                                    isSearchable={true}
                                    onChange={data=>setPosID(data.value)}/>
                            </Form.Group>
                            {idPos !== -1?
                            <>
                                <Form.Group className="mb-3" controlId="controlInput_namePos">
                                    <Form.Label>Новое название должности</Form.Label>
                                    <Form.Control
                                        placeholder="Введите название..." 
                                        onChange={e=>setName(e.target.value)}/>
                                </Form.Group>
                                <Form.Group className="mb-3" controlId="controlSelectUser">
                                    <Form.Label>Выберите сотрудника на должность</Form.Label>
                                    <Select 
                                        options={optionsUser}
                                        isSearchable={true}
                                        onChange={data=>setUserID(data.value)}/>
                                </Form.Group>
                                <Form.Group className="mb-3" controlId="controlSelectNewGroup">
                                    <Form.Label>Выберите новую группу</Form.Label>
                                    <Select 
                                        options={optionsGroup}
                                        isSearchable={true}
                                        onChange={data=>setNewGroupID(data.value)}/>
                                </Form.Group>
                                <br></br>
                                {name || idNewGroup !== -1 || idUser !== -1?
                                <>
                                    <div className="d-flex justify-content-center align-items-center">
                                        <Button variant="primary" onClick={handleShow}>
                                            Предпросмотр
                                        </Button>
                                    </div>
                                    <Modal show={state.show} fullscreen={true} onHide={handleClose}>
                                        <Modal.Header closeButton>
                                            <Modal.Title>Предпросмотр изменение должности [{Demon.oldPosName}]</Modal.Title>
                                        </Modal.Header>
                                        <Modal.Body>    
                                            <Container>
                                                <div className="table-responsive">
                                                    <table style={{fontSize:'14px'}} className="table table-striped table-sm">
                                                        <thead>
                                                            <tr key="thead">
                                                                <th scope="col"></th>
                                                                <th scope="col">Старая информация о должности</th>
                                                                <th scope="col">Новая информация о должности</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr key="tbody1">
                                                                <th>Название</th>
                                                                <td>{Demon.oldPosName.length > 0? <>{Demon.oldPosName}</> : <>-</>}</td>
                                                                <td>{name ? <>{name}</> : <small>нет изменений</small>}</td>
                                                            </tr>
                                                            <tr key="tbody2">
                                                                <th>Группа</th>
                                                                <td>{Demon.oldNameGroup.length > 0 ? <>{Demon.oldNameGroup}</> : <>-</>}</td>
                                                                <td>{idNewGroup !== -1? <>{Demon.youngNameGroup}</> : <small>нет изменений</small>}</td>
                                                            </tr>
                                                            {idUser !== -1?
                                                                <>
                                                                    <tr key="tbody3">
                                                                        <th>Сотрудник</th>
                                                                        <td>{Demon.oldUserName.trim().length > 0 ? <>{Demon.oldUserName}</> : <>-</>}</td>
                                                                        <td>{idUser !== -1? <>{Demon.youngUserName}</> : <small>нет изменений</small>}</td>
                                                                    </tr>
                                                                </>
                                                            :<></>}
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </Container>
                                        </Modal.Body>
                                        <Modal.Footer>
                                            <Button variant="outline-primary" onClick={B_ChangePosition}>
                                                Изменить
                                            </Button>
                                            <Button variant="secondary" onClick={handleClose}>
                                                Отменить
                                            </Button>
                                        </Modal.Footer>
                                    </Modal>
                                </>
                                :<></>}
                            </>
                            :<></>}
                        </>
                        :<></>}
                    </Form>
                </Card>
                <div style={{width: '30vw'}}></div>
            </Stack>
        </Container>
    );
}
export default ChangePosition;