import React, {useEffect, useState } from "react";
import { Button, Card, Container, Form, Modal, Stack } from "react-bootstrap";
import {Group} from "../../models/Group";
import {UserData} from "../../models/UserData";
import { ADMIN_OPTIONS_ROUTE } from "../../utils/consts";
import { useNavigate } from "react-router-dom";
import Select from "react-select";
import { GetListGroup } from "../../apiRequests/GroupRequests";
import { GetFreeUsers, SetPosition, UpdatePosition } from "../../apiRequests/PositionRequests";

class Data {
    users : UserData[];
    groups : Group[];
}

function AddPosition() {
    const [data, setData] = useState<Data>({
        users : [], groups : []
    })

    useEffect(() => {
        GetFreeUsers().then(users => GetListGroup().then(groups => setData({users : users, groups : groups})));
    }, []);
    return <AddPositionCard users={data.users} groups={data.groups} />;
}
class Demonstration{
    nameGroup : string;
    nameUser : string;
    constructor(){
        this.nameGroup = "group";
        this.nameUser = "user";
    }
}
var Demon : Demonstration;
class cardProps {
    users : UserData[];
    groups : Group[];
}

class viewStates {
    show : boolean;
}

function AddPositionCard(props : cardProps) {
    const navigate = useNavigate();
    const [name, setName] = useState("");
    const [idGroup, setGroup] = useState(-1);
    const [idUser, setUser] = useState(-1);
    let [state, setStates] = useState<viewStates>({ show : false});

    // Настройки
    const optionsGroup = props.groups.map(data => ({label: data.name, value: data.id}));
    optionsGroup.push({label: "Не выбрано", value: -1});
    const optionsUser = props.users.map(data => ({label: data.secondName + " " + data.firstName + " " + data.thirdName, value: data.id}));
    optionsUser.push({label: "Не выбрано", value: -1});

    useEffect(() => {
        Demon = new Demonstration();
    }, []);
    function handleClose() {
        setStates({show : false});
    }
    function handleShow(){
        setStates({show : true});
    }
    const setGroupID=(id)=>{
        if(id !== -1)
        {
            Demon.nameGroup = props.groups.find(f => f.id === id).name;
            setGroup(id);
        }           
        else
            setGroup(-1)
    }
    const setUserID=(id)=>{
        if(id !== -1)
        {
            const user = props.users.find(f => f.id === id);
            Demon.nameUser = user.secondName + " " + user.firstName + " " + user.thirdName;
            setUser(id);
        }           
        else
            setUser(-1)
    }
    const B_AddPosition = async () =>{
        setStates({show : false});
        if(idUser === -1){
            await SetPosition(idGroup, name);
        }else{
            let id : number = await SetPosition(idGroup, name);
            await UpdatePosition(id, undefined, idUser, undefined);
        }
        navigate(ADMIN_OPTIONS_ROUTE);
    }
    return(
        <Container>
            <Stack direction="horizontal" gap={3} style={{ padding: '15px'}}>
                <div style={{width: '30vw'}}></div>
                <Card style={{width: 550, overflowX: 'hidden', overflowY: 'auto'}} className="p-5">
                    <h2 className="m-auto">Добавление должности</h2>
                    <br></br>
                    <Form style={{minHeight: '50vh'}}>
                        <Form.Group className="mb-3" controlId="controlInput_namePos">
                            <Form.Label>Название должности</Form.Label>
                            <Form.Control
                                placeholder="Введите название..." 
                                onChange={e=>setName(e.target.value)}/>
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="controlSelectGroup">
                            <Form.Label>Выберите группу</Form.Label>
                            <Select 
                                options={optionsGroup}
                                isSearchable={true}
                                onChange={data=>setGroupID(data.value)}/>
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="controlSelectUser">
                            <Form.Label>Выберите сотрудника</Form.Label>
                            <Select 
                                options={optionsUser}
                                isSearchable={true}
                                onChange={data=>setUserID(data.value)}/>
                        </Form.Group>
                        <br></br>
                        {idGroup !== -1 && name.trim().length > 0 ?
                        <>
                            <div className="d-flex justify-content-center align-items-center">
                                <Button variant="primary" onClick={handleShow}>
                                    Предпросмотр
                                </Button>
                            </div>
                            <Modal show={state.show} fullscreen={true} onHide={handleClose}>
                                <Modal.Header closeButton>
                                    <Modal.Title>Предпросмотр добавления должности в группу <strong>{Demon.nameGroup}</strong></Modal.Title>
                                </Modal.Header>
                                <Modal.Body>
                                    <Container className="d-flex justify-content-center align-items-center">
                                        <h1 style={{color: 'gray', minWidth: '1vw', position: 'absolute', top: '0', backgroundColor: 'white', borderRadius: '5px', paddingLeft: '6px', paddingRight: '6px'}}>{name}</h1>
                                        <div style={{border: '2px solid gray',  margin: '1%', padding: '20px', width: '50%'}}>
                                            <Card>
                                                <Card.Body>
                                                    <Card.Title>Новая должность</Card.Title>
                                                    <Card.Text style={{overflow: 'auto'}}>
                                                        <label>Группа: {Demon.nameGroup}</label><br></br>
                                                        <br></br>
                                                        <label>Добавляемая должность: {name}</label><br></br>
                                                        <br></br>
                                                        <label>Сотрудник: {idUser !== -1? <>{Demon.nameUser}</> : <small>не назначен</small>}</label><br></br>
                                                    </Card.Text>
                                                </Card.Body>
                                            </Card>
                                        </div>   
                                    </Container>
                                </Modal.Body>
                                <Modal.Footer>
                                    <Button variant="outline-primary" onClick={B_AddPosition}>
                                        Добавить
                                    </Button>
                                    <Button variant="secondary" onClick={handleClose}>
                                        Отменить
                                    </Button>
                                </Modal.Footer>
                            </Modal>
                        </>
                        :<></>}
                    </Form>
                </Card>
                <div style={{width: '30vw'}}></div>
            </Stack>
        </Container>
    );
}
export default AddPosition;