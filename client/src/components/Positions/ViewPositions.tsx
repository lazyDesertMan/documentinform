import React, { useEffect, useState } from "react";
import { GetListPosition } from "../../apiRequests/PositionRequests";
import { Position } from "../../models/Position";

class loadStates {
    isFetching: boolean;
    positions: Position[];
}

function LoadPositions_Comp() {
    let [state, setState] = useState<loadStates>({ isFetching: false, positions: [] });

    function loadData() {
        setState({ positions: state.positions, isFetching: true });
        GetListPosition().then(r => {
            setState({ positions: r, isFetching: false });
        }).catch(e => {
            console.log(e);
            setState({ positions: state.positions, isFetching: true });
        });
    }

    useEffect(() => loadData(), []);

    return (
        <div>
            {this.state.isFetching ? "Loading..." : <ViewPositions list={this.state.positions} />}
        </div>
    );
}

class viewProps {
    list: Position[];
}

function ViewPositions(props: viewProps) {
    return (
        <pre>
            {JSON.stringify(this.props.list, null, 3)}
        </pre>
    );
}
export default LoadPositions_Comp;
