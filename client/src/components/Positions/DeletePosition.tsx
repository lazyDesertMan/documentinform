import React, {useEffect, useState } from "react";
import { Button, Card, Container, Form, Modal, Stack } from "react-bootstrap";
import {Group} from "../../models/Group";
import { ADMIN_OPTIONS_ROUTE } from "../../utils/consts";
import { useNavigate } from "react-router-dom";
import Select from "react-select";
import { Position } from "../../models/Position";
import { GetListGroup } from "../../apiRequests/GroupRequests";
import { GetAllPositionsInGroup, DeletePos } from "../../apiRequests/PositionRequests";

class Data {
    groups : Group[];
}

function DeletePosition() {
    const [data, setData] = useState<Data>({
        groups : []
    })

    useEffect(() => {
        GetListGroup().then(groups => setData({groups : groups}));
    }, []);
    return <DeletePositionCard groups={data.groups} />;
}
class cardProps {
    groups : Group[];
}

class viewStates {
    show : boolean;
}
class Pair{
    label: string;
    value: number
}
class GroupStates{
    idGroup: number;
    data: Pair[];
}

function DeletePositionCard(props : cardProps) {
    const navigate = useNavigate();
    const [name, setName] = useState("");
    const [idGroup, setGroup] = useState<GroupStates>({idGroup: -1, data: []});
    const [idDeletePos, setDeletePos] = useState(-1);

    /// Безопасность для выбора удаляемой должности
    const [dataPos, setDataPos] = useState([]);

    let [state, setStates] = useState<viewStates>({ show : false});

    // Настройки
    const optionsGroup = props.groups.map(data => ({label: data.name, value: data.id}));
    optionsGroup.push({label: "Не выбрано", value: -1});

    function handleClose() {
        setStates({show : false});
    }
    function handleShow(){
        setStates({show : true});
    }
    const setGroupID=async(id : number)=>{
        if(id !== -1)
        {
            let tmp: Position[] = await GetAllPositionsInGroup(id);
            const optionsPos = tmp.map(data => ({label: data.name, value: data.id}));
            optionsPos.push({label: "Не выбрано", value: -1});
            setName(props.groups.find(f => f.id === id).name);
            setGroup({idGroup: id, data: optionsPos});
            setPosID(-1);
        }           
        else
            setGroup({idGroup: -1, data: []});
    }
    const setPosID=(id: number)=>{
        if(id !== -1)
        {
            setDeletePos(id);
            let tmp : Pair[] = [];
            tmp.push({label : idGroup.data.find(f => f.value === id).label, value : id});
            setDataPos(tmp);
        }           
        else{
            setDeletePos(-1);
            setDataPos([]);
        }
            
    }
    const B_DeletePosition = async () =>{
        setStates({show : false});
        await DeletePos(idDeletePos);
        navigate(ADMIN_OPTIONS_ROUTE);
    }
    return(
        <Container>
            <Stack direction="horizontal" gap={3} style={{ padding: '15px'}}>
                <div style={{width: '30vw'}}></div>
                <Card style={{width: 550, overflowX: 'hidden', overflowY: 'auto'}} className="p-5">
                    <h2 className="m-auto">Удаление должности</h2>
                    <br></br>
                    <Form style={{minHeight: '40vh'}}>
                        <Form.Group className="mb-3" controlId="controlSelectGroup">
                            <Form.Label>Выберите группу</Form.Label>
                            <Select 
                                options={optionsGroup}
                                isSearchable={true}
                                onChange={data=>setGroupID(data.value)}/>
                        </Form.Group>
                        {idGroup.idGroup !== -1 && idGroup.data.length > 1?
                        <>
                            <Form.Group className="mb-3" controlId="controlSelectPos">
                                <Form.Label>Выберите удаляемую должность</Form.Label>
                                <Select 
                                    value={dataPos}
                                    options={idGroup.data}
                                    isSearchable={true}
                                    onChange={data=>setPosID(data.value)}/>
                            </Form.Group>
                            <br></br>
                            {idDeletePos !== -1?
                            <>
                                <div className="d-flex justify-content-center align-items-center">
                                    <Button variant="primary" onClick={handleShow}>
                                        Предпросмотр
                                    </Button>
                                </div>
                                <Modal show={state.show} fullscreen={true} onHide={handleClose}>
                                    <Modal.Header closeButton>
                                        <Modal.Title>Предпросмотр удаления должности в группе <strong>{name}</strong></Modal.Title>
                                    </Modal.Header>
                                    <Modal.Body>    
                                        <Container>
                                        <div className="table-responsive">
                                            <table style={{fontSize:'14px'}} className="table table-striped table-sm">
                                                <thead>
                                                    <tr key={"thead"}>
                                                        <th scope="col">#</th>
                                                        <th scope="col">Должности до</th>
                                                        <th scope="col">Должности после</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                {Array.from({ length: idGroup.data.length - 1 }).map((_, idx) => (
                                                    <tr key={"tbody" + idx}>
                                                    <td>{idx + 1}</td>
                                                    <td>{idGroup.data[idx].label}</td>
                                                    {idGroup.data[idx].value === idDeletePos?
                                                        <td><small>удалена</small></td>
                                                    :   
                                                        <td>{idGroup.data[idx].label}</td>
                                                    }
                                                    </tr>
                                                ))}
                                                </tbody>
                                            </table>
                                        </div>
                                        </Container>
                                    </Modal.Body>
                                    <Modal.Footer>
                                        <Button variant="outline-primary" onClick={B_DeletePosition}>
                                            Удалить
                                        </Button>
                                        <Button variant="secondary" onClick={handleClose}>
                                            Отменить
                                        </Button>
                                    </Modal.Footer>
                                </Modal>
                            </>
                            :<></>}
                        </>
                        :<></>}
                    </Form>
                </Card>
                <div style={{width: '30vw'}}></div>
            </Stack>
        </Container>
    );
}
export default DeletePosition;