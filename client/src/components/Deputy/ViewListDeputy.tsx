import React, { useEffect, useMemo } from "react";
import { Badge, Form, Spinner, Stack } from "react-bootstrap";
import { useState } from "react";
import { PositionDeputies } from "../../models/PositionsDeputies";
import { Position } from "../../models/Position";
import { Group } from "../../models/Group";
import { GetListDeputies, GetListPosition } from "../../apiRequests/PositionRequests";
import { GetListGroup } from "../../apiRequests/GroupRequests";

class Data {
    positions : Position[];
    group : Group[];
}

function LoadListDeputy_Comp() {
    const [data, setData] = useState<Data>({
        positions : [], group : []
    })

    useEffect(() => {
        GetListPosition().then(positions => 
            GetListGroup().then(group => setData({positions : positions, group : group})));
    }, []);
    return <ViewListDeputy positions={data.positions} group={data.group}/>;
}
class cardProps {
    positions : Position[];
    group : Group[];
}
class Deputies {
    list : PositionDeputies[];
}

const ViewListDeputy = (props : cardProps) => {
    const [search, setSearch] = useState("");
    const [list, setList] = useState<Deputies>({list : []});

    const Method = async()=>{
        let res : PositionDeputies[] = [];
        for(let i : number = 0; i < props.group.length; i++){
            if(props.group[i].leaderPositionID != null)
                res.push(await GetListDeputies(props.group[i].leaderPositionID));
        }
        return res;
    }
    useEffect(() => {
        Method().then(data => setList({list : data}));
    }, [props]);

    const filteredDocs = useMemo(() => {
        if (search) {
          return list.list.filter(
            (item) =>
              item.position.name
                .toLowerCase()
                .indexOf(search.toLocaleLowerCase()) > -1
          );
        }
        return list.list;
      }, [search]);
    return(
        
        <>  
        {list.list.length > 0?
        <>
            <h2 className="d-grid gap-2 d-sm-flex justify-content-sm-center">Список должностей и их заместителей</h2>
            <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
                <Stack direction="horizontal" gap={3} style={{ width: '60%', marginTop: '1%' }}>
                    <Form.Control
                        id="myInput"
                        className="me-auto"
                        value={search} 
                        onChange={e => setSearch(e.target.value)}
                        placeholder="Найти должность..." />
                </Stack>
            </div>
            <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
                <Form style={{ width: '100%', marginLeft: '2%', marginRight: '2%', padding: '20px'}}>
                    <div className="list-group list-group-flush border-bottom scrollarea">
                        {filteredDocs.length > 0?
                            <>
                            {filteredDocs && filteredDocs.map(item =>
                                item.deputies.length > 0 || item.temporaryDeputies.length > 0?
                                <>
                                <a id="item" key={item.position.id} className="list-group-item list-group-item-action py-3 lh-tight" style={{borderRadius: '15px', backgroundColor: 'whitesmoke', marginTop: '10px'}}>
                                    <div className="d-flex w-100 align-items-center justify-content-between">
                                        <strong className="mb-1">{item.position.name} <label style={{color : 'green'}}>(Группа: {props.group.find(f => f.leaderPositionID === item.position.id).name})</label></strong>
                                    </div>
                                    <Badge bg="success">Постоянные заместители:</Badge><br></br>
                                    {item.deputies.length > 0?
                                        item.deputies.map(perDep =>
                                            <><small>{props.positions.find(f => f.id === perDep.replacer).name}</small><br></br></>
                                        )
                                    :<><small style={{color : 'gray'}}>Нет постоянных заместителей</small><br></br></>
                                    }
                                    <Badge bg="info">Временные заместители:</Badge><br></br>
                                    {item.temporaryDeputies.length > 0?
                                        item.temporaryDeputies.map(tempDep =>
                                            <><small>{props.positions.find(f => f.id === tempDep.replacer).name}</small><br></br></>
                                        )
                                    :<><small style={{color : 'gray'}}>Нет временных заместителей</small><br></br></>
                                    }
                                </a>
                                </>
                                :<></>
                            )}
                            </>
                        :
                            <>
                            {Array.from({ length: list.list.length }).map((_, idx) => (
                                list.list[idx].deputies.length > 0 || list.list[idx].temporaryDeputies.length > 0?
                                <>
                                <a id="item" key={list.list[idx].position.id} className="list-group-item list-group-item-action py-3 lh-tight" style={{borderRadius: '15px', backgroundColor: 'whitesmoke', marginTop: '10px'}}>
                                    <div className="d-flex w-100 align-items-center justify-content-between">
                                        <strong className="mb-1">{list.list[idx].position.name} <label style={{color : 'green'}}>(Группа: {props.group.find(f => f.leaderPositionID === list.list[idx].position.id).name})</label></strong>
                                    </div>
                                    <Badge bg="success">Постоянные заместители:</Badge><br></br>
                                    {list.list[idx].deputies.length > 0?
                                        list.list[idx].deputies.map(perDep =>
                                            <><small>{props.positions.find(f => f.id === perDep.replacer).name}</small><br></br></>
                                        )
                                    :<><small style={{color : 'gray'}}>Нет постоянных заместителей</small><br></br></>
                                    }
                                    <Badge bg="info">Временные заместители:</Badge><br></br>
                                    {list.list[idx].temporaryDeputies.length > 0?
                                        list.list[idx].temporaryDeputies.map(tempDep =>
                                            <><small><>{props.positions.find(f => f.id === tempDep.replacer).name} ({new Date(tempDep.startDate).toLocaleString()} - {new Date(tempDep.endDate).toLocaleString()})</></small><br></br></>
                                        )
                                    :<><small style={{color : 'gray'}}>Нет временных заместителей</small><br></br></>
                                    }
                                </a>
                                </>
                                :<></>
                            ))}
                            </>
                        }
                    </div>
                </Form>
            </div>
            </>
        :<div className="d-grid gap-2 d-sm-flex justify-content-sm-center"><Spinner animation="border"/></div>}
        </>
    
    );
};
export default LoadListDeputy_Comp;