import React, { useEffect } from "react";
import { Button, Card, Container, Form } from "react-bootstrap";
import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { Position } from "../../models/Position";
import { Group } from "../../models/Group";
import { ADMIN_DEPUTYOPTS_ROUTE, ERROR_ROUTE } from "../../utils/consts";
import Select from "react-select";
import { PositionDeputies } from "../../models/PositionsDeputies";
import { UserData } from "../../models/UserData";
import { GetListGroup } from "../../apiRequests/GroupRequests";
import { GetListDeputies, GetListPosition, RemoveDeputy } from "../../apiRequests/PositionRequests";
import { GetListUsers } from "../../apiRequests/UserRequests";

class Data {
    groups : Group[];
    users : UserData[];
    positions : Position[];
}

function DeletePerDeputy() {
    const [data, setData] = useState<Data>({
        groups : [], users : [], positions : []
    })

    useEffect(() => {
        GetListGroup().then(groups => GetListPosition().then(positions => 
            GetListUsers().then(users => setData({groups : groups, positions : positions, users : users}))));
    }, []);
    return <DeletePerDeputyCard groups={data.groups} users={data.users} positions={data.positions}/>;
}
class cardProps {
    groups : Group[];
    users : UserData[];
    positions : Position[];
}

class Pair {
    label : string;
    value : number;
}
class GroupStates {
    idLeaderGroup : number;
    data : Pair[];
}
const DeletePerDeputyCard = (props: cardProps) => {
    const navigate = useNavigate();
    const [idPosDeputy, setIdPosDeputy] = useState(-1);

    /// Безопасность для выбора удаляемого заместителя
    const [dataDeputy, setDataDeputy] = useState([]);

    const [idLeaderGroup, setIdLeaderGroup] = useState<GroupStates>({data: [], idLeaderGroup: -1});
    const [checked, setChecked] = useState(true);

    // Настройки для селекта
    const optionsGroup = props.groups.map(data => ({label: data.name, value: data.id}));
    optionsGroup.push({label: "Не выбрано", value: -1});

    const setGroupID=async(id)=>{
        if(id !== -1)
        {
            const tmp : PositionDeputies = await GetListDeputies(props.groups.find(f => f.id === id).leaderPositionID);
            const optionsPos = tmp.deputies.map(data => ({label: props.positions.find(f => f.id === data.replacer).name, value: data.replacer}));
            tmp.temporaryDeputies.map(data => optionsPos.push({label: props.positions.find(f => f.id === data.replacer).name, value: data.replacer}))
            optionsPos.push({label: "Не выбрано", value: -1});
            setIdLeaderGroup({ idLeaderGroup: props.groups.find(f => f.id === id).leaderPositionID, data: optionsPos });
            setPosID(-1);
        }           
        else
            setIdLeaderGroup({idLeaderGroup: -1, data: []});
    }
    const setPosID=(id)=>{
        if(id !== -1)
        {
            setIdPosDeputy(id);
            let tmp : Pair[] = [];
            tmp.push({label : idLeaderGroup.data.find(f => f.value === id).label, value : id});
            setDataDeputy(tmp);
        }           
        else{
            setIdPosDeputy(-1);
            setDataDeputy([]);
        }
            
    }
    const B_DeletePerDeputy = async () =>{
        if(idPosDeputy !== -1){
            if(await RemoveDeputy(idLeaderGroup.idLeaderGroup, idPosDeputy, checked))
                navigate(ADMIN_DEPUTYOPTS_ROUTE);
            else
                navigate(ERROR_ROUTE);
        }     
    }
    return(
        <Container 
            className="d-flex justify-content-center align-items-center"
        >
            <Card style={{width: 550, overflowX: 'hidden', overflowY: 'auto'}} className="p-5">
                    <h2 className="m-auto">Удаление заместителя</h2>
                    <br></br>
                    <Form style={{minHeight: '55vh'}}>
                        <Form.Group className="mb-3" controlId="controlSelectGroup">
                            <Form.Label>Выберите группу</Form.Label>
                            <Select 
                                options={optionsGroup}
                                isSearchable={true}
                                onChange={data=>setGroupID(data.value)}/>
                        </Form.Group>
                        {idLeaderGroup.idLeaderGroup !== -1?
                        <Form.Group className="mb-3" controlId="controlSelectUser">
                            <Form.Label>Выберите должность заместителя</Form.Label>
                            <Select 
                                id="id"
                                value={dataDeputy}
                                options={idLeaderGroup.data}
                                isSearchable={true}
                                onChange={data=>setPosID(data.value)}/>
                        </Form.Group>
                        :<></>}
                        <Form.Group className="mb-3" controlId="controlCheckbox_1">
                            <Form.Check 
                                type="checkbox" 
                                defaultChecked
                                label="Удалить не только постоянного заместителя?" 
                                onChange={(e) => { setChecked(e.target.checked) }}/>
                        </Form.Group>
                        {idPosDeputy !== -1?
                            <div className="d-flex justify-content-center align-items-center">
                            <Button 
                                className="mt-3 align-self-center" 
                                style={{width: 160}} 
                                variant="outline-primary"
                                onClick={B_DeletePerDeputy}
                                >Удалить</Button>
                            </div>
                        :<></>}
                    </Form>
                </Card>
        </Container>
    );
};
export default DeletePerDeputy;