import React, { useEffect } from "react";
import { Button, Card, Container, Form, Modal } from "react-bootstrap";
import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { Position } from "../../models/Position";
import { Group } from "../../models/Group";
import { ADMIN_DEPUTYOPTS_ROUTE, ERROR_ROUTE } from "../../utils/consts";
import Select from "react-select";
import { AddDeputy, GetListPosition } from "../../apiRequests/PositionRequests";
import { GetListGroup } from "../../apiRequests/GroupRequests";

class Data {
    groups : Group[];
    positions : Position[];
}

function AddPerDeputy() {
    const [data, setData] = useState<Data>({
        groups : [], positions : []
    })

    useEffect(() => {
        GetListPosition().then(positions => GetListGroup().then(groups => setData({groups : groups, positions : positions})));
    }, []);
    return <AddPerDeputyCard groups={data.groups} positions={data.positions}/>;
}
class cardProps {
    groups : Group[];
    positions : Position[];
}

class Pair {
    label : string;
    value : number;
}
class GroupStates {
    idLeaderGroup : number;
    data : Pair[];
}
class Demonstration{
    nameGroup : string;
    replacerName : string;
    namePos : string;

    constructor(){
        this.nameGroup = "_group";
        this.replacerName = "_replacer";
        this.namePos ="_pos";
    }
}
var Demon : Demonstration;
class viewStates {
    show : boolean;
}
const AddPerDeputyCard = (props: cardProps) => {
    const navigate = useNavigate();
    const [idReplacer, setIdReplacer] = useState(-1);
    
    /// Безопасность для выбора должности заместителя
    const [dataReplacer, setDataReplacer] = useState([]);

    const [idLeaderGroup, setIdLeaderGroup] = useState<GroupStates>({data: [], idLeaderGroup: -1});
    let [state, setStates] = useState<viewStates>({ show : false});

    // Настройки для селекта
    const optionsGroup = props.groups.map(data => ({label: data.name, value: data.id}));
    optionsGroup.push({label: "Не выбрано", value: -1});

    function handleClose() {
        setStates({show : false});
    }
    function handleShow(){
        setStates({show : true});
    }

    useEffect(() => {
        Demon = new Demonstration();
    }, []);
    
    function WithoutCurrentLeader(idLeader : number){
        let tmp : Position[] = [];
        for(let i : number = 0; i < props.positions.length; i++){
            if(idLeader !== props.positions[i].id){
                tmp.push(props.positions[i]);
            }
        }
        console.log(tmp);
        return tmp;
    }
    const setGroupID=(id)=>{
        if(id !== -1)
        {
            Demon.namePos = props.positions.find(f => f.id === props.groups.find(f => f.id === id).leaderPositionID).name;
            Demon.nameGroup = props.groups.find(f => f.id === id).name;
            let tmp : Position[] = WithoutCurrentLeader(props.groups.find(f => f.id === id).leaderPositionID);
            const optionsPos = tmp.map(data => ({label: data.name, value: data.id}));
            optionsPos.push({label: "Не выбрано", value: -1});
            setIdLeaderGroup({ idLeaderGroup: props.groups.find(f => f.id === id).leaderPositionID, data: optionsPos });
            setReplacerID(-1);
        }           
        else
            setIdLeaderGroup({idLeaderGroup: -1, data: []});
    }
    const setReplacerID=(id)=>{
        if(id !== -1)
        {
            Demon.replacerName = props.positions.find(f => f.id === id).name;
            setIdReplacer(id);
            let tmp : Pair[] = [];
            tmp.push({label : Demon.replacerName, value : id});
            setDataReplacer(tmp);
        }           
        else{
            setIdReplacer(-1);
            setDataReplacer([]);
        }
            
    }
    const B_AddPerDeputy = async () =>{
        setStates({show : false});
        if(await AddDeputy(idLeaderGroup.idLeaderGroup, idReplacer, null, null))
            navigate(ADMIN_DEPUTYOPTS_ROUTE);
        else
            navigate(ERROR_ROUTE);
    }
    return(
        <Container 
            className="d-flex justify-content-center align-items-center"
        >
            <Card style={{width: 550, overflowX: 'hidden', overflowY: 'auto'}} className="p-5">
                    <h2 className="m-auto">Добавление заместителя</h2>
                    <br></br>
                    <Form style={{minHeight: '55vh'}}>
                        <Form.Group className="mb-3" controlId="controlSelectGroup">
                            <Form.Label>Выберите группу</Form.Label>
                            <Select 
                                options={optionsGroup}
                                isSearchable={true}
                                onChange={data=>setGroupID(data.value)}/>
                        </Form.Group>
                        {idLeaderGroup.idLeaderGroup !== -1?
                        <>
                            <Form.Group className="mb-3" controlId="controlSelectUser">
                                <Form.Label>Выберите должность заместителя</Form.Label>
                                <Select 
                                    value={dataReplacer}
                                    options={idLeaderGroup.data}
                                    isSearchable={true}
                                    onChange={data=>setReplacerID(data.value)}/>
                            </Form.Group>
                            <br></br>
                            {idReplacer !== -1?
                                <>
                                    <div className="d-flex justify-content-center align-items-center">
                                        <Button variant="primary" onClick={handleShow}>
                                            Предпросмотр
                                        </Button>
                                    </div>
                                    <Modal show={state.show} fullscreen={true} onHide={handleClose}>
                                        <Modal.Header closeButton>
                                            <Modal.Title>Предпросмотр назначения постоянного заместителя</Modal.Title>
                                        </Modal.Header>
                                        <Modal.Body>    
                                            <Container className="d-flex justify-content-center align-items-center">
                                                <h1 style={{color: 'gray', minWidth: '1vw', position: 'absolute', top: '0', backgroundColor: 'white', borderRadius: '5px', paddingLeft: '6px', paddingRight: '6px'}}>Замещение</h1>
                                                <div style={{border: '2px solid gray',  margin: '1%', padding: '20px', width: '50%'}}>
                                                    <Card>
                                                        <Card.Body>
                                                            <Card.Text style={{overflow: 'auto'}}>
                                                            <label>Группа: {Demon.nameGroup}</label><br></br>
                                                            <br></br>
                                                            <label>Заменяемая должность: {Demon.namePos}</label><br></br>
                                                            <br></br>
                                                            <label>Заместитель: {Demon.replacerName}</label><br></br>
                                                            </Card.Text>
                                                        </Card.Body>
                                                    </Card>
                                                </div>   
                                                
                                            </Container>
                                        </Modal.Body>
                                        <Modal.Footer>
                                            <Button variant="outline-primary" onClick={B_AddPerDeputy}>
                                                Назначить
                                            </Button>
                                            <Button variant="secondary" onClick={handleClose}>
                                                Отменить
                                            </Button>
                                        </Modal.Footer>
                                    </Modal>
                                </>
                            :<></>}
                        </>
                        :<></>}
                    </Form>
                </Card>
        </Container>
    );
};
export default AddPerDeputy;