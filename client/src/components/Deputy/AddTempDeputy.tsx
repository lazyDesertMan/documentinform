import React , {useEffect, useState } from "react";
import { Button, Card, Container, Form, Modal, Stack } from "react-bootstrap";
import { useNavigate} from "react-router-dom";
import { ADMIN_DEPUTYOPTS_ROUTE, ERROR_ROUTE } from "../../utils/consts";
import { Group } from "../../models/Group";
import Select from "react-select";
import { Position } from "../../models/Position";
import { AddDeputy, GetListPosition } from "../../apiRequests/PositionRequests";
import { GetListGroup } from "../../apiRequests/GroupRequests";

class Data {
    positions : Position[];
    groups : Group[];
}
function ChoiceDeputy() {
    const [data, setData] = useState<Data>({
        positions : [], groups : []
    })

    useEffect(() => {
        GetListPosition().then(positions => GetListGroup().then(groups => setData({positions : positions, groups : groups})));
    }, []);
    return <ChoiceDeputyCard positions={data.positions} groups={data.groups} />;
}

class cardProps {
    positions : Position[];
    groups : Group[];
}
class Demonstration{
    nameGroup : string;
    replacerName : string;
    namePos : string;

    constructor(){
        this.nameGroup = "_group";
        this.replacerName = "_replacer";
        this.namePos ="_pos";
    }
}
var Demon : Demonstration;
class viewStates {
    show : boolean;
}
class Pair {
    label : string;
    value : number;
}
class GroupStates {
    idLeaderGroup : number;
    data : Pair[];
}
function ChoiceDeputyCard(props : cardProps) {
    const navigate = useNavigate();
    const [idReplacer, setReplacer] = useState(-1);

    /// Безопасность для выбора должности заместителя
    const [dataReplacer, setDataReplacer] = useState([]);

    const [idLeaderGroup, setIdLeaderGroup] = useState<GroupStates>({data: [], idLeaderGroup: -1});
    const [startDate, setStartDate] = useState("");
    const [deadline, setDeadline] = useState("");
    let [state, setStates] = useState<viewStates>({ show : false});

    // Настройки
    const optionsGroup = props.groups.map(data => ({label: data.name, value: data.id}));
    optionsGroup.push({label: "Не выбрано", value: -1});

    useEffect(() => {
        Demon = new Demonstration();
    }, []);
    function handleClose() {
        setStates({show : false});
    }
    function handleShow(){
        setStates({show : true});
    }
    const setReplacerID=(id)=>{
        if(id !== -1){
            setReplacer(id);
            Demon.replacerName = props.positions.find(f => f.id === id).name;
            let tmp : Pair[] = [];
            tmp.push({label : Demon.replacerName, value : id});
            setDataReplacer(tmp);
        }
        else{
            setReplacer(-1);
            setDataReplacer([]);
        }    
    }
    function WithoutCurrentLeader(idLeader : number){
        let tmp : Position[] = [];
        for(let i : number = 0; i < props.positions.length; i++){
            if(idLeader !== props.positions[i].id){
                tmp.push(props.positions[i]);
            }
        }
        console.log(tmp);
        return tmp;
    }
    const setGroupID=async(id)=>{
        if(id !== -1){
            
            Demon.nameGroup = props.groups.find(f => f.id === id).name;
            Demon.namePos = props.positions.find(f => f.id === props.groups.find(f => f.id === id).leaderPositionID).name;
            let tmp : Position[] = WithoutCurrentLeader(props.groups.find(f => f.id === id).leaderPositionID);
            const optionsReplacer = tmp.map(data => ({label: data.name, value: data.id}));
            optionsReplacer.push({label: "Не выбрано", value: -1});
            setIdLeaderGroup({ idLeaderGroup: props.groups.find(f => f.id === id).leaderPositionID, data: optionsReplacer });
            setReplacerID(-1);
        }
        else
            setIdLeaderGroup({idLeaderGroup: -1, data: []});
    }
    const B_ChoiceTemporaryDeputy = async () =>{
        setStates({show : false});
        if(await AddDeputy(idLeaderGroup.idLeaderGroup, idReplacer, startDate, deadline))
            navigate(ADMIN_DEPUTYOPTS_ROUTE);
        else
            navigate(ERROR_ROUTE);
    }
    return(
        <Container>
            <Stack direction="horizontal" gap={3} style={{ padding: '15px'}}>
                <div style={{width: '30vw'}}></div>
                <Card style={{width: 550, overflowX: 'hidden', overflowY: 'auto'}} className="p-5">
                    <h2 className="m-auto">Замещение</h2>
                    <br></br>
                    <Form style={{minHeight: '40vh'}}>
                        <Form.Group className="mb-3" controlId="controlSelectGroup">
                            <Form.Label>Выберите группу</Form.Label>
                            <Select 
                                options={optionsGroup}
                                isSearchable={true}
                                onChange={data=>setGroupID(data.value)}/>
                        </Form.Group>
                        {idLeaderGroup.idLeaderGroup !== -1?
                        <>
                            <Form.Group className="mb-3" controlId="controlSelectUser">
                                <Form.Label>Выберите заместителя</Form.Label>
                                <Select 
                                    value={dataReplacer}
                                    options={idLeaderGroup.data}
                                    isSearchable={true}
                                    onChange={data=>setReplacerID(data.value)}/>
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="controlInput_StartDate">
                                <Form.Label>Дата начала замещения</Form.Label>
                                <Form.Control
                                    type="date"
                                    onChange={e=>setStartDate(e.target.value)}/>
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="controlInput_Deadline">
                                <Form.Label>Дата завершения замещения</Form.Label>
                                <Form.Control
                                    type="date"
                                    onChange={e=>setDeadline(e.target.value)}/>
                            </Form.Group>
                            <br></br>
                            {idReplacer !== -1 && startDate !== "" && deadline !== ""?
                            <>
                                <div className="d-flex justify-content-center align-items-center">
                                    <Button variant="primary" onClick={handleShow}>
                                        Предпросмотр
                                    </Button>
                                </div>
                                <Modal show={state.show} fullscreen={true} onHide={handleClose}>
                                    <Modal.Header closeButton>
                                        <Modal.Title>Предпросмотр назначения временного заместителя</Modal.Title>
                                    </Modal.Header>
                                    <Modal.Body>    
                                        <Container className="d-flex justify-content-center align-items-center">
                                            <h1 style={{color: 'gray', minWidth: '1vw', position: 'absolute', top: '0', backgroundColor: 'white', borderRadius: '5px', paddingLeft: '6px', paddingRight: '6px'}}>Замещение</h1>
                                            <div style={{border: '2px solid gray',  margin: '1%', padding: '20px', width: '50%'}}>
                                                <Card>
                                                    <Card.Body>
                                                        <Card.Text style={{overflow: 'auto'}}>
                                                        <label>Группа: {Demon.nameGroup}</label><br></br>
                                                        <br></br>
                                                        <label>Заменяемая должность: {Demon.namePos}</label><br></br>
                                                        <br></br>
                                                        <label>Заместитель: {Demon.replacerName}</label><br></br>
                                                        <br></br>
                                                        <label>Дата начала замещения: {startDate}</label><br></br>
                                                        <br></br>
                                                        <label>Дата завершения замещения: {deadline}</label><br></br>
                                                        </Card.Text>
                                                    </Card.Body>
                                                </Card>
                                            </div>   
                                            
                                        </Container>
                                    </Modal.Body>
                                    <Modal.Footer>
                                        <Button variant="outline-primary" onClick={B_ChoiceTemporaryDeputy}>
                                            Назначить
                                        </Button>
                                        <Button variant="secondary" onClick={handleClose}>
                                            Отменить
                                        </Button>
                                    </Modal.Footer>
                                </Modal>
                            </>
                            :<></>}
                        </>
                        :<></>}
                        
                    </Form>
                </Card>
                <div style={{width: '30vw'}}></div>
            </Stack>
        </Container>
    );
}
export default ChoiceDeputy;