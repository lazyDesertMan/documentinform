import React, { useEffect, useState} from "react";
import { Button, Container, Modal } from "react-bootstrap";
import {SelfMonitoringReport, TaskStatus} from "../../models/SelfMonitoringReport";
import jwtDecode from "jwt-decode";
import Cookies from 'js-cookie'
import { DoughnutChart } from "../Charts/DoughnutChart";
import '../../css/style.css';
import { ACCESS_TOKEN_COOKIE } from "../../utils/consts";
import Cookie from "../../classes/Cookie";
import { GetReport } from "../../apiRequests/ReportRequests";

interface loadStates {
    isFetching: boolean;
    report: SelfMonitoringReport;
}

function LoadSelfMonitoringReport_Comp() {
    let [state, setStates] = useState<loadStates>({ isFetching: false, report: new SelfMonitoringReport("")});
    function loadData(id: number) {
        setStates({isFetching: true, report : state.report});
        GetReport(id).then(r => {
            setStates({isFetching: false, report: r});
        }).catch(e => {
            console.log(e);
            setStates({isFetching: true, report : state.report});
        });
    }
    useEffect(() => {
        const usrCookie = Cookies.get(ACCESS_TOKEN_COOKIE);
        if (usrCookie != null){
            const decodeUsrCookie : any = jwtDecode<Cookie>(usrCookie);
            loadData(decodeUsrCookie.user.id);
        }
    }, []);
    return(
        <div>
            {state.isFetching ? "Loading..." : <ViewReport list={state.report} />}
        </div>
    );
}

class viewProps {
    list : SelfMonitoringReport;
}
class viewStates {
    show : boolean;
}
  
function ViewReport(props: viewProps) {
    let [state, setStates] = useState<viewStates>({ show : false});
    var color = '';
    function handleClose() {
        setStates({show : false});
    }
    function handleShow(){
        setStates({show : true});
    }
    if(props.list === null){
        return <div></div>
    }
    else{ 
        return(
            <>
                <Button variant="primary" onClick={handleShow}>
                    Получить отчет
                </Button>
                <Modal show={state.show} fullscreen={true} onHide={handleClose}>
                    <Modal.Header closeButton>
                        <Modal.Title style={{width: '100%'}}><small>Отчет о заданиях</small><strong> {props.list.userName} </strong><label style={{float: 'right', whiteSpace: 'nowrap'}}><small>Дата формирования </small><strong>{new Date(props.list.date).toLocaleString()}</strong></label></Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Container >
                            <div className="pages">
                                <h4 style={{minWidth: '18vw', verticalAlign: 'baseline', left: '7vw', backgroundColor: 'white', borderRadius: '5px', paddingLeft: '6px', paddingRight: '6px'}}>Диаграмма самоконтроля</h4>
                                <br></br>
                                <div style={{border: '2px solid black', borderRadius: '10px'}} className="d-grid gap-2 d-sm-flex justify-content-sm-center">
                                    <div style={{width: '30%', padding: '15px'}}><DoughnutChart report={props.list}/></div>
                                    
                                </div>
                                <br></br>
                                <br></br>
                                <h4 style={{minWidth: '19vw', verticalAlign: 'baseline', left: '7vw', backgroundColor: 'white', borderRadius: '5px', paddingLeft: '6px', paddingRight: '6px'}}>Таблица самоконтроля</h4>
                                <br></br>
                                <div id="tmp" style={{border: '2px solid black', borderRadius: '10px'}}>
                                    {Array.from({ length: props.list.records.length }).map((_, idx) => (
                                    <div key={idx}>
                                        <br></br>
                                        <div style={{display: 'flex', flexDirection: 'column', minWidth: '18vw'}}><strong style={{padding: '10px'}}>Должность: {props.list.records[idx].position}</strong></div>
                                        <div style={{display: 'flex', flexDirection: 'column', borderTop: '1px solid black', textAlign: 'center', borderRadius: '5px'}}>
                                            <div className="table-responsive">
                                                <table style={{fontSize:'14px', width: '100%'}} className="table-striped table-sm">
                                                    <thead>
                                                        <tr key={"thead"}>
                                                            <th scope="col">#</th>
                                                            <th scope="col">Документ</th>
                                                            <th scope="col">Дата выдачи</th>
                                                            <th scope="col">Предельный срок</th>
                                                            <th scope="col">Тип задания</th>
                                                            <th scope="col">Статус задания</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    {Array.from({ length: props.list.records[idx].tasks.length }).map((_, j) => (
                                                        <tr key={"tbody" + j}>
                                                        <td style={{whiteSpace: 'nowrap', background: props.list.records[idx].tasks[j].state === TaskStatus.TSTAT_DEADLINE_VIOLATION? 'rgb(255, 200, 200)' : 'white'}}>{j + 1}</td>
                                                        <td style={{whiteSpace: 'nowrap', background: props.list.records[idx].tasks[j].state === TaskStatus.TSTAT_DEADLINE_VIOLATION? 'rgb(255, 200, 200)' : 'white'}}>{props.list.records[idx].tasks[j].task.document}</td>
                                                        <td style={{whiteSpace: 'nowrap', background: props.list.records[idx].tasks[j].state === TaskStatus.TSTAT_DEADLINE_VIOLATION? 'rgb(255, 200, 200)' : 'white'}}>{new Date(props.list.records[idx].tasks[j].task.startDate).toLocaleDateString()}</td>
                                                        <td style={{whiteSpace: 'nowrap', background: props.list.records[idx].tasks[j].state === TaskStatus.TSTAT_DEADLINE_VIOLATION? 'rgb(255, 200, 200)' : 'white'}}>{new Date(props.list.records[idx].tasks[j].task.deadline).toLocaleDateString()}</td>
                                                        <td style={{whiteSpace: 'nowrap', background: props.list.records[idx].tasks[j].state === TaskStatus.TSTAT_DEADLINE_VIOLATION? 'rgb(255, 200, 200)' : 'white'}}>{props.list.records[idx].tasks[j].task.type === 1 ? "Прочитать" : "Переслать" }</td>
                                                        <td style={{whiteSpace: 'nowrap', background: props.list.records[idx].tasks[j].state === TaskStatus.TSTAT_DEADLINE_VIOLATION? 'rgb(255, 200, 200)' : 'white'}}>{props.list.records[idx].tasks[j].state === TaskStatus.TSTAT_COMPLETE ? "Выполнено" 
                                                            :props.list.records[idx].tasks[j].state === TaskStatus.TSTAT_ACTIVE ? "Не выполненно (без нарушений)" 
                                                            : <label style={{background: color}}>Не выполненно (с нарушением)</label>}</td>
                                                        </tr>
                                                    ))}
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    ))}
                                </div>
                            </div>
                        </Container>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={handleClose}>
                            Закрыть
                        </Button>
                    </Modal.Footer>
                </Modal>
            </>
        );
    }
}
export default LoadSelfMonitoringReport_Comp;