import React, { useEffect, useState, useMemo } from "react";
import { Form, Stack } from "react-bootstrap";
import jwtDecode from "jwt-decode";
import Cookies from 'js-cookie'
import { ITask } from "../../models/Task";
import { ACCESS_TOKEN_COOKIE } from "../../utils/consts";
import Cookie from "../../classes/Cookie";
import { GetSecondTypeTasks } from "../../apiRequests/TaskRequests";

class Data {
    tasks : ITask[];
}
class Dates{
    startDate : string;
    endDate : string;
    constructor(){
        this.startDate = "_startdate";
        this.endDate = "_enddate";
    }
}
function LoadResendList_Comp(info : Dates) {
    const [data, setData] = useState<Data>({
        tasks : []
    })
    const User = Cookies.get(ACCESS_TOKEN_COOKIE);
    let id : number = -1;
    if (User != null){
        const decodeCookies : any = jwtDecode<Cookie>(User);
        id = decodeCookies.user.id;
    }
    useEffect(() => {
        GetSecondTypeTasks(id, info.startDate, info.endDate).then(tasks => {console.log(JSON.stringify(tasks)); setData({tasks : tasks})});
    }, [info]);
    return <Search list={data.tasks}/>;
}

class searchProp {
    list: ITask[];
}
function Search(props: searchProp) {
    const [search, setSearch] = useState("");

    const filteredDocs = useMemo(() => {
        if (search) {
          return props.list.filter(
            (item) =>
              item.document
                .toLowerCase()
                .indexOf(search.toLocaleLowerCase()) > -1
          );
        }
        return props.list;
      }, [props, search]);

    let filtred: ITask[] = [];
    if(filteredDocs && filteredDocs.length > 0){
        filteredDocs.map(item => filtred.push(item));
    }else{
        filtred = props.list;
    }

    return (
        <>
            <div>
                <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
                    <Stack direction="horizontal" gap={4} style={{ width: '60%', marginTop: '1%' }}>
                        <Form.Control
                            id="myInput"
                            className="me-auto"
                            value={search} 
                            onChange={e => setSearch(e.target.value)}
                            placeholder="Найти документ..." />
                    </Stack>
                </div>
            </div>
            <ViewResendList tasks={filtred} />
        </>
    );
}

class viewProps {
    tasks : ITask[];
}
function ViewResendList(props: viewProps) { 
    return (
        <>
            <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
                <div style={{ width: '60%', marginTop: '1%' }}>
                    <div className="list-group list-group-flush border-bottom scrollarea">
                            <>
                            {Array.from({ length: props.tasks.length }).map((_, idx) => (
                                <a id="list" key={props.tasks[idx].ID} href={process.env.REACT_APP_CLIENT_URL +"/tasksreport/" + props.tasks[idx].ID} className="list-group-item list-group-item-action py-3 lh-tight">
                                    <div>
                                        <div className="d-flex w-100 align-items-center justify-content-between">
                                            <strong className="mb-1">{props.tasks[idx].document}</strong>
                                            <small>Переслать</small>
                                        </div>
                                        <div className="col-10 mb-1 small">Выдано {new Date(props.tasks[idx].startDate).toLocaleDateString()}</div>
                                        <div className="col-10 mb-1 small">Выполнить до {new Date(props.tasks[idx].deadline).toLocaleDateString()}</div>
                                    </div>
                                </a>
                            ))}
                            </>
                    </div>
                </div>
            </div>
        </>
    );
}
export default LoadResendList_Comp;