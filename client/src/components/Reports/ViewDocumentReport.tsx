import React, { useEffect, useState} from "react";
import { Badge, Button, Form, OverlayTrigger, Popover } from "react-bootstrap";
import { BarDocumentChart } from "../Charts/BarDocumentChart";
import '../../css/style.css';
import { DocumentReport } from "../../models/DocumentReport";
import { ReadTask, ResendTask, TaskType } from "../../models/Task";
import { Position } from "../../models/Position";
import { UserData } from "../../models/UserData";
import { TaskStatusTree } from "../../models/TaskReport";
import { GetListPosition } from "../../apiRequests/PositionRequests";
import { GetDocumentReport } from "../../apiRequests/ReportRequests";
import { GetAllUsersWithDirector } from "../../apiRequests/UserRequests";

class Data {
    report : DocumentReport;
    positions : Position[];
    users : UserData[];
}
class INFO {
    idDoc : number;
    startDate : string;
    endDate : string;
    constructor(){
        this.idDoc = -1;
        this.startDate = "";
        this.endDate = "";
    }
}
function LoadDocumentReport_Comp(info : INFO) {
    const [data, setData] = useState<Data>({
        report : new DocumentReport(), positions : [], users : []
    })
    useEffect(() => {
        GetDocumentReport(info.idDoc, info.startDate, info.endDate).then(report => 
            GetListPosition().then(positions => GetAllUsersWithDirector().then(users => setData({report : report, positions : positions, users : users}))));
    }, []);
    return <ViewDocumentReport rep={data.report} users={data.users} positions ={data.positions}/>;
}

class viewProps {
    rep : DocumentReport;
    positions : Position[];
    users : UserData[];
}
function ViewDocumentReport(props : viewProps) {
    return(
        <>
            {props.rep !== null && typeof props.rep.report !== "undefined" && props.rep.report !== null?
            <>
            <br></br>
            <br></br>
            <div className="pages">
                <h4 style={{minWidth: '18vw', verticalAlign: 'baseline', left: '7vw', backgroundColor: 'white', borderRadius: '5px', paddingLeft: '6px', paddingRight: '6px'}}>Диаграмма от <strong>{new Date(props.rep.date).toLocaleString()}</strong></h4>
                <div style={{border: '2px solid black', borderRadius: '10px'}} className="d-grid gap-2 d-sm-flex justify-content-sm-center">
                        <div style={{width: '50%', padding: '15px'}}><BarDocumentChart report={props.rep} /></div>     
                </div>
                <br></br>
                <br></br>
                <h4 style={{minWidth: '19vw', verticalAlign: 'baseline', left: '7vw', backgroundColor: 'white', borderRadius: '5px', paddingLeft: '6px', paddingRight: '6px'}}>Иерархия от <strong>{new Date(props.rep.date).toLocaleString()}</strong></h4>
                    <ViewHierarchy  rep={props.rep} users={props.users} positions ={props.positions}/>
             </div>
            </>
            :<div className="d-grid gap-2 d-sm-flex justify-content-sm-center"
                style={{color: 'green', fontSize: '50px', marginTop: '10%'}} >Нет данных по документу</div>
            }
        </>
    );
}

let Pos : Position[];
let Users : UserData[];
function ViewHierarchy(props: viewProps) {
    Pos = props.positions;
    Users = props.users;
    return (
        <ul className="ulGroup">{  props.rep.report.map(c => ParseTree(c)) }</ul>
    );
}
function ParseTree (tree : TaskStatusTree) {
    const sender = Users.find(f => f.id === tree.task.task.senderID);
    const senderName = sender ? sender.secondName + " " + sender.firstName + " " + sender.thirdName : "";
    const recipient = tree.task.task.type === TaskType.READ_TASK_TYPE ? Users.find(f => f.id === (tree.task.task as ReadTask).recipient) : null;
    const recipientName = recipient ? recipient.secondName + " " + recipient.firstName + " " + recipient.thirdName : "";
    return (
        <div key="tree_root">
            <Form style={{marginTop: '1%'}}>
                <div className="list-group list-group-flush">
                    <div id="item" key={"task_" + tree.task.task.ID} className="list-group-item list-group-item-action py-3 lh-tight">
                        <li key={"task_" + tree.task.task.ID}>
                            <OverlayTrigger
                                trigger="click"
                                key="right"
                                placement="right"
                                overlay={
              <Popover id="popover_1">
                                    <Popover.Header as="h3">Данные задания</Popover.Header>
                                    <Popover.Body>
                                        <Form style={{overflow: "hidden"}}>
                                            <Form.Group className="mb-3" controlId="controlInput_nameGroup">
                                                <Form.Label><small>Документ:</small>
                                                    &nbsp;
                                                    <Badge
                                                        bg="info"
                                                        className="mb-1"
                                                    >{tree.task.task.document}</Badge>
                                                </Form.Label>
                                            </Form.Group>
                                            <Form.Group className="mb-3" controlId="controlInput_nameGroup">
                                                <Form.Label><small>Статус:</small>
                                                    &nbsp;
                                                    <Badge
                                                        bg={tree.task.status === 3? "danger" : "success"}
                                                        className="mb-1"
                                                    >{tree.task.status === 1?
                                                        <small>Выполнено</small>
                                                    :  tree.task.status === 2? <small>Выполняется</small>
                                                    :  <small>Не выполнено</small>
                                                    }</Badge>
                                                </Form.Label>
                                            </Form.Group>
                                            <Form.Group className="mb-3" controlId="controlInput_nameGroup">
                                                <Form.Label><small>Отправитель:</small>
                                                    &nbsp;
                                                    <Badge
                                                        bg="info"
                                                        className="mb-1"
                                                    >{senderName}</Badge>
                                                </Form.Label>   
                                            </Form.Group>
                                            <Form.Group className="mb-3" controlId="controlInput_nameGroup">
                                                <Form.Label><small>Дата выдачи задания:</small>
                                                    &nbsp;
                                                    <Badge
                                                        bg="info"
                                                        className="mb-1"
                                                    >{new Date(tree.task.task.startDate).toLocaleDateString()}</Badge>
                                                </Form.Label>
                                            </Form.Group>
                                            <Form.Group className="mb-3" controlId="controlInput_nameGroup">
                                                <Form.Label><small>Дата завершения:</small>
                                                    &nbsp;
                                                    <Badge
                                                        bg="info"
                                                        className="mb-1"
                                                    >{new Date(tree.task.task.deadline).toLocaleDateString()}</Badge>
                                                </Form.Label>
                                            </Form.Group>
                                        </Form>
                                    </Popover.Body>
                                    </Popover>
                                }
                            >
                                <Badge as={Button} bg={tree.task.status === 3? "danger" : "primary"}  className="mb-1">{
                                tree.task.task.type === 1?
                                    recipientName
                                    :Pos.find(f => f.id === (tree.task.task as ResendTask).recipient).name
                                }</Badge>
                            </OverlayTrigger>
                            &nbsp;
                            <Badge bg={tree.task.task.type === 1? "info" : "secondary"} className="mb-1">{
                                tree.task.task.type === 1?
                                    <small>Ознакомление</small>
                                    :<small>Пересылка</small>
                            }</Badge>
                        </li>
                        <div style={{borderLeftStyle: 'outset'}}>
                            {
                                tree.childs.length > 0 ?
                                    <ul key={"child_" + tree.task.task.ID} className="ulGroup">{ tree.childs.map(c => ParseTree(c))}</ul>
                                : ""
                            }
                        </div>
                    </div>
                </div>
            </Form>
        </div> 
    );
}
export default LoadDocumentReport_Comp;