import React, { useEffect, useState} from "react";
import { Badge, Button, Form, OverlayTrigger, Popover } from "react-bootstrap";
import { BarTaskChart } from "../Charts/BarTaskChart";
import '../../css/style.css';
import { TaskReport, TaskStatusTree } from "../../models/TaskReport";
import { ReadTask, ResendTask, TaskType } from "../../models/Task";
import { Position } from "../../models/Position";
import { UserData } from "../../models/UserData";
import { GetListPosition } from "../../apiRequests/PositionRequests";
import { GetTaskReport } from "../../apiRequests/ReportRequests";
import { GetAllUsersWithDirector } from "../../apiRequests/UserRequests";

class Data {
    report : TaskReport;
    positions : Position[];
    users : UserData[];
}
class INFO {
    idTask : number;
    startDate : string;
    endDate : string;
    constructor(){
        this.idTask = -1;
        this.startDate = "";
        this.endDate = "";
    }
}
function LoadTaskReport_Comp(info : INFO) {
    const [data, setData] = useState<Data>({
        report : new TaskReport(), positions : [], users : []
    })
    useEffect(() => {
        GetTaskReport(info.idTask, info.startDate, info.endDate).then(report => 
            GetListPosition().then(positions => GetAllUsersWithDirector().then(users => setData({report : report, positions : positions, users : users}))));
    }, []);
    return <ViewTaskReport rep={data.report} users={data.users} positions ={data.positions}/>;
}

class viewProps {
    rep : TaskReport;
    positions : Position[];
    users : UserData[];
}

function ViewTaskReport(props : viewProps) {
    return(
        <>
            {props.rep !== null && props.rep.report !== undefined?
            <>
            <br></br>
            <br></br>
            <div className="pages">
                <h4 style={{minWidth: '18vw', verticalAlign: 'baseline', left: '7vw', backgroundColor: 'white', borderRadius: '5px', paddingLeft: '6px', paddingRight: '6px'}}>Диаграмма от <strong>{new Date(props.rep.date).toLocaleString()}</strong></h4>
                <div style={{border: '2px solid black', borderRadius: '10px'}} className="d-grid gap-2 d-sm-flex justify-content-sm-center">
                        <div style={{width: '50%', padding: '15px'}}><BarTaskChart report={props.rep} /></div>     
                </div>
                <br></br>
                <br></br>
                <h4 style={{minWidth: '19vw', verticalAlign: 'baseline', left: '7vw', backgroundColor: 'white', borderRadius: '5px', paddingLeft: '6px', paddingRight: '6px'}}>Иерархия от <strong>{new Date(props.rep.date).toLocaleString()}</strong></h4>
                    <ViewHierarchy  rep={props.rep} users={props.users} positions ={props.positions}/>
             </div>
            </>
            :<></>
            }
        </>
    );
}
let Pos : Position[];
let Users : UserData[];

function ViewHierarchy(props: viewProps) {
    Pos = props.positions;
    Users = props.users;
    return (
        <ul className="ulGroup">{ ParseTree(props.rep.report) }</ul>
    );
}

function ParseTree (tree : TaskStatusTree) {
    const sender = Users.find(f => f.id === tree.task.task.senderID);
    const senderName = sender ? sender.secondName + " " + sender.firstName + " " + sender.thirdName : "";
    const recipient = tree.task.task.type === TaskType.READ_TASK_TYPE ? Users.find(f => f.id === (tree.task.task as ReadTask).recipient) : null;
    const recipientName = recipient ? recipient.secondName + " " + recipient.firstName + " " + recipient.thirdName : "";
    return (
        <div>
            <Form style={{marginTop: '1%'}}>
                <div className="list-group list-group-flush">
                    <div id="item" key={"task_" + tree.task.task.ID} className="list-group-item list-group-item-action py-3 lh-tight">
                        <li>
                            <OverlayTrigger
                                trigger="click"
                                key="right"
                                placement="right"
                                overlay={
                                    <Popover id="popover_1">
                                    <Popover.Header as="h3">Карточка задания</Popover.Header>
                                    <Popover.Body>
                                        <Form style={{overflow: "hidden"}}>
                                            <Form.Group className="mb-3" controlId="controlInput_nameGroup">
                                                <Form.Label><small>Документ:</small>
                                                    &nbsp;
                                                    <Badge
                                                        bg="info"
                                                        className="mb-1"
                                                    >{tree.task.task.document}</Badge>
                                                </Form.Label>
                                            </Form.Group>
                                            <Form.Group className="mb-3" controlId="controlInput_nameGroup">
                                                <Form.Label><small>Статус:</small>
                                                    &nbsp;
                                                    <Badge
                                                        bg={tree.task.status === 3? "danger" : "success"}
                                                        className="mb-1"
                                                    >{tree.task.status === 1?
                                                        <small>Выполнено</small>
                                                    :  tree.task.status === 2? <small>Выполняется</small>
                                                    :  <small>Не выполнено</small>
                                                    }</Badge>
                                                </Form.Label>
                                            </Form.Group>
                                            <Form.Group className="mb-3" controlId="controlInput_nameGroup">
                                                <Form.Label><small>Отправитель:</small>
                                                    &nbsp;
                                                    <Badge
                                                        bg="info"
                                                        className="mb-1"
                                                    >{senderName}</Badge>
                                                </Form.Label>   
                                            </Form.Group>
                                            <Form.Group className="mb-3" controlId="controlInput_nameGroup">
                                                <Form.Label><small>Дата выдачи задания:</small>
                                                    &nbsp;
                                                    <Badge
                                                        bg="info"
                                                        className="mb-1"
                                                    >{new Date(tree.task.task.startDate).toLocaleDateString()}</Badge>
                                                </Form.Label>
                                            </Form.Group>
                                            <Form.Group className="mb-3" controlId="controlInput_nameGroup">
                                                <Form.Label><small>Дата завершения:</small>
                                                    &nbsp;
                                                    <Badge
                                                        bg="info"
                                                        className="mb-1"
                                                    >{new Date(tree.task.task.deadline).toLocaleDateString()}</Badge>
                                                </Form.Label>
                                            </Form.Group>
                                        </Form>
                                    </Popover.Body>
                                    </Popover>
                                }
                            >
                                <Badge as={Button} bg={tree.task.status === 3? "danger" : "primary"}  className="mb-1">{
                                tree.task.task.type === 1?
                                    recipientName
                                    : Pos.find(f => f.id === (tree.task.task as ResendTask).recipient).name
                                }</Badge>
                            </OverlayTrigger>
                            &nbsp;
                            <Badge bg={tree.task.task.type === 1? "info" : "secondary"} className="mb-1">{
                                tree.task.task.type === 1?
                                    <small>Ознакомление</small>
                                    :<small>Пересылка</small>
                            }</Badge>
                        </li>
                        <div style={{borderLeftStyle: 'outset'}}>
                            {
                                tree.childs.length > 0 ?
                                    <ul className="ulGroup">{ tree.childs.map(c => ParseTree(c))}</ul>
                                : ""
                            }
                        </div>
                    </div>
                </div>
            </Form>
        </div> 
    );
}
export default LoadTaskReport_Comp;