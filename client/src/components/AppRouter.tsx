import React, { useState } from "react";
import {Routes, Route, Navigate} from "react-router-dom"
import { adminRoutes, authRoutes, publicRoutes, directorRoutes } from "../routes";
import { ACCESS_TOKEN_COOKIE, HOME_ROUTE, PROFILE_ROUTE } from "../utils/consts";
import {Roles} from "../models/UserData";
import jwtDecode from "jwt-decode";
import Cookies from 'js-cookie'
import Home from "../pages/Home";
import Cookie from "../classes/Cookie";

const AppRouter = () => {
    const [rerender, callRender] = useState<null>();
    const usrCookie = Cookies.get(ACCESS_TOKEN_COOKIE);
    const decodeUsrCookie : Cookie = usrCookie ?  jwtDecode<Cookie>(usrCookie) : undefined;
    const role = (decodeUsrCookie && decodeUsrCookie.user && decodeUsrCookie.user.role) ? decodeUsrCookie.user.role : undefined;

    return(
        <Routes>
            {role === Roles.ROLE_DIRECTOR && directorRoutes.map(({path, Component}) =>
                <Route key={path} path={path} element={<Component />} />
            ).concat(<Route key="default" path="*" element={<Navigate to={PROFILE_ROUTE} />} />)}
            {role === Roles.ROLE_ADMIN && adminRoutes.map(({path, Component}) =>
                <Route key={path} path={path} element={<Component />} />
            ).concat(<Route key="default" path="*" element={<Navigate to={PROFILE_ROUTE} />} />)}
            {role === Roles.ROLE_WORKER && authRoutes.map(({path, Component}) =>
                <Route key={path} path={path} element={<Component />} />
            ).concat(<Route key="default" path="*" element={<Navigate to={PROFILE_ROUTE} />} />)}
            {typeof role === "undefined" && publicRoutes.map(({path, Component}) =>
                <Route key={path} path={path} element={<Component />} />
            ).concat(<Route key={HOME_ROUTE} path={HOME_ROUTE} element={<Home func={callRender} />} />)
            .concat(<Route key="default" path="*" element={<Navigate to={HOME_ROUTE} />} />) }
        </Routes>
    );
};
export default AppRouter;