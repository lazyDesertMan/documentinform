import React , {useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { Button, Card, Container, Form, Modal, Stack } from "react-bootstrap";
import { ADMIN_OPTIONS_ROUTE } from "../../utils/consts";
import { UserData } from "../../models/UserData";
import Select from "react-select";
import { GetListUsers, UpdateUser } from "../../apiRequests/UserRequests";

class Data {
    users : UserData[];
}

function ChangeUser() {
    const [data, setData] = useState<Data>({
        users : []
    })

    useEffect(() => {
        GetListUsers().then(users => setData({ users : users}));
    }, []);
    return <ChangeUserCard users={data.users} />;
}

class cardProps {
    users : UserData[];
}

class viewStates {
    show : boolean;
}
class Demonstration{
    oldUser : UserData;
}
var Demon : Demonstration;
function ChangeUserCard(props : cardProps) {
    const navigate = useNavigate();
    const [idUser, setUser] = useState(-1);
    const [Fname, setFName] = useState("");
    const [isDisableMail, setDisable] = useState(false);
    const [Email, setEmail] = useState("");
    const [Sname, setSName] = useState("");
    const [Tname, setTName] = useState("");
    const [login, setLogin] = useState("");
    let [state, setStates] = useState<viewStates>({ show : false});

    // Настройки
    const optionsUser = props.users.map(data => ({label: data.secondName + " " + data.firstName + " " + data.thirdName, value: data.id}));
    optionsUser.push({label: "Не выбрано", value: -1});

    function handleClose() {
        setStates({show : false});
    }
    function handleShow(){
        setStates({show : true});
    }

    useEffect(() => {
        Demon = new Demonstration();
    }, []);

    const setUserID=(id)=>{
        if(id !== -1){
            setUser(id);
            Demon.oldUser = props.users.find(f => f.id === id);
        }   
        else
            setUser(null);
    }
    const B_ChangeUser = async () =>{
        setStates({show : false});
        let sname : string = Sname.length > 0 ? Sname : undefined;
        let fname : string = Fname.length > 0 ? Fname : undefined;
        let tname : string = Tname.length > 0 ? Tname : undefined;
        let log : string = login.length > 0 ? login : undefined;
        let email : string = Email.length > 0 ? Email : undefined;
        if(await UpdateUser(idUser, sname, fname, tname, log, email, isDisableMail))
            navigate(ADMIN_OPTIONS_ROUTE);
    }
    return(
        <Container>
            <Stack direction="horizontal" gap={3} style={{ padding: '15px'}}>
                <div style={{width: '30vw'}}></div>
                <Card style={{width: 550, overflowX: 'hidden', overflowY: 'auto'}} className="p-5">
                    <h2 className="m-auto">Изменение сотрудника</h2>
                    <br></br>
                    <Form style={{minHeight: '40vh'}}>
                        <Form.Group className="mb-3" controlId="controlSelectUser">
                            <Form.Label>Выберите изменяемого сотрудника</Form.Label>
                            <Select 
                                options={optionsUser}
                                isSearchable={true}
                                onChange={data=>setUserID(data.value)}/>
                        </Form.Group>
                        {idUser !== -1?
                        <>
                            <Form.Group className="mb-3" controlId="controlInput_Fname">
                                <Form.Label>Новое имя</Form.Label>
                                <Form.Control
                                    placeholder="Введите имя..." 
                                    onChange={e=>setFName(e.target.value)}/>
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="controlInput_Sname">
                                <Form.Label>Новая фамилия</Form.Label>
                                <Form.Control
                                    placeholder="Введите фамилию..." 
                                    onChange={e=>setSName(e.target.value)}/>
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="controlInput_Tname">
                                <Form.Label>Новое отчество</Form.Label>
                                <Form.Control
                                    placeholder="Введите отчество..." 
                                    onChange={e=>setTName(e.target.value)}/>
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="controlInput_Email">
                                <Form.Label>Новая почта</Form.Label>
                                <Form.Control
                                    placeholder="Введите почту..." 
                                    onChange={e=>setEmail(e.target.value)}/>
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="controlInput_Login">
                                <Form.Label>Логин</Form.Label>
                                <Form.Control
                                    placeholder="Введите логин..." 
                                    onChange={e=>setLogin(e.target.value)}/>
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="controlCheckbox_2">
                                <Form.Check 
                                    type="checkbox" 
                                    label="Отвязать почту" 
                                    onChange={(e) => { setDisable(e.target.checked) }}/>
                            </Form.Group>
                            <br></br>
                            {Fname.length > 0 || Sname.length > 0 || Tname.length > 0 || login.length > 0 || Email.length > 0 || isDisableMail ?
                            <>
                                <div className="d-flex justify-content-center align-items-center">
                                    <Button variant="primary" onClick={handleShow}>
                                        Предпросмотр
                                    </Button>
                                </div>
                                <Modal show={state.show} fullscreen={true} onHide={handleClose}>
                                    <Modal.Header closeButton>
                                        <Modal.Title>Предпросмотр изменения сотрудника</Modal.Title>
                                    </Modal.Header>
                                    <Modal.Body>
                                        <Container>
                                            <div className="table-responsive">
                                                <table style={{fontSize:'14px'}} className="table table-striped table-sm">
                                                    <thead>
                                                        <tr key="thead">
                                                            <th scope="col"></th>
                                                            <th scope="col">ФИО</th>
                                                            <th scope="col">Логин</th>
                                                            <th scope="col">Почта</th>
                                                            <th scope="col">Отвязка почты</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr key="tbody1">
                                                            <th>Новая информация о сотруднике</th>
                                                            <td>
                                                                {Sname.length > 0 ? <>{Sname} </> : <>- </>}
                                                                {Fname.length > 0 ? <>{Fname} </> : <>- </>}
                                                                {Tname.length > 0 ? <>{Tname}</> : <>-</>}
                                                            </td>
                                                            <td>{login.length > 0 ? <>{login}</> : <small>нет изменений</small>}</td>
                                                            <td>{Email.length > 0 ? <>{Email}</> : <small>нет изменений</small>}</td>
                                                            <td>{isDisableMail? <>Отвязать</> : <small>нет изменений</small>}</td>
                                                        </tr>
                                                        <tr key="tbody1">
                                                            <th>Старая информация о сотруднике</th>
                                                            <td>{Demon.oldUser.secondName.length > 0 ? 
                                                                <>
                                                                    {Demon.oldUser.secondName} <small> </small>
                                                                    {Demon.oldUser.firstName.length > 0 ?
                                                                    <>
                                                                        {Demon.oldUser.firstName} <small> </small>
                                                                        {Demon.oldUser.thirdName.length > 0 ?
                                                                        <>{Demon.oldUser.thirdName}</>
                                                                        :<></>}
                                                                    </>
                                                                    : <></>}
                                                                </> 
                                                                : <>-</>}
                                                            </td>
                                                            <td>{Demon.oldUser.login.length > 0 ? <>{Demon.oldUser.login}</> : <>-</>}</td>
                                                            <td>{Demon.oldUser.email.length > 0 ? <>{Demon.oldUser.email}</> : <>-</>}</td>
                                                            <td></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </Container>
                                    </Modal.Body>
                                    <Modal.Footer>
                                        <Button variant="outline-primary" onClick={B_ChangeUser}>
                                            Изменить
                                        </Button>
                                        <Button variant="secondary" onClick={handleClose}>
                                            Отменить
                                        </Button>
                                    </Modal.Footer>
                                </Modal>
                            </>
                            : <></>}
                        </>
                        :<></>}
                    </Form>
                </Card>
                <div style={{width: '30vw'}}></div>
            </Stack>
        </Container>
    );
}
export default ChangeUser;