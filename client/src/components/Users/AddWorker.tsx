import React , {useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { Button, Card, Container, Form, Modal } from "react-bootstrap";
import { ADMIN_OPTIONS_ROUTE } from "../../utils/consts";
import { GroupHierarchy } from "../../models/Group";
import { Position } from "../../models/Position";
import { Roles } from "../../models/UserData";
import Select from "react-select";
import { Registration } from "../../apiRequests/AccountRequests";
import { GetStructureGroup } from "../../apiRequests/GroupRequests";
import { GetListPosition, GetFreePosition, UpdatePosition } from "../../apiRequests/PositionRequests";

class forGroups{
    id : number;
    name : string;
}
var GroupMap : forGroups[] = [];

class Data {
    positions : Position[];
    groups : GroupHierarchy[];
}

function AddWorker() {
    const [data, setData] = useState<Data>({
        positions : [], groups : []
    })

    useEffect(() => {
        GetListPosition().then(positions => GetStructureGroup().then(groups => setData({positions : positions, groups : groups})));
    }, []);
    return <AddWorkerCard positions={data.positions} groups={data.groups} />;
}

class Demonstration{
    group : string;
    pos : string;
    constructor(){
        this.group = "group";
        this.pos = "-1";
    }
}
var Demon : Demonstration;
class cardProps {
    positions : Position[];
    groups : GroupHierarchy[];
}

function ParseTree (gs : GroupHierarchy) {
    let iGroup : forGroups = new forGroups();
    iGroup.id = gs.group.id;
    iGroup.name = gs.group.name;
    GroupMap.push(iGroup);
    if(gs.subgroups.length > 0)
        gs.subgroups.map(c => ParseTree(c));
}
/*function UniqueID (list : number[]) : number[] {
    let uniq = true;
    let tmp : number[];
    if(list.length > 0){
        for(let i = 0; i < list.length - 1; i++){
            for(let j = i + 1; j < list.length && uniq; j++){
                if(list[i] == list[j])
                    uniq = false;
            }
            if (uniq){
                tmp.push(list[i]);
            }
            uniq = true;
        }
        tmp.push(list[list.length - 1]);
    }
    return tmp;
}
function UniqueName (list : string[]) : string[] {
    let uniq = true;
    let tmp : string[];
    if(list.length > 0){
        for(let i = 0; i < list.length - 1; i++){
            for(let j = i + 1; j < list.length && uniq; j++){
                if(list[i] == list[j])
                    uniq = false;
            }
            if (uniq){
                tmp.push(list[i]);
            }
            uniq = true;
        }
        tmp.push(list[list.length - 1]);
    }
    return tmp;
}*/
class viewStates {
    show : boolean;
}
class Pair{
    label: string;
    value: number;
}
class GroupStates{
    idGroup: number;
    data: Pair[];
}
function AddWorkerCard(props : cardProps) {
    const navigate = useNavigate();
    const [Fname, setFName] = useState("");
    const [Sname, setSName] = useState("");
    const [Tname, setTName] = useState("");
    const [Email, setEmail] = useState("");
    const [idPos, setPos] = useState(-1);
    const [idGroup, setGroup] = useState<GroupStates>({idGroup: -1, data: []});
    const [login, setLogin] = useState("");
    const [password, setPass] = useState("");
    let [state, setStates] = useState<viewStates>({ show : false});
    const [required, setRequired] = useState(false);
    useEffect(() => {
        Demon = new Demonstration();
    }, []);
    function handleClose() {
        setStates({show : false});
    }
    function handleShow(){
        setStates({show : true});
    }
    GroupMap = [];
    props.groups.map(c => ParseTree(c));

    // Настройки
    const optionsGroup = GroupMap.map(data => ({label: data.name, value: data.id}));
    optionsGroup.push({label: "Не выбрано", value: -1});
    
    const setGroupID = async (id)=>{
        if(id !== -1){
            let tmp : Position[] = await GetFreePosition(id);
            const optionsPos = tmp.map(data => ({label: data.name, value: data.id}));
            optionsPos.push({label: "Не выбрано", value: -1});
            setGroup({idGroup: id, data: optionsPos});
            Demon.group = GroupMap.find(f => f.id === id).name;
        }
        else
            setGroup({idGroup: -1, data: []});
    }
    const setPosID=(id)=>{
        if(id !== -1){
            setPos(id);
            Demon.pos = idGroup.data.find(f => f.value === id).label;
        }  
        else
            setPos(-1);
    }
    const B_AddWorker = async () =>{
        setStates({show : false});
        try{
            let idUser : number = await Registration(login, password, Sname, Fname, Tname, Roles.ROLE_WORKER, Email);
            if (idPos !== -1){
                await UpdatePosition(idPos, undefined, idUser, undefined);
            }
            navigate(ADMIN_OPTIONS_ROUTE);
        }
        catch{
            setRequired(true);
        }
    }
    return(
        <Container 
            className="d-flex justify-content-center align-items-center"
        >
            <Card style={{width: 550, overflowX: 'hidden', overflowY: 'auto'}} className="p-5">
                <h2 className="m-auto">Добавление сотрудника</h2>
                <br></br>
                <Form>
                    <Form.Group className="mb-3" controlId="controlInput_Fname">
                        <Form.Label>Имя</Form.Label>
                        <Form.Control
                            placeholder="Введите имя..." 
                            onChange={e=>setFName(e.target.value)}/>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="controlInput_Sname">
                        <Form.Label>Фамилия</Form.Label>
                        <Form.Control
                            placeholder="Введите фамилию..." 
                            onChange={e=>setSName(e.target.value)}/>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="controlInput_Tname">
                        <Form.Label>Отчество</Form.Label>
                        <Form.Control
                            placeholder="Введите отчество..." 
                            onChange={e=>setTName(e.target.value)}/>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="controlInput_Email">
                        <Form.Label>Электронная почта</Form.Label>
                        <Form.Control
                            placeholder="Введите почту..." 
                            onChange={e=>setEmail(e.target.value)}/>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="controlInput_Login">
                        <Form.Label>Логин</Form.Label>
                        <Form.Control
                            placeholder="Введите логин..." 
                            onChange={e=>setLogin(e.target.value)}/>
                        {required? <small style={{color : 'red', fontSize : '12px'}}>Такой логин уже существует</small> :<></>}
                    </Form.Group>
                    
                    <Form.Group className="mb-3" controlId="controlInput_Pass">
                        <Form.Label>Пароль</Form.Label>
                        <Form.Control
                            type="password"
                            placeholder="Введите пароль..." 
                            onChange={e=>setPass(e.target.value)}/>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="controlSelectGroup">
                        <Form.Label>Выберите группу сотрудника</Form.Label>
                        <Select 
                            options={optionsGroup}
                            isSearchable={true}
                            onChange={data=>setGroupID(data.value)}/>
                    </Form.Group>
                    {idGroup.idGroup !== -1 && idGroup.data.length > 1?
                        <>
                            <Form.Group className="mb-3" controlId="controlSelectPos">
                                <Form.Label>Выберите должность сотрудника</Form.Label>
                                <Select 
                                    options={idGroup.data}
                                    isSearchable={true}
                                    onChange={data=>setPosID(data.value)}/>
                            </Form.Group>
                        </>
                    :<></>}
                    <br></br>
                    {(Fname.trim().length > 0 || Sname.trim().length > 0 || Tname.trim().length > 0) && login.trim().length > 0 && password.trim().length > 0?
                    <>
                        <div className="d-flex justify-content-center align-items-center">
                            <Button variant="primary" onClick={handleShow}>
                                Предпросмотр
                            </Button>
                        </div>
                        <Modal show={state.show} fullscreen={true} onHide={handleClose}>
                            <Modal.Header closeButton>
                                <Modal.Title>Предпросмотр добавления сотрудника</Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                                <Container className="d-flex justify-content-center align-items-center">
                                    <h1 style={{color: 'gray', minWidth: '1vw', position: 'absolute', top: '0', backgroundColor: 'white', borderRadius: '5px', paddingLeft: '6px', paddingRight: '6px'}}>{login}</h1>
                                    <div style={{border: '2px solid gray',  margin: '1%', padding: '20px', width: '50%'}}>
                                        <Card>
                                            <Card.Body>
                                                <Card.Title>Новый сотрудник</Card.Title>
                                                <Card.Text style={{overflow: 'auto'}}>
                                                <label>Имя: {Fname.trim().length > 0 ? <>{Fname.trim()}</> : <small>отсутствует</small>}</label><br></br>
                                                <br></br>
                                                <label>Фамилия: {Sname.trim().length > 0 ? <>{Sname.trim()}</> : <small>отсутствует</small>}</label><br></br>
                                                <br></br>
                                                <label>Отчество: {Tname.trim().length > 0 ? <>{Tname.trim()}</> : <small>отсутствует</small>}</label><br></br>
                                                <br></br>
                                                <label>Логин: {login}</label><br></br>
                                                <br></br>
                                                <label>Пароль: {password}</label><br></br>
                                                <br></br>
                                                <label>Почта: {Email.trim().length > 0? <>{Email.trim()}</> : <small>отсутствует</small>}</label><br></br>
                                                <br></br>
                                                {idPos !== -1?
                                                    <><label>Группа: {Demon.group.trim()}</label><br></br>
                                                    <br></br>
                                                    <label>Должность: {Demon.pos.trim()}</label><br></br></>
                                                :<><label>Группа: <small>не назначено</small></label><br></br>
                                                    <br></br>
                                                    <label>Должность: <small>не назначено</small></label><br></br></>} 
                                                </Card.Text>
                                            </Card.Body>
                                        </Card>
                                    </div>  
                                    
                                </Container>
                            </Modal.Body>
                            <Modal.Footer>
                                <Button variant="outline-primary" onClick={B_AddWorker}>
                                    Добавить
                                </Button>
                                <Button variant="secondary" onClick={handleClose}>
                                    Отменить
                                </Button>
                            </Modal.Footer>
                        </Modal>
                    </>
                    : <></>
                    }
                </Form>
            </Card>
        </Container>
    );
}
export default AddWorker;