import React, {useState, useContext} from "react";
import { Button, Card, Container, Form } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import { Context } from "../../index";
import { ACCESS_TOKEN_COOKIE, ERROR_ROUTE, HOME_ROUTE, PROFILE_ROUTE } from "../../utils/consts";
import Cookies from "js-cookie";
import jwtDecode from "jwt-decode";
import {UserData, Roles} from "../../models/UserData";
import AuthCallback from "../../classes/AuthCallback";
import Cookie from "../../classes/Cookie";
import { Authentication } from "../../apiRequests/AccountRequests";

const LogIn = (callback : AuthCallback) => {
    const user : UserData = useContext(Context);
    const navigate = useNavigate();
    const [login, setLogin] = useState('');
    const [password, setPassword] = useState('');
    const [required, setRequired] = useState(true);
    const signIn = async () =>{
        try{
            let data = await Authentication(login, password);
            setRequired(data);
            if(!data){
                navigate(HOME_ROUTE);
            }else if(data){
                const usrCookie = jwtDecode<Cookie>(Cookies.get(ACCESS_TOKEN_COOKIE));
                user.role = (usrCookie && usrCookie.user && usrCookie.user.role) ? usrCookie.user.role : Roles.ROLE_WORKER;
                console.log(user.role);
                callback.func(null);
                navigate(PROFILE_ROUTE);
            }else{
                navigate(ERROR_ROUTE);
            }
        }catch(e){console.log(e)}
    }
    return(
        <Container 
            className="d-flex justify-content-center align-items-center"
        >
            <Card style={{width: '100%'}} className="p-5">
                <h2 className="m-auto">Авторизация</h2>
                <Form className="d-flex flex-column">
                    <Form.Control 
                        className="mt-3"
                        placeholder="Введите ваш логин..." 
                        value={login}
                        onChange={e => setLogin(e.target.value)}/>
                    <Form.Control 
                        className="mt-3"
                        placeholder="Введите ваш пароль..." 
                        value={password}
                        onChange={e => setPassword(e.target.value)}
                        type="password"/>
                    {!required? <small style={{color : 'red', fontSize: '12px'}}>Неправильный логин и/или пароль</small> : <></>}
                    <Button 
                        className="mt-3 align-self-center" 
                        style={{width: 100}} 
                        variant="outline-primary"
                        onClick={signIn}>
                            Войти
                    </Button>
                </Form>
            </Card>
        </Container>
    );
};
export default LogIn;