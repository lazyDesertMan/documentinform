import React, {useEffect, useState } from "react";
import { Button, Card, Container, Form, Modal, Stack } from "react-bootstrap";
import { ADMIN_OPTIONS_ROUTE } from "../../utils/consts";
import { useNavigate } from "react-router-dom";
import { UserData } from "../../models/UserData";
import Select from "react-select";
import { GetListUsers, RemoveUser } from "../../apiRequests/UserRequests";

class Data {
    users : UserData[];
}

function DeleteUser() {
    const [data, setData] = useState<Data>({
        users : []
    })

    useEffect(() => {
        GetListUsers().then(users => setData({users : users}));
    }, []);
    return <DeleteUserCard users={data.users} />;
}
class cardProps {
    users : UserData[];
}

class viewStates {
    show : boolean;
}

function DeleteUserCard(props : cardProps) {
    const navigate = useNavigate();
    const [name, setName] = useState("");
    const [login, setLogin] = useState("");
    const [idUser, setUser] = useState(-1);
    let [state, setStates] = useState<viewStates>({ show : false});

    // Настройки
    const optionsUser = props.users.map(data => ({label: data.secondName + " " + data.firstName + " " + data.thirdName, value: data.id}));
    optionsUser.push({label: "Не выбрано", value: -1});

    function handleClose() {
        setStates({show : false});
    }
    function handleShow(){
        setStates({show : true});
    }
    const setUserID=(id)=>{
        if(id !== -1)
        {
            const user = props.users.find(f => f.id === id);
            if (typeof user !== "undefined") {
                setUser(id);
                setName(user.secondName + " " + user.firstName + " " + user.thirdName);
                setLogin(user.login);
            }
        }           
        else
            setUser(-1);
    }
    const B_DeleteUser = async () =>{
        setStates({show : false});
        await RemoveUser(idUser);
        navigate(ADMIN_OPTIONS_ROUTE);
    }
    return(
        <Container>
            <Stack direction="horizontal" gap={3} style={{ padding: '15px'}}>
                <div style={{width: '30vw'}}></div>
                <Card style={{width: 550, overflowX: 'hidden', overflowY: 'auto'}} className="p-5">
                    <h2 className="m-auto">Удаление сотрудника</h2>
                    <br></br>
                    <Form style={{minHeight: '45vh'}}>
                        <Form.Group className="mb-3" controlId="controlSelectUser">
                            <Form.Label>Выберите удаляемого сотрудника</Form.Label>
                            <Select 
                                options={optionsUser}
                                isSearchable={true}
                                onChange={data=>setUserID(data.value)}/>
                        </Form.Group>
                        <br></br>
                        {idUser !== -1?
                        <>
                            <div className="d-flex justify-content-center align-items-center">
                                <Button variant="primary" onClick={handleShow}>
                                    Предпросмотр
                                </Button>
                            </div>
                            <Modal show={state.show} fullscreen={true} onHide={handleClose}>
                                <Modal.Header closeButton>
                                    <Modal.Title>Предпросмотр удаления сотрудника</Modal.Title>
                                </Modal.Header>
                                <Modal.Body>    
                                    <Container className="d-flex justify-content-center align-items-center">
                                        <h1 style={{color: 'gray', minWidth: '1vw', position: 'absolute', top: '0', backgroundColor: 'white', borderRadius: '5px', paddingLeft: '6px', paddingRight: '6px'}}>{login}</h1>
                                        <div style={{border: '2px solid gray',  margin: '1%', padding: '20px', width: '50%'}}>
                                            <Card>
                                                <Card.Body>
                                                    <Card.Title>Удаление сотрудника</Card.Title>
                                                    <Card.Text style={{overflow: 'auto'}}>
                                                        <label>ФИО: {name}</label><br></br>
                                                        <br></br>
                                                        <label>Логин: {login}</label><br></br>
                                                        <br></br>
                                                    </Card.Text>
                                                </Card.Body>
                                            </Card>
                                        </div>  
                                    </Container>
                                </Modal.Body>
                                <Modal.Footer>
                                    <Button variant="outline-primary" onClick={B_DeleteUser}>
                                        Удалить
                                    </Button>
                                    <Button variant="secondary" onClick={handleClose}>
                                        Отменить
                                    </Button>
                                </Modal.Footer>
                            </Modal>
                        </>
                        :<></>}
                    </Form>
                </Card>
                <div style={{width: '30vw'}}></div>
            </Stack>
        </Container>
    );
}
export default DeleteUser;