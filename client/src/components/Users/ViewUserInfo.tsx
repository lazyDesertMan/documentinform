import React , {useState, useEffect, Suspense } from "react";
import { Card, Form, ListGroup, ListGroupItem, Row, Spinner, Stack } from "react-bootstrap";
import { ACCESS_TOKEN_COOKIE } from "../../utils/consts";
import { Group} from "../../models/Group";
import { Position } from "../../models/Position";
import { Roles, UserData } from "../../models/UserData";
import Cookies from 'js-cookie'
import jwtDecode from "jwt-decode";
import Footer from "../Footer";
import { ITask } from "../../models/Task";
import ViewStructureGroup from "../Groups/ViewStructureGroup";

import Cookie from "../../classes/Cookie";
import ChangePasswordComp from "./ChangePassword";

import LoadAllDocumentsComp from "../Documents/ViewAllDocuments";
import { GetInfoAboutGroup } from "../../apiRequests/GroupRequests";
import { GetAuthPos } from "../../apiRequests/PositionRequests";
import { GetActiveTasks } from "../../apiRequests/TaskRequests";
import { GetListUsers } from "../../apiRequests/UserRequests";

class Data {
    positions : Position[];
    users : UserData[];
    tasks : ITask[];
}

function LoadDataForProfile() {
    const [data, setData] = useState<Data>({
        positions : [], users: [], tasks: []
    })
    useEffect(() => {
        GetAuthPos().then(positions => GetListUsers().then(users => GetActiveTasks().then(tasks => setData({positions : positions, users : users, tasks : tasks}))));
    }, []);

    return <CardWorker positions={data.positions} users={data.users} tasks={data.tasks}/>;
}

class cardProps {
    positions : Position[];
    users: UserData[];
    tasks: ITask[];
}

function CardWorker(props : cardProps) {
    const [groups, setGroups] = useState([]);

    const Method = async(pos : Position[])=>{
        let res : Group[] = [];
        for(let i :number = 0; i < pos.length; i++){
            res.push(await GetInfoAboutGroup(pos[i].groupID));
        }
        return res;
    }
    useEffect(() => {
        Method(props.positions).then(groups => setGroups(groups));
    }, [props.positions]);
    
    let login = "";
    let admin: boolean = false;
    let director: boolean = false;
    let FIO: string = "";
    const usrCookie = Cookies.get(ACCESS_TOKEN_COOKIE);
    if (usrCookie != null) {
        const decodeUsrCookie= jwtDecode<Cookie>(usrCookie);
        FIO = decodeUsrCookie.user.secondName + " " + decodeUsrCookie.user.firstName + " " + decodeUsrCookie.user.thirdName;
        login = decodeUsrCookie.user.login;
        admin = decodeUsrCookie.user.role === Roles.ROLE_ADMIN;
        director = decodeUsrCookie.user.role === Roles.ROLE_DIRECTOR;
    }
    props.tasks.sort((l, r) => l.startDate < r.startDate ? 1 : -1)
    
    return(
        <>
        <Row>
            <Card style={{ width: '20%', marginLeft: '2%'}}>
                <Card.Body style={{width: '100%'}}>
                    {admin?
                    <>
                        <Card.Title className="d-flex justify-content-center align-items-center">Карточка администратора</Card.Title>
                        <br></br>
                        <Card.Text>
                            <strong>ФИО:</strong>&nbsp;{FIO}<br></br><br></br> 
                            <strong>Возможности:</strong>&nbsp;
                            <ul>
                                <li>Просмотр иерархии группы</li>
                                <li>Добавление <small style={{color: 'darkred'}}>группы / должности / сотрудника</small></li>
                                <li>Изменение <small style={{color: 'darkred'}}>группы / должности / сотрудника</small></li>
                                <li>Удаление <small style={{color: 'darkred'}}>группы / должности / сотрудника</small></li>
                                <li>Назначение <small style={{color: 'darkred'}}>временного / постоянного</small> заместителя</li>
                                <li>Удаление заместителей</li>
                            </ul>
                        </Card.Text>
                    </>
                    :director?
                    <>
                        <Card.Title className="d-flex justify-content-center align-items-center">Карточка директора</Card.Title>
                        <br></br>
                        <Card.Text>
                            <strong>ФИО:</strong>&nbsp;{FIO}<br></br><br></br> 
                            <strong>Возможности:</strong>&nbsp;
                            <ul>
                                <li>Просмотр списка документов</li>
                                <li>Поиск документов</li>
                                <li>Добавление документов</li>
                                <li>Изменение документов</li>
                                <li>Удаление документов</li>
                                <li>Выдача первых заданий</li>
                                <li>Вывод отчета по документу</li>
                            </ul>
                        </Card.Text>
                    </>
                    :<>
                        <Card.Title className="d-flex justify-content-center align-items-center">Пользователь:<strong>&nbsp;{typeof login === 'string' && login.trim().length > 0 ? <>{login}</>: <>Loading...</>}</strong></Card.Title>
                        <br></br>
                    </>} 
                    {!admin && !director?
                    <>
                        <ListGroup className="list-group-flush">
                            <ListGroupItem><strong>ФИО:</strong>&nbsp;{typeof FIO === 'string' && FIO.trim().length > 0 ? <>{FIO}</> : <>Loading...</>}</ListGroupItem>
                            {props.positions.length > 0?
                            <>  {props.positions.length === 1? 
                                    <ListGroupItem><strong>Должность:</strong>&nbsp;{props.positions[props.positions.length - 1].name}</ListGroupItem> 
                                :   
                                    <><ListGroupItem><strong>Должности:</strong>&nbsp;
                                        {Array.from({ length: props.positions.length - 1 }).map((_, idx) => (
                                            <>{props.positions[idx].name}, </>
                                        ))}
                                    {props.positions[props.positions.length - 1].name}</ListGroupItem></>
                                }
                            </>
                            :<></>}
                            {groups.length > 0?
                            <>  {groups.length === 1? 
                                    <ListGroupItem><strong>Группа:</strong>&nbsp;{groups[groups.length - 1].name}</ListGroupItem> 
                                :   
                                    <><ListGroupItem><strong>Группы:</strong>&nbsp;
                                        {Array.from({ length: groups.length - 1 }).map((_, idx) => (
                                            <>{groups[idx].name}, </>
                                        ))}
                                    {groups[groups.length - 1].name}</ListGroupItem></>
                                }
                            </>
                            :<></>}
                        </ListGroup>
                    </>
                    :<></>}
                    <br></br>
                    {!director && !admin?
                        <Card.Text>
                            <strong>Активных заданий:</strong>&nbsp;{props.tasks.length}
                        </Card.Text>
                    :<></>}
                    <ChangePasswordComp />
                </Card.Body>
            </Card>
            <Card style={{ width: '75%', marginLeft: '1%' }}>
                <Card.Body>
                {admin?
                    <>
                        <Card.Title>Группы</Card.Title>
                        <Card.Text style={{width: '100%'}}>
                            <ViewStructureGroup />
                        </Card.Text>
                    </>
                :director?
                    <>
                        <Card.Title>Документы</Card.Title>
                        <Card.Text style={{width: '100%'}}>
                            <Suspense fallback={<div className="d-grid gap-2 d-sm-flex justify-content-sm-center"><Spinner animation="border"/></div>}>
                                <LoadAllDocumentsComp/>
                            </Suspense>
                        </Card.Text>
                    </>
                :   <>
                        <Card.Title>Обновления</Card.Title>
                        <Card.Text style={{width: '100%'}}>
                            <Form style={{ width: '100%', marginTop: '1%' }}>
                                <div className="list-group list-group-flush scrollarea">
                                    <>
                                        {props.tasks && props.tasks.map(item =>
                                            <>
                                            <Stack direction="horizontal" gap={2} >
                                                <div style={{marginTop: '0', height: '10vh'}}><label style={{backgroundColor: 'purple', color: 'white', fontSize: '9px', minWidth: '3%', padding: '1px', fontFamily: 'inherit', marginRight: '1%'}}>новое</label></div>
                                                <div className="list-group-item list-group-item-action py-3 lh-tight" style={{borderRadius: '15px', backgroundColor: 'whitesmoke', marginTop: '10px'}}>
                                                    <div className="d-flex w-100 align-items-center justify-content-between">
                                                        <label className="mb-1"> Документ "{item.document}" был выдан</label>
                                                        <small>{new Date(item.startDate).toLocaleDateString()} </small>
                                                    </div>
                                                    <div className="col-10 mb-1 small">Тип: {item.type === 1? <b>Ознакомление</b> : <b>Пересылка</b>}</div>
                                                </div>
                                            </Stack>
                                            </>
                                        )}
                                    </>
                                </div>
                            </Form>
                        </Card.Text>
                    </>
                }
                </Card.Body>
            </Card>
        </Row>
        <Footer/>
        </>
    );
}
export default LoadDataForProfile;