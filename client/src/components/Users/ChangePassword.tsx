import React, { useState } from "react";
import { Button, Container, Form, Modal } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import { ChangePassword } from "../../apiRequests/AccountRequests";
import { ERROR_ROUTE } from "../../utils/consts";

class viewStates {
    show : boolean;
}
const ChangePasswordComp = () => {
    const navigate = useNavigate();
    const [oldPass, setOldPass] = useState("");
    const [youngPass, setYoungPass] = useState("");
    const [repeatPass, setRepeatPass] = useState("");
    const [required, setRequiredPass] = useState(false);
    
    let [state, setStates] = useState<viewStates>({ show : false});
    function handleClose() {
        setStates({show : false});
    }
    const B_ChangePass = async () =>{
        try{
            if(youngPass.length > 0 && repeatPass === youngPass){
                await ChangePassword(oldPass, youngPass);
                setStates({show : false});
                setRequiredPass(false);
            }
            else if(repeatPass !== youngPass){
                setRequiredPass(true);
            }
        } catch{navigate(ERROR_ROUTE);}
            
    }
    function handleShow(){
        setStates({show : true});
    }
    return(
        <>
            <Button 
                style={{width: '100%'}}
                onClick={handleShow}>Редактировать</Button>
            <Modal show={state.show} fullscreen={true} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Редактирование профиля</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Container style={{width: '45vw'}}>
                        <div style={{border: '2px solid gray', borderRadius: '15px', padding: '15px'}}>
                        <h5 style={{textAlign: 'center'}}>Изменить пароль</h5>
                        <Form.Group className="mb-3" controlId="controlInput_OldPass">
                            <Form.Label>Старый пароль</Form.Label>
                            <Form.Control
                                type="password"
                                placeholder="Введите старый пароль..." 
                                onChange={e=>setOldPass(e.target.value)}/>
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="controlInput_YongPass">
                            <Form.Label>Новый пароль</Form.Label>
                            <Form.Control
                                type="password"
                                placeholder="Введите новый пароль..." 
                                onChange={e=>setYoungPass(e.target.value)}/>
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="controlInput_RepeatPass">
                            <Form.Label>Повторите пароль</Form.Label>
                            <Form.Control
                                type="password"
                                placeholder="Введите новый пароль..." 
                                onChange={e=>setRepeatPass(e.target.value)}/>
                                {required? <small style={{color : 'red', fontSize : '12px'}}>Неверно подтвержден пароль</small> :<></>}
                        </Form.Group>
                        </div>
                    </Container>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="outline-primary" onClick={B_ChangePass}>
                        Сохранить
                    </Button>
                    <Button variant="secondary" onClick={handleClose}>
                        Закрыть
                    </Button>
                </Modal.Footer>
            </Modal>
        </>
    );
};
export default ChangePasswordComp;