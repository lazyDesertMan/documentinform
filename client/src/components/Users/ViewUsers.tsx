import React, { useEffect, useState } from "react";
import { GetListUsers } from "../../apiRequests/UserRequests";
import {UserData} from "../../models/UserData";
import Error from "../../pages/Error";

class loadStates {
    isFetching : boolean;
    isError : boolean;
    errorMsg? : string;
    users : UserData[];
}

function LoadUsers_Comp() {
    let [state, setState] = useState<loadStates>({ isFetching: false, isError: false, users: [] });

    function loadData() {
        setState({users: state.users, isError: false, isFetching: true});
        GetListUsers().then(r => {
            setState({users: r, isFetching: false, isError: false});
        }).catch(e => {
            setState({users: state.users, isFetching: false, isError: true, errorMsg: e});
        });
    }
    useEffect(() => loadData(), []);
    
        return(
            <div>
                {this.state.isFetching ? "Loading..." : this.state.isError ? <Error msg={this.state.errorMsg} /> : <ViewUsers list={this.state.users} />}
            </div>
        );
}

class viewProps {
    list : UserData[];
}
  
function ViewUsers(props : viewProps) {
        return(
            <pre>
                {JSON.stringify(props.list, null, 3)}
            </pre>
        );
}
export default LoadUsers_Comp;