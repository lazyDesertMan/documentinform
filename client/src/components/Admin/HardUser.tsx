import React from "react";
import { Button, Card, Stack } from "react-bootstrap";
import "../../css/footer-styles.css";

const HardUser = () => {
    return(
        <Stack direction="horizontal" gap={4} style={{ padding: '15px', border: '1px solid gray'}}>
            <Card style={{minHeight: '15%', width: '35%'}}>
                <Card.Body>
                    <Card.Title>Добавить сотрудника</Card.Title>
                    <Card.Text style={{textAlign: 'left'}}>
                    При добавлении сотрудника необходимо:
                    <ul>
                        <li style={{color: 'red'}}>ФИО</li>
                        <li style={{color: 'red'}}>Логин</li>
                        <li style={{color: 'red'}}>Пароль</li>
                        <li>Возможная группа</li>
                        <li>Свободна должность в группе</li>
                    </ul>
                    <br></br>
                    </Card.Text>
                    <Button style={{width: '100%'}} variant="primary" href={process.env.REACT_APP_CLIENT_URL +"/admin/functional/addUser"}>Открыть</Button>
                </Card.Body>
            </Card>
            <Card  style={{minHeight: '15%', width: '35%'}}>
                <Card.Body>
                    <Card.Title>Изменение сотрудника</Card.Title>
                    <Card.Text style={{textAlign: 'left'}}>
                        При изменении сотрудника возможно изменить:
                        <ul>
                            <li>Имя</li>
                            <li>Фамилию</li>
                            <li>Отчество</li>
                            <li>Логин</li>
                        </ul>
                        <label style={{color: 'red'}}>Примечение: </label> изменять пароль может только сам сотрудник!
                    </Card.Text>
                    <Button style={{width: '100%'}} variant="primary" href={process.env.REACT_APP_CLIENT_URL +"/admin/functional/updateUser"}>Открыть</Button>
                </Card.Body>
            </Card>
            <Card  style={{minHeight: '15%', width: '35%'}}>
                <Card.Body>
                    <Card.Title>Удаление сотрудника</Card.Title>
                    <Card.Text style={{textAlign: 'left'}}>
                        При удалении сотрудника нужно указать удаляемого сотрудника<p></p>
                        <br></br><br></br><br></br><br></br><br></br>
                        <br></br>
                    </Card.Text>
                    <Button style={{width: '100%'}} variant="primary" href={process.env.REACT_APP_CLIENT_URL +"/admin/functional/deleteUser"}>Открыть</Button>
                </Card.Body>
            </Card>
            <div style={{minHeight: '15%', width: '5%'}}></div>
            <div style={{minHeight: '15%', width: '5%'}}></div>
        </Stack>
    );
};
export default HardUser;