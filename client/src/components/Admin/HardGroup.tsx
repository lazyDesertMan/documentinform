import React from "react";
import { Button, Card, Stack } from "react-bootstrap";
import "../../css/footer-styles.css";

const HardGroup = () => {
    return(
        <Stack direction="horizontal" gap={5} style={{ padding: '15px', border: '1px solid gray'}}>
            <Card  style={{minHeight: '15%', width: '25%'}}>
                <Card.Body>
                    <Card.Title>Добавить группу</Card.Title>
                    <Card.Text style={{textAlign: 'left'}}>
                        Необходимо:
                        <ul>
                            <li style={{color: 'red'}}>Название</li>
                            <li>Группа-родитель</li>
                            <li>Должность руководителя</li>
                            <li>Сотрудник на должность руководителя</li>
                            <br></br>
                        </ul>
                    </Card.Text>
                    <Button style={{width: '100%'}} variant="primary" href={process.env.REACT_APP_CLIENT_URL +"/admin/functional/addGroup"}>Открыть</Button>
                </Card.Body>
            </Card>
            <Card style={{minHeight: '15%', width: '25%'}}>
                <Card.Body>
                    <Card.Title>Изменить группу</Card.Title>
                    <Card.Text style={{textAlign: 'left'}}>
                        возможно изменить:
                        <ul>
                            <li>Название</li>
                            <li>Группу-родителя</li>
                            <li>Подгруппы</li>
                            <li>Должность руководителя</li>
                            <li>Сотрудника на эту должность</li>
                            <br></br>
                        </ul>
                    </Card.Text>
                    <Button style={{width: '100%'}} variant="primary" href={process.env.REACT_APP_CLIENT_URL +"/admin/functional/updateGroup"}>Открыть</Button>
                </Card.Body>
            </Card>
            <Card style={{minHeight: '15%', width: '25%'}}>
                <Card.Body>
                    <Card.Title >Удаление группы</Card.Title>
                    <Card.Text style={{textAlign: 'left'}}>
                        При удалении группы нужно указать удаляемую группу
                        <br></br><br></br><br></br><br></br><br></br><br></br><br></br>
                    </Card.Text>
                    <Button style={{width: '100%'}} variant="primary" href={process.env.REACT_APP_CLIENT_URL +"/admin/functional/deleteGroup"}>Открыть</Button>
                </Card.Body>
            </Card>
        </Stack>
    );
};
export default HardGroup;