import React, { useState } from "react";
import { Button, Stack } from "react-bootstrap";
import "../../css/style.css";
import HardGroup from "./HardGroup";
import HardPos from "./HardPos";
import HardUser from "./HardUser";

const AdminHard = () => {
    let [b_Group, setGroup] = useState(false);
    let [b_Pos, setPos] = useState(false);
    let [b_User, setUser] = useState(false);

    let [activeGroup, setActiveGroup] = useState(false);
    let [activePos, setActivePos] = useState(false);
    let [activeUser, setActiveUser] = useState(false);
    function ShowGroup(){
        setActiveGroup(true);
        setActiveUser(false);
        setActivePos(false);

        setGroup(true);
        setUser(false);
        setPos(false);
    }
    function ShowPos(){
        setActiveGroup(false);
        setActiveUser(false);
        setActivePos(true);

        setPos(true);
        setUser(false);
        setGroup(false);
    }
    function ShowUser(){
        setActiveGroup(false);
        setActiveUser(true);
        setActivePos(false);

        setUser(true);
        setGroup(false);
        setPos(false);
    }
    return(
        <>
            <div style={{overflow: 'auto',minHeight: '100vh'}}>
            <Stack direction="horizontal" gap={3} style={{ padding: '10px'}}>
                <Button variant={activeGroup? 'success' : 'outline-success'} onClick={ShowGroup} style={{width: '15%'}}>Группы</Button>
                <Button variant={activePos? 'success' : 'outline-success'} onClick={ShowPos} style={{width: '15%'}}>Должности</Button>
                <Button variant={activeUser? 'success' : 'outline-success'} onClick={ShowUser} style={{width: '15%'}}>Сотрудники</Button>
            </Stack>
            {b_Group?
                <HardGroup />
            :b_Pos?
                <HardPos />
            :b_User ?
                <HardUser/>
            :<div/>}
            </div>
        </>
    );
};
export default AdminHard;