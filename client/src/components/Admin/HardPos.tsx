import React from "react";
import { Button, Card, Stack } from "react-bootstrap";
import "../../css/footer-styles.css";

const HardTask = () => {
    return(
        <Stack direction="horizontal" gap={4} style={{ padding: '15px', border: '1px solid gray'}}>
            <Card  style={{minHeight: '15%', width: '30%'}}>
                <Card.Body>
                    <Card.Title>Добавить должность</Card.Title>
                    <Card.Text style={{textAlign: 'left'}}>
                        При добавлении должности необходимо:
                        <ul>
                            <li style={{color: 'red'}}>Название</li>
                            <li style={{color: 'red'}}>Группа</li>
                            <li>Сотрудник</li>
                        </ul>
                        <br></br><br></br>
                    </Card.Text>
                    <Button style={{width: '100%'}} variant="primary" href={process.env.REACT_APP_CLIENT_URL +"/admin/functional/addPos"}>Открыть</Button>
                </Card.Body>
            </Card>
            <Card  style={{minHeight: '15%', width: '30%'}}>
                <Card.Body>
                    <Card.Title>Изменить должность</Card.Title>
                    <Card.Text style={{textAlign: 'left'}}>
                        При изменении должности возможно изменить:
                        <ul>
                            <li>Название</li>
                            <li>Группу</li>
                            <li>Сотрудника</li>
                        </ul>
                        <br></br><br></br>
                    </Card.Text>
                    <Button style={{width: '100%'}} variant="primary" href={process.env.REACT_APP_CLIENT_URL +"/admin/functional/updatePos"}>Открыть</Button>
                </Card.Body>
            </Card>
            <Card style={{minHeight: '15%', width: '30%'}}>
                <Card.Body>
                    <Card.Title>Удалить должность</Card.Title>
                    <Card.Text style={{textAlign: 'left'}}>
                        Необходимо:
                        <ul>
                            <li style={{color: 'red'}}>Группу с должностью</li>
                            <li style={{color: 'red'}}>Удаляемую должность</li>
                        </ul>
                        <br></br><br></br><br></br><br></br>
                    </Card.Text>
                    <Button style={{width: '100%'}} variant="primary" href={process.env.REACT_APP_CLIENT_URL +"/admin/functional/deletePos"}>Открыть</Button>
                </Card.Body>
            </Card>
            <div style={{minHeight: '15%', width: '10%'}}></div>
        </Stack>
    );
};
export default HardTask;