import React from "react";
import { Button, Card, Stack} from "react-bootstrap";
import '../../css/style.css';
import { ADMIN_DELETEPOS_ROUTE } from "../../utils/consts";

const AdminSimplified = () => {
    return(
        <>
            <Stack direction="horizontal" gap={5} style={{marginLeft: '5%', marginTop: '1%', textAlign: 'center', padding: '10px'}}>
                <h3 style={{width: '25%'}}>Группы</h3>
                <div style={{width: '1px'}} className="vr" />
                <h3 style={{width: '25%'}}>Должности</h3>
                <div style={{width: '1px'}} className="vr" />
                <h3 style={{width: '25%'}}>Сотрудники</h3>
            </Stack>
            <Stack direction="horizontal" gap={5} style={{marginLeft: '5%', marginTop: '1%', textAlign: 'center', padding: '10px'}}>
                <Card style={{ width: '25%' }}>
                    <Card.Body>
                        <Card.Title>Добавить группу</Card.Title>
                        <Card.Text style={{textAlign: 'left'}}>
                        При добавлении группы необходимо:
                        <ul>
                            <li style={{color: 'red'}}>Название</li>
                            <li>Группа-родитель (если такая имеется)</li>
                            <li>Должность руководителя группы (если такая имеется)</li>
                            <li>Сотрудник на должность руководителя (если не назначен)</li>
                        </ul>
                        </Card.Text>
                        <Button variant="primary" href={process.env.REACT_APP_CLIENT_URL + "/admin/functional/addGroup"}>Открыть</Button>
                    </Card.Body>
                </Card>
                <div style={{width: '1px'}} className="vr" />
                <Card style={{ width: '25%' }}>
                    <Card.Body>
                        <Card.Title>Добавить должность</Card.Title>
                        <Card.Text style={{textAlign: 'left'}}>
                        При добавлении должности необходимо:
                        <ul>
                            <li style={{color: 'red'}}>Название</li>
                            <li style={{color: 'red'}}>Группа</li>
                            <li>Сотрудник на добавляемую должность</li>
                        </ul>
                        <br></br><br></br><br></br><p></p>
                        </Card.Text>
                        <Button variant="primary" href={process.env.REACT_APP_CLIENT_URL +"/admin/functional/addPos"}>Открыть</Button>
                    </Card.Body>
                </Card>
                <div style={{width: '1px'}} className="vr" />
                <Card style={{ width: '25%' }}>
                    <Card.Body>
                        <Card.Title>Добавить сотрудника</Card.Title>
                        <Card.Text style={{textAlign: 'left'}}>
                        При добавлении сотрудника необходимо:
                        <ul>
                            <li style={{color: 'red'}}>ФИО</li>
                            <li style={{color: 'red'}}>Логин</li>
                            <li style={{color: 'red'}}>Пароль</li>
                            <li>Возможная группа</li>
                            <li>Свободна должность в группе</li>
                        </ul>
                        <br></br><br></br>
                        </Card.Text>
                        <Button variant="primary" href={process.env.REACT_APP_CLIENT_URL +"/admin/functional/addUser"}>Открыть</Button>
                    </Card.Body>
                </Card>
            </Stack>
            <Stack direction="horizontal" gap={5} style={{marginLeft: '5%', marginTop: '1%', textAlign: 'center', padding: '10px'}}>
                <Card style={{ width: '25%' }}>
                    <Card.Body>
                        <Card.Title>Изменить группу</Card.Title>
                        <Card.Text style={{textAlign: 'left'}}>
                        При изменении группы возможно изменить:
                        <ul>
                            <li>Название</li>
                            <li>Группу-родителя</li>
                            <li>Подгруппы</li>
                            <li>Должность руководителя группы</li>
                            <li>Сотрудника на эту должность</li>
                        </ul>
                        </Card.Text>
                        <Button variant="primary" href={process.env.REACT_APP_CLIENT_URL +"/admin/functional/updateGroup"}>Открыть</Button>
                    </Card.Body>
                </Card>
                <div style={{width: '1px'}} className="vr" />
                <Card style={{ width: '25%' }}>
                    <Card.Body>
                        <Card.Title>Изменить должность</Card.Title>
                        <Card.Text style={{textAlign: 'left'}}>
                        При изменении должности возможно изменить:
                        <ul>
                            <li>Название</li>
                            <li>Группу</li>
                            <li>Сотрудника на эту должность</li>
                        </ul>
                        <br></br><br></br>
                        </Card.Text>
                        <Button variant="primary" href={process.env.REACT_APP_CLIENT_URL +"/admin/functional/updatePos"}>Открыть</Button>
                    </Card.Body>
                </Card>
                <div style={{width: '1px'}} className="vr" />
                <Card style={{ width: '25%' }}>
                    <Card.Body>
                        <Card.Title>Изменение сотрудника</Card.Title>
                        <Card.Text style={{textAlign: 'left'}}>
                        При изменении сотрудника возможно изменить:
                        <ul>
                            <li>Имя</li>
                            <li>Фамилию</li>
                            <li>Отчество</li>
                            <li>Логин</li>
                        </ul>
                        <label style={{color: 'red'}}>Примечение: </label> изменять пароль может только сам сотрудник!
                        </Card.Text>
                        <Button variant="primary" href={process.env.REACT_APP_CLIENT_URL +"/admin/functional/updateUser"}>Открыть</Button>
                    </Card.Body>
                </Card>
            </Stack>
            <Stack direction="horizontal" gap={5} style={{marginLeft: '5%', marginTop: '1%', textAlign: 'center', padding: '10px'}}>
                <Card style={{ width: '25%' }}>
                    <Card.Body>
                        <Card.Title>Удаление группы</Card.Title>
                        <Card.Text style={{textAlign: 'left'}}>
                        При удалении группы нужно указать удаляемую группу
                        <br></br><br></br><br></br><br></br>
                        </Card.Text>
                        <Button variant="primary" href={process.env.REACT_APP_CLIENT_URL +"/admin/functional/deleteGroup"}>Открыть</Button>
                    </Card.Body>
                </Card>
                <div style={{width: '1px'}} className="vr" />
                <Card style={{ width: '25%' }}>
                    <Card.Body>
                        <Card.Title>Удалить должность</Card.Title>
                        <Card.Text style={{textAlign: 'left'}}>
                        При удалении должности нужно указать:
                        <ul>
                            <li style={{color: 'red'}}>Группу, где находится удаляемая должность</li>
                            <li style={{color: 'red'}}>Удаляемую должность</li>
                        </ul>
                        </Card.Text>
                        <Button variant="primary" href={process.env.REACT_APP_CLIENT_URL + ADMIN_DELETEPOS_ROUTE}>Открыть</Button>
                    </Card.Body>
                </Card>
                <div style={{width: '1px'}} className="vr" />
                <Card style={{ width: '25%' }}>
                    <Card.Body>
                        <Card.Title>Удаление сотрудника</Card.Title>
                        <Card.Text style={{textAlign: 'left'}}>
                        При удалении сотрудника нужно указать удаляемого сотрудника
                        <br></br><br></br><br></br><br></br>
                        </Card.Text>
                        <Button variant="primary" href={process.env.REACT_APP_CLIENT_URL +"/admin/functional/deleteUser"}>Открыть</Button>
                    </Card.Body>
                </Card>
            </Stack> 
        </>
    );
};
export default AdminSimplified;