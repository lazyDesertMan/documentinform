import React, {useEffect, useState } from "react";
import { Button, Card, Container, Form, Modal, Stack } from "react-bootstrap";
import {Group} from "../../models/Group";
import {UserData} from "../../models/UserData";
import { Position } from "../../models/Position";
import { ADMIN_OPTIONS_ROUTE } from "../../utils/consts";
import { useNavigate } from "react-router-dom";
import Select from "react-select";
import { GetListUsers } from "../../apiRequests/UserRequests";
import { GetListGroup, SetGroup, UpdateGroup } from "../../apiRequests/GroupRequests";
import { GetListPosition, SetPosition, UpdatePosition } from "../../apiRequests/PositionRequests";

class Data {
    users : UserData[];
    positions : Position[];
    groups : Group[];
}

function AddGroup() {
    const [data, setData] = useState<Data>({
        users : [], groups : [], positions : []
    })

    useEffect(() => {
        GetListUsers().then(users => GetListGroup().then(groups => GetListPosition().then(positions => setData({users : users, groups : groups, positions : positions}))));
    }, []);
    return <AddGroupCard users={data.users} groups={data.groups} positions={data.positions} />;
}

class Demonstration{
    nameParent : string;
    nameLeader : string;
    constructor(){
        this.nameParent = "parent";
        this.nameLeader = "name";
    }
}
var Demon : Demonstration;
class cardProps {
    users : UserData[];
    positions : Position[];
    groups : Group[];
}
class viewStates {
    show : boolean;
}

function AddGroupCard(props : cardProps) {
    const navigate = useNavigate();
    const [name, setName] = useState("");
    const [namePosLeader, setPosLeader] = useState("");
    const [idNameLeader, setNameLeader] = useState(-1);
    const [idParent, setParent] = useState(-1);
    let [state, setStates] = useState<viewStates>({ show : false});

    // Настройки
    const optionsParentGroup = props.groups.map(data => ({label: data.name, value: data.id}));
    optionsParentGroup.push({label: "Не выбрано", value: -1});
    const optionsUser = props.users.map(data => ({label: data.secondName + " " + data.firstName + " " + data.thirdName, value: data.id}));
    optionsUser.push({label: "Не выбрано", value: -1});

    useEffect(() => {
        Demon = new Demonstration();
    }, []);
    function handleClose() {
        setStates({show : false});
    }
    function handleShow(){
        setStates({show : true});
    }
    const setNameLeaderID=(id)=>{
        if(id !== -1)
        {
            setNameLeader(id);
            Demon.nameLeader = props.users.find(f => f.id === id).secondName + " " + props.users.find(f => f.id === id).firstName + " " + props.users.find(f => f.id === id).thirdName;
        }           
        else
            setNameLeader(-1)
    }
    const setParentID=(id)=>{
        if(id !== -1)
        {
            Demon.nameParent = props.groups.find(f => f.id === id).name;
            setParent(id);
        }
        else
            setParent(-1)
    }
    const B_AddGroup = async () =>{
        setStates({show : false});
        let parent: number = idParent
        if(idParent === -1){
            setParent(undefined);
            parent = undefined;
        }
        let idG: number = await SetGroup(parent, name);
        if(namePosLeader.length !== 0){
            let id : number = await SetPosition(idG, namePosLeader);
            if(idNameLeader !== -1){     
                await UpdatePosition(id, undefined, idNameLeader, undefined);
            }
            await UpdateGroup(idG, id, undefined, undefined);
        }
        navigate(ADMIN_OPTIONS_ROUTE);
    }
    return(
        <Container>
            <Stack direction="horizontal" gap={3} style={{ padding: '15px'}}>
                <div style={{width: '30vw'}}></div>
                <Card style={{width: 550, overflowX: 'hidden', overflowY: 'auto'}} className="p-5">
                    <h2 className="m-auto">Добавление группы</h2>
                    <br></br>
                    <Form style={{minHeight: '40vh'}}>
                        <Form.Group className="mb-3" controlId="controlInput_nameGroup">
                            <Form.Label>Название группы</Form.Label>
                            <Form.Control
                                placeholder="Введите название..." 
                                onChange={e=>setName(e.target.value)}/>
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="controlSelectGroupParent">
                            <Form.Label>Выберите группу-лидера</Form.Label>
                            <Select 
                                options={optionsParentGroup}
                                isSearchable={true}
                                onChange={data=>setParentID(data.value)}/>
                        </Form.Group>
                        {name.length !== 0?
                            <>
                                <Form.Group className="mb-3" controlId="controlInput_nameGroup">
                                    <Form.Label>Должность руководителя</Form.Label>
                                    <Form.Control
                                        placeholder="Введите название должности..." 
                                        onChange={e=>setPosLeader(e.target.value)}/>
                                </Form.Group>
                                {namePosLeader !== ""?
                                    <Form.Group className="mb-3" controlId="controlSelectUser">
                                    <Form.Label>Выберите сотрудника на должность руководителя</Form.Label>
                                    <Select 
                                        options={optionsUser}
                                        isSearchable={true}
                                        onChange={data=>setNameLeaderID(data.value)}/>
                                </Form.Group>
                                : <></>}
                                <br></br>
                                <div className="d-flex justify-content-center align-items-center">
                                    <Button variant="primary" onClick={handleShow}>
                                        Предпросмотр
                                    </Button>
                                </div>
                                <Modal show={state.show} fullscreen={true} onHide={handleClose}>
                                    <Modal.Header closeButton>
                                        <Modal.Title>Предпросмотр добавления новой группы</Modal.Title>
                                    </Modal.Header>
                                    <Modal.Body>
                                        <Container className="d-flex justify-content-center align-items-center">
                                        <h1 style={{color: 'gray', minWidth: '1vw', position: 'absolute', top: '0', backgroundColor: 'white', borderRadius: '5px', paddingLeft: '6px', paddingRight: '6px'}}>{name}</h1>
                                        <div style={{border: '2px solid gray',  margin: '1%', padding: '20px', width: '50%'}}>
                                            <Card>
                                                <Card.Body>
                                                    <Card.Title>Новая группа</Card.Title>
                                                    <Card.Text style={{overflow: 'auto'}}>
                                                    <label>Группа-родитель: {idParent === -1? <label>нет</label> : Demon.nameParent}</label><br></br>
                                                    <br></br>
                                                    <label>Должность руководителя: {namePosLeader.length === 0 ? <label>нет</label> : namePosLeader}</label><br></br>
                                                    <br></br>
                                                    <label>Руководитель: {idNameLeader === -1? <label>нет</label> : Demon.nameLeader}</label><br></br>
                                                    </Card.Text>
                                                </Card.Body>
                                            </Card>
                                        </div>
                                        </Container>
                                    </Modal.Body>
                                    <Modal.Footer>
                                        <Button variant="outline-primary" onClick={B_AddGroup}>
                                            Добавить
                                        </Button>
                                        <Button variant="secondary" onClick={handleClose}>
                                            Отменить
                                        </Button>
                                    </Modal.Footer>
                                </Modal>
                            </>
                        :<></>
                        }
                    </Form>
                </Card>
                <div style={{width: '30vw'}}></div>
            </Stack>
        </Container>
    );
}
export default AddGroup;