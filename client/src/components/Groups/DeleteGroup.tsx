import React, {useEffect, useState } from "react";
import { Button, Card, Container, Form, Modal, Stack } from "react-bootstrap";
import {Group} from "../../models/Group";
import { ADMIN_OPTIONS_ROUTE, ERROR_ROUTE } from "../../utils/consts";
import { useNavigate } from "react-router-dom";
import Select from "react-select";
import { GetListGroup, RemoveGroup } from "../../apiRequests/GroupRequests";

class Data {
    groups : Group[];
}

function DeleteGroup() {
    const [data, setData] = useState<Data>({
        groups : []
    })

    useEffect(() => {
        GetListGroup().then(groups => setData({groups : groups}));
    }, []);
    return <DeleteGroupCard groups={data.groups} />;
}
class cardProps {
    groups : Group[];
}

class viewStates {
    show : boolean;
}

function DeleteGroupCard(props : cardProps) {
    const navigate = useNavigate();
    const [name, setName] = useState("");
    const [idGroup, setGroup] = useState(-1);
    let [state, setStates] = useState<viewStates>({ show : false});

    // Настройки
    const optionsGroup = props.groups.map(data => ({label: data.name, value: data.id}));
    optionsGroup.push({label: "Не выбрано", value: -1});

    function handleClose() {
        setStates({show : false});
    }
    function handleShow(){
        setStates({show : true});
    }
    const setGroupID=async(id)=>{
        if(id !== -1)
        {
            setName(props.groups.find(f => f.id === id).name);
            setGroup(id);
        }           
        else
            setGroup(-1);
    }
    const B_DeleteGroup = async () =>{
        setStates({show : false});
        if(await RemoveGroup(idGroup))
            navigate(ADMIN_OPTIONS_ROUTE);
        else
            navigate(ERROR_ROUTE);
    }
    return(
        <Container>
            <Stack direction="horizontal" gap={3} style={{ padding: '15px'}}>
                <div style={{width: '30vw'}}></div>
                <Card style={{width: 550, overflowX: 'hidden', overflowY: 'auto'}} className="p-5">
                    <h2 className="m-auto">Удаление группы</h2>
                    <br></br>
                    <Form style={{minHeight: '30vh'}}>
                        <Form.Group className="mb-3" controlId="controlSelectGroup">
                            <Form.Label>Выберите группу</Form.Label>
                            <Select 
                                options={optionsGroup}
                                isSearchable={true}
                                onChange={data=>setGroupID(data.value)}/>
                        </Form.Group>
                        <br></br>
                        {idGroup !== -1?
                        <>
                            <div className="d-flex justify-content-center align-items-center">
                                <Button variant="primary" onClick={handleShow}>
                                    Предпросмотр
                                </Button>
                            </div>
                            <Modal show={state.show} fullscreen={true} onHide={handleClose}>
                                <Modal.Header closeButton>
                                    <Modal.Title>Предпросмотр удаления группы</Modal.Title>
                                </Modal.Header>
                                <Modal.Body>    
                                    <Container className="d-flex justify-content-center align-items-center">
                                        <h1 style={{color: 'gray', minWidth: '1vw', position: 'absolute', top: '0', backgroundColor: 'white', borderRadius: '5px', paddingLeft: '6px', paddingRight: '6px'}}>{name}</h1>
                                        <div style={{border: '2px solid gray',  margin: '1%', padding: '20px', width: '50%'}}>
                                            <Card>
                                                <Card.Body>
                                                    <Card.Title>Удаление группы</Card.Title>
                                                    <Card.Text style={{overflow: 'auto'}}>
                                                        <label>Группа: {name}</label><br></br>
                                                    </Card.Text>
                                                </Card.Body>
                                            </Card>
                                        </div>  
                                    </Container>
                                </Modal.Body>
                                <Modal.Footer>
                                    <Button variant="outline-primary" onClick={B_DeleteGroup}>
                                        Удалить
                                    </Button>
                                    <Button variant="secondary" onClick={handleClose}>
                                        Отменить
                                    </Button>
                                </Modal.Footer>
                            </Modal>
                        </>
                        :<></>}
                    </Form>
                </Card>
                <div style={{width: '30vw'}}></div>
            </Stack>
        </Container>
    );
}
export default DeleteGroup;