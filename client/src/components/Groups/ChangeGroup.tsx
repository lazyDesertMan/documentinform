import React , {useEffect, useState } from "react";
import { Button, Card, Container, Form, Modal, Stack } from "react-bootstrap";
import { useNavigate} from "react-router-dom";
import { UserData } from "../../models/UserData";
import {Group} from "../../models/Group";
import { ADMIN_OPTIONS_ROUTE } from "../../utils/consts";
import { Position } from "../../models/Position";
import Select from "react-select";
import { GetListGroup, UpdateGroup } from "../../apiRequests/GroupRequests";
import { GetListPosition, GetAllPositionsInGroup, UpdatePosition } from "../../apiRequests/PositionRequests";
import { GetListUsers } from "../../apiRequests/UserRequests";

class Data {
    users : UserData[];
    positions : Position[];
    groups : Group[];
}
function ChangeGroup() {
    const [data, setData] = useState<Data>({
        users : [], positions : [], groups : []
    })

    useEffect(() => {
        GetListUsers().then(users => GetListPosition().then(positions => 
            GetListGroup().then(groups => setData({users : users, positions : positions, groups : groups}))));
    }, []);
    return <ChangeGroupCard users={data.users} positions={data.positions} groups={data.groups} />;
}
function SearchPossibleParent(groups : Group[], id : number) : Group[]{
    const res : Group[] = [];
    for(let i = 0; i < groups.length; i++){
        if(groups[i].id !== id)
            res.push(groups[i]);
    }
    return res;
}

class Demonstration{
    nameParent : string;
    positionLeader : string;
    nameLeader : string;

    oldGroup : Group;
    
    constructor(){
        this.nameParent = "";
        this.positionLeader = "";
        this.nameLeader = "";
    }
}
var Demon : Demonstration;
class cardProps {
    users : UserData[];
    positions : Position[];
    groups : Group[];
}
class viewStates {
    show : boolean;
}

class Pair{
    label: string;
    value: number;
}
class ParentStates{
    idGroup: number;
    dataParent: Pair[];
    dataPos: Pair[];
}

function ChangeGroupCard(props : cardProps) {
    const navigate = useNavigate();
    const [name, setName] = useState("");
    const [idPosLeader, setPosLeader] = useState(-1);
    /// Безопасность для выбора должности лидера
    const [dataPosLeader, setDataPosLeader] = useState([]);

    const [idParent, setParent] = useState(-1);
    /// Безопасность для выбора группы-лидера
    const [dataParent, setDataParent] = useState([]);

    const [idNameLeader, setNameLeader] = useState(-1);
    const [idGroup, setGroup] = useState<ParentStates>({idGroup: -1, dataParent: [], dataPos: []});
    let [state, setStates] = useState<viewStates>({ show : false});
    
    // Настройки Select
    const optionsGroup = props.groups.map(data => ({label: data.name, value: data.id}));
    optionsGroup.push({label: "Не выбрано", value: -1});

    const optionsUser = props.users.map(data => ({label: data.secondName + " " + data.firstName + " " + data.thirdName, value: data.id}));
    optionsUser.push({label: "Не выбрано", value: -1});

    useEffect(() => {
        Demon = new Demonstration();
    }, []);
    function handleClose() {
        setStates({show : false});
    }
    function handleShow(){
        setStates({show : true});
    }
    const setGroupID=async(id)=>{
        if(id !== -1){
            Demon.oldGroup = props.groups.find(f => f.id === id);
            const optionsPossibleParent = SearchPossibleParent(props.groups, id).map(data => ({label: data.name, value: data.id}));
            optionsPossibleParent.push({label: "Не выбрано", value: -1});
            optionsPossibleParent.push({label: "Нет родителя", value: null});

            let tmp : Group[] = await GetAllPositionsInGroup(id);
            const optionsPos = tmp.map(data => ({label: data.name, value: data.id}));
            optionsPos.push({label: "Не выбрано", value: -1});

            setGroup({idGroup: id, dataParent: optionsPossibleParent, dataPos : optionsPos});
            setParentID(-1);
            setPosID(-1);
        }
        else
            setGroup({idGroup: -1, dataParent: [], dataPos: []});
    }
    
    const setParentID=(id)=>{
        if(id !== -1)
        {
            Demon.nameParent = idGroup.dataParent.find(f => f.value === id).label;
            setParent(id);
            let tmp : Pair[] = [];
            tmp.push({label : Demon.nameParent, value: id});
            setDataParent(tmp);
        }
        else{
            Demon.nameParent = "не изменено";
            setParent(-1);
            setDataParent([]);
        }
            
    }
    const setPosID=(id)=>{
        if(id !== -1)
        {
            setPosLeader(id);
            Demon.positionLeader = props.positions.find(f => f.id === id).name;
            let tmp : Pair[] = [];
            tmp.push({label : Demon.positionLeader, value: id});
            setDataPosLeader(tmp);
        }    
        else{
            Demon.positionLeader = "не изменено";
            setPosLeader(-1);
            setDataPosLeader([]);
        }
            
    }
    const setUserID=(id)=>{
        if(id !== -1)
        {
            setNameLeader(id);
            Demon.nameLeader = props.users.find(f => f.id === id).secondName + " " + props.users.find(f => f.id === id).firstName + " " + props.users.find(f => f.id === id).thirdName;
        }    
        else{
            Demon.nameLeader = "не изменено";
            setNameLeader(-1);
        }
            
    }
    const B_ChangeGroup = async () =>{
        setStates({show : false});
        let posLeader: number = idPosLeader;
        let parent: number = idParent;
        let nameGroup: string = name;
        if(idPosLeader === -1){
            setPosLeader(undefined);
            posLeader = undefined;
        }
        if(idParent === -1){
            setParent(undefined);
            parent = undefined;
        }
        if (name.length === 0){
            setName(undefined);
            nameGroup = undefined;
        }
        await UpdateGroup(idGroup.idGroup, posLeader, parent, nameGroup);
        if(idNameLeader !== -1){
            let idPos : number = posLeader;
            if(idPos === undefined){  
                idPos = props.groups.find(f => f.id === idGroup.idGroup).leaderPositionID;
                console.log(idPos);
            }
            await UpdatePosition(idPos, idGroup.idGroup, idNameLeader, undefined);
        }
        navigate(ADMIN_OPTIONS_ROUTE);
    }
    return(
        <Container>
            <Stack direction="horizontal" gap={3} style={{ padding: '15px'}}>
                <div style={{width: '30vw'}}></div>
                <Card style={{width: 550, overflowX: 'hidden', overflowY: 'auto'}} className="p-5">
                    <h2 className="m-auto">Изменение групп</h2>
                    <br></br>
                    <Form style={{minHeight: '40vh'}}>
                        <Form.Group className="mb-3" controlId="controlSelectGroup">
                            <Form.Label>Выберите редактируемую группу</Form.Label>
                            <Select 
                                options={optionsGroup}
                                isSearchable={true}
                                onChange={data=>setGroupID(data.value)}/>
                        </Form.Group>
                        {idGroup.idGroup !== -1 && idGroup.dataParent.length > 1?
                            <>
                                <Form.Group className="mb-3" controlId="controlInput_nameGroup">
                                    <Form.Label>Новое название группы</Form.Label>
                                    <Form.Control
                                        placeholder="Введите название..." 
                                        onChange={e=>setName(e.target.value)}/>
                                </Form.Group>
                                <Form.Group className="mb-3" controlId="controlSelectGroupParent">
                                    <Form.Label>Выберите группу-лидера</Form.Label>
                                    <Select
                                        value={dataParent}
                                        options={idGroup.dataParent}
                                        isSearchable={true}
                                        onChange={data=>setParentID(data.value)}/>
                                </Form.Group>
                                {idGroup.dataPos.length > 1?
                                <>
                                    <Form.Group className="mb-3" controlId="controlSelectPos">
                                        <Form.Label>Выберите должность руководителя</Form.Label>
                                        <Select 
                                            value={dataPosLeader}
                                            options={idGroup.dataPos}
                                            isSearchable={true}
                                            onChange={data=>setPosID(data.value)}/>
                                    </Form.Group>
                                    <Form.Group className="mb-3" controlId="controlSelectUser">
                                        <Form.Label>Выберите сотрудника на должность руководителя</Form.Label>
                                        <Select 
                                            options={optionsUser}
                                            isSearchable={true}
                                            onChange={data=>setUserID(data.value)}/>
                                    </Form.Group>
                                </>
                                : <></>}
                                <br></br>
                                {name !== "" || idParent !== -1 || idPosLeader !== -1 || idNameLeader !== -1?
                                <>
                                    <div className="d-flex justify-content-center align-items-center">
                                        <Button variant="primary" onClick={handleShow}>
                                            Предпросмотр
                                        </Button>
                                    </div>
                                    <Modal show={state.show} fullscreen={true} onHide={handleClose}>
                                        <Modal.Header closeButton>
                                            <Modal.Title>Предпросмотр изменений в группе [{Demon.oldGroup.name}]</Modal.Title>
                                        </Modal.Header>
                                        <Modal.Body>
                                            
                                            <Container>
                                                <div className="table-responsive">
                                                    <table style={{fontSize:'14px'}} className="table table-striped table-sm">
                                                        <thead>
                                                            <tr key="thead">
                                                                <th scope="col">#</th>
                                                                <th scope="col">Новая информация о группе</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr key="tbody1">
                                                                <th>Название</th>
                                                                <td>{name !== ""? name : <small>нет изменений</small>}</td>
                                                            </tr>
                                                            <tr key="tbody2">
                                                                <th>Группа-родитель</th>
                                                                <td>{idParent !== -1? Demon.nameParent : <small>нет изменений</small>}</td>
                                                            </tr>
                                                            <tr key="tbody4">
                                                                <th>Руководитель (должность)</th>
                                                                <td>{idPosLeader !== -1? <>{Demon.positionLeader}</> : <small>нет изменений</small>}</td>
                                                            </tr>
                                                            <tr key="tbody4">
                                                                <th>Руководитель (сотрудник)</th>
                                                                <td>{idNameLeader !== -1? <>{Demon.nameLeader}</> : <small>нет изменений</small>}</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </Container>
                                        </Modal.Body>
                                        <Modal.Footer>
                                            <Button variant="outline-primary" onClick={B_ChangeGroup}>
                                                Изменить
                                            </Button>
                                            <Button variant="secondary" onClick={handleClose}>
                                                Отменить
                                            </Button>
                                        </Modal.Footer>
                                    </Modal>
                                </>
                                :<></>}
                            </>
                        :<></>
                        }
                    </Form>
                </Card>
                <div style={{width: '30vw'}}></div>
            </Stack>
        </Container>
    );
}
export default ChangeGroup;