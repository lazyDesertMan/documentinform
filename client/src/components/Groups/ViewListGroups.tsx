import React, { useEffect, useState } from "react";
import { GetListGroup } from "../../apiRequests/GroupRequests";
import { Group } from "../../models/Group";

class loadStates {
    isFetching: boolean;
    groups: Group[];
}

function LoadGroups_Comp() {
    let [state, setState] = useState<loadStates>({ isFetching: false, groups: [] });

    function loadData() {
        setState({ groups: state.groups, isFetching: true });
        GetListGroup().then(r => {
            setState({ groups: r, isFetching: false });
        }).catch(e => {
            console.log(e);
            setState({ groups: this.state, isFetching: true });
        });
    }

    useEffect(() => loadData(), []);

    return (
        <div>
            {this.state.isFetching ? "Loading..." : <ViewGroups list={this.state.groups} />}
        </div>
    );
}

class viewProps {
    list: Group[];
}

function ViewGroups(props: viewProps) {
    return (
        <pre>
            {JSON.stringify(props.list, null, 3)}
        </pre>
    );
}

export default LoadGroups_Comp;
