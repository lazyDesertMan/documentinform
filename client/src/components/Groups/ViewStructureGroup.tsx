import React, { useEffect, useState } from "react";
import { Button, Form, Badge, OverlayTrigger, Popover } from "react-bootstrap";
import { GroupHierarchy } from "../../models/Group";
import { Position } from "../../models/Position";
import "../../css/style.css";
import { UserData } from "../../models/UserData";
import { GetStructureGroup } from "../../apiRequests/GroupRequests";
import { GetListPosition } from "../../apiRequests/PositionRequests";
import { GetListUsers } from "../../apiRequests/UserRequests";

class Data {
    positions : Position[];
    users : UserData[];
    groups : GroupHierarchy[];
}

function LoadStructureGroups_Comp() {

    const [data, setData] = useState<Data>({
        positions : [], groups : [], users : []
    })
    useEffect(() => {
        GetStructureGroup().then(groups => {
            GetListPosition().then(positions => GetListUsers().then(users => setData({users : users, positions : positions, groups : groups})))
            console.log(JSON.stringify(groups));
        });
    }, []);
    return <ViewStructureGroups list={data.groups} positions ={data.positions} users ={data.users}/>;
}

class viewProps {
    list: GroupHierarchy[];
    users : UserData[];
    positions : Position[];
}

var Pos : Position[] = [];
var Users : UserData[] = [];
function ViewStructureGroups(props: viewProps) {
    Pos = props.positions;
    Users = props.users;
    return (
        <ul className="ulGroup">{ props.list.map(c => ParseTree(c)) }</ul>
    );
}
function ParseTree (gs: GroupHierarchy) {
    return (
        <div>
            <Form style={{marginTop: '1%'}}>
                <div className="list-group list-group-flush">
                    <a id="item" key={gs.group.id} className="list-group-item list-group-item-action py-3 lh-tight">
                        
                        <li>
                            <OverlayTrigger
                                trigger="click"
                                key="right"
                                placement="right"
                                overlay={
                                    <Popover id="popover_1" style={{minWidth: '50%'}}>
                                    <Popover.Header as="h3">Сотрудники группы</Popover.Header>
                                    <Popover.Body>
                                        <Form style={{overflow: "hidden"}}>
                                            {Pos.map(dataPos =>
                                                dataPos.groupID === gs.group.id?
                                                    <Form.Group className="mb-3" controlId="controlInput_nameGroup">
                                                            <Badge
                                                                bg="success"
                                                                className="mb-1"
                                                            >
                                                                {dataPos.name}:&nbsp;
                                                            </Badge><br></br>
                                                            <Badge
                                                                bg="info"
                                                                className="mb-1"
                                                            >
                                                                {Users.map(dataUsers =>
                                                                    dataUsers.id === dataPos.workerID?
                                                                        <>{dataUsers.secondName + " " + dataUsers.firstName + " " + dataUsers.thirdName}</>
                                                                    :<></>)}
                                                            </Badge>
                                                            
                                                    </Form.Group>
                                                :<></>
                                            )}
                                        </Form>
                                    </Popover.Body>
                                    </Popover>
                                }
                            >
                                <Badge as={Button} bg="primary"  className="mb-1">{
                                gs.group.name}</Badge>
                            </OverlayTrigger>
                        </li>
                        <div style={{borderLeftStyle: 'outset'}}>
                            {
                                gs.subgroups.length > 0 ?
                                    <ul className="ulGroup">{ gs.subgroups.map((c: any) => ParseTree(c))}</ul>
                                : ""
                            }
                        </div>
                    </a>
                </div>
            </Form>
        </div> 
    );
}
export default LoadStructureGroups_Comp;