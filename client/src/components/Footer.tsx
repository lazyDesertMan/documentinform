import React from "react";
import "../css/footer-styles.css";

const Footer = () => {
    return(
        <footer className="footer">
            <section className="d-flex justify-content-center justify-content-lg-between p-4 border-bottom"></section>
            <div className="text-center p-4">
                © 2022 Copyright:
                <a className="text-reset fw-bold" href="https://gitlab.com/lazyDesertMan/documentinform/-/tree/dev"> document_inform</a>
            </div>
        </footer>
    );
};
export default Footer;