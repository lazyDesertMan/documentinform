import React, { useContext } from "react";
import { Container, Nav, Navbar, Button, Offcanvas } from "react-bootstrap";
import { Context } from "..";
import { PROFILE_ROUTE , DOCUMENT_ROUTE, HOME_ROUTE, REGISTRATION_ROUTE, TASKS_ROUTE, DIRREPORT_ROUTE, DIROPTIONS_ROUTE, ADMIN_OPTIONS_ROUTE, LISTTASKFORREPORT_ROUTE, ADMIN_DEPUTYOPTS_ROUTE, ACCESS_TOKEN_COOKIE, MYGROUP_ROUTE, FEEDBACK_ROUTE } from "../utils/consts";
import '../css/style.css';
import { useNavigate } from "react-router-dom";
import Cookies from 'js-cookie'
import {Roles} from "../models/UserData";

const NavBar = () => {
    var {role} = useContext(Context);
    const navigate = useNavigate();
    const Exit = ()=>{
        Cookies.remove(ACCESS_TOKEN_COOKIE);
        role = null;
        navigate(HOME_ROUTE);
        document.location.reload();
    }
    
    return(
        <div className="pages">
        <meta name="viewport" content="width=device-width, initial-scale=1"></meta>
            {role === Roles.ROLE_DIRECTOR ?
                <Navbar bg="light" expand={false}>
                    <Container fluid>
                        <Navbar.Brand href={HOME_ROUTE} className="home">Главная</Navbar.Brand>
                        <Navbar.Toggle aria-controls="offcanvasNavbar" />
                        <Navbar.Offcanvas
                            id="offcanvasNavbar"
                            aria-labelledby="offcanvasNavbarLabel"
                            placement="end"
                        >
                            <Offcanvas.Header closeButton>
                                <Offcanvas.Title id="offcanvasNavbarLabel">Меню</Offcanvas.Title>
                            </Offcanvas.Header>
                            <Offcanvas.Body>
                                <Nav className="justify-content-end flex-grow-1 pe-3">
                                    <Button 
                                        variant={"outline-dark"} 
                                        className="mt-2"
                                        onClick={() => navigate(PROFILE_ROUTE)}
                                    >
                                        Профиль
                                    </Button>
                                    <Button 
                                        variant={"outline-dark"} 
                                        className="mt-2"
                                        onClick={() => navigate(DIROPTIONS_ROUTE)}
                                    >
                                        Документы
                                    </Button>
                                    <Button 
                                        variant={"outline-dark"} 
                                        className="mt-2"
                                        onClick={() => navigate(DIRREPORT_ROUTE)}
                                    >
                                        Отчет
                                    </Button>
                                    <Button 
                                        variant={"outline-dark"} 
                                        className="mt-2"
                                        onClick={() => navigate(FEEDBACK_ROUTE)}
                                    >
                                        Обратная связь
                                    </Button>
                                    <Button 
                                        variant={"outline-dark"} 
                                        className="mt-2"
                                        onClick={() => Exit()}
                                    >
                                        Выход
                                    </Button>
                                </Nav>
                            </Offcanvas.Body>
                        </Navbar.Offcanvas>
                    </Container>
                </Navbar>
            : role === Roles.ROLE_ADMIN ?
                <Navbar bg="light" expand={false}>
                    <Container fluid>
                        <Navbar.Brand href={HOME_ROUTE} className="home">Главная</Navbar.Brand>
                        <Navbar.Toggle aria-controls="offcanvasNavbar" />
                        <Navbar.Offcanvas
                            id="offcanvasNavbar"
                            aria-labelledby="offcanvasNavbarLabel"
                            placement="end"
                        >
                            <Offcanvas.Header closeButton>
                                <Offcanvas.Title id="offcanvasNavbarLabel">Меню</Offcanvas.Title>
                            </Offcanvas.Header>
                            <Offcanvas.Body>
                                <Nav className="justify-content-end flex-grow-1 pe-3">
                                    <Button 
                                        variant={"outline-dark"} 
                                        className="mt-2"
                                        onClick={() => navigate(PROFILE_ROUTE)}
                                    >
                                        Профиль
                                    </Button>
                                    <Button 
                                        variant={"outline-dark"} 
                                        className="mt-2"
                                        onClick={() => navigate(REGISTRATION_ROUTE)}
                                    >
                                        Регистрация
                                    </Button>
                                    <Button 
                                        variant={"outline-dark"} 
                                        className="mt-2"
                                        onClick={() => navigate(ADMIN_OPTIONS_ROUTE)}
                                    >
                                        Справочники
                                    </Button>
                                    <Button 
                                        variant={"outline-dark"} 
                                        className="mt-2"
                                        onClick={() => navigate(ADMIN_DEPUTYOPTS_ROUTE)}
                                    >
                                        Заместители
                                    </Button>
                                    <Button 
                                        variant={"outline-dark"} 
                                        className="mt-2"
                                        onClick={() => navigate(FEEDBACK_ROUTE)}
                                    >
                                        Обратная связь
                                    </Button>
                                    <Button 
                                        variant={"outline-dark"} 
                                        className="mt-2"
                                        onClick={() => Exit()}
                                    >
                                        Выход
                                    </Button>
                                </Nav>
                            </Offcanvas.Body>
                        </Navbar.Offcanvas>
                    </Container>
                </Navbar>
            :role === Roles.ROLE_WORKER ?
                <Navbar bg="light" expand={false}>
                    <Container fluid>
                        <Navbar.Brand href={HOME_ROUTE} className="home">Главная</Navbar.Brand>
                        <Navbar.Toggle aria-controls="offcanvasNavbar" />
                        <Navbar.Offcanvas
                            id="offcanvasNavbar"
                            aria-labelledby="offcanvasNavbarLabel"
                            placement="end"
                        >
                            <Offcanvas.Header closeButton>
                                <Offcanvas.Title id="offcanvasNavbarLabel">Меню</Offcanvas.Title>
                            </Offcanvas.Header>
                            <Offcanvas.Body>
                                <Nav className="justify-content-end flex-grow-1 pe-3">
                                    <Button 
                                        variant={"outline-dark"} 
                                        className="mt-2"
                                        onClick={() => navigate(PROFILE_ROUTE)}
                                    >
                                        Профиль
                                    </Button>
                                    <Button 
                                        variant={"outline-dark"} 
                                        className="mt-2"
                                        onClick={() => navigate(TASKS_ROUTE)}
                                    >
                                        Задачи
                                    </Button>
                                    <Button 
                                        variant={"outline-dark"} 
                                        className="mt-2"
                                        onClick={() => navigate(MYGROUP_ROUTE)}
                                    >
                                        Моя группа
                                    </Button>
                                    <Button 
                                        variant={"outline-dark"} 
                                        className="mt-2"
                                        onClick={() => navigate(DOCUMENT_ROUTE)}
                                    >
                                        Файлы
                                    </Button>
                                    <Button 
                                        variant={"outline-dark"} 
                                        className="mt-2"
                                        onClick={() => navigate(LISTTASKFORREPORT_ROUTE)}
                                    >
                                        Отчет
                                    </Button>
                                    <Button 
                                        variant={"outline-dark"} 
                                        className="mt-2"
                                        onClick={() => navigate(FEEDBACK_ROUTE)}
                                    >
                                        Обратная связь
                                    </Button>
                                    <Button 
                                        variant={"outline-dark"} 
                                        className="mt-2"
                                        onClick={() => Exit()}
                                    >
                                        Выход
                                    </Button>
                                </Nav>
                            </Offcanvas.Body>
                        </Navbar.Offcanvas>
                    </Container>
                </Navbar>
            :
                <Navbar bg="light" expand={false}>
                    <Container>
                        <Navbar.Brand href={HOME_ROUTE} className="home2">Главная</Navbar.Brand>
                        <Nav style={{color: 'white'}}></Nav>
                    </Container>
                </Navbar>
            }
        </div>   
    );
};
export default NavBar;