import React from "react";
import { Button, Card, Container, Form } from "react-bootstrap";
import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { DIROPTIONS_ROUTE } from "../../utils/consts";
import { AddDocument, SetFile } from "../../apiRequests/DocumentRequests";

// для передачи на сервер
const formData = new FormData();
let files = [];

const AddDoc = () => {

    const navigate = useNavigate();
    const [fileName, setFileName] = useState("");
    const [description, setDescription] = useState("");
    const [info, setInfo] = useState("");
    const [drag, setDrag] = useState(false);
    const [startDate, setStartDate] = useState("");

    const DragStartHandler = (e)=>{
        e.preventDefault()
        setDrag(true);
    }
    const DragLeaveHandler = (e)=>{
        e.preventDefault()
        setDrag(false);
    }
    const onDropHandler = (e) =>{
        e.preventDefault();
        files = [...e.dataTransfer.files]
        formData.append('file', files[0]);
        setInfo(files[0].name + " " + files[0].size/1000 + "KB");
        setDrag(false);
    }
    
    const loadFile = (e) => {
        e.preventDefault();
        files = [...e.target.files];
        formData.append('file', files[0]);
        setInfo(files[0].name + " " + files[0].size/1000 + "KB");
    }
    const B_AddDocument = async () =>{
        if (files && files[0]) {
            const docID = await AddDocument(fileName, description, startDate, undefined);
            console.log("docID: " + docID + ", filename: " + fileName + ", descr: " + description + ", startD: " + startDate);
            await SetFile(docID, files[0]);
            navigate(DIROPTIONS_ROUTE);
        }
    }
    return(
        <Container 
            className="d-flex justify-content-center align-items-center"
        >
            <Card style={{width: 550, overflowX: 'hidden', overflowY: 'auto'}} className="p-5">
                <h2 className="m-auto">Новый документ</h2>
                <br></br>
                <Form>
                    <Form.Group className="mb-3" controlId="controlInput_nameDoc">
                        <Form.Label>Название документа</Form.Label>
                        <Form.Control 
                            placeholder="Введите название..."
                            onChange={e => setFileName(e.target.value)} 
                            value={fileName}/>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="controlInput_Description">
                        <Form.Label>Информация о документе</Form.Label>
                        <Form.Control 
                            as="textarea" rows={3}
                            placeholder="Введите описание"
                            onChange={e => setDescription(e.target.value)} 
                            value={description}/>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="controlInput_StartDate">
                        <Form.Label>Дата вступления в силу</Form.Label>
                        <Form.Control 
                            type="date"
                            onChange={e=>setStartDate(e.target.value)} 
                            value={startDate}/>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="controlInput_Load">
                        <div style={{marginTop: '3%'}} className="d-flex justify-content-center align-items-center">
                            <div className="frame">
                                {drag
                                    ? <div className="drop-area-after"
                                        onDragStart={e => DragStartHandler(e)}
                                        onDragLeave={e => DragLeaveHandler(e)}
                                        onDragOver={e => DragStartHandler(e)}
                                        onDrop={e => onDropHandler(e)}
                                        >Отпустите файл</div>
                                    : <div className="drop-area-before"
                                        onDragStart={e => DragStartHandler(e)}
                                        onDragLeave={e => DragLeaveHandler(e)}
                                        onDragOver={e => DragStartHandler(e)}
                                        >Область загрузки</div>}
                            </div>
                            <Form.Control 
                                type="file"
                                onChange={e => loadFile(e)} />
                        </div>
                    </Form.Group> 
                    <Form.Group className="mb-3" controlId="controlOutput_Info">
                        <Form.Label>Подробная информация о документе</Form.Label>
                        <Form.Control
                            placeholder={info} 
                            disabled={true}/>
                    </Form.Group> 
                    {fileName.trim().length > 0 && startDate.trim().length > 0 && files.length > 0?
                        <div className="d-flex justify-content-center align-items-center">
                        <Button 
                            className="mt-3 align-self-center" 
                            style={{width: 160}} 
                            variant="outline-primary"
                            onClick={B_AddDocument}
                            >Внести</Button>
                        </div>
                    :<></>} 
                </Form>
            </Card>
        </Container>
    );
};
export default AddDoc;