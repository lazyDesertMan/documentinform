import React, { useEffect } from "react";
import { Button, Card, Container, Form, Modal, Stack } from "react-bootstrap";
import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { DIROPTIONS_ROUTE } from "../../utils/consts";
import Document from "../../models/Document";
import Select from "react-select";
import { GetListDocuments, SetFile, UpdateDoc } from "../../apiRequests/DocumentRequests";

// для передачи на сервер
const formData = new FormData();
let files = [];
class Data {
    documents : Document[];
}
function ChangeVersionDoc() {
    const [data, setData] = useState<Data>({
        documents : []
    })

    useEffect(() => {
        GetListDocuments().then(documents => setData({documents : documents}));
    }, []);
    
    return <ChangeVersionDocCard documents={data.documents} />;
}

class cardProps {
    documents : Document[];
}
class Demonstration{
    oldNameDoc : string;
    oldStartDate : string;
    oldDescription : string;

    oldVersionNameDoc : string;

    constructor(){
        this.oldNameDoc = "";
        this.oldVersionNameDoc = "";
        this.oldStartDate = "";
        this.oldDescription = "";
    }
}
var Demon : Demonstration;
class viewStates {
    show : boolean;
}

class Pair{
    label: string;
    value: number;
}
class DocStates{
    idDoc: number;
    data: Pair[];
}

const ChangeVersionDocCard = (props : cardProps) => {
    const navigate = useNavigate();
    const [fileName, setFileName] = useState("");
    const [description, setDescription] = useState("");
    const [info, setInfo] = useState("");
    const [drag, setDrag] = useState(false);
    const [startDate, setStartDate] = useState("");
    const [idDoc, setDoc] = useState<DocStates>({idDoc : -1, data : []});
    const [idOldVersionDoc, setOldVersionDoc] = useState(-1);
    let [state, setStates] = useState<viewStates>({ show : false});
    // Настройки
    const options = props.documents.map(data => ({label: data.name, value: data.id}));
    options.push({label: "Не выбрано", value: -1});

    useEffect(() => {
        Demon = new Demonstration();
    }, []);
    function handleClose() {
        setStates({show : false});
    }
    function handleShow(){
        setStates({show : true});
    }

    const WithoutCurDoc = (curDoc : number) =>{
        let res : Document[] = [];
        for(let i : number = 0; i < props.documents.length; i++){
            if(props.documents[i].id !== curDoc){
                res.push(props.documents[i]);
            }
        }
        return res;
    }
    const setDocID = (id)=>{
        if(id !== -1){
            Demon.oldNameDoc = props.documents.find(f => f.id === id).name;
            Demon.oldStartDate = new Date(props.documents.find(f => f.id === id).effectiveDate).toLocaleString();
            Demon.oldDescription = props.documents.find(f => f.id === id).description;
            let tmp : Document[] = WithoutCurDoc(id);
            const optionsOldVersion = tmp.map(data => ({label: data.name, value: data.id}));
            optionsOldVersion.push({label: "Не выбрано", value: -1});
            setDoc({idDoc : id, data : optionsOldVersion});
            setOldDocID(-1);
        }else{
            setDoc({idDoc : -1, data : []});
        }
    }
    const setOldDocID = (id)=>{
        if(id !== -1){
            Demon.oldVersionNameDoc = props.documents.find(f => f.id === id).name;
            setOldVersionDoc(id);
        }else{
            Demon.oldVersionNameDoc = "не изменено";
            setOldVersionDoc(-1);
        }
    }
    const DragStartHandler = (e)=>{
        e.preventDefault()
        setDrag(true);
    }
    const DragLeaveHandler = (e)=>{
        e.preventDefault()
        setDrag(false);
    }
    const onDropHandler = (e) =>{
        e.preventDefault();
        files = [...e.dataTransfer.files]
        /*for(let i = 0; i < files.length; i++){
            formData.append('file', files[i]);
            name += files[i].name + " ";
        }*/
        formData.append('file', files[0]);
        setInfo(files[0].name + " " + files[0].size/1000 + "KB");
        setDrag(false);
    }


    const loadFile = (e) => {
        e.preventDefault();
        files = [...e.target.files];
        formData.append('file', files[0]);
        setInfo(files[0].name + " " + files[0].size/1000 + "KB");
    }
    const B_ChangeDocument = async () =>{
        let desc: string = null;
        let startD: string = null;
        let fname: string = null;
        let oldDoc: number = null;
        if(description.trim().length > 0)
            desc = description;
        if(startDate.trim().length > 0)
            startD = startDate;
        if(fileName.trim().length > 0)
            fname = fileName;
        if(idOldVersionDoc !== -1){
            oldDoc = idOldVersionDoc;
        }
        if(files && files[0]){
            await SetFile(idDoc.idDoc, files[0]);
        }
        await UpdateDoc(idDoc.idDoc, fname, desc, startD, oldDoc)
        navigate(DIROPTIONS_ROUTE);
    }
    return(
        <Container>
            <Stack direction="horizontal" gap={3} style={{ padding: '15px'}}>
                <div style={{width: '30vw'}}></div>
                <Card style={{width: 570, overflowX: 'hidden', overflowY: 'auto'}} className="p-5">
                    <h2 className="m-auto">Изменение документа</h2>
                    <br></br>
                    <Form style={{minHeight: '30vh'}}>
                        <Form.Group className="mb-3" controlId="controlSelectDoc">
                            <Form.Label>Выберите редактируемый документ</Form.Label>
                            <Select 
                                options={options}
                                isSearchable={true}
                                onChange={data=>setDocID(data.value)}/>
                        </Form.Group>
                        {idDoc.idDoc !== -1?
                        <>
                            <Form.Group className="mb-3" controlId="controlInput_NameDoc">
                                <Form.Label>Название версии документа</Form.Label>
                                <Form.Control
                                    placeholder="Введите название..."
                                    onChange={e => setFileName(e.target.value)} />
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="controlInput_Description">
                                <Form.Label>Описание новой версии</Form.Label>
                                <Form.Control
                                    placeholder="Введите описание..."
                                    onChange={e => setDescription(e.target.value)} />
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="controlInput_StartDate">
                                <Form.Label>Дата вступления в силу</Form.Label>
                                <Form.Control
                                    type="date" 
                                    onChange={e=>setStartDate(e.target.value)} />
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="controlInput_Load">
                                <div style={{marginTop: '3%'}} className="d-flex justify-content-center align-items-center">
                                    <div className="frame">
                                        {drag
                                            ? <div className="drop-area-after"
                                                onDragStart={e => DragStartHandler(e)}
                                                onDragLeave={e => DragLeaveHandler(e)}
                                                onDragOver={e => DragStartHandler(e)}
                                                onDrop={e => onDropHandler(e)}
                                                >Отпустите файл</div>
                                            : <div className="drop-area-before"
                                                onDragStart={e => DragStartHandler(e)}
                                                onDragLeave={e => DragLeaveHandler(e)}
                                                onDragOver={e => DragStartHandler(e)}
                                                >Область загрузки</div>}
                                    </div>
                                    <Form.Control
                                        type="file"
                                        onChange={e => loadFile(e)} />
                                </div>
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="controlSelectDoc">
                                <Form.Label>Выберите старую версию документа</Form.Label>
                                <Select 
                                    options={idDoc.data}
                                    isSearchable={true}
                                    onChange={data=>setOldDocID(data.value)}/>
                            </Form.Group>
                            <Form.Group className="mb-3" style={{marginLeft: '10px'}} controlId="controlOutput_Info">
                                <Form.Label>Подробная информация о документе</Form.Label>
                                <Form.Control
                                    id="info"
                                    placeholder={info} 
                                    disabled={true}/>
                            </Form.Group> 
                            <br></br>
                            {fileName.trim().length > 0 || startDate.trim().length > 0 || files.length > 0 || description.trim().length > 0 || idOldVersionDoc !== -1?
                                <>
                                    <div className="d-flex justify-content-center align-items-center">
                                        <Button variant="primary" onClick={handleShow}>
                                            Предпросмотр
                                        </Button>
                                    </div>
                                    <Modal show={state.show} fullscreen={true} onHide={handleClose}>
                                        <Modal.Header closeButton>
                                            <Modal.Title>Предпросмотр изменения версии документа <b>{Demon.oldNameDoc}</b></Modal.Title>
                                        </Modal.Header>
                                        <Modal.Body>        
                                            <Container>
                                                <div className="table-responsive">
                                                    <table style={{fontSize:'14px'}} className="table table-striped table-sm">
                                                        <thead>
                                                            <tr key="thead">
                                                                <th scope="col"></th>
                                                                <th scope="col">Старая версия документа</th>
                                                                <th scope="col">Новая версия документа</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr key="tbody1">
                                                                <th>Название</th>
                                                                <td>{Demon.oldNameDoc}</td>
                                                                <td>{fileName.trim().length > 0 ? <>{fileName}</> : <small>нет изменений</small>}</td>
                                                            </tr>
                                                            <tr key="tbody2">
                                                                <th>Дата вступления в силу</th>
                                                                <td>{Demon.oldStartDate}</td>
                                                                <td>{startDate.trim().length > 0 ? <>{startDate}</> : <small>нет изменений</small>}</td>
                                                            </tr>
                                                            <tr key="tbody3">
                                                                <th>Описание</th>
                                                                <td>{Demon.oldDescription.trim().length > 0 ? <>{Demon.oldDescription}</> : <>-</>}</td>
                                                                <td>{description.trim().length > 0 ? <>{description}</> : <small>нет изменений</small>}</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <label style={{color: 'red'}}>Примечание (информация о загружаемом документе): </label><> {info.trim().length > 0 ? <>{info}</> : <small>нет</small>}</><br></br>
                                                    <label style={{color: 'red'}}>Старая версия документа: </label><> {idOldVersionDoc !== -1? <>{Demon.oldVersionNameDoc}</> : <small>не выбрано</small>}</><br></br>
                                                </div>
                                                
                                            </Container>
                                        </Modal.Body>
                                        <Modal.Footer>
                                            <Button variant="outline-primary" onClick={B_ChangeDocument}>
                                                Изменить
                                            </Button>
                                            <Button variant="secondary" onClick={handleClose}>
                                                Отменить
                                            </Button>
                                        </Modal.Footer>
                                    </Modal>
                                </>
                            :<></>}
                        </>
                        :<></>}
                    </Form>
                </Card>
                <div style={{width: '30vw'}}></div>
            </Stack>
        </Container>
    );
};
export default ChangeVersionDoc;