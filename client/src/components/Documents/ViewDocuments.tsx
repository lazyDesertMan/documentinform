import React, { useEffect, useState, useMemo } from "react";
import { Form, Stack } from "react-bootstrap";
import { GetDocuments } from "../../apiRequests/DocumentRequests";
import Document from "../../models/Document";

class loadStates {
    isFetching: boolean;
    documents: Document[];
}

function LoadDocuments_Comp() {
    let [state, setState] = useState<loadStates>({ documents: [], isFetching: false });

    function loadData() {
        setState({ documents: state.documents, isFetching: true });
        GetDocuments().then(r => {
            setState({ documents: r, isFetching: false });
        }).catch(e => {
            console.log(e);
            setState({ documents: state.documents, isFetching: true });
        });
    }

    useEffect(() => loadData(), []);

    return (
        <div>
            {state.isFetching ? "Loading..." : <ViewDocuments list={state.documents} />}
        </div>
    );
}

class viewProps {
    list: Document[];
}

function ViewDocuments(props: viewProps) {
    const [search, setSearch] = useState("");

    const filteredDocs = useMemo(() => {
        if (search) {
          return props.list.filter(
            (item) =>
              item.name
                .toLowerCase()
                .indexOf(search.toLocaleLowerCase()) > -1
          );
        }
        return props.list;
      }, [search]);
    return (
        <>
            <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
                <h1>Файлы</h1>
            </div>
            <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
                <Stack direction="horizontal" gap={3} style={{ width: '60%', marginTop: '1%' }}>
                    <Form.Control
                        id="myInput"
                        className="me-auto"
                        value={search} 
                        onChange={e => setSearch(e.target.value)}
                        placeholder="Найти документ..." />
                </Stack>
            </div>
            <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
                <Form style={{ width: '60%', marginTop: '1%' }}>
                    <div className="list-group list-group-flush border-bottom scrollarea">
                        {filteredDocs.length > 0?
                            <>
                            {filteredDocs && filteredDocs.map(item =>
                                <a id="item" key={item.id} href={process.env.REACT_APP_CLIENT_URL + "/pdf/" + item.id} className="list-group-item list-group-item-action py-3 lh-tight" style={{borderRadius: '15px', backgroundColor: 'whitesmoke', marginTop: '10px'}}>
                                    <div className="d-flex w-100 align-items-center justify-content-between">
                                        <strong className="mb-1">{item.name}</strong>
                                        <small>{new Date(item.effectiveDate).toLocaleString()}</small>
                                    </div>
                                    <div className="col-10 mb-1 small">{item.description}</div>
                                </a>
                            )}
                            </>
                        :
                            <>
                            {Array.from({ length: props.list.length }).map((_, idx) => (
                                <a id="item" key={props.list[idx].id} href={process.env.REACT_APP_CLIENT_URL + "/pdf/" + props.list[idx].id} className="list-group-item list-group-item-action py-3 lh-tight" style={{borderRadius: '15px', backgroundColor: 'whitesmoke', marginTop: '5px'}}>
                                    <div className="d-flex w-100 align-items-center justify-content-between">
                                        <strong className="mb-1">{props.list[idx].name}</strong>
                                        <small>{new Date(props.list[idx].effectiveDate).toLocaleString()}</small>
                                    </div>
                                    <div className="col-10 mb-1 small">{props.list[idx].description}</div>
                                </a>
                            ))}
                            </>
                        }
                    </div>
                </Form>
            </div>
        </>
    );
}
export default LoadDocuments_Comp;
