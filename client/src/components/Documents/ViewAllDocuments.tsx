import React, { useEffect, useState, useMemo } from "react";
import { Form, Stack } from "react-bootstrap";
import { GetListDocuments } from "../../apiRequests/DocumentRequests";
import Document from "../../models/Document";

class loadStates {
    documents: Document[];
}

function LoadAllDocuments_Comp() {
    const [docs, setDocument] = useState<loadStates>({ documents: [] });
    
    useEffect(() => {
        GetListDocuments().then(data =>
            setDocument({documents : data}));
    }, []);

    return <ViewAllDocuments list={docs.documents} />
}

class viewProps {
    list: Document[];
}
let length: number;
function ViewAllDocuments(props: viewProps) {
    const [res, setRes] = useState<loadStates>({ documents: [] });
    const [fetching, setFetching] = useState(0);
    const [startRange, setStartR] = useState(0);
    const [endRange, setEndR] = useState(5);
    useEffect(() =>{
        length = 0;
    }, []);
    useEffect(() => { 
        if(fetching >= 0){
            let tmp : Document[] = [];
            if(props.list.length > 0){
                for(let i : number = startRange; i < endRange; i++){
                    tmp.push(props.list[i]);
                    length++;
                }
                setRes({documents : [...res.documents, ...tmp]});
                setStartR(prevVal => prevVal + 5);
                if(endRange + 5 > props.list.length){
                    setEndR(props.list.length);
                }else
                    setEndR(prevVal => prevVal + 5);
            }
        }
    }, [fetching, props.list]);

    useEffect(() => {
        if(length === 0 && 5 > props.list.length && props.list.length !== 0){
            setEndR(props.list.length);
        }
        if(props.list.length === 0)
            setFetching(-1);
        else
            setFetching(prevVal => prevVal + 1);
        document.addEventListener('scroll', scrollHandler);
        return function (){
            document.removeEventListener('scroll', scrollHandler);
        }
    }, [props.list]);

    const scrollHandler = (e) => {
        if(e.target.documentElement.scrollHeight - (e.target.documentElement.scrollTop + window.innerHeight) < 100
            && length < props.list.length){
            setFetching(prevVal => prevVal + 1);
        }else if(length === props.list.length && length !== 0){
            setFetching(-1);
        }
    }
    
    const [search, setSearch] = useState("");

    const filteredDocs = useMemo(() => {
        if (search) {
          return props.list.filter(
            (item) =>
              item.name
                .toLowerCase()
                .indexOf(search.toLocaleLowerCase()) > -1
          );
        }
        return res.documents;
      }, [search]);
    return (
        <>
            <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
                <Stack direction="horizontal" gap={3} style={{ width: '60%', marginTop: '1%' }}>
                    <Form.Control
                        id="myInput"
                        className="me-auto"
                        value={search} 
                        onChange={e => setSearch(e.target.value)}
                        placeholder="Найти документ..." />
                </Stack>
            </div>
            <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
                <Form style={{ width: '100%', marginTop: '1%' }}>
                    <div className="list-group list-group-flush border-bottom scrollarea">
                        {filteredDocs.length > 0?
                            <>
                            {filteredDocs && filteredDocs.map(item =>
                                <a id="item" key={item.id} href={process.env.REACT_APP_CLIENT_URL + "/dirreport/" + item.id} className="list-group-item list-group-item-action py-3 lh-tight" style={{borderRadius: '15px', backgroundColor: 'whitesmoke', marginTop: '10px'}}>
                                    <div className="d-flex w-100 align-items-center justify-content-between">
                                        <strong className="mb-1">{item.name}</strong>
                                        <small>{new Date(item.effectiveDate).toLocaleDateString()}</small>
                                    </div>
                                    <div className="col-10 mb-1 small">{item.description}</div>
                                </a>
                            )}
                            </>
                        :
                            <>
                            {Array.from({ length: res.documents.length }).map((_, idx) => (
                                <a id="item" key={res.documents[idx].id} href={process.env.REACT_APP_CLIENT_URL + "/dirreport/" + res.documents[idx].id} className="list-group-item list-group-item-action py-3 lh-tight"  style={{borderRadius: '15px', backgroundColor: 'whitesmoke', marginTop: '5px'}}>
                                    <div className="d-flex w-100 align-items-center justify-content-between">
                                        <strong className="mb-1">{res.documents[idx].name}</strong>
                                        <small>{new Date(res.documents[idx].effectiveDate).toLocaleDateString()}</small>
                                    </div>
                                    <div className="col-10 mb-1 small">{res.documents[idx].description}</div>
                                </a>
                            ))}
                            </>
                        }
                    </div>
                </Form>
            </div>
        </>
    );
}
export default LoadAllDocuments_Comp;
