import React , {useEffect, useState } from "react";
import { Button, Card, Container, Form, Modal } from "react-bootstrap";
import { useNavigate} from "react-router-dom";
import { UserData } from "../../models/UserData";
import { TASKS_ROUTE } from "../../utils/consts";
import { Position } from "../../models/Position";
import Select from "react-select";
import { GetSubordinateWorkers, GetSubordinateLeaders } from "../../apiRequests/GroupRequests";
import { SetTask, SetCompleted } from "../../apiRequests/TaskRequests";

class Data {
    users : UserData[];
    positions : Position[];
}
class Doc{
    idDoc : number;
    nameDoc : string;
    taskID : number;
    recipient : number;
    constructor(){
        this.idDoc = -1;
        this.nameDoc = "_name";
        this.taskID = -1;
        this.recipient = -1;
    }
}
function SendTask(info : Doc) {
    const [data, setData] = useState<Data>({
        users : [], positions : []
    })
    useEffect(() => {
        GetSubordinateWorkers(info.recipient).then(users => GetSubordinateLeaders(info.recipient).then(positions => 
            setData({users : users, positions : positions})));
    }, []);

    return <SendTaskCard users={data.users} positions={data.positions} idDoc={info.idDoc} nameDoc={info.nameDoc} taskID={info.taskID} />;
}

class cardProps {
    users : UserData[];
    positions : Position[];
    nameDoc : string;
    idDoc : number;
    taskID : number;
}

class Demonstration{
    usersName : string[];
    posName : string[];

    constructor(){
        this.usersName = [];
        this.posName = [];
    }
}
var Demon : Demonstration;
class viewStates {
    show : boolean;
}


function SendTaskCard(props : cardProps) {
    const navigate = useNavigate();
    const [idUsers, setUsers] = useState([]);
    const [idPos, setPos] = useState([]);
    const [nameUser, setNameUser] = useState([]);
    const [namePos, setNamePos] = useState([]);
    const [type, setType] = useState(-1);
    const [deadline, setDeadline] = useState("");
    const [checkedTask, setCheckedTask] = useState(false);
    let [state, setStates] = useState<viewStates>({ show : false});

    console.log(JSON.stringify(props.users));
    // Настройки
    const optionsUser = props.users.map(data => ({label: data.secondName + " " + data.firstName + " " + data.thirdName, value: data.id}));
    optionsUser.push({label: "Не выбрано", value: -1});
    const optionsPosition = props.positions.map(data => ({label: data.name, value: data.id}));
    optionsPosition.push({label: "Не выбрано", value: -1});
    const optionsType = [{label: "Ознакомиться", value: 1}, {label: "Переслать", value: 2}];

    useEffect(() => {
        Demon = new Demonstration();
    }, []);
    function handleClose() {
        setStates({show : false});
    }
    function handleShow(){
        setStates({show : true});
    }
    const setTypeControl = (type: React.SetStateAction<number>) => {
        setType(type);
        setUsersID([]);
        setPositionsID([]);
    }
    const B_SendTask = async () =>{
        setStates({show : false});
        if(type === 1){
            if(idUsers.length > 0){
                for(let i = 0; i < idUsers.length; i++){
                    await SetTask(type, props.idDoc, idUsers[i], new Date().toString(), deadline, props.taskID);
                }
            }
        }   
        else if(type === 2){
            if(idPos.length > 0){
                for(let i = 0; i < idPos.length; i++){
                    await SetTask(type, props.idDoc, idPos[i], new Date().toString(), deadline, props.taskID);
                }
            }
        }
        if(checkedTask)
            await SetCompleted(props.taskID);
        navigate(TASKS_ROUTE);
    }
    const setUsersID = (data) => {
        if(data.length != 0){
            var value = [];
            var names = [];
            for (var i = 0; i < data.length; i++) {
                if (data[i].value !== -1){
                    value.push(data[i].value);
                    names.push(data[i].label)
                }   
            }
            Demon.usersName = names;
            setUsers(value);
            setNameUser(data);
        }else{
            Demon.usersName = [];
            setUsers([]);
            setNameUser([]);
        }
    }
    const setPositionsID = (data) => {
        if(data.length != 0){
            console.log(data.length);
            var value = [];
            var names = [];
            for (var i = 0; i < data.length; i++) {
                if (data[i].value !== -1){
                    value.push(data[i].value);
                    names.push(data[i].label)
                }   
            }
            Demon.posName = names;
            setPos(value);
            setNamePos(data);
        }else{
            Demon.posName = [];
            setPos([]);
            setNamePos([]);
        }
    }
    return(
        <Container 
            className="d-flex justify-content-center align-items-center"
        >
            <Card style={{width: 550, overflowX: 'hidden', overflowY: 'auto'}} className="p-5">
                <h2 className="m-auto">Переслать задание</h2>
                <br></br>
                <Form>
                    <Form.Group className="mb-3" controlId="controlSelectTypeTask">
                        <Form.Label>Выберите тип задания</Form.Label>
                        <Select 
                            options={optionsType}
                            isSearchable={true}
                            onChange={data=>setTypeControl(data.value)}/>
                    </Form.Group>
                    {type === 1?
                        <Form.Group className="mb-3" controlId="controlSelectUser">
                            <Form.Label>Выберите получателя/лей (сотрудники)</Form.Label>
                            <Select 
                                value={nameUser}
                                options={optionsUser}
                                isSearchable={true}
                                isMulti
                                onChange={data=>setUsersID(data)}/>
                        </Form.Group>
                    :type === 2?
                        <Form.Group className="mb-3" controlId="controlSelectPos">
                            <Form.Label>Выберите получателя/лей (должности)</Form.Label>
                            <Select 
                                value={namePos}
                                options={optionsPosition}
                                isSearchable={true}
                                isMulti
                                onChange={data=>setPositionsID(data)}/>
                        </Form.Group>
                    :<></>
                    }
                    <Form.Group className="mb-3" controlId="controlInput_Deadline">
                        <Form.Label>Дата завершения задания</Form.Label>
                        <Form.Control
                            type="date"
                            onChange={e=>setDeadline(e.target.value)}
                            value={deadline}/>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="controlCheckbox_1">
                        <Form.Check 
                            type="checkbox" 
                            label="Закрыть задание" 
                            onChange={(e) => { setCheckedTask(e.target.checked) }}/>
                    </Form.Group>
                    {((type !== -1 && (Demon.posName.length > 0 || Demon.usersName.length > 0) && deadline.length > 0) || checkedTask)?
                    <>
                        <div className="d-flex justify-content-center align-items-center">
                            <Button variant="primary" onClick={handleShow}>
                                Предпросмотр
                            </Button>
                        </div>
                        <Modal show={state.show} fullscreen={true} onHide={handleClose}>
                            <Modal.Header closeButton>
                                <Modal.Title>Предпросмотр пересылки документа [{props.nameDoc}] на {type === 1 ? <b>ознакомление</b>:type === 2? <b>пересылку</b>: <b>Ошибочка</b>}</Modal.Title>
                            </Modal.Header>
                            <Modal.Body>        
                                <Container className="d-flex justify-content-center align-items-center">
                                    <h1 style={{color: 'gray', minWidth: '1vw', position: 'absolute', top: '0', backgroundColor: 'white', borderRadius: '5px', paddingLeft: '6px', paddingRight: '6px'}}>{type === 1? <>Ознакомиться</> : <>Переслать</>}</h1>
                                    <div style={{border: '2px solid gray',  margin: '1%', padding: '20px', width: '50%'}}>
                                        <Card>
                                            <Card.Body>
                                                <Card.Text style={{overflow: 'auto'}}>
                                                    <label>Документ: {props.nameDoc}</label><br></br>
                                                    <br></br>
                                                    <label>Тип: {type === 1 ? <b>Ознакомление</b>:type === 2? <b>Пересылка</b> : <b>Закрытие задания</b>}</label><br></br>
                                                    <br></br>
                                                    <label>Дата вступления в силу: {new Date().toLocaleDateString()}</label><br></br>
                                                    <br></br>
                                                    <label>Дата закрытия задания (дедлайн): {deadline}</label><br></br>
                                                    <br></br>
                                                    {type === 1 ? 
                                                    <>
                                                        <label>Сотрудник/ки: 
                                                        {Array.from({ length: Demon.usersName.length - 1 }).map((_, idx) => (
                                                            <> {Demon.usersName[idx]}, </>
                                                        ))}
                                                        {Demon.usersName[Demon.usersName.length - 1]}</label><br></br>
                                                    </>
                                                    :type === 2? 
                                                    <>
                                                        <label>Руководитель/ли: 
                                                        {Array.from({ length: Demon.posName.length - 1 }).map((_, idx) => (
                                                            <> {Demon.posName[idx]}, </>
                                                        ))}
                                                        {Demon.posName[Demon.posName.length - 1]}</label><br></br>
                                                    </> 
                                                    : <></>}
                                                    <br></br>
                                                    <label>{checkedTask? <strong>Задание закрывается</strong> : <strong>Оставить задание открытым</strong>}</label>
                                                </Card.Text>
                                            </Card.Body>
                                        </Card>
                                    </div>  
                                </Container>
                            </Modal.Body>
                            <Modal.Footer>
                                <Button variant="outline-primary" onClick={B_SendTask}>
                                    Выдать задание
                                </Button>
                                <Button variant="secondary" onClick={handleClose}>
                                    Отменить
                                </Button>
                            </Modal.Footer>
                        </Modal>
                    </>
                    :<></>}
                </Form>
            </Card>
        </Container>
    );
}
export default SendTask;