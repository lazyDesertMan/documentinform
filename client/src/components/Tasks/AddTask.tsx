import React , {useEffect, useState } from "react";
import { Button, Card, Container, Form, Modal } from "react-bootstrap";
import { useNavigate} from "react-router-dom";
import { UserData } from "../../models/UserData";
import Document from "../../models/Document";
import { DIROPTIONS_ROUTE } from "../../utils/consts";
import { Position } from "../../models/Position";
import Select from "react-select";
import { GetListDocuments } from "../../apiRequests/DocumentRequests";
import { GetTopLeaders } from "../../apiRequests/GroupRequests";
import { SetTask } from "../../apiRequests/TaskRequests";
import { GetListUsers } from "../../apiRequests/UserRequests";

class Data {
    users : UserData[];
    positions : Position[];
    documents : Document[];
}
function AddTask() {
    const [data, setData] = useState<Data>({
        users : [], positions : [], documents : []
    })

    useEffect(() => {
        GetListUsers().then(users => GetTopLeaders().then(positions => 
            GetListDocuments().then(documents => setData({users : users, positions : positions, documents : documents}))));
    }, []);
    return <AddTaskCard users={data.users} positions={data.positions} documents={data.documents} />;
}

class cardProps {
    users : UserData[];
    positions : Position[];
    documents : Document[];
}
class Demonstration{
    nameDoc : string;
    usersName : string[];
    posName : string[];

    constructor(){
        this.nameDoc = "_doc";
        this.usersName = [];
        this.posName = [];
    }
}
var Demon : Demonstration;
class viewStates {
    show : boolean;
}
function AddTaskCard(props : cardProps) {
    const navigate = useNavigate();
    const [idUsers, setUsers] = useState([]);
    const [nameUser, setNameUser] = useState([]);
    const [idDoc, setDoc] = useState(-1);
    const [idPos, setPos] = useState([]);
    const [namePos, setNamePos] = useState([]);
    const [type, setType] = useState(-1);
    const [deadline, setDeadline] = useState("");
    let [state, setStates] = useState<viewStates>({ show : false});

    // Настройки
    const optionsDoc = props.documents.map(data => ({label: data.name, value: data.id}));
    optionsDoc.push({label: "Не выбрано", value: -1});
    const optionsUser = props.users.map(data => ({label: data.secondName + " " + data.firstName + " " + data.thirdName, value: data.id}));
    optionsUser.push({label: "Не выбрано", value: -1});
    const optionsPosition = props.positions.map(data => ({label: data.name, value: data.id}));
    optionsPosition.push({label: "Не выбрано", value: -1});
    const optionsType = [{label: "Ознакомиться", value: 1}, {label: "Переслать", value: 2}];

    useEffect(() => {
        Demon = new Demonstration();
    }, []);
    function handleClose() {
        setStates({show : false});
    }
    function handleShow(){
        setStates({show : true});
    }
    const setTypeControl=(type)=>{
        setUsersID([]);
        setPositionsID([]);
        setType(type);
    }
    const setDocID=(id)=>{
        if(id !== -1){
            setDoc(id);
            Demon.nameDoc = props.documents.find(f => f.id === id).name;
        }
        else{
            setDoc(-1);
        } 
    }
    const setUsersID = (data) => {
        if(data.length !== 0){
            var value = [];
            var names = [];
            for (var i = 0; i < data.length; i++) {
                if (data[i].value !== -1){
                    value.push(data[i].value);
                    names.push(data[i].label)
                }
            }
            Demon.usersName = names;
            setUsers(value);
            setNameUser(data);
        }else{
            Demon.usersName = [];
            setUsers([]);
            setNameUser([]);
        }
        
    }
    const setPositionsID = (data) => {
        if(data.length !== 0){
            var value = [];
            var names = [];
            for (var i = 0; i < data.length; i++) {
                if (data[i].value !== -1){
                    value.push(data[i].value);
                    names.push(data[i].label)
                }   
            }
            Demon.posName = names;
            setPos(value);
            setNamePos(data);
        }else{
            Demon.posName = [];
            setPos([]);
            setNamePos([]);
        }
        
    }
    const B_AddTask = async () =>{
        setStates({show : false});
        if(type === 1){
            if(idUsers.length > 0){
                for(let i = 0; i < idUsers.length; i++){
                    await SetTask(type, idDoc, idUsers[i], new Date().toString(), deadline, null);
                }
            }
        }   
        else if(type === 2){
            if(idPos.length > 0){
                for(let i = 0; i < idPos.length; i++){
                    await SetTask(type, idDoc, idPos[i], new Date().toString(), deadline, null);
                }
            }
        }
        navigate(DIROPTIONS_ROUTE);
    }
    return(
        <Container 
            className="d-flex justify-content-center align-items-center"
        >
            <Card style={{width: 550, overflowX: 'hidden', overflowY: 'auto'}} className="p-5">
                <h2 className="m-auto">Новое задание</h2>
                <br></br>
                <Form>
                    <Form.Group className="mb-3" controlId="controlSelectTypeTask">
                        <Form.Label>Выберите тип задания</Form.Label>
                        <Select 
                            options={optionsType}
                            isSearchable={true}
                            onChange={data=>setTypeControl(data.value)}/>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="controlSelectDoc">
                        <Form.Label>Выберите документ</Form.Label>
                        <Select 
                            options={optionsDoc}
                            isSearchable={true}
                            onChange={data=>setDocID(data.value)}/>
                    </Form.Group>
                    {type === 1?
                        <Form.Group className="mb-3" controlId="controlSelectUser">
                            <Form.Label>Выберите получателя/лей (сотрудники)</Form.Label>
                            <Select
                                value={nameUser}
                                options={optionsUser}
                                isSearchable={true}
                                isMulti
                                onChange={data=>setUsersID(data)}/>
                        </Form.Group>
                    :type === 2?
                        <Form.Group className="mb-3" controlId="controlSelectPos">
                            <Form.Label>Выберите получателя/лей (должности)</Form.Label>
                            <Select
                                value={namePos}
                                options={optionsPosition}
                                isSearchable={true}
                                isMulti
                                onChange={data=>setPositionsID(data)}/>
                        </Form.Group>
                    :<></>
                    }
                    <Form.Group className="mb-3" controlId="controlInput_Deadline">
                        <Form.Label>Дата завершения задания</Form.Label>
                        <Form.Control
                            type="date"
                            onChange={e=>setDeadline(e.target.value)}
                            value={deadline}/>
                    </Form.Group>
                    {((type === 1 && idUsers.length > 0) || (idPos.length > 0 && type === 2)) && deadline !== "" && idDoc !== -1?
                    <>
                        <div className="d-flex justify-content-center align-items-center">
                            <Button variant="primary" onClick={handleShow}>
                                Предпросмотр
                            </Button>
                        </div>
                        <Modal show={state.show} fullscreen={true} onHide={handleClose}>
                            <Modal.Header closeButton>
                                <Modal.Title>Предпросмотр выдачи документа {Demon.nameDoc} на {type === 1 ? <strong>ознакомление</strong>:type === 2? <strong>пересылку</strong>: <strong>error</strong>}</Modal.Title>
                            </Modal.Header>
                            <Modal.Body>        
                                <Container className="d-flex justify-content-center align-items-center">
                                    <h1 style={{color: 'gray', minWidth: '1vw', position: 'absolute', top: '0', backgroundColor: 'white', borderRadius: '5px', paddingLeft: '6px', paddingRight: '6px'}}>{Demon.nameDoc}</h1>
                                    <div style={{border: '2px solid gray',  margin: '1%', padding: '20px', width: '50%'}}>
                                        <Card>
                                            <Card.Body>
                                                <Card.Title>Новое задание</Card.Title>
                                                <Card.Text style={{overflow: 'auto'}}>
                                                    <label>Документ: {Demon.nameDoc}</label><br></br>
                                                    <br></br>
                                                    <label>Тип: {type === 1 ? <strong>Ознакомление</strong>:type === 2? <strong>Пересылка</strong> : <strong>error</strong>}</label><br></br>
                                                    <br></br>
                                                    <label>Дата вступления в силу: {new Date().toLocaleDateString()}</label><br></br>
                                                    <br></br>
                                                    <label>Дата закрытия задания (дедлайн): {deadline}</label><br></br>
                                                    <br></br>
                                                    {type === 1 ? 
                                                    <><label>Сотрудник/ки: </label>
                                                        {Array.from({ length: Demon.usersName.length - 1 }).map((_, idx) => (
                                                            <> {Demon.usersName[idx]}, </>
                                                        ))}
                                                        <label> {Demon.usersName[Demon.usersName.length - 1]}</label><br></br>
                                                    </>
                                                    :type === 2? 
                                                    <><label>Руководитель/ли: </label>
                                                        {Array.from({ length: Demon.posName.length - 1 }).map((_, idx) => (
                                                            <> {Demon.posName[idx]}, </>
                                                        ))}
                                                        <label> {Demon.posName[Demon.posName.length - 1]}</label><br></br>
                                                    </>
                                                    : <b>error</b>}
                                                </Card.Text>
                                            </Card.Body>
                                        </Card>
                                    </div>
                                </Container>
                            </Modal.Body>
                            <Modal.Footer>
                                <Button variant="outline-primary" onClick={B_AddTask}>
                                    Добавить
                                </Button>
                                <Button variant="secondary" onClick={handleClose}>
                                    Отменить
                                </Button>
                            </Modal.Footer>
                        </Modal>
                    </>
                    :<></>}
                </Form>
            </Card>
        </Container>
    );
}
export default AddTask;