import React, { useEffect, useState, useMemo } from "react";
import { Form, Stack } from "react-bootstrap";
import { ITask, ResendTask } from "../../models/Task";
import LoadSelfMonitoringReportComp from "../Reports/ViewSelfMonitoringReport";
import "../../css/style.css"
import { GetActiveTasks } from "../../apiRequests/TaskRequests";

interface loadStates {
    isFetching: boolean;
    tasks: ITask[];
}

function LoadTasksComp() {
    let [state, setStates] = useState<loadStates>({ isFetching: false, tasks: [] });
    function LoadData(): void {
        setStates({ isFetching: true, tasks: state.tasks });
        GetActiveTasks().then(r => {
            setStates({ tasks: r, isFetching: false });
        }).catch(e => {
            console.log(e);
            setStates({ tasks: state.tasks, isFetching: true });
        });
    }

    useEffect(() => {
        LoadData();
    }, []);

    return (
        <div>
            {state.isFetching ? "Loading..." : <Search list={state.tasks} />}
        </div>
    );
}
class searchProp {
    list: ITask[];
}

class FilterState {
    familiarize: boolean;
    resend: boolean;
}
const CorrectDate =(list : ITask[]) =>{
    return list.filter(
        item => new Date(item.startDate) <= new Date()
    );
}

function Search(props: searchProp) {
    let [filterState, setFilter] = useState<FilterState>({ resend: false, familiarize: false });
    const [search, setSearch] = useState("");

    let tasks : ITask[] = CorrectDate(props.list);

    const filteredDocs = useMemo(() => {
        if (search) {
          return tasks.filter(
            (item) =>
              item.document
                .toLowerCase()
                .indexOf(search.toLocaleLowerCase()) > -1
          );
        }
        return tasks;
      }, [search]);

    function CheckedFamiliarize(e) {
        setFilter({ familiarize: e.target.checked, resend: filterState.resend });
    }

    function CheckedSend(e) {
        setFilter({ familiarize: filterState.familiarize, resend: e.target.checked });
    }

    let filtred: ITask[] = [];
    let tmp: ITask[] = [];
    if(filteredDocs && filteredDocs.length > 0){
        filteredDocs.map(item => tmp.push(item));
    }

    if (filterState.familiarize || filterState.resend) {
        if (filterState.familiarize) {
            for (let i = 0; i < tmp.length; i++) {
                if (tmp[i].type === 1) {
                    filtred.push(tmp[i]);
                }
            }
        }
        if (filterState.resend) {
            for (let i = 0; i < tmp.length; i++) {
                if (tmp[i].type === 2)
                    filtred.push(tmp[i]);
            }
        }
    }
    else {
        filtred = tmp;
    }

    return (
        <>
            <div>
                <h1 className="d-grid gap-2 d-sm-flex justify-content-sm-center">Задания</h1>
                <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
                    <Stack direction="horizontal" gap={4} style={{ width: '60%', marginTop: '1%' }}>
                        <Form.Control
                            id="myInput"
                            className="me-auto"
                            value={search} 
                            onChange={e => setSearch(e.target.value)}
                            placeholder="Найти задание..." />
                        <div className="vr"></div>
                        <Form.Group className="mb-3" controlId="controlCheckbox_1">
                            <Form.Check 
                                type="checkbox" 
                                label="Ознакомиться" 
                                onChange={(e) => { CheckedFamiliarize(e) }}/>
                            <Form.Check 
                                type="checkbox" 
                                label="Переслать" 
                                onChange={(e) => { CheckedSend(e) }}/>
                        </Form.Group>
                        <div className="vr"></div>
                        <LoadSelfMonitoringReportComp />
                    </Stack>
                </div>
            </div>
            <ViewTasks list={filtred} />
        </>
    );
}

class viewProps {
    list: ITask[];
}

function ViewTasks(props: viewProps) { 
    return (
        <>
            <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
                <Form style={{ width: '60%', marginTop: '1%' }}>
                    <div className="list-group list-group-flush border-bottom scrollarea">
                            <>
                            {Array.from({ length: props.list.length }).map((_, idx) => (
                                props.list[idx].type === 1 ?
                                    <a id="famil" key={props.list[idx].ID} href={process.env.REACT_APP_CLIENT_URL +"/pdf/" + props.list[idx].documentID
                                        + "/" + props.list[idx].ID} className="list-group-item list-group-item-action py-3 lh-tight" style={{borderRadius: '15px', backgroundColor: 'whitesmoke', marginTop: '10px'}}>
                                        <div className="d-flex w-100 align-items-center justify-content-between">
                                            <strong className="mb-1">{props.list[idx].document}</strong>
                                            <small>Прочитать</small>
                                        </div>
                                        <div className="d-flex flex-wrap justify-content-between">
                                                <div className="leftPart small mt-2">Выдано</div> 
                                                <div className="rightPart small mt-2">{new Date(props.list[idx].startDate).toLocaleDateString()}</div>
                                                <div className="leftPart small mt-1">Выполнить до</div>
                                                <div className="rightPart small mt-1">{new Date(props.list[idx].deadline).toLocaleDateString()}</div>
                                        </div>
                                    </a>
                                    :
                                    <a id="send" key={props.list[idx].ID} href={process.env.REACT_APP_CLIENT_URL +"/send/" + props.list[idx].documentID + "/" + props.list[idx].document + "/" + props.list[idx].ID + "/" + (props.list[idx] as ResendTask).recipient} className="list-group-item list-group-item-action py-3 lh-tight" style={{borderRadius: '15px', backgroundColor: 'whitesmoke', marginTop: '10px'}}>
                                        <div>
                                            <div className="d-flex w-100 align-items-center justify-content-between">
                                                <strong className="mb-1">{props.list[idx].document}</strong>
                                                <small>Переслать</small>
                                            </div>
                                            <div className="d-flex flex-wrap justify-content-between">
                                                    <div className="leftPart small mt-2">Выдано</div> 
                                                    <div className="rightPart small mt-2">{new Date(props.list[idx].startDate).toLocaleDateString()}</div>
                                                    <div className="leftPart small mt-1">Выполнить до</div>
                                                    <div className="rightPart small mt-1">{new Date(props.list[idx].deadline).toLocaleDateString()}</div>
                                            </div>
                                        </div>
                                    </a>
                            ))}
                            </>
                    </div>
                </Form>
            </div>
        </>
    );
}
export default LoadTasksComp;