import React from 'react';
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend,
} from 'chart.js';
import { Bar } from 'react-chartjs-2';
import { TaskReport } from '../../models/TaskReport';

ChartJS.register(
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend
);

let countComplet : number = 0;
let countActive : number = 0;
let countDeadlineViolation : number = 0;
class reportProps{
  report : TaskReport
}
function ParseTree(tree){
  if(tree.task.status === 1){
      countComplet++;
    }else if(tree.task.status === 2){
      countActive++;
    }else{
      countDeadlineViolation++;
    }
    tree.childs.map(c => ParseTree(c));
}
export function BarTaskChart(props : reportProps) {
  countComplet = 0;
  countActive = 0;  
  countDeadlineViolation = 0;
  ParseTree(props.report.report);
  console.log(countComplet + ", " + countActive + ", " + countDeadlineViolation);
  const options = {
    responsive: true,
    plugins: {
      legend: {
        position: 'top' as const,
      },
      title: {
        display: true,
      },
    },
  };
  let vd: string = props.report.report.task.task.document;
  const data = {
    labels : ['Отчет по документу: ' + vd],
    datasets: [
      {
        label: 'Выполнили',
        data: [countComplet],
        backgroundColor: ['blue'],
        borderColor: ['black'],
        borderWidth: 1,
      },
      {
          label: 'Выполняется',
          data: [countActive],
          backgroundColor: ['green'],
          borderColor: ['black'],
          borderWidth: 1,
      },
      {
          label: 'Не выполнили (нарушение)',
          data: [countDeadlineViolation],
          backgroundColor: ['red'],
          borderColor: ['black'],
          borderWidth: 1,
      },
    ]
  };
  return <Bar options={options} data={data} />;
}