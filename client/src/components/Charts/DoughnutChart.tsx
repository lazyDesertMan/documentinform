import React from 'react';
import { Chart as ChartJS, ArcElement, Tooltip, Legend } from 'chart.js';
import { Doughnut } from 'react-chartjs-2';
import { SelfMonitoringReport } from '../../models/SelfMonitoringReport';

ChartJS.register(ArcElement, Tooltip, Legend);

class reportProps{
  report : SelfMonitoringReport
}
export function DoughnutChart(props : reportProps) {
  var countComplet : number = 0;
  var countActive : number = 0;
  var countDeadlineViolation : number = 0;
  for(let i = 0; i < props.report.records.length; i++){
    for(let j = 0; j < props.report.records[i].tasks.length; j++){
      if(props.report.records[i].tasks[j].state === 1)
        countComplet++;
      else if(props.report.records[i].tasks[j].state === 2)
        countActive++;
      else
        countDeadlineViolation++;
    }
  }
  const data = {
    labels: ['Выполнено [' + countComplet + ']', 'Выполняется [' + countActive + ']', 'Не выполнено (нарушение) [' + countDeadlineViolation + ']'],
    datasets: [
      {
        label: '# of Votes',
        data: [countComplet, countActive, countDeadlineViolation],
        backgroundColor: [
          'blue',
          'green',
          'red',
        ],
        borderColor: [
          'black',
          'black',
          'black',
        ],
        borderWidth: 1,
      },
    ],
  };
  return <Doughnut data={data}/>;
}