import React, { useEffect, useState } from "react";
import { Card, Container, ListGroupItem } from "react-bootstrap";
import { GetInfoAboutGroup } from "../../apiRequests/GroupRequests";
import { GetAuthPos, GetListPosition } from "../../apiRequests/PositionRequests";
import { GetListUsers } from "../../apiRequests/UserRequests";
import { Group } from "../../models/Group";
import { Position } from "../../models/Position";
import { UserData } from "../../models/UserData";

class loadStates {
    positions: Position[];
    users: UserData[];
    allPosition: Position[];
}

function LoadInfoAboutMyGrop_Comp() {
    const [data, setData] = useState<loadStates>({ positions: [], users: [], allPosition: []});
    
    useEffect(() => {
        GetAuthPos().then(positions => GetListUsers().then(users => GetListPosition().then(allPosition => setData({positions : positions, users : users, allPosition : allPosition}))));
    }, []);

    return <ViewInfoAboutGroup positions={data.positions} users={data.users} allPosition={data.allPosition} />
}

class viewProps {
    positions: Position[];
    users: UserData[];
    allPosition: Position[];
}
let length: number;
function ViewInfoAboutGroup(props: viewProps) {
    const [groups, setGroups] = useState<Group[]>([]);
    const [showDetails, setShowDetails] = useState(false);

    const Method = async(pos : Position[])=>{
        let res : Group[] = [];
        for(let i :number = 0; i < pos.length; i++){
            res.push(await GetInfoAboutGroup(pos[i].groupID));
        }
        return res;
    }
    useEffect(() => {
        Method(props.positions).then(groups => setGroups(groups));
    }, [props.positions]);

    return (
        <>
        <h1 className="d-flex justify-content-center align-items-center">Информация о личных группах</h1>
        <Container style={{marginTop: '5%'}}>
            {groups.length > 0?
                <>               
                    {Array.from({ length: groups.length - 1 }).map((_, idx) => (
                    <div className="d-flex justify-content-center align-items-center" style={{marginBottom: '3%'}}>
                    <Card
                        style={{ width: '35%', borderRadius: '40px', margin: '10px'}}
                        className="mb-2"
                        border="2px">
                        <>
                            <Card.Header className="d-flex justify-content-center align-items-center">{groups[idx].name}</Card.Header>
                            <Card.Body>
                                <Card.Title className="d-flex justify-content-center align-items-center">{props.positions.find(f => f.groupID === groups[idx].id).name}</Card.Title>
                                <Card.Text>
                                    {props.allPosition.map(allP => 
                                        allP.groupID === groups[idx].id && allP.id !== props.positions.find(f => f.groupID === groups[idx].id).id?
                                            <><ListGroupItem variant="primary" style={{borderRadius: '20px', color: 'black'}}>
                                                <strong>{props.users.find(f => f.id === props.positions.find(f => f.groupID === groups[idx].id).workerID).secondName + " " + 
                                                props.users.find(f => f.id === props.positions.find(f => f.groupID === groups[idx].id).workerID).firstName + " " + 
                                                props.users.find(f => f.id === props.positions.find(f => f.groupID === groups[idx].id).workerID).thirdName}</strong><br></br>
                                                <small>
                                                    {allP.name}
                                                </small>
                                            </ListGroupItem></>
                                        :<></>       
                                    )}
                                </Card.Text>
                            </Card.Body>
                        </>
                    </Card>
                    </div>
                    ))}
                    <div className="d-flex justify-content-center align-items-center">
                    <Card
                        style={{ width: '35%', borderRadius: '40px', margin: '10px'}}
                        className="mb-2"
                        border="2px">
                        <>
                            <Card.Header className="d-flex justify-content-center align-items-center">{groups[groups.length - 1].name}</Card.Header>
                            <Card.Body>
                                <Card.Title className="d-flex justify-content-center align-items-center" >{props.positions.find(f => f.groupID == groups[groups.length - 1].id).name}</Card.Title>
                                <Card.Text>
                                    {props.allPosition.map(allP => 
                                        allP.groupID === groups[groups.length - 1].id && allP.id !== props.positions.find(f => f.groupID === groups[groups.length - 1].id).id?
                                            <><ListGroupItem variant="primary" style={{borderRadius: '20px', color: 'black'}}>
                                                <strong>{props.users.find(f => f.id == props.positions.find(f => f.groupID === groups[groups.length - 1].id).workerID).secondName + " " + 
                                                props.users.find(f => f.id === props.positions.find(f => f.groupID === groups[groups.length - 1].id).workerID).firstName + " " + 
                                                props.users.find(f => f.id === props.positions.find(f => f.groupID === groups[groups.length - 1].id).workerID).thirdName}</strong><br></br>
                                                <small>
                                                    {allP.name}
                                                </small>
                                            </ListGroupItem></>
                                        :<></>       
                                    )}
                                </Card.Text>
                            </Card.Body>
                        </>
                    </Card>
                    </div>
                </>
            :<></>}
        </Container>
    </>
    );
}
export default LoadInfoAboutMyGrop_Comp;