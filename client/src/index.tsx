import React, {createContext} from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import {UserData} from './models/UserData';
import 'bootstrap/dist/css/bootstrap.min.css'


export const Context = createContext<UserData>(null);
ReactDOM.render(
  <Context.Provider value={new UserData()}>
    <App />
  </Context.Provider>,
  document.getElementById('root')
);
