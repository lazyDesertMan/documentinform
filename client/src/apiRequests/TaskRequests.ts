import { get_request, post_request, wait_json } from "./ApiRequests";

/// Запрос списка невыполненных заданий авторизованного пользователя 
export async function GetActiveTasks() {
    const request = new Request(
        process.env.REACT_APP_API_URL + process.env.REACT_APP_API_TASK_PATH + process.env.REACT_APP_API_ACTIVE_TASKS,
        get_request
    );
    return wait_json(request);
}

/// Запрос списка заданий на пересылку
export async function GetSecondTypeTasks(id : number, startDate : string, endDate : string) {
    const data = {userID: id, startDate : startDate, endDate : endDate};
    const request = new Request(
        process.env.REACT_APP_API_URL + process.env.REACT_APP_API_TASK_PATH + process.env.REACT_APP_API_RESEND_TASKS,
        post_request(JSON.stringify(data))
    );
    return wait_json(request);
}

/// Завершение выполнения задания с ID = %taskID
export async function SetCompleted(taskID) {
    const request = new Request(
        process.env.REACT_APP_API_URL + process.env.REACT_APP_API_TASK_PATH + process.env.REACT_APP_API_COMPLETE_TASK,
        post_request(JSON.stringify({id : taskID}))
    );
    try {
        await fetch(request);
    }
    catch(e) {
        console.log(e);
    }
}

/// Добавление нового задания
export async function SetTask(type, document : number, recipient : number, startDate : string, deadline : string, prevTask : number) {
    const data = { type : type, document : document, recipient : recipient, start : startDate, end : deadline, prevTask : prevTask}
    const request = new Request(
        process.env.REACT_APP_API_URL + process.env.REACT_APP_API_TASK_PATH + process.env.REACT_APP_API_ADD_TASK,
        post_request(JSON.stringify(data))
    );
    try {
        await fetch(request);
    }
    catch(e) {
        console.log(e);
    }
}
