import { post_request, wait_json } from "./ApiRequests";

/// Вход в систему
export async function Authentication (login, password) {
    const data = { login : login, password : password }
    const request = new Request(
        process.env.REACT_APP_API_URL + process.env.REACT_APP_API_LOGIN_PATH + process.env.REACT_APP_API_LOGIN,
        post_request(JSON.stringify(data))
    );
    return wait_json(request);
}

/// Изменение пароля пользователя
export async function ChangePassword(password : string, newPassword : string) {
    const data = {password : password, newPassword : newPassword};
    const request = new Request(
        process.env.REACT_APP_API_URL + process.env.REACT_APP_API_LOGIN_PATH + process.env.REACT_APP_API_CHANGE_PASSWORD,
        post_request(JSON.stringify(data))
    );
    return wait_json(request);
}

/// Регистрация нового пользователя
export async function Registration(login : string, password : string, firstName : string, secondName : string, thirdName : string, role : string, email : string) {
    const data = { login : login, password : password, secondName : secondName, firstName : firstName, thirdName : thirdName, role : role, email : email};
    const request = new Request(
        process.env.REACT_APP_API_URL + process.env.REACT_APP_API_LOGIN_PATH + process.env.REACT_APP_API_REGISTRATION,
        post_request(JSON.stringify(data))
    );
    return wait_json(request);
}
