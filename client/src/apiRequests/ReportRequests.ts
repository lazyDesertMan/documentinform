import { get_request, wait_json } from "./ApiRequests";

/// Запрос отчёта самоконтроля для пользователя с ID = %id
export async function GetReport(id) {
    const request = new Request(
        process.env.REACT_APP_API_URL + process.env.REACT_APP_API_REPORT_PATH
            + process.env.REACT_APP_API_SELF_REPORT +'/' + id,
        get_request
    );
    return wait_json(request);
}

/// Запрос отчёта по заданию с ID = %taskID
export async function GetTaskReport(taskID : number, startDate : string, endDate : string) {
    const request = new Request(
        process.env.REACT_APP_API_URL + process.env.REACT_APP_API_REPORT_PATH
            + process.env.REACT_APP_API_TASK_REPORT +'/' + taskID,
        get_request
    );
    return wait_json(request);
}

/// Запрос отчёта по документу с ID = %taskID
export async function GetDocumentReport(docID : number, startDate : string, endDate : string) {
    const request = new Request(
        process.env.REACT_APP_API_URL + process.env.REACT_APP_API_REPORT_PATH
        + process.env.REACT_APP_API_DOCUMENT_REPORT +'/' + docID,
        get_request
    );
    return wait_json(request);
}
