import { post_request, wait_json } from "./ApiRequests";

/**
 * Запрос на отправку обратной связи
 * @param message Текст сообщения
 * @param subject Тема сообщения
 * @param sender Имя отправителя
 * @returns Ответ API
 */
export async function SetFeedBack(message : string, subject? : string, sender? : string) {
    const data = {message : message, subject : subject, sender : sender };
    const request = new Request(
        process.env.REACT_APP_API_URL + process.env.REACT_APP_API_FEEDBACK_PATH + process.env.REACT_APP_API_BUG,
        post_request(JSON.stringify(data))
    );
    return wait_json(request);
}
