import { get_request, post_request, wait_json } from "./ApiRequests";

// Список всех пользователей, кроме администраторов
export async function GetListUsers() {
    const request = new Request(
        process.env.REACT_APP_API_URL + process.env.REACT_APP_API_USER_PATH + process.env.REACT_APP_API_WORKERS_LIST,
        get_request
    );
    return wait_json(request);
}

/// Удаление пользователя с ID = %id
export async function RemoveUser(id : number) {
    const data = {id : id};
    const request = new Request(
        process.env.REACT_APP_API_URL + process.env.REACT_APP_API_USER_PATH + process.env.REACT_APP_API_REMOVE_USER,
        post_request(JSON.stringify(data))
    );
    try {
        await fetch(request);
    }
    catch(e) {
        console.log(e);
    }
}

/// Изменение информации о сотруднике
export async function UpdateUser(id : number, sname : string, fname : string, tname : string, login : string, email : string, isDisableMail : boolean) {
    const data = {id : id, secondName : sname, firstName : fname, thirdName : tname, login : login, email : email, removeEmail : isDisableMail};
    const request = new Request(
        process.env.REACT_APP_API_URL + process.env.REACT_APP_API_USER_PATH + process.env.REACT_APP_API_UPDATE_USER,
        post_request(JSON.stringify(data))
    );
    return wait_json(request);
}

/// Запрос списка всех пользователей
export async function GetAllUsersWithDirector() {
    const request = new Request(
        process.env.REACT_APP_API_URL + process.env.REACT_APP_API_USER_PATH + process.env.REACT_APP_API_USERS_LIST,
        get_request
    );
    return wait_json(request);
}
