import { get_request, post_request, wait_json } from "./ApiRequests";

/// Запрос иерархии групп организации
export async function GetStructureGroup() {
    const request = new Request(
        process.env.REACT_APP_API_URL + process.env.REACT_APP_API_GROUP_PATH + process.env.REACT_APP_API_GROUP_STRUCTURE,
        get_request
    );
    return wait_json(request);
}

/// Запрос списка всех групп
export async function GetListGroup() {
    const request = new Request(
        process.env.REACT_APP_API_URL + process.env.REACT_APP_API_GROUP_PATH + process.env.REACT_APP_API_GROUP_LIST,
        get_request
    );
    return wait_json(request);
}

/// Запрос списка подгрупп группы с ID = %idGroup
export async function GetSubgroups(idGroup : number) {
    const request = new Request(
        process.env.REACT_APP_API_URL + process.env.REACT_APP_API_GROUP_PATH
            + process.env.REACT_APP_API_GROUP_HIERARCHY + "/" + idGroup,
        get_request
    );
    return wait_json(request);
}

/// Добавление новой группы
export async function SetGroup(parent : number, name : string) {
    const data = { parent : parent, name : name};
    const request = new Request(
        process.env.REACT_APP_API_URL + process.env.REACT_APP_API_GROUP_PATH + process.env.REACT_APP_API_ADD_GROUP,
        post_request(JSON.stringify(data))
    );
    return wait_json(request);
}

/// Запрос списка подчиненных сотрудников
export async function GetSubordinateWorkers(id : number) {
    const request = new Request(
        process.env.REACT_APP_API_URL + process.env.REACT_APP_API_GROUP_PATH
            + process.env.REACT_APP_API_SUBORDINATE_WORKERS + '/' + id,
        get_request
    );
    return wait_json(request);
}

/// Запрос списка подчиненных руководителей
export async function GetSubordinateLeaders(id : number) {
    const request = new Request(
        process.env.REACT_APP_API_URL + process.env.REACT_APP_API_GROUP_PATH
            + process.env.REACT_APP_API_SUBORDINATE_LEADERS + '/' + id,
        get_request
    );
    return wait_json(request);
}

/// Удаление группы с ID = %id
export async function RemoveGroup(id : number) {
    const data = {id : id};
    const request = new Request(
        process.env.REACT_APP_API_URL + process.env.REACT_APP_API_GROUP_PATH + process.env.REACT_APP_API_REMOVE_GROUP,
        post_request(JSON.stringify(data))
    );
    return wait_json(request);
}

/// Изменение сведений о группе
export async function UpdateGroup(group : number, leader : number, parent : number, name : string) {
    const data = { group : group, leader : leader, parent : parent, name : name};
    const request = new Request(
        process.env.REACT_APP_API_URL + process.env.REACT_APP_API_GROUP_PATH + process.env.REACT_APP_API_UPDATE_GROUP,
        post_request(JSON.stringify(data))
    );
    try {
        await fetch(request);
    }
    catch(e) {
        console.log(e);
    }
}

/// Запрос списка руководителей групп, не являющихся подгруппами
export async function GetTopLeaders() {
    const request = new Request(
        process.env.REACT_APP_API_URL + process.env.REACT_APP_API_GROUP_PATH + process.env.REACT_APP_API_TOP_LEADERS,
        get_request
    );
    return wait_json(request);
}

/// Сведения о группе с ID = groupID
export async function GetInfoAboutGroup(groupId : number) {
    const request = new Request(
        process.env.REACT_APP_API_URL + process.env.REACT_APP_API_GROUP_PATH + process.env.REACT_APP_API_INFO_ABOUT_GROUP + "/" + groupId,
        get_request
    );
    return wait_json(request);
}
/// Запрос на проверку активного пользователя как лидера какой-либо группы
export async function CheckOnLeader() {
    const request = new Request(
        process.env.REACT_APP_API_URL + process.env.REACT_APP_API_GROUP_PATH
        + process.env.REACT_APP_API_ISLEADER,
        get_request
    );
    return wait_json(request);
}
