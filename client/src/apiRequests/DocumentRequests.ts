import { get_request, post_request, wait_json } from "./ApiRequests";

/// Запрос списка документов, доступных авторизованному пользователю
export async function GetDocuments() {
    const request = new Request(
        process.env.REACT_APP_API_URL + process.env.REACT_APP_API_DOCUMENT_PATH + process.env.REACT_APP_API_ALLOWED_DOCUMENTS,
        get_request
    );
    return wait_json(request);
}

/// Загрузка данных файла %fData, связанного с документом с ID = %id
export async function SetFile(id : number, fData : any) {
    const request = new Request(
        process.env.REACT_APP_API_URL + process.env.REACT_APP_API_DOCUMENT_PATH + process.env.REACT_APP_API_UPLOAD_FILE,
        {
            mode: 'cors',
            method: "POST",
            credentials: "include",
            headers: {
                'Content-Type': 'application/pdf',
                'X-File-Id': id.toString()
            },
            body: fData
        }
    );
    try {
        await fetch(request);
    } catch(e) {
        console.log(e);
    }
}

/// Запрос списка всех документов
export async function GetListDocuments() {
    const request = new Request(
        process.env.REACT_APP_API_URL + process.env.REACT_APP_API_DOCUMENT_PATH + process.env.REACT_APP_API_DOCUMENTS_LIST,
        get_request
    );
    return wait_json(request);
}

/// Добавление нового документа
export async function AddDocument(name : string, description : string, effectiveDate : string, oldVersion: number) {
    const data = { name: name, description: description, effectiveDate: effectiveDate, oldVersion : oldVersion };
    const request = new Request(
        process.env.REACT_APP_API_URL + process.env.REACT_APP_API_DOCUMENT_PATH + process.env.REACT_APP_API_ADD_DOCUMENT,
        post_request(JSON.stringify(data))
    );
    return wait_json(request);
}

/// Удаление документа с ID = %id
export async function RemoveDoc(id : number) {
    const data = {id : id};
    const request = new Request(
        process.env.REACT_APP_API_URL + process.env.REACT_APP_API_DOCUMENT_PATH + process.env.REACT_APP_API_ADD_DOCUMENT,
        post_request(JSON.stringify(data))
    );
    try {
        await fetch(request);
    }
    catch(e) {
        console.log(e);
    }
}

/// Изменение информации о документе
export async function UpdateDoc(document : number, name : string, description : string, effectiveDate : string, oldVersion : number) {
    const data = { document : document, name : name, description : description, effectiveDate : effectiveDate, oldVersion : oldVersion};
    const request = new Request(
        process.env.REACT_APP_API_URL + process.env.REACT_APP_API_DOCUMENT_PATH + process.env.REACT_APP_API_UPDATE_DOCUMENT,
        post_request(JSON.stringify(data))
    );
    return wait_json(request);
}
