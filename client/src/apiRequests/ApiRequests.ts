export const get_request : RequestInit = {
    mode: 'cors',
    method: "GET",
    credentials: "include"
}

export function post_request(json_data : string) : RequestInit {
    return {
        mode: 'cors',
        method: "POST",
        credentials: "include",
        headers: {
            'Content-Type': 'application/json'
        },
        body: json_data
    }
}

export async function wait_json(req : Request) {
    try {
        return (await fetch(req)).json();
    }
    catch(e) {
        console.log(e);
        return null;
    }
}
