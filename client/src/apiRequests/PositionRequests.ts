import { get_request, post_request, wait_json } from "./ApiRequests";

/// Запрос списка всех должностей
export async function GetListPosition () {
    const request = new Request(
        process.env.REACT_APP_API_URL + process.env.REACT_APP_API_POSITION_PATH + process.env.REACT_APP_API_POSITIONS_LIST,
        get_request
    );
    return wait_json(request);
}

/// Запрос должности авторизованного пользователя
export async function GetAuthPos () {
    const request = new Request(
        process.env.REACT_APP_API_URL + process.env.REACT_APP_API_POSITION_PATH + process.env.REACT_APP_API_POSITION_ACTIVE,
        get_request
    );
    return wait_json(request);
}

/// Добавление новой должности в группу с ID = %group
export async function SetPosition(group : number, name : string) {
    const data = { group : group, name : name};
    const request = new Request(
        process.env.REACT_APP_API_URL + process.env.REACT_APP_API_POSITION_PATH + process.env.REACT_APP_API_ADD_POSITION,
        post_request(JSON.stringify(data))
    );
    return wait_json(request);
}

/// Запрос списка свободных должностей группы с ID = %idGroup
export async function GetFreePosition(idGroup : number) {
    const request = new Request(
        process.env.REACT_APP_API_URL + process.env.REACT_APP_API_POSITION_PATH
            + process.env.REACT_APP_API_GROUP_FREE_POSITIONS + "/" + idGroup,
        get_request
    );
    return wait_json(request);
}

/// Запрос списка всех должностей группы с ID = %idGroup
export async function GetAllPositionsInGroup(idGroup : number) {
    const request = new Request(
        process.env.REACT_APP_API_URL + process.env.REACT_APP_API_POSITION_PATH
            + process.env.REACT_APP_API_GROUP_POSITIONS + "/" + idGroup,
        get_request
    );
    return wait_json(request);
}

/// Запрос списка сотрудников, не занимающих должности
export async function GetFreeUsers() {
    const request = new Request(
        process.env.REACT_APP_API_URL + process.env.REACT_APP_API_POSITION_PATH + process.env.REACT_APP_API_FREE_WORKERS,
        get_request
    );
    return wait_json(request);
}

/// Изменение сведений о должности
export async function UpdatePosition(positionID : number, groupID : number, workerID : number, name : string) {
    const data = { positionID : positionID, groupID : groupID, workerID : workerID, name : name};
    const request = new Request(
        process.env.REACT_APP_API_URL + process.env.REACT_APP_API_POSITION_PATH + process.env.REACT_APP_API_UPDATE_POSITION,
        post_request(JSON.stringify(data))
    );
    return wait_json(request);
}

/// Удаление должности с ID = %position
export async function DeletePos(position : number) {
    const data = {id : position};
    const request = new Request(
        process.env.REACT_APP_API_URL + process.env.REACT_APP_API_POSITION_PATH + process.env.REACT_APP_API_REMOVE_POSITION,
        post_request(JSON.stringify(data))
    );
    try {
        await fetch(request);
    }
    catch(e) {
        console.log(e);
    }
}

/// Запрос списка заместителей должности с ID = id
export async function GetListDeputies(id : number) {
    const request = new Request(
        process.env.REACT_APP_API_URL + process.env.REACT_APP_API_POSITION_PATH + process.env.REACT_APP_API_LIST_DEPUTIES_IN_GROUP + "/" + id,
        get_request
    );
    return wait_json(request);
}

/// Добавление заместителя с ID = replacer к руководителю(должности) с ID = position
export async function AddDeputy(position : number, replacer : number, startDate : string, endDate : string) {
    const data = {position : position, replacer : replacer, startDate : startDate, endDate : endDate};
    const request = new Request(
        process.env.REACT_APP_API_URL + process.env.REACT_APP_API_POSITION_PATH + process.env.REACT_APP_API_ADD_DEPUTY,
        post_request(JSON.stringify(data))
    );
    return wait_json(request);
}

/// Удаление заместителя с ID = idPos
export async function RemoveDeputy(position : number, replacer : number, withTemporary : boolean) {
    const data = { position : position, replacer : replacer, withTemporary : withTemporary};
    const request = new Request(
        process.env.REACT_APP_API_URL + process.env.REACT_APP_API_POSITION_PATH + process.env.REACT_APP_API_REMOVE_DEPUTY,
        post_request(JSON.stringify(data))
    );
    return wait_json(request);
}
