import Profile from "./pages/Profile";
import Registration from "./pages/Admin/Registration";
import Document from "./pages/Document";
import {
    ERROR_ROUTE,
    DOCUMENT_ROUTE,
    PROFILE_ROUTE,
    REGISTRATION_ROUTE,
    TASKS_ROUTE,
    PDF_ROUTE,
    SEND_ROUTE,
    DIROPTIONS_ROUTE,
    DIRFUNCT_ROUTE,
    DIRREPORT_ROUTE,
    ADMIN_FUNCT_ROUTE,
    ADMIN_OPTIONS_ROUTE,
    TASKSREPORT_ROUTE,
    LISTTASKFORREPORT_ROUTE,
    ADMIN_DEPUTYOPTS_ROUTE,
    ADMIN_DEPUTYFUNC_ROUTE,
    MYGROUP_ROUTE,
    FEEDBACK_ROUTE
} from "./utils/consts";
import Tasks from "./pages/Tasks";
import Error from "./pages/Error";
import PDF from "./pages/PDF"
import Send from "./pages/Leader/Send";
import ListTasksForReport from "./pages/Leader/ListTasksForReport";
import DirOptions from "./pages/Director/DirOptions";
import Functional from "./pages/Director/DirFunctional";
import DirReport from "./pages/Director/DirReport";
import AdmFunctional from "./pages/Admin/AdmFunctional";
import AdmOptions from "./pages/Admin/AdmOptions";
import TasksReport from "./pages/Leader/TasksReport";
import DeputyOptions from "./pages/Admin/Deputy/DeputyOptions";
import DeputyFunctional from "./pages/Admin/Deputy/DeputyFunctional";

import MyGroup from "./pages/Leader/MyGroup";
import FeedBack from "./pages/FeedBack";

export const authRoutes = [
    {
        path: PROFILE_ROUTE ,
        Component: Profile
    },
    {
        path: DOCUMENT_ROUTE,
        Component: Document
    },
    {
        path: ERROR_ROUTE ,
        Component: Error
    },
    {
        path: PDF_ROUTE + '/:id' ,
        Component: PDF
    },
    {
        path: PDF_ROUTE + '/:id/:taskID' ,
        Component: PDF
    },
    {
        path: TASKS_ROUTE ,
        Component: Tasks
    },
    {
        path: SEND_ROUTE + '/:idDoc' ,
        Component: Send
    },
    {
        path: SEND_ROUTE + '/:idDoc/:nameDoc' ,
        Component: Send
    },
    {
        path: SEND_ROUTE + '/:idDoc/:nameDoc/:taskID' ,
        Component: Send
    },
    {
        path: SEND_ROUTE + '/:idDoc/:nameDoc/:taskID/:recipient' ,
        Component: Send
    },
    {
        path: TASKSREPORT_ROUTE  + '/:idTask' ,
        Component: TasksReport
    },
    {
        path: LISTTASKFORREPORT_ROUTE ,
        Component: ListTasksForReport
    },
    {
        path: MYGROUP_ROUTE ,
        Component: MyGroup
    },
    {
        path: FEEDBACK_ROUTE ,
        Component: FeedBack
    },
]

export const adminRoutes = [
    {
        path: REGISTRATION_ROUTE ,
        Component: Registration
    },
    {
        path: PROFILE_ROUTE ,
        Component: Profile
    },
    {
        path: ERROR_ROUTE ,
        Component: Error
    },
    {
        path: ADMIN_OPTIONS_ROUTE ,
        Component: AdmOptions
    },
    {
        path: ADMIN_FUNCT_ROUTE + '/:path',
        Component: AdmFunctional
    },
    {
        path: ADMIN_DEPUTYOPTS_ROUTE ,
        Component: DeputyOptions
    },
    {
        path: ADMIN_DEPUTYFUNC_ROUTE + '/:path',
        Component: DeputyFunctional
    },
    {
        path: FEEDBACK_ROUTE ,
        Component: FeedBack
    },
]

export const directorRoutes = [
    {
        path: PROFILE_ROUTE ,
        Component: Profile
    },
    {
        path: ERROR_ROUTE ,
        Component: Error
    },
    {
        path: DIROPTIONS_ROUTE ,
        Component: DirOptions
    },
    {
        path: DIRFUNCT_ROUTE + '/:path',
        Component: Functional
    },
    {
        path: DIRREPORT_ROUTE + '/:idDoc',
        Component: DirReport
    },
    {
        path: FEEDBACK_ROUTE ,
        Component: FeedBack
    },
]

export const publicRoutes = [
    {
        path: ERROR_ROUTE ,
        Component: Error
    },
]