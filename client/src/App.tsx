import React, { useContext } from "react";
import Cookies from 'js-cookie'
import { BrowserRouter } from "react-router-dom";
import { Context } from "."
import AppRouter from "./components/AppRouter";
import './App.css';
import NavBar from "./components/NavBar";
import jwtDecode from "jwt-decode";
import {UserData, Roles} from "./models/UserData";
import { ACCESS_TOKEN_COOKIE } from "./utils/consts";
import Cookie from "./classes/Cookie";

function App() {
  const user : UserData = useContext(Context);
  const usrCookie = Cookies.get(ACCESS_TOKEN_COOKIE);
  if (usrCookie){
    const userData = jwtDecode<Cookie>(usrCookie);
    user.role = (userData && userData.user && userData.user.role) ? userData.user.role : Roles.ROLE_WORKER;
  }
  return (
    <BrowserRouter>
      <NavBar />
      <AppRouter />
    </BrowserRouter>
  );
}

export default App;
