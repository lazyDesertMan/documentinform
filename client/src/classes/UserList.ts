export default class UserList {
    private _data : any[];

    constructor() {
        this._data = [];
    }

    set list(data) {
        this._data = data
    }
    
    get list() {
        return this._data
    }
}