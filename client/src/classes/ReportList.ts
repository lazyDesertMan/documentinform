export default class ReportList {
    protected _data : any[];

    constructor() {
        this._data = null;
    }

    set list(data) {
        this._data = data
    }
    
    get list() {
        return this._data
    }
}