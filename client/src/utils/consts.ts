// Руков + сотрудники
export const DOCUMENT_ROUTE     : string = '/document'
export const TASKS_ROUTE        : string = '/tasks'
export const PDF_ROUTE          : string = '/pdf'

// Руководитель
export const SEND_ROUTE                 : string = '/send'
export const LISTTASKFORREPORT_ROUTE    : string = '/forreport'
export const TASKSREPORT_ROUTE          : string = '/tasksreport'
export const MYGROUP_ROUTE              : string = '/mygroup'

// Админ
export const ADMIN_OPTIONS_ROUTE        : string = '/admin'
export const ADMIN_FUNCT_ROUTE          : string = ADMIN_OPTIONS_ROUTE + '/functional'
export const ADMIN_DEPUTYFUNC_ROUTE     : string = ADMIN_OPTIONS_ROUTE + '/depFunc'
export const ADMIN_DEPUTYOPTS_ROUTE     : string = ADMIN_OPTIONS_ROUTE + '/depOpts'
export const ADMIN_DELETEPOS_ROUTE      : string = ADMIN_FUNCT_ROUTE + '/deletePos'
export const REGISTRATION_ROUTE         : string = '/registration'

// Директор
export const DIROPTIONS_ROUTE       : string = '/director'
export const DIRFUNCT_ROUTE         : string = '/director/functional'
export const DIRREPORT_ROUTE        : string = '/dirreport'

// Общее
export const PROFILE_ROUTE      : string = '/profile'
export const ERROR_ROUTE        : string = '/error'
export const FEEDBACK_ROUTE     : string = '/feedback'

// Не авториз
export const HOME_ROUTE         : string = '/'

// Cookie
export const ACCESS_TOKEN_COOKIE : string = 'AccessToken'