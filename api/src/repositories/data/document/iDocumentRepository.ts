import Document from "../../../models/document/document";

interface IDocumentRepository {
    list() : Promise<Document[]>;
    find(docsID : number[]) : Promise<Document[]>;
    findOne(docID : number) : Promise<Document>;
    add(doc : Document) : Promise<number>;
    update(docID : number, name : string, description : string, effectiveDate : Date, oldVersionId : number) : Promise<boolean>;
}

export default IDocumentRepository;
