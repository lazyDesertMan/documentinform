import { Pool } from "pg";
import Document from "../../../models/document/document";
import IDocumentRepository from "./iDocumentRepository";

class DBDocumentData {
    public id             : string;   //!< ID документа
    public name           : string;   //!< Название документа
    public description    : string;   //!< Описание документа
    public path           : string;   //!< Путь до файла
    public effective_date : string;   //!< Дата вступления документа в силу
    public parent         : string;   //!< Предыдущая версия
}

class DBFuncReturn { public return_data : string; }  //!< Результат выполнения функции
class UpdateResult { public isUpdated : boolean; }   //!< Результат обновления данных документа

export default class PGDocumentRepository implements IDocumentRepository
{
    private pool: Pool;

    constructor(pool: Pool) {
        this.pool = pool;
    }

    public async list(): Promise<Document[]> {
        const result: Document[] = [];
        const users = await this.pool.query("SELECT * FROM document_view");
        for (let idx = 0; idx < users.rowCount; idx++) {
            const document = users.rows[idx] as DBDocumentData;
            result.push(new Document().init(
                document.id != undefined ? parseInt(document.id) : null,
                document.name != undefined ? document.name : '',
                document.description != undefined ? document.description : '',
                document.path != undefined ? document.path : "",
                document.effective_date != undefined ? new Date(document.effective_date) : null,
                document.parent != undefined ? parseInt(document.parent) : null
            ));
        }
        return result;
    }
    
    public async find(docsID: number[]): Promise<Document[]> {
        const result: Document[] = [];
        const users = await this.pool.query("SELECT * FROM find_documents($1)", [docsID]);
        for (let idx = 0; idx < users.rowCount; idx++) {
            const document = users.rows[idx] as DBDocumentData;
            result.push(new Document().init(
                document.id != undefined ? parseInt(document.id) : null,
                document.name != undefined ? document.name : '',
                document.description != undefined ? document.description : '',
                document.path != undefined ? document.path : "",
                document.effective_date != undefined ? new Date(document.effective_date) : null,
                document.parent != undefined ? parseInt(document.parent) : null
            ));
        }
        return result;
    }

    public async findOne(docID: number): Promise<Document> {
        const doc = await this.find([docID]);
        return doc.length == 1 ? doc[0] : null;
    }

    public async add(doc: Document): Promise<number> {
        const documentData = await this.pool.query(
            "SELECT * FROM add_document($1, $2, $3, $4, $5) AS \"return_data\"",
            [doc.name, doc.description, doc.filePath, doc.effectiveDate, doc.oldVersionId]);
        if (documentData.rowCount != 0) {
            const document = documentData.rows[0] as DBFuncReturn;
            return document.return_data == null ? null : parseInt(document.return_data);
        }
        return null;
    }

    public async update(docID: number, name: string, description: string, effectiveDate: Date, oldVersionId: number): Promise<boolean> {
        const documentData = await this.pool.query(
            "SELECT * FROM update_document($1, $2, $3, $4, $5) AS \"isUpdated\"",
            [docID, name, description, effectiveDate, oldVersionId]);
        if (documentData.rowCount != 0) {
            const document = documentData.rows[0] as UpdateResult;
            return document.isUpdated;
        }
        return false;
    }
}
