import Document from "../../../models/document/document";
import IDocumentRepository from "./iDocumentRepository";


class DbgDocumentRepository implements IDocumentRepository {
    private static documents : Document[] = [
        new Document().init(1, "doc 1", "Документ №1", "doc.pdf", new Date(0), null),
        new Document().init(2, "doc 2", "Документ №2", "doc2.pdf", new Date(1), null),
        new Document().init(3, "doc 3", "Документ №3", "doc3.pdf", new Date(2), null),
    ];
    private static currentIdx = DbgDocumentRepository.documents.length + 1;

    public async list(): Promise<Document[]> {
        return new Promise<Document[]>((resolve) => {
            return resolve([...DbgDocumentRepository.documents])
        });
    }

    public async find(docsID: number[]): Promise<Document[]> {
        const docs : Document[] = [];
        for (let curIdx = 0; curIdx < docsID.length; curIdx++) {
            const curDoc = await this.findOne(docsID[curIdx]);
            if (curDoc != null) {
                docs.push(curDoc);
            }
        }
        return docs;
    }

    public async findOne(docID: number): Promise<Document> {
        return new Promise<Document>((resolve) => {
            for (let idx = 0; idx < DbgDocumentRepository.documents.length; idx++)
                if (DbgDocumentRepository.documents[idx].id === docID)
                    return resolve(DbgDocumentRepository.documents[idx]);
            return resolve(null);
        });
    }

    public async add(doc: Document): Promise<number> {
        return new Promise<number>((resolve) => {
            DbgDocumentRepository.currentIdx++;
            doc.id = DbgDocumentRepository.currentIdx;
            DbgDocumentRepository.documents.push(doc);
            return resolve(DbgDocumentRepository.currentIdx);
        });
    }

    public async update(docID: number, name: string, description: string, effectiveDate: Date, oldVersionId: number): Promise<boolean> {
        const doc = await this.findOne(docID);
        if (doc != null) {
            if (name != null)
                doc.name = name;
            if (description != null)
                doc.description = description;
            if (effectiveDate != null)
                doc.effectiveDate = effectiveDate;
            if (oldVersionId != null)
                doc.oldVersionId = oldVersionId;
            return true;
        }
        return false;
    }
}

export default DbgDocumentRepository;
