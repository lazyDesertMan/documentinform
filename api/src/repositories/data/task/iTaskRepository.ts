import { CompleteFact } from "../../../models/task/completeFact";
import { ITask } from "../../../models/task/iTask";
import { TaskReport } from "../../../models/report/taskReport";
import { TaskDetailsTree } from "../../../models/report/taskDetails";

interface ITaskRepository {
    findByID(id : number) : Promise<ITask>;
    add(task : ITask) : Promise<number>;

    userActiveTasks(userID : number) : Promise<ITask[]>;
    userCompleteTasks(userID : number) : Promise<CompleteFact[]>;
    positionActiveTasks(posID : number) : Promise<ITask[]>;
    positionCompleteTasks(posID : number) : Promise<CompleteFact[]>;
    resendList(positions: number[], startDate : Date, endDate : Date): Promise<ITask[]>;
    
    setCompleted(taskID : number, userID : number) : Promise<void>;
    allowedDocs(userID : number) : Promise<number[]>;

    taskReport(taskID: number) : Promise<TaskReport>;
    documentTasks(documentID : number) : Promise<TaskDetailsTree[]>;
}

export default ITaskRepository;
