import { Pool } from "pg";
import { TaskDetailsTree } from "../../../models/report/taskDetails";
import { TaskReport } from "../../../models/report/taskReport";
import { CompleteFact } from "../../../models/task/completeFact";
import { ITask, TaskType } from "../../../models/task/iTask";
import { ReadTask } from "../../../models/task/readTask";
import { ResendTask } from "../../../models/task/resendTask";
import ITaskRepository from "./iTaskRepository";

class DBFuncReturn {
    public return_data : string;  //!< Результат выполнения функции
}

class AddTaskResult           { public id             : string;            }
class ReadTaskListResult      { public tasks          : ReadTask[];        }
class SingleTaskResult        { public task           : ITask;             }
class ResendTaskListResult    { public tasks          : ResendTask[];      }
class CompletedTaskListResult { public facts          : CompleteFact[];    }
class TaskReportResult        { public taskReport     : TaskDetailsTree;   }
class DocumentTasksResult     { public documentReport : TaskDetailsTree[]; }

export default class PGTaskRepository implements ITaskRepository {
    private pool: Pool;

    constructor(pool: Pool) {
        this.pool = pool;
    }

    protected static fixTaskDate(task : ITask) : void {
        task.startDate = new Date(task.startDate);
        task.deadline = new Date(task.deadline);
    }

    protected static fixFactDate(fact : CompleteFact) : void {
        fact.completedTask.startDate = new Date(fact.completedTask.startDate);
        fact.completedTask.deadline = new Date(fact.completedTask.deadline);
        fact.completeDate = new Date(fact.completeDate);
    }

    protected async addReadTask(task : ReadTask) : Promise<number> {
        const taskData = await this.pool.query<AddTaskResult>(
            "SELECT * FROM add_read_task($1, $2, $3, $4, $5, $6) AS \"id\"",
            [task.documentID, task.senderID, task.recipient, task.startDate, task.deadline, task.prevTask]
        );
        return taskData.rowCount != 0 ? parseInt(taskData.rows[0].id) : null;
    }

    protected async addResendTask(task : ResendTask) : Promise<number> {
        const taskData = await this.pool.query<AddTaskResult>(
            "SELECT * FROM add_resend_task($1, $2, $3, $4, $5, $6) AS \"id\"",
            [task.documentID, task.senderID, task.recipient, task.startDate, task.deadline, task.prevTask]
        );
        return taskData.rowCount != 0 ? parseInt(taskData.rows[0].id) : null;
    }

    public async findByID(id: number): Promise<ITask> {
        const tasks = await this.pool.query<SingleTaskResult>("SELECT * FROM find_task_by_id($1) AS \"task\"", [id]);
        if (tasks.rowCount != 0) {
            PGTaskRepository.fixTaskDate(tasks.rows[0].task);
            return tasks.rows[0].task;
        }
        return null;
    }

    public async add(task: ITask): Promise<number> {
        switch(task.type) {
            case TaskType.READ_TASK_TYPE:
                return this.addReadTask(task as ReadTask);
            case TaskType.RESEND_TASK_TYPE:
                return this.addResendTask(task as ResendTask);
            default:
                return null;
        }
    }

    public async userActiveTasks(userID: number): Promise<ITask[]> {
        const tasks = await this.pool.query<ReadTaskListResult>("SELECT * FROM get_user_active_tasks($1) AS \"tasks\"", [userID]);
        if (tasks.rowCount != 0) {
            tasks.rows[0].tasks.forEach(t => PGTaskRepository.fixTaskDate(t));
            return tasks.rows[0].tasks;
        }
        return [];
    }

    public async userCompleteTasks(userID: number): Promise<CompleteFact[]> {
        const facts = await this.pool.query<CompletedTaskListResult>("SELECT * FROM get_user_complete_tasks($1) AS \"facts\"", [userID]);
        if (facts.rowCount > 0) {
            facts.rows[0].facts.forEach(f => PGTaskRepository.fixFactDate(f));
            return facts.rows[0].facts;
        }
        return [];
    }

    public async positionActiveTasks(posID: number): Promise<ITask[]> {
        const tasks = await this.pool.query<ResendTaskListResult>("SELECT * FROM get_position_active_tasks($1) AS \"tasks\"", [posID]);
        if (tasks.rowCount > 0) {
            tasks.rows[0].tasks.forEach(t => PGTaskRepository.fixTaskDate(t));
            return tasks.rows[0].tasks;
        }
        return [];
    }

    public async positionCompleteTasks(posID: number): Promise<CompleteFact[]> {
        const facts = await this.pool.query<CompletedTaskListResult>("SELECT * FROM get_position_complete_tasks($1) AS \"facts\"", [posID]);
        if (facts.rowCount > 0) {
            facts.rows[0].facts.forEach(f => PGTaskRepository.fixFactDate(f));
            return facts.rows[0].facts;
        }
    }

    public async resendList(positions: number[], startDate : Date, endDate : Date): Promise<ITask[]> {
        const tasks = await this.pool.query<ResendTaskListResult>("SELECT * FROM get_resend_tasks($1, $2, $3) AS \"tasks\"", [positions, startDate, endDate]);
        if (tasks.rowCount > 0) {
            for (const curTask of tasks.rows[0].tasks) {
                curTask.startDate = new Date(curTask.startDate);
                curTask.deadline = new Date(curTask.deadline);
            }
            return tasks.rows[0].tasks;
        }
        return [];
    }

    public async setCompleted(taskID: number, userID: number): Promise<void> {
        await this.pool.query("CALL complete_task($1, $2)", [taskID, userID]);
    }

    public async allowedDocs(userID: number): Promise<number[]> {
        const result: number[] = [];
        const docs = await this.pool.query("SELECT * FROM get_allowed_documents($1) AS return_data", [userID]);
        for (let idx = 0; idx < docs.rowCount; idx++) {
            const doc = docs.rows[idx] as DBFuncReturn;
            result.push(parseInt(doc.return_data));
        }
        return result;
    }

    public async taskReport(taskID: number): Promise<TaskReport> {
        const report : TaskReport = new TaskReport();
        report.date = new Date();
        const data = await this.pool.query<TaskReportResult>("SELECT * FROM task_report($1) AS \"taskReport\"", [taskID]);
        if (data.rowCount != 0)
            report.report = data.rows[0].taskReport;
        return report;
    }

    public async documentTasks(documentID : number) : Promise<TaskDetailsTree[]> {
        const data = await this.pool.query<DocumentTasksResult>("SELECT * FROM document_tasks($1) AS \"documentReport\"", [documentID]);
        return data.rowCount != 0 ? data.rows[0].documentReport : [];
    }
}