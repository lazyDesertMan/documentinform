import { CompleteFact } from "../../../models/task/completeFact";
import { ReadTask } from "../../../models/task/readTask";
import { ITask, TaskType } from "../../../models/task/iTask";
import ITaskRepository from "./iTaskRepository";
import { ResendTask } from "../../../models/task/resendTask";
import { TaskReport } from "../../../models/report/taskReport";
import { TaskDetails, TaskDetailsTree, TaskState } from "../../../models/report/taskDetails";

class DbgTaskRepository implements ITaskRepository {
    private static activeTasks : ITask[] = [
        new ReadTask  (1, "doc 1", 1, 1, 6, new Date(Date.now() - 100000), new Date(Date.now() + 100000)),
        new ReadTask  (2, "doc 2", 2, 1, 6, new Date(Date.now() - 100000), new Date(Date.now() - 10)),
        new ResendTask(3, "doc 3", 3, 1, 1, new Date(Date.now() - 100000), new Date(Date.now()))
    ];
    private static completedTasks : CompleteFact[] = [
        new CompleteFact(6, new Date(100), new ReadTask(5, "doc 2", 2, 2, 1, new Date(0), new Date(1000))),
        new CompleteFact(2, new Date(100), new ReadTask(6, "doc 2", 2, 3, 2, new Date(0), new Date(1000)))
    ];
    private static currentIdx : number = DbgTaskRepository.activeTasks.length + DbgTaskRepository.completedTasks.length + 1;

    public async findByID(id: number): Promise<ITask> {
        return new Promise<ITask>((resolve) => {
            for (let idx  = 0; idx < DbgTaskRepository.activeTasks.length; idx++)
                if (DbgTaskRepository.activeTasks[idx].ID == id)
                    return resolve(DbgTaskRepository.activeTasks[idx]);
            for (let idx  = 0; idx < DbgTaskRepository.completedTasks.length; idx++)
                if (DbgTaskRepository.completedTasks[idx].completedTask.ID == id)
                    return resolve(DbgTaskRepository.completedTasks[idx].completedTask);
            return resolve(null);
        });
    }

    public async add(tsk: ITask): Promise<number> {
        return new Promise<number>((resolve) => {
            tsk.ID = DbgTaskRepository.currentIdx++;
            DbgTaskRepository.activeTasks.push(tsk);
            return resolve(tsk.ID);
        });
    }

    public async userActiveTasks(userID: number): Promise<ITask[]> {
        return new Promise<ITask[]>((resolve) => {
            const tasks : ITask[] = [];
            for (let idx  = 0; idx < DbgTaskRepository.activeTasks.length; idx++)
                if (DbgTaskRepository.activeTasks[idx].type == TaskType.READ_TASK_TYPE
                    && (DbgTaskRepository.activeTasks[idx] as ReadTask).recipient == userID)
                    tasks.push(DbgTaskRepository.activeTasks[idx]);
            return resolve(tasks);
        });
    }

    public async userCompleteTasks(userID: number): Promise<CompleteFact[]> {
        return new Promise<CompleteFact[]>((resolve) => {
            const tasks : CompleteFact[] = [];
            for (let idx  = 0; idx < DbgTaskRepository.completedTasks.length; idx++)
                if (DbgTaskRepository.completedTasks[idx].completedTask.type == TaskType.READ_TASK_TYPE
                    && (DbgTaskRepository.completedTasks[idx].completedTask as ReadTask).recipient == userID)
                    tasks.push(DbgTaskRepository.completedTasks[idx]);
            return resolve(tasks);
        });
    }

    public async positionActiveTasks(posID: number): Promise<ITask[]> {
        return new Promise<ITask[]>((resolve) => {
            const tasks : ITask[] = [];
            for (let idx  = 0; idx < DbgTaskRepository.activeTasks.length; idx++)
                if (DbgTaskRepository.activeTasks[idx].type == TaskType.RESEND_TASK_TYPE
                    && (DbgTaskRepository.activeTasks[idx] as ResendTask).recipient == posID)
                    tasks.push(DbgTaskRepository.activeTasks[idx]);
            return resolve(tasks);
        });
    }

    public async positionCompleteTasks(posID: number): Promise<CompleteFact[]> {
        return new Promise<CompleteFact[]>((resolve) => {
            const tasks : CompleteFact[] = [];
            for (let idx  = 0; idx < DbgTaskRepository.completedTasks.length; idx++)
                if (DbgTaskRepository.completedTasks[idx].completedTask.type == TaskType.RESEND_TASK_TYPE
                    && (DbgTaskRepository.completedTasks[idx].completedTask as ResendTask).recipient == posID)
                    tasks.push(DbgTaskRepository.completedTasks[idx]);
            return resolve(tasks);
        });
    }
    
    public async resendList(positions: number[], startDate : Date, endDate : Date): Promise<ITask[]> {
        let tasks : ITask[] = [];
        for (const posID of positions)
            tasks = tasks.concat(
                ((await this.positionActiveTasks(posID))
                .concat((await this.positionCompleteTasks(posID))
                .map(p => p.completedTask))
                .filter(p => p.startDate >= startDate && p.startDate <= endDate)));
        return tasks;
    }

    public async setCompleted(taskID: number, userID : number): Promise<void> {
        return new Promise((resolve) => {
            for (let idx  = 0; idx < DbgTaskRepository.activeTasks.length; idx++)
                if (DbgTaskRepository.activeTasks[idx].ID == taskID) {
                    DbgTaskRepository.completedTasks.push(
                        new CompleteFact(userID, new Date(), DbgTaskRepository.activeTasks.splice(idx, 1)[0])
                    );
                }
            return resolve();
        });
    }

    public async allowedDocs(userID: number): Promise<number[]> {
        return new Promise<number[]>((resolve) => {
            const docs : Set<number> = new Set<number>();
            for (let idx  = 0; idx < DbgTaskRepository.activeTasks.length; idx++)
                if (DbgTaskRepository.activeTasks[idx].type == TaskType.READ_TASK_TYPE
                    && (DbgTaskRepository.activeTasks[idx] as ReadTask).recipient == userID)
                docs.add(DbgTaskRepository.activeTasks[idx].documentID);
            for (let idx  = 0; idx < DbgTaskRepository.completedTasks.length; idx++)
                if (DbgTaskRepository.completedTasks[idx].completedTask.type == TaskType.READ_TASK_TYPE
                    && (DbgTaskRepository.completedTasks[idx].completedTask as ReadTask).recipient == userID)
                docs.add(DbgTaskRepository.completedTasks[idx].completedTask.documentID);
            return resolve(Array.from(docs));
        });
    }

    protected static createTaskInfo(task : ITask, reportCreateDate : Date) {
        return new TaskDetails(task, (task.deadline < reportCreateDate) ? TaskState.TSTAT_DEADLINE_VIOLATION : TaskState.TSTAT_ACTIVE);
    }

    protected findChilds(taskID : number) : number[] {
        return (
            DbgTaskRepository.activeTasks
            .filter(p => p.prevTask == taskID)
            .map(p => p.ID)
        ).concat (
            DbgTaskRepository.completedTasks
            .filter(p => p.completedTask.prevTask == taskID)
            .map(p => p.completedTask.ID)
        );
    }


    protected createTaskTree(taskID : number, reportCreateDate : Date) : TaskDetailsTree {
        const taskData : TaskDetailsTree = new TaskDetailsTree();
        const task = DbgTaskRepository.activeTasks.find(p => p.ID == taskID);
        if (task != undefined) {
            taskData.task = DbgTaskRepository.createTaskInfo(task, reportCreateDate);
        }
        else {
            const completedTask = DbgTaskRepository.completedTasks.find(p => p.completedTask.ID == taskID);
            if (completedTask != undefined) {
                taskData.task = new TaskDetails(completedTask.completedTask, TaskState.TSTAT_COMPLETE)
            }
        }
        taskData.childs = this.findChilds(taskID).map(p => this.createTaskTree(p, reportCreateDate));
        return taskData;
    }

    public async taskReport(taskID: number): Promise<TaskReport> {
        return new Promise((resolve) => {
            const report : TaskReport = new TaskReport();
            report.date = new Date();
            report.report = this.createTaskTree(taskID, report.date);
            return resolve(report);
        });
    }

    public async documentTasks(documentID : number) : Promise<TaskDetailsTree[]> {
        const tree : TaskDetailsTree[] = [];
        const tasks = DbgTaskRepository.activeTasks.filter(task => task.documentID === documentID)
            .concat(DbgTaskRepository.completedTasks.filter(task => task.completedTask.documentID === documentID)
                .map(task => task.completedTask))
        for (const task of tasks)
            tree.push((await this.taskReport(task.ID)).report);
        return tree;
    }
}

export default DbgTaskRepository;
