import { Roles } from "../../../models/user/userData";
import { UserData } from "../../../models/user/userData";

interface IUserRepository {
    list() : Promise<UserData[]>;
    workersList() : Promise<UserData[]>;
    findByID(id : number) : Promise<UserData>;
    auth(login : string, password : string) : Promise<UserData>;
    add(login : string, password : string, salt : string, firstName : string, secondName : string, thirdName : string, role : Roles, email? : string) : Promise<number>;
    delete(id: number): Promise<boolean>;
    update(id: number, firstName: string, secondName : string, thirdName : string, login : string, email : string, removeEmail : boolean) : Promise<boolean>;
    changePassword(userID: number, password: string, salt: string) : Promise<boolean>;
}

export default IUserRepository;
