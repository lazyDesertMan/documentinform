import { Pool } from 'pg';
import { Roles } from '../../../models/user/userData';
import { UserData } from '../../../models/user/userData';
import passwordService from '../../../services/passwordService';
import IUserRepository from './iUserRepository';

class UserResult           { public user      : UserData;   }  //!< Данные пользователя
class ListResult           { public users     : UserData[]; }  //!< Список данных пользователей
class SaltResult           { public salt      : string;     }  //!< Соль
class RegistrationResult   { public id        : number;     }  //!< ID зарегистрированного сотрудника
class DeleteResult         { public isDeleted : boolean;    }  //!< Был ли удалён пользователь
class UpdateResult         { public isUpdated : boolean;    }  //!< Были ли обновлены данные пользователя
class ChangePasswordResult { public isChanged : boolean;    }  //!< Был ли изменён пароль пользователя

class PGUserRepository implements IUserRepository {
    private pool: Pool;

    constructor(pool: Pool) {
        this.pool = pool;
    }

    public async list(): Promise<UserData[]> {
        const users = await this.pool.query<ListResult>("SELECT * FROM get_users() as users");
        return users.rowCount != 0 ? users.rows[0].users : [];
    }

    public async workersList(): Promise<UserData[]> {
        const users = await this.pool.query<ListResult>("SELECT * FROM get_workers() AS users");
        return users.rowCount != 0 ? users.rows[0].users : [];
    }

    public async findByID(id: number): Promise<UserData> {
        const users = await this.pool.query<UserResult>("SELECT * FROM find_user_by_id($1) as \"user\"", [id]);
        return users.rowCount == 1 ? users.rows[0].user : null;
    }

    public async auth(login: string, password: string): Promise<UserData> {
        const saltReq = await this.pool.query<SaltResult>("SELECT * FROM get_salt($1) AS salt", [login]);
        if (saltReq.rowCount != 0 && saltReq.rows[0].salt) {
            const hashedPassword = passwordService.hash(password, saltReq.rows[0].salt);
            const userReq = await this.pool.query<UserResult>("SELECT * FROM user_authorization($1, $2) as \"user\"", [login, hashedPassword]);
            return userReq.rowCount != 0 ? userReq.rows[0].user : null;
        }
        return null;
    }

    public async add(login : string, password : string, salt : string, firstName : string, secondName : string, thirdName : string, role : Roles, email? : string): Promise<number> {
        const user = await this.pool.query<RegistrationResult>(
            "SELECT * FROM registration($1, $2, $3, $4, $5, $6, $7, $8) AS \"id\"",
            [firstName, secondName, thirdName, login, password, salt, role, email ?? null]
        );
        return user.rowCount != 0 ? user.rows[0].id : null;
    }

    public async delete(id: number): Promise<boolean> {
        const resp = await this.pool.query<DeleteResult>("SELECT * FROM remove_user($1) AS \"isDeleted\"", [id]);
        return resp.rowCount != 0 ? resp.rows[0].isDeleted : false;
    }

    public async changePassword(userID: number, password: string, salt: string): Promise<boolean> {
        const positionData = await this.pool.query<ChangePasswordResult>("SELECT * FROM change_password($1, $2, $3) AS \"isChanged\"", [userID, password, salt]);
        return positionData.rowCount != 0 ? positionData.rows[0].isChanged : false;
    }

    public async update(id: number, firstName: string, secondName : string, thirdName : string, login: string, email : string, removeEmail : boolean): Promise<boolean> {
        const positionData = await this.pool.query<UpdateResult>(
            "SELECT * FROM update_user($1, $2, $3, $4, $5, $6, $7) AS \"isUpdated\"", 
            [id, firstName, secondName, thirdName, login, email, removeEmail]
        );
        return positionData.rowCount != 0 ? positionData.rows[0].isUpdated : false;
    }
}

export default PGUserRepository;
