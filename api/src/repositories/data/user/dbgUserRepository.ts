import { Roles } from '../../../models/user/userData';
import { UserData } from '../../../models/user/userData';
import IUserRepository from './iUserRepository';

class DbgUserRepository implements IUserRepository {
    private static users : UserData[] = [
        new UserData().init(1,  "Director",     Roles.ROLE_DIRECTOR, "Director", "", ""),
        /// Руководство
        new UserData().init(2,  "Leader_A",     Roles.ROLE_WORKER, "Leader", "A",     ""),
        new UserData().init(3,  "Leader_A_1",   Roles.ROLE_WORKER, "Leader", "A",     "1"),
        new UserData().init(4,  "Leader_A_1_1", Roles.ROLE_WORKER, "Leader", "A",     "1.1"),
        new UserData().init(5,  "Leader_B",     Roles.ROLE_WORKER, "Leader", "B",     ""),
        /// Работники
        new UserData().init(6,  "User_A.1",     Roles.ROLE_WORKER, "User",   "A",     "1"),
        new UserData().init(7,  "User_A.2",     Roles.ROLE_WORKER, "User",   "A",     "2"),
        new UserData().init(8,  "User_A_1.1",   Roles.ROLE_WORKER, "User",   "A.1",   "1"),
        new UserData().init(9,  "User_A_1.2",   Roles.ROLE_WORKER, "User",   "A.1",   "2"),
        new UserData().init(10, "User_A_1_1.1", Roles.ROLE_WORKER, "User",   "A.1.1", "1"),
        new UserData().init(11, "User_A_1_1.2", Roles.ROLE_WORKER, "User",   "A.1.1", "2"),
        new UserData().init(12, "User_B.1",     Roles.ROLE_WORKER, "User",   "B",     "1"),
        new UserData().init(13, "User_B.2",     Roles.ROLE_WORKER, "User",   "B",     "2"),
        new UserData().init(14, "Freeman",      Roles.ROLE_WORKER, "Вася",   "",      "Вольный"),
        /// Администраторы
        new UserData().init(15, "Admin",        Roles.ROLE_ADMIN,  "Admin",  "",      "")
    ];
    
    private static id : number = DbgUserRepository.users.length + 1;

    public async list(): Promise<UserData[]> {
        return new Promise<UserData[]>((resolve) => {
            return resolve([...DbgUserRepository.users]);
        });
    }

    public async workersList(): Promise<UserData[]> {
        return new Promise<UserData[]>((resolve) => {
            const users = [...DbgUserRepository.users];
            for (let idx = 0; idx < users.length; idx++) {
                if (users[idx].role !== Roles.ROLE_WORKER) {
                    users.splice(idx, 1);
                }
            }
            return resolve(users);
        });
    }

    public async findByID(id: number): Promise<UserData> {
        return new Promise<UserData>((resolve) => {
            for (let idx  = 0; idx < DbgUserRepository.users.length; idx++) {
                if (DbgUserRepository.users[idx].id == id)
                    return resolve(DbgUserRepository.users[idx]);
            }
            resolve(null);
        });
    }

    public async auth(login: string, password: string): Promise<UserData> {
        return new Promise<UserData>((resolve) => {
            for (let idx  = 0; idx < DbgUserRepository.users.length; idx++) {
                if (DbgUserRepository.users[idx].login == login || DbgUserRepository.users[idx].login == password)
                    return resolve(DbgUserRepository.users[idx]);
            }
            return resolve(null);
        });
    }

    public async add(login: string, password: string, salt : string, firstName : string, secondName : string, thirdName : string, role: Roles, email? : string): Promise<number> {
        return new Promise<number>((resolve) => {
            const userID : number = DbgUserRepository.id++;
            DbgUserRepository.users.push(new UserData().init(userID, login, role, firstName, secondName, thirdName, email));
            return resolve(userID);
        })
    }

    public async changePassword(userID: number, password: string, salt: string): Promise<boolean> {
        return new Promise<boolean>((resolve) => resolve(true));
    }

    public async delete(id: number): Promise<boolean> {
        return new Promise<boolean>((resolve) => {
            for (let idx  = 0; idx < DbgUserRepository.users.length; idx++)
                if (DbgUserRepository.users[idx].id == id) {
                    DbgUserRepository.users.splice(idx, 1);
                    return resolve(true);
                }
            return resolve(false);
        });
    }

    public async update(id: number, firstName: string, secondName : string, thirdName : string, login: string, email : string, removeEmail : boolean): Promise<boolean> {
        const user = await this.findByID(id);
        if (user != null) {
            if (typeof firstName === 'string' && firstName.length > 0)
                user.firstName = firstName;
            if (typeof secondName === 'string' && secondName.length > 0)
                user.secondName = secondName;
            if (typeof thirdName === 'string' && firstName.length > 0)
                user.thirdName = thirdName;
            if (login != null && typeof login === 'string' && login.length > 0)
                user.login = login;
            if (removeEmail)
                user.email = null;
            else if (typeof email === 'string' && email.length > 0)
                user.email = email;
        }
        return false;
    }
}

export default DbgUserRepository;
