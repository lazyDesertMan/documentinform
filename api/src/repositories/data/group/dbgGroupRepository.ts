import { Group, GroupHierarchy } from "../../../models/organization/group";
import Queue from "../../../models/Queue";
import IGroupRepository from "./iGroupRepository";

class DbgGroupRepository implements IGroupRepository {
    private static groups: Group[] = [
        new Group().init(0, 1, null, "Группа A"),
        new Group().init(1, 2, 0, "Группа A.1"),
        new Group().init(2, 3, 1, "Группа A.1.1"),
        new Group().init(3, 4, null, "Группа B"),
    ];
    private static currentIdx = 5;

    protected findChilds(parentID: number): number[] {
        const childs: number[] = [];
        for (let idx = 0; idx < DbgGroupRepository.groups.length; idx++) {
            if (DbgGroupRepository.groups[idx].parentID === parentID)
                childs.push(DbgGroupRepository.groups[idx].id);
        }
        return childs;
    }

    public async groupList(): Promise<Group[]> {
        return new Promise<Group[]>((resolve) => {
            return resolve([...DbgGroupRepository.groups]);
        });
    }

    public async hierarchyList(): Promise<GroupHierarchy[]> {
        const groups: GroupHierarchy[] = [];
        for (let idx = 0; idx < DbgGroupRepository.groups.length; idx++)
            if (DbgGroupRepository.groups[idx].parentID === null)
                groups.push(await this.getHierarchy(DbgGroupRepository.groups[idx].id));
        return groups;
    }

    public async getHierarchy(groupID: number): Promise<GroupHierarchy> {
        const hierarchy: GroupHierarchy = new GroupHierarchy().init(await this.findByID(groupID), []);
        const childs = this.findChilds(groupID);
        for (let idx = 0; idx < childs.length; idx++)
            hierarchy.subgroups.push(await this.getHierarchy(childs[idx]));
        return hierarchy;
    }

    public async add(group: Group, parentID: number): Promise<number> {
        group.id = DbgGroupRepository.currentIdx++;
        group.parentID = (await this.findByID(parentID)) != null ? parentID : null
        DbgGroupRepository.groups.push(group);
        return group.id;
    }

    public async update(groupID: number, leaderID: number, parentID: number, name: string): Promise<boolean> {
        const group = await this.findByID(groupID);
        if (group != null) {
            if (leaderID != null)
                group.leaderPositionID = leaderID;
            if (parentID != null)
                group.parentID = parentID;
            if (name != null && name != '')
                group.name = name;
            return true;
        }
        return false;
    }

    public async remove(id: number): Promise<boolean> {
        return new Promise((resolve) => {
            const groupIdx = DbgGroupRepository.groups.findIndex(p => p.id == id);
            if (groupIdx != -1) {
                DbgGroupRepository.groups.forEach(p => {
                    if (p.parentID == id)
                        p.parentID = null;
                });
                DbgGroupRepository.groups.splice(groupIdx, 1);
                return resolve(true);
            }
            return resolve(false);
        });
    }

    public async findByID(id: number): Promise<Group> {
        return new Promise((resolve) => {
            for (let idx = 0; idx < DbgGroupRepository.groups.length; idx++) {
                if (DbgGroupRepository.groups[idx].id === id)
                    return resolve(DbgGroupRepository.groups[idx]);
            }
            return resolve(null);
        });
    }

    public async subgroups(groupID: number): Promise<Group[]> {
        return new Promise((resolve) => {
            return resolve(DbgGroupRepository.groups.filter(p => p.parentID == groupID));
        });
    }

    public async findByLeader(leaderID: number): Promise<number> {
        return new Promise<number>((resolve) => {
            const group = DbgGroupRepository.groups.find(p => p.leaderPositionID == leaderID);
            return resolve(group == undefined ? null : group.id);
        })
    }

    public async topLeaders(): Promise<number[]> {
        return new Promise(
            (resolve) => resolve(DbgGroupRepository.groups
               .filter(group => group.parentID == null)
               .map(group => group.leaderPositionID)
            )
        )
    }

    public async subordinateGroupsByList(leaderPositions: number[]): Promise<number[]> {
        const groups : number[] = [];
        for (const curPos of leaderPositions)
            groups.push(await this.findByLeader(curPos));
        const queue : Queue<number> = new Queue<number>(groups);
        while(!queue.empty()) {
            const subGroups = this.findChilds(queue.pop());
            queue.pushAll(subGroups);
            groups.push(...subGroups);
        }
        return groups;
    }

    public async isLeader(idList: number[]): Promise<boolean> {
        return new Promise((resolve) => {
            for (const group of DbgGroupRepository.groups)
                if (idList.indexOf(group.id) != -1)
                    return resolve(true);
            return resolve(false);
        });
    }
}

export default DbgGroupRepository;
