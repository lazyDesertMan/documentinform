import { Group, GroupHierarchy } from "../../../models/organization/group";

interface IGroupRepository {
    /**
     * Получение списка групп
     */
    groupList() : Promise<Group[]>;

    /**
     * Получение полной иерархии всех групп
     */
    hierarchyList() : Promise<GroupHierarchy[]>;

    /**
     * Добавление группы
     * @param group Данные о группу
     * @param parentID Родительская группа
     * @returns ID добавленной группы
     */
    add(group : Group, parentID : number) : Promise<number>;

    /**
     * Поиск группы по ID
     * @param id 
     * @returns Найденная группа (null, если ничего не найдено)
     */
    findByID(id : number) : Promise<Group>;

    /**
     * Получение иерархии группы с заданным ID
     * @param groupID ID группы, для которой запрошена иерархия
     * @returns Иерархия группы (null в GroupHierarchy.group, если группа не существует)
     */
    getHierarchy(groupID : number) : Promise<GroupHierarchy>;

    /**
     * Обновление данных о группе
     * @param groupID ID обновляемой группы 
     * @param leaderID Новый руководитель группы (не обновляется, если передано null)
     * @param parentID Новая родительская группа (не обновляется, если передано null)
     * @param name Новое название группы (не обновляется, если передано null или пустая строка)
     * @returns Существует ли группа с переданным ID (результат не зависит от того, были обновлены данные или нет)
     */
    update(groupID: number, leaderID: number, parentID : number, name : string) : Promise<boolean>;

    /**
     * Удаление группы
     * @param id ID удаляемой группы
     * @returns Была ли удалена группа
     */
    remove(id: number): Promise<boolean>;

    /**
     * Запрос подгрупп группы
     * @param groupID ID группы
     * @returns Список дочерних групп
     */
    subgroups(groupID : number) : Promise<Group[]>;

    /**
     * Поиск группы по ID должности руководителя
     * @param leaderID ID должности руководителя
     * @returns ID найденной группы (null, если должности с заданным ID нет группы или должность не существует)
     */
    findByLeader(leaderID : number) : Promise<number>;

    /**
     * Получение ID всех групп, не являющихся подгруппами
     */
    topLeaders() : Promise<number[]>;

    /**
     * Получение списка подчинённых групп по списку ID должностей руководителей
     * @param leaderPositions ID должностей руководителей
     */
    subordinateGroupsByList(leaderPositions : number[]) : Promise<number[]>;

    /**
     * Проверка, является ли хоть 1 из указанных должностей руководителем группы
     * @param idList ID должностей
     */
    isLeader(idList: number[]): Promise<boolean>;
}

export default IGroupRepository;