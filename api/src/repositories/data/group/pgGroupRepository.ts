import { Pool } from "pg";
import { Group, GroupHierarchy } from "../../../models/organization/group";
import IGroupRepository from "./iGroupRepository";

class DBGroupData {
    public id      : string;   //!< ID группы
    public name    : string;   //!< Название группы
    public leader  : string;   //!< ID должности руководителя группы
    public parent  : string;   //!< ID родительской группы (если есть)
}

class DBFuncReturn        { public return_data   : string; }            //!< Строковый ответ
class HierarchyResult     { public hierarchy     : GroupHierarchy; }    //!< Структура группы
class FullHierarchyResult { public hierarchyList : GroupHierarchy[]; }  //!< Структура организации 
class RemoveGroupResult   { public isDeleted     : boolean; }           //!< Результат удаления группы
class UpdateGroupResult   { public isUpdated     : boolean; }           //!< Результат обновления данных группы
class LeaderSearchResult  { public leader        : number; }            //!< ID руководителя группы
class TopLeadersResult    { public leaders       : number[]; }          //!< ID руководителей групп, не являющихся подгруппами
class SubordinateResult   { public groups        : number[]; }          //!< ID подчинённых групп
class IsLeaderResult      { public isLeader      : boolean }            //!< Является ли руководителем

class PGGroupRepository implements IGroupRepository {
    private pool: Pool;

    constructor(pool: Pool) {
        this.pool = pool;
    }

    public async groupList(): Promise<Group[]> {
        const result: Group[] = [];
        const groups = await this.pool.query("SELECT * FROM group_view");
        for (let idx = 0; idx < groups.rowCount; idx++) {
            const group = groups.rows[idx] as DBGroupData;
            result.push(new Group().init(
                group.id != undefined ? parseInt(group.id) : null,
                group.leader != undefined ? parseInt(group.leader) : null,
                group.parent != undefined ? parseInt(group.parent) : null,
                group.name != undefined ? group.name : "",
            ));
        }
        return result;
    }

    public async hierarchyList(): Promise<GroupHierarchy[]> {
        const groupData = await this.pool.query<FullHierarchyResult>("SELECT * FROM get_full_hierarchy() AS \"hierarchyList\"");
        if (groupData.rowCount != 0)
            return groupData.rows[0].hierarchyList;
        return null;
    }

    public async add(group: Group, parentID: number): Promise<number> {
        const groupData = await this.pool.query("SELECT add_group.add_group AS \"return_data\" FROM add_group($1, $2)", [group.name, parentID]);
        if (groupData.rowCount != 0) {
            const group = groupData.rows[0] as DBFuncReturn;
            return group.return_data == null ? null : parseInt(group.return_data);
        }
        return null;
    }

    public async findByID(id: number): Promise<Group> {
        const group = await this.pool.query<DBGroupData>("SELECT * FROM find_group_by_id($1)", [id]);
        if (group.rowCount === 1) {
            return new Group().init(
                group.rows[0].id != undefined ? parseInt(group.rows[0].id) : null,
                group.rows[0].leader != undefined ? parseInt(group.rows[0].leader) : null,
                group.rows[0].parent != undefined ? parseInt(group.rows[0].parent) : null,
                group.rows[0].name != undefined ? group.rows[0].name : "",
            );
        }
        return null;
    }

    public async getHierarchy(groupID: number): Promise<GroupHierarchy> {
        const groupData = await this.pool.query<HierarchyResult>("SELECT * FROM get_hierarchy($1) AS \"hierarchy\"", [groupID]);
        if (groupData.rowCount != 0)
            return groupData.rows[0].hierarchy;
        return null;
    }

    public async update(groupID: number, leaderID: number, parentID: number, name: string): Promise<boolean> {
        const groupData = await this.pool.query<UpdateGroupResult>(
            "SELECT * FROM update_group($1, $2, $3, $4) AS \"isUpdated\"", [groupID, name, leaderID, parentID]
        );
        if (groupData.rowCount != 0)
            return groupData.rows[0].isUpdated ?? false
        return false;
    }

    public async remove(id: number): Promise<boolean> {
        const groupData = await this.pool.query<RemoveGroupResult>("SELECT * FROM remove_group($1) AS \"isDeleted\"", [id]);
        if (groupData.rowCount != 0)
            return groupData.rows[0].isDeleted ?? false;
        return false;
    }

    public async subgroups(groupID: number): Promise<Group[]> {
        const result: Group[] = [];
        const groups = await this.pool.query<DBGroupData>("SELECT * FROM find_subgroups($1)", [groupID]);
        for (let idx = 0; idx < groups.rowCount; idx++) {
            result.push(new Group().init(
                groups.rows[idx].id != undefined ? parseInt(groups.rows[idx].id) : null,
                groups.rows[idx].leader != undefined ? parseInt(groups.rows[idx].leader) : null,
                groups.rows[idx].parent != undefined ? parseInt(groups.rows[idx].parent) : null,
                groups.rows[idx].name != undefined ? groups.rows[idx].name : "",
            ));
        }
        return result;
    }
    
    public async findByLeader(leaderID: number): Promise<number> {
        const groupData = await this.pool.query<LeaderSearchResult>("SELECT * FROM find_group_by_leader($1) AS \"leader\"", [leaderID]);
        if (groupData.rowCount != 0)
            return groupData.rows[0].leader;
        return null;
    }

    public async topLeaders(): Promise<number[]> {
        const groupData = await this.pool.query<TopLeadersResult>("SELECT * FROM get_top_leaders() AS \"leaders\"");
        if (groupData.rowCount != 0)
            return groupData.rows[0].leaders;
        return [];
    }

    public async subordinateGroupsByList(leaderPositions: number[]): Promise<number[]> {
        const groupData = await this.pool.query<SubordinateResult>("SELECT * FROM get_subordinate_groups($1) AS \"groups\"", [leaderPositions]);
        if (groupData.rowCount != 0)
            return groupData.rows[0].groups;
        return null;
    }

    public async isLeader(idList: number[]): Promise<boolean> {
        const isLeaderFact = await this.pool.query<IsLeaderResult>("SELECT * FROM is_leader($1) AS \"isLeader\"", [idList]);
        if (isLeaderFact.rowCount != 0)
            return isLeaderFact.rows[0].isLeader;
        return false;
    }
}

export default PGGroupRepository;