import { Deputy, Position, TemporaryDeputy } from "../../../models/organization/position";

interface IPositionRepository {
    add(position : Position) : Promise<number>;
    list(): Promise<Position[]>;
    update(posID: number, groupID: number, workerID: number, name: string): Promise<boolean>;
    remove(id: number) : Promise<boolean>;

    addDeputy(posID : number, replacerID : number, startDate: Date, endDate: Date) : Promise<boolean>;
    removeDeputy(posID : number, replacerID : number, removeTemporary : boolean) : Promise<boolean>;
    positionDeputiers(posID : number) : Promise<[TemporaryDeputy[], Deputy[]]>;
    workerDeputies(workerID : number) : Promise<number[]>;

    findByID(posID : number) : Promise<Position>;
    findByIDList(posID : number[]) : Promise<Position[]>;
    getUserPositions(userID: number): Promise<Position[]>;
    freePositions(groupID: number) : Promise<Position[]>;
    groupPositions(groupID: number) : Promise<Position[]>;
    groupPositionsByList(groupIDList: number[]) : Promise<Position[]>;
}

export default IPositionRepository;
