import { Pool } from "pg";
import { Deputy, Position, TemporaryDeputy } from "../../../models/organization/position";
import IPositionRepository from "./iPositionRepository";

class DBPositionData {
    public id        : string;  //!< ID должности
    public name      : string;  //!< Название должности
    public worker_id : string;  //!< ID сотрудника, занимающего должность
    public group_id  : string;  //!< ID группы, к которой относится должность 
}

class DBFuncReturn              { public return_data : string; }
class DeputyListResult          { public deputies    : Deputy[]; }
class TemporaryDeputyListResult { public deputies    : TemporaryDeputy[]; }
class WorkerDeputiesListResult  { public deputies    : number[]; }
class WorkerPositionsListResult { public positions   : Position[]; }
class AddDeputyResult           { public isAdd       : boolean; }
class RemoveDeputyResult        { public isRemove    : boolean; }
class UpdatePositionResult      { public isUpdated   : boolean; }
class RemovePositionResult      { public isRemoved   : boolean; }

export default class PGPositionRepository implements IPositionRepository {
    private pool: Pool;

    constructor(pool: Pool) {
        this.pool = pool;
    }

    protected parsePositionData(data : DBPositionData) : Position {
        return new Position().init(
            data.id != undefined ? parseInt(data.id) : null,
            data.name != undefined ? data.name : "",
            data.group_id != undefined ? parseInt(data.group_id) : null,
            data.worker_id != undefined ? parseInt(data.worker_id) : null
        );
    }

    public async add(position: Position): Promise<number> {
        const positionData = await this.pool.query("SELECT add_position.add_position AS \"return_data\" FROM add_position($1, $2, $3)",
            [position.name, position.groupID, position.workerID]);
        if (positionData.rowCount != 0) {
            const result = positionData.rows[0] as DBFuncReturn;
            return result.return_data == null ? null : parseInt(result.return_data);
        }
        return null;
    }

    public async list(): Promise<Position[]> {
        const result: Position[] = [];
        const positions = await this.pool.query<DBPositionData>("SELECT * FROM position_view");
        for (let idx = 0; idx < positions.rowCount; idx++) {
            result.push(this.parsePositionData(positions.rows[idx]));
        }
        return result;
    }

    public async update(posID: number, groupID: number, workerID: number, name: string): Promise<boolean> {
        const positionData = await this.pool.query<UpdatePositionResult>("SELECT * FROM update_position($1, $2, $3, $4) AS \"isUpdated\"", [posID, name, groupID, workerID]);
        if (positionData.rowCount != 0)
            return positionData.rows[0].isUpdated ?? false;
        return false;
    }

    public async remove(id: number): Promise<boolean> {
        const positionData = await this.pool.query<RemovePositionResult>("SELECT * FROM remove_position($1) AS \"isRemoved\"", [id]);
        if (positionData.rowCount != 0)
            return positionData.rows[0].isRemoved ?? false;
        return false;
    }

    public async addDeputy(posID: number, replacerID: number, startDate: Date, endDate: Date): Promise<boolean> {
        const positionData = await (startDate != null && endDate != null ?
            this.pool.query<AddDeputyResult>("SELECT * FROM add_temporary_deputy($1, $2, $3, $4) AS \"isAdd\"", [posID, replacerID, startDate, endDate])
            : this.pool.query<AddDeputyResult>("SELECT * FROM add_deputy($1, $2) AS \"isAdd\"", [posID, replacerID]));
        if (positionData.rowCount != 0)
            return positionData.rows[0].isAdd ?? false;
        return false;
    }

    public async removeDeputy(posID: number, replacerID: number, removeTemporary: boolean): Promise<boolean> {
        const positionData = await this.pool.query<RemoveDeputyResult>("SELECT * FROM remove_deputy($1, $2, $3) AS \"isRemove\"", [posID, replacerID, removeTemporary]);
        if (positionData.rowCount != 0)
            return positionData.rows[0].isRemove ?? false;
        return false;
    }
    
    public async positionDeputiers(posID: number): Promise<[TemporaryDeputy[], Deputy[]]> {
        const deputyData = await this.pool.query("SELECT * FROM get_deputiers($1) AS \"deputies\"", [posID]);
        const temporaryDeputyData = await this.pool.query("SELECT * FROM get_temporary_deputiers($1) AS \"deputies\"", [posID]);
        let deputies : Deputy[] = [];
        let temporaryDeputies : TemporaryDeputy[] = [];
        if (deputyData.rowCount != 0)
            deputies = (deputyData.rows[0] as DeputyListResult).deputies;
        if (temporaryDeputyData.rowCount != 0)
            temporaryDeputies = (temporaryDeputyData.rows[0] as TemporaryDeputyListResult).deputies;
        return [temporaryDeputies, deputies];
    }


    public async workerDeputies(workerID : number) : Promise<number[]> {
        const positions = await this.pool.query<WorkerDeputiesListResult>("SELECT * FROM worker_deputies($1) AS \"deputies\"", [workerID]);
        if (positions.rowCount != 0)
            return positions.rows[0].deputies;
        return [];
    }

    public async findByID(posID: number): Promise<Position> {
        const positions = await this.pool.query("SELECT * FROM find_position_by_id($1)", [posID]);
        if (positions.rowCount > 0)
            return this.parsePositionData(positions.rows[0] as DBPositionData);
        return null;
    }

    public async findByIDList(posID: number[]): Promise<Position[]> {
        const result: Position[] = [];
        const positions = await this.pool.query("SELECT * FROM find_position_by_id_list($1)", [posID]);
        for (let idx = 0; idx < positions.rowCount; idx++) {
            const position = positions.rows[idx] as DBPositionData;
            result.push(this.parsePositionData(position));
        }
        return result;
    }

    public async getUserPositions(userID: number): Promise<Position[]> {
        const positions = await this.pool.query<WorkerPositionsListResult>("SELECT * FROM get_worker_positions($1) AS \"positions\"", [userID]);
        if(positions.rowCount != 0) {
            return positions.rows[0].positions;
        }
        return [];
    }

    // TODO: Возможно следует зарефакторить эти функции, убрать цикл фор, добавив тип к query<>
    public async freePositions(groupID: number): Promise<Position[]> {
        const result: Position[] = [];
        const positions = await this.pool.query("SELECT * FROM find_free_positions($1)", [groupID]);
        for (let idx = 0; idx < positions.rowCount; idx++) {
            const position = positions.rows[idx] as DBPositionData;
            result.push(this.parsePositionData(position));
        }
        return result;
    }

    public async groupPositions(groupID: number): Promise<Position[]> {
        const result: Position[] = [];
        const positions = await this.pool.query("SELECT * FROM get_group_positions($1)", [groupID]);
        for (let idx = 0; idx < positions.rowCount; idx++) {
            const position = positions.rows[idx] as DBPositionData;
            result.push(this.parsePositionData(position));
        }
        return result;
    }

    public async groupPositionsByList(groupIDList: number[]): Promise<Position[]> {
        const result: Position[] = [];
        const positions = await this.pool.query("SELECT * FROM get_group_positions_by_list($1)", [groupIDList]);
        for (let idx = 0; idx < positions.rowCount; idx++) {
            const position = positions.rows[idx] as DBPositionData;
            result.push(this.parsePositionData(position));
        }
        return result;
    }
}