import { Deputy, Position, TemporaryDeputy } from "../../../models/organization/position";
import IPositionRepository from "./iPositionRepository";

class DbgPositionRepository implements IPositionRepository {
    private static positions : Position[] = [
        new Position().init(1,  "Руководитель A",       0, 2),
        new Position().init(2,  "Руководитель A-1",     1, 3),
        new Position().init(3,  "Руководитель A-1.1",   2, 4),
        new Position().init(4,  "Руководитель B",       3, 5),
        new Position().init(5,  "Разнорабочий A 1",     0, 6),
        new Position().init(6,  "Разнорабочий A 2",     0, 7),
        new Position().init(7,  "Разнорабочий A-1 1",   1, 8),
        new Position().init(8,  "Разнорабочий A-1 2",   1, 9),
        new Position().init(9,  "Разнорабочий A-1.1 1", 2, 10),
        new Position().init(10, "Разнорабочий A-1.1 2", 2, 11),
        new Position().init(11, "Разнорабочий B 1",     3, 12),
        new Position().init(12, "Разнорабочий B 2",     3, 13),
        new Position().init(13, "Свободный",            2, null),
    ]
    private static id : number = DbgPositionRepository.positions.length + 1;

    private static tempDeputy : TemporaryDeputy[] = [
        new TemporaryDeputy().init(1, 3, new Date(Date.now()), new Date(Date.now() + 100_000_000))
    ]

    private static deputy : Deputy[] = [
        new Deputy().init(4, 1)
    ]

    public async getUserPositions(userID: number): Promise<Position[]> {
        return new Promise<Position[]>((resolve) => {
            const positions = [];
            for (let idx = 0; idx < DbgPositionRepository.positions.length; idx++)
                if (DbgPositionRepository.positions[idx].workerID === userID) {
                    positions.push(DbgPositionRepository.positions[idx]);
                }
            return resolve(positions);
        });
    }

    public async update(posID: number, groupID: number, workerID: number, name: string): Promise<boolean> {
        return new Promise<boolean>((resolve) => {
            for (let idx = 0; idx < DbgPositionRepository.positions.length; idx++)
                if (DbgPositionRepository.positions[idx].id === posID) {
                    if (groupID != null)
                        DbgPositionRepository.positions[idx].groupID = groupID;
                    if (workerID != null)
                        DbgPositionRepository.positions[idx].workerID = workerID;
                    if (name != null)
                        DbgPositionRepository.positions[idx].name = name;
                    return resolve(true);
                }
        });
    }

    public async setPosition(userID: number, posID: number): Promise<void> {
        return new Promise<void>((resolve) => {
            for (let idx = 0; idx < DbgPositionRepository.positions.length; idx++)
                if (DbgPositionRepository.positions[idx].id === posID) {
                    DbgPositionRepository.positions[idx].workerID = userID;
                    return resolve();
                }
        });
    }

    public async findByID(posID: number): Promise<Position> {
        return new Promise<Position>((resolve) => {
            for (let idx = 0; idx < DbgPositionRepository.positions.length; idx++)
                if (DbgPositionRepository.positions[idx].id === posID)
                    return resolve(DbgPositionRepository.positions[idx]);
            return resolve(null);
        });
    }

    public async findByIDList(posID: number[]): Promise<Position[]> {
        const positions : Position[] = [];
        for (const id of posID) {
            positions.push(await this.findByID(id));
        }
        return positions;
    }

    public async add(position: Position): Promise<number> {
        return new Promise<number>((resolve) => {
            position.id = DbgPositionRepository.id++;
            DbgPositionRepository.positions.push(position);
            return resolve(position.id);
        });
    }

    public async list(): Promise<Position[]> {
        return new Promise<Position[]>((resolve) => {
            return resolve([...DbgPositionRepository.positions]);
        });
    }

    public async remove(id: number): Promise<boolean> {
        return new Promise<boolean>((resolve) => {
            for (let idx  = 0; idx < DbgPositionRepository.positions.length; idx++)
                if (DbgPositionRepository.positions[idx].id == id) {
                    DbgPositionRepository.positions.splice(idx, 1);
                    return resolve(true);
                }
            return resolve(false);
        });
    }

    public async setGroup(posID: number, groupID: number): Promise<boolean> {
        return new Promise<boolean>((resolve) => {
            DbgPositionRepository.positions.find(p => p.id == posID) != undefined;
            for (let idx  = 0; idx < DbgPositionRepository.positions.length; idx++)
                if (DbgPositionRepository.positions[idx].id == posID) {
                    DbgPositionRepository.positions[idx].groupID = groupID;
                    return resolve(true);
                }
            return resolve(false);
        });
    }

    public async freePositions(groupID: number): Promise<Position[]> {
        return new Promise<Position[]>((resolve) => {
            return resolve(DbgPositionRepository.positions
                .filter(p => p.groupID == groupID && p.workerID == null));
        });
    }

    public async groupPositions(groupID: number): Promise<Position[]> {
        return new Promise<Position[]>((resolve) => {
            return resolve(DbgPositionRepository.positions
                .filter(p => p.groupID == groupID));
        });
    }

    public async groupPositionsByList(groupIDList: number[]): Promise<Position[]> {
        const positions : Position[] = [];
        for (const curGroup of groupIDList)
            positions.push(...(await this.groupPositions(curGroup)));
        return positions;
    }

    public async addDeputy(posID: number, replacerID: number, startDate: Date, endDate: Date): Promise<boolean> {
        return new Promise<boolean>((resolve) => {
            // Поиск существующего замещения с теми же ID должностей или обратного замещения
            // (чтобы не создавать петель)
            const existingDeputies = DbgPositionRepository.deputy
                .filter(d => (d.position == posID && d.replacer == replacerID)
                || (d.position == posID && d.replacer == replacerID));
            if (existingDeputies.length == 0) {
                if (startDate != null && endDate != null)
                    DbgPositionRepository.tempDeputy.push(new TemporaryDeputy().init(posID, replacerID, startDate, endDate));
                else
                    DbgPositionRepository.deputy.push(new Deputy().init(posID, replacerID));
                return resolve(true);
            }
            return resolve(false);
        });
    }

    public async removeDeputy(posID: number, replacerID: number, removeTemporary: boolean): Promise<boolean> {
        return new Promise<boolean>((resolve) => {
            for (let idx  = 0; idx < DbgPositionRepository.deputy.length; idx++)
                if (DbgPositionRepository.deputy[idx].position === posID && DbgPositionRepository.deputy[idx].replacer === replacerID)
                    DbgPositionRepository.deputy.splice(idx, 1);
            if (removeTemporary)
                for (let idx  = 0; idx < DbgPositionRepository.tempDeputy.length; idx++)
                    if (
                        DbgPositionRepository.tempDeputy[idx].position === posID
                        && DbgPositionRepository.tempDeputy[idx].replacer === replacerID
                        && DbgPositionRepository.tempDeputy[idx].endDate <= new Date()
                    )
                        DbgPositionRepository.tempDeputy.splice(idx, 1);
            return resolve(true);
        });
    }

    public async positionDeputiers(posID: number): Promise<[TemporaryDeputy[], Deputy[]]> {
        return new Promise<[TemporaryDeputy[], Deputy[]]>((resolve) => {
            const temporaryDeputies = DbgPositionRepository.tempDeputy.filter(d => d.position === posID);
            const deputies = DbgPositionRepository.deputy.filter(d => d.position === posID);
            return resolve([temporaryDeputies, deputies]);
        });
    }

    public async workerDeputies(workerID: number): Promise<number[]> {
        const currentDate = new Date();
        const positions = await this.getUserPositions(workerID);
        const deputies : number[] = []
        for (const position of positions) {
            deputies.push(...DbgPositionRepository.deputy.filter(d => d.replacer === position.id).map(d => d.position)
            .concat(
                ...DbgPositionRepository.tempDeputy
                .filter(d => d.replacer === position.id
                    && d.startDate <= currentDate
                    && d.endDate >= currentDate)
                .map(d => d.position)
            ));
        }
        return deputies;
    }
}

export default DbgPositionRepository;
