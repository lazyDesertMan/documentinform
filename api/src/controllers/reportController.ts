import express = require("express");
import { isSelfOrSubordinate } from "../middlewares/accessMiddlewares";
import { checkRole } from "../middlewares/roleCheckMiddleware";
import { Roles } from "../models/user/userData";
import GroupService from "../services/groupService";
import ReportService from "../services/reportService";
import UserService from "../services/userService";

/**
 * Параметры запроса отчёта самоконтроля
 */
class selfcontrolParams {
    public userID : string;   //!< ID пользователя, по заданиям которого формируется отчёт
}

/**
 * Параметры запроса отчёта по заданию
 */
 class taskParams {
    public taskID : string;   //!< ID задания, по которому формируется отчёт
}

/**
 * Параметры запроса отчёта по документу
 */
 class documentParams {
    public documentID : string;   //!< ID документа, по которому формируется отчёт
}

/**
 * Контроллер, обрабатывающий запросы отчётов
 */
class ReportController {
    public router: express.Router;

    constructor(reportService: ReportService, userService: UserService, groupService: GroupService) {
        this.router = express.Router();

        ///
        /// Запрос отчёта самоконтроля
        ///
        this.router.get("/selfcontrol/:userID", isSelfOrSubordinate(groupService), async (req: express.Request<selfcontrolParams>, res: express.Response) => {
            try {
                const userID = parseInt(req.params.userID);
                if (!isNaN(userID)) {
                    const user = await userService.findUserByID(userID);
                    if (user)
                        res.send(await reportService.getSelfcontrolReport(user));
                    else
                        res.sendStatus(404);
                }
                else
                    res.sendStatus(404);
            }
            catch(e) {
                console.error("Ошибка формирования отчёта самоконтроля: ", e);
                res.sendStatus(404);
            }
        });

        ///
        /// Запрос отчёта по заданию
        ///
        this.router.get("/task/:taskID", async (req: express.Request<taskParams>, res: express.Response) => {
            try {
                const taskID = parseInt(req.params.taskID);
                if (!isNaN(taskID)) {
                    res.send(await reportService.getTaskReport(taskID));
                }
                else
                    res.sendStatus(404);
            }
            catch(e) {
                console.error("Ошибка формирования отчёта по заданию: ", e);
                res.sendStatus(404);
            }
        });

        ///
        /// Запрос отчёта по документу
        ///
        this.router.get("/document/:documentID", checkRole([Roles.ROLE_DIRECTOR]), async (req: express.Request<documentParams>, res: express.Response) => {
            try {
                const docID = parseInt(req.params.documentID);
                if (!isNaN(docID)) {
                    res.send(await reportService.getDocumentReport(docID));
                }
                else
                    res.sendStatus(404);
            }
            catch(e) {
                console.error("Ошибка формирования отчёта по заданию: ", e);
                res.sendStatus(404);
            }
        });
    }
}

export default ReportController;