import express = require('express');
import TaskService from '../services/taskService';
import UserService from '../services/userService';
import * as console from 'console';
import { UserData } from '../models/user/userData';
import { ITask, TaskIdData, TaskType } from '../models/task/iTask';
import { CompleteFact } from '../models/task/completeFact';
import { AddReadTaskData, ReadTask } from '../models/task/readTask';
import { AddResendTaskData, ResendTask } from '../models/task/resendTask';
import TypedRequest from '../models/TypedRequest';
import { CookieService } from '../services/cookieService';
import { MailProps } from '../services/mailServeice';

/**
 * Данные запроса списка заданий на пересылку
 */
class ResendTaskBody {
    userID     : number;  //!< ID пользователя, для которого выполняется поиск заданий
    startDate? : string;  //!< Минимальная дата выдачи задания
    endDate?   : string;  //!< Максимальная дата выдачи задания
}

/**
 * Контроллер, обрабатывающий запросы заданий
 */
class TaskController {
    public router: express.Router;
    constructor(mailProps : MailProps, taskService: TaskService) {
        this.router = express.Router();

        ///
        /// Запрос списка активных заданий авторизованного пользователя
        ///
        this.router.get("/active", async (req: express.Request, res: express.Response) => {
            try {
                const user: UserData = CookieService.getActiveUser(req);
                const tasks: ITask[] = await taskService.activeTaskList(user);
                res.send(tasks);
            } catch (e) {
                console.error("Ошибка запроса списка активных заданий: ", e);
                res.sendStatus(404);
            }
        });

        ///
        /// Запрос списка выполненных заданий авторизованного пользователя
        ///
        this.router.get("/completed", async (req: express.Request, res: express.Response) => {
            try {
                const user: UserData = CookieService.getActiveUser(req);
                const tasks: CompleteFact[] = await taskService.completedTaskList(user);
                res.send(tasks);
            } catch (e) {
                console.error("Ошибка получения списка выполненных заданий: ", e);
                res.sendStatus(404);
            }
        });

        ///
        /// Запрос списка всех заданий на пересылку для указанного пользователя
        ///
        this.router.post("/resendList", async (req: TypedRequest<ResendTaskBody>, res: express.Response) => {
            try {
                if (typeof req.body.userID === 'number') {
                    const user = req.body.userID;
                    const startDate = req.body.startDate == undefined ? null : new Date(req.body.startDate);
                    const endDate = req.body.endDate == undefined ? null : new Date(req.body.endDate);
                    res.send(await taskService.resendList(user, startDate, endDate));
                }
                else
                    res.sendStatus(404);
            } catch (e) {
                console.error("Ошибка получения списка заданий на пересылку: ", e);
                res.sendStatus(404);
            }
        });

        /// Добавление нового задания
        this.router.post("/add", async (req: TypedRequest<AddReadTaskData | AddResendTaskData>, res: express.Response) => {
            try {
                const user: UserData = CookieService.getActiveUser(req);
                const type: number = parseInt(req.body.type);
                let tsk: ITask;
                if (type === TaskType.READ_TASK_TYPE)
                    tsk = new ReadTask(
                        0, "",
                        parseInt(req.body.document),
                        user.id,
                        parseInt(req.body.recipient),
                        new Date(req.body.start),
                        new Date(req.body.end),
                        req.body.prevTask != undefined ? parseInt(req.body.prevTask) : null);
                else if (type === TaskType.RESEND_TASK_TYPE)
                    tsk = new ResendTask(
                        0, "",
                        parseInt(req.body.document),
                        user.id,
                        parseInt(req.body.recipient),
                        new Date(req.body.start),
                        new Date(req.body.end),
                        req.body.prevTask != undefined ? parseInt(req.body.prevTask) : null);
                else
                    throw new Error("Неизвестный тип задания: " + type.toString());
                tsk.type = type;
                const tskID = await taskService.addTask(mailProps, tsk);
                res.send(tskID.toString());
            } catch (e) {
                console.error("Ошибка добавления задания: ", e);
                res.status(404);
            }
        });

        /// Запись факта выполнения задания
        this.router.post("/complete", async (req: TypedRequest<TaskIdData>, res: express.Response) => {
            try {
                const user: UserData = CookieService.getActiveUser(req);
                const taskID = parseInt(req.body.id);
                await taskService.comleteTask(user, taskID);
                res.sendStatus(200);
            } catch (e) {
                console.error("Ошибка записи выполнения задания: ", e);
                res.sendStatus(404);
            }
        });
    }
}

export default TaskController;
