import express = require('express');
import { checkRole } from '../middlewares/roleCheckMiddleware';
import TypedRequest from '../models/TypedRequest';
import { Roles, UserData } from '../models/user/userData';
import UserService from '../services/userService'

/// Параметры запроса на удаление пользователя
class DeleteRequestParams {
    id : number;
}

/// Параметры запроса на изменение данных пользователя
class UpdateRequestParams {
    id           : number;  //!< ID пользователя
    firstName?   : string;  //!< Новое имя пользователя
    secondName?  : string;  //!< Новое отчество пользователя
    thirdName?   : string;  //!< Новая фамилия пользователя
    login?       : string;  //!< Новый логин
    email?       : string;  //!< Новый E-mail пользователя
    removeEmail? : boolean; //!< Требуется ли удалить E-mail
}

/**
 * Контроллер, обрабатывающий запросы пользователей
 */
class UserController {
    public router: express.Router;

    constructor(userService: UserService) {
        this.router = express.Router();

        ///
        /// Запрос списка работников (без учёта директоров и администраторов)
        ///
        this.router.get("/workers", async (req: express.Request, res: express.Response<UserData[]>) => {
            try {
                res.send(await userService.workersList());
            } catch (e) {
                console.error("Ошибка запроса списка работников: ", e);
                res.sendStatus(404);
            }
        });

        ///
        /// Запрос списка всех зарегистрированных пользователей
        ///
        this.router.get("/list", async (req: express.Request, res: express.Response<UserData[]>) => {
            try {
                res.send(await userService.list());
            } catch (e) {
                console.error("Ошибка запроса списка пользователей: ", e);
                res.sendStatus(404);
            }
        });

        ///
        /// Удаление пользователя
        ///
        this.router.post("/remove", checkRole([Roles.ROLE_ADMIN]), async (req : TypedRequest<DeleteRequestParams>, res: express.Response) => {
            try {
                if (typeof req.body.id === 'number')
                    res.send(await userService.delete(req.body.id));
            } catch (e) {
                console.error("Ошибка запроса списка пользователей: ", e);
                res.sendStatus(404);
            }
        });

        ///
        /// Обвновление данных о пользователе
        ///
        this.router.post("/update", checkRole([Roles.ROLE_ADMIN]), async (req : TypedRequest<UpdateRequestParams>, res: express.Response) => {
            try {
                if (typeof req.body.id === 'number'){
                    const result = await userService.update(req.body.id, req.body.firstName, req.body.secondName, req.body.thirdName, req.body.login, req.body.email, req.body.removeEmail);
                    res.send(result);
                }
            } catch (e) {
                console.error("Ошибка запроса списка пользователей: ", e);
                res.sendStatus(404);
            }
        });
    }
}

export default UserController;
