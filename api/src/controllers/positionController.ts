import express = require("express");
import PositionService from "../services/positionService";
import UserService from "../services/userService";
import { Roles, UserData } from "../models/user/userData";
import TypedRequest from '../models/TypedRequest';
import { checkRole } from "../middlewares/roleCheckMiddleware";
import { CookieService } from "../services/cookieService";

class PositionIdData {
    id : string;
}

class AddPositionData {
    group   : number;
    name    : string;
}

class UpdatePositionData {
    positionID   : number;  //!< ID должности
    name?        : string;  //!< Название должности
    groupID?     : number;  //!< ID группы, к которой относится должность
    workerID?    : number;  //!< ID сотрудника, занимающего должность
}

class addDeputyParams {
    position  : number;    //!< ID замещаемой должность
    replacer  : number;    //!< ID сотрудника-заместителя
    startDate : string;    //!< Дата с которой начинается замещение
    endDate   : string;    //!< Дата завершения замещения
}

class removeDeputyParams {
    position         : number;    //!< ID замещаемой должность
    replacer         : number;    //!< ID должности-заместителя
    withTemporary    : boolean;   //!< Требуется ли удалять действующих временных заместителей
}

class deputiesListParams {
    id : string;    //!< ID должности
}

/**
 * Контроллер, обрабатывающий запросы должностей
 */
export default class PositionController {
    public router: express.Router;
    
    constructor(positionService: PositionService, userService: UserService) {
        this.router = express.Router();

        ///
        /// Запрос должностей авторизованного пользователя
        ///
        this.router.get("/active", async (req: express.Request, res: express.Response) => {
            try {
                const userData: UserData = CookieService.getActiveUser(req);
                if (userData)
                    res.send(await positionService.getPositions(userData.id));
                else
                    res.sendStatus(400);
            } catch(e) {
                console.error("Ошибка запроса должности активного пользователя: ", e);
                res.sendStatus(404);
            }
        });

        ///
        /// Запрос списка всех должностей
        ///
        this.router.get("/list", async (req: express.Request, res: express.Response) => {
            try {
                res.send(await positionService.positionList());
            } catch(e) {
                console.error("Ошибка запроса списка должностей: ", e);
                res.sendStatus(404);
            }
        });

        ///
        /// Добавление новой должности
        ///
        this.router.post("/add", checkRole([Roles.ROLE_ADMIN]), async (req: TypedRequest<AddPositionData>, res: express.Response) => {
            try {
                if (req.body.name != undefined && typeof req.body.group === "number") {
                    const name = req.body.name;
                    const groupID = req.body.group;
                    if (name.length > 0 && !isNaN(groupID))
                        res.send((await positionService.addPosition(name, groupID)).toString());
                    else
                        res.send(400);
                }
                else
                    res.send(400);
            } catch(e) {
                console.error("Ошибка добавления должности: ", e);
                res.sendStatus(404);
            }
        });

        ///
        /// Удаление должности
        ///
        this.router.post("/remove", checkRole([Roles.ROLE_ADMIN]), async (req: TypedRequest<PositionIdData>, res: express.Response) => {
            try {
                if (typeof req.body.id === "number") {
                    const positionID = req.body.id;
                    res.send(await positionService.removePosition(positionID));
                }
                else
                    res.send(400);
            } catch(e) {
                console.error("Ошибка удаления должности: ", e);
                res.sendStatus(404);
            }
        });

        /// Обновление данных о должности
        this.router.post("/update", checkRole([Roles.ROLE_ADMIN]), async (req: TypedRequest<UpdatePositionData>, res: express.Response) => {
            try {
                if (typeof req.body.positionID === "number") {
                    const positionID = req.body.positionID;
                    const groupID = typeof req.body.groupID === "number" ? req.body.groupID : null;
                    const userID = typeof req.body.workerID === "number" ? req.body.workerID : null;
                    const name = req.body.name != undefined ? String(req.body.name) : null;
                    res.send(await positionService.updatePosition(positionID, groupID, userID, name));
                }
                else
                    res.sendStatus(400);
            } catch (e) {
                console.error("Ошибка обновления должности: ", e);
                res.sendStatus(404);
            }
        });

        ///
        /// Запрос списка свободных должностей в группе
        ///
        this.router.get("/groupFree/?:id", checkRole([Roles.ROLE_DIRECTOR, Roles.ROLE_ADMIN]), async (req : express.Request<PositionIdData>, res : express.Response) => {
            try {
                const id = Number(req.params.id);
                if (id != null && id != undefined)
                    res.send(JSON.stringify(await positionService.getFreePositions(id)));
                else
                    res.sendStatus(404);
            } catch (e) {
                console.error("Ошибка запроса списка свободных должностей группы: ", e);
                res.sendStatus(404);
            }
        });

        ///
        /// Запрос списка должностей в группе
        ///
        this.router.get("/groupPositions/?:id", checkRole([Roles.ROLE_DIRECTOR, Roles.ROLE_ADMIN]), async (req : express.Request<PositionIdData>, res : express.Response) => {
            try {
                const id = Number(req.params.id);
                if (id != null && id != undefined)
                    res.send(await positionService.getGroupPositions(id));
                else
                    res.sendStatus(404);
            } catch (e) {
                console.error("Ошибка запроса списка должностей группы: ", e);
                res.sendStatus(404);
            }
        });

        ///
        /// Запрос списка свободных должностей
        ///
        this.router.get("/freeWorkers", checkRole([Roles.ROLE_DIRECTOR, Roles.ROLE_ADMIN]), async (req : express.Request, res : express.Response) => {
            try {
                res.send(await positionService.getFreeWorkers());
            } catch (e) {
                console.error("Ошибка запроса списка свободных должностей: ", e);
                res.sendStatus(404);
            }
        });

        ///
        /// Добавление заместителя
        ///
        this.router.post("/addDeputy", checkRole([Roles.ROLE_ADMIN]), async (req : TypedRequest<addDeputyParams>, res : express.Response) => {
            try {
                if (typeof req.body.position === 'number' && typeof req.body.replacer === 'number') {
                    const posID = req.body.position;
                    const replacerID = req.body.replacer;
                    const startDate : Date = req.body.startDate == undefined ? null : new Date(req.body.startDate);
                    const endDate : Date = req.body.endDate == undefined ? null : new Date(req.body.endDate);
                    res.send(await positionService.addDeputy(posID, replacerID, startDate, endDate));
                }
                else
                    res.sendStatus(400);
            } catch (error) {
                console.error("Ошибка добавления временного заместителя: ", error);
                res.sendStatus(404);
            }
        });
        
        ///
        /// Удаление заместителя
        ///
        this.router.post("/removeDeputy", checkRole([Roles.ROLE_ADMIN]), async (req : TypedRequest<removeDeputyParams>, res : express.Response) => {
            try {
                if (typeof req.body.position === 'number' && typeof req.body.replacer === 'number' && typeof req.body.withTemporary === 'boolean') {
                    res.send(await positionService.removeDeputy(req.body.position, req.body.replacer, req.body.withTemporary));
                }
                else
                    res.sendStatus(400);
            } catch (error) {
                console.error("Ошибка удаления заместителя: ", error);
                res.sendStatus(404);
            }
        });

        ///
        /// Запрос списка заместителей
        ///
        this.router.get("/positionDeputies/:id", async (req : express.Request<deputiesListParams>, res : express.Response) => {
            try {
                const id = parseInt(req.params.id);
                if (!isNaN(id)) {
                    res.send(await positionService.positionDeputiers(id));
                }
                else
                    res.sendStatus(400);
            } catch (error) {
                console.error("Ошибка запроса списка заместителей: ", error);
                res.sendStatus(404);
            }
        });
    }
}
