import express = require("express");
import TypeRequest from "../models/TypedRequest";
import { UserData } from "../models/user/userData";
import { CookieService } from "../services/cookieService";
import MailService, { MailProps } from "../services/mailServeice";

/**
 * Тело запроса на отправку обратной связи
 */
class FeedbackData {
    message  : string; //!< Основной текст сообщения
    subject? : string; //!< Тема сообщения, при наличии
    sender?  : string; //!< Имя отправителя, при наличии
}

export default class FeedbackController {
    public router: express.Router;
    
    constructor(mailProps : MailProps) {
        this.router = express.Router();

        ///
        /// Запрос на отправку обратной связи
        ///
        this.router.post("/send", async (req: TypeRequest<FeedbackData>, res: express.Response) => {
            try {
                const user: UserData = CookieService.getActiveUser(req);
                MailService.sendFeedback(mailProps, user.id, typeof req.body.message === "string" ? req.body.message : "", req.body.sender, req.body.subject);
                res.sendStatus(200);
            } catch (e) {
                console.error("Ошибка отправки обратной связи: ", e);
                res.sendStatus(404);
            }
        });
    }
}
