import express = require('express');
import UserService from '../services/userService';
import * as console from 'console';
import TypedRequest from '../models/TypedRequest';
import { Roles, UserData } from '../models/user/userData';
import { checkRole } from '../middlewares/roleCheckMiddleware';
import { checkPassword } from '../middlewares/passwordCheckMiddleware';
import { CookieService } from '../services/cookieService';

class AuthorizationData {
    login    : string;
    password : string;
}

class RegistrationData {
    login      : string;
    password   : string;
    firstName  : string;
    secondName : string;
    thirdName  : string;
    role       : Roles;
    email?     : string;
}

class ChangePasswordData {
    newPassword : string;
}

/**
 * Контроллер, обрабатывающий запросы авторизации
 */
class AuthController {
    public router : express.Router;
    constructor(userService : UserService) {
        this.router = express.Router();

        ///
        /// Авторизация
        ///
        this.router.post("/login", async (req: TypedRequest<AuthorizationData>, res: express.Response<boolean>) => {
            try {
                const login = String(req.body.login);
                const password = String(req.body.password);
                const isAuth = await userService.login(res, login, password);
                res.send(isAuth);
            } catch (e) {
                console.error("Ошибка в авторизации: ", e);
                res.sendStatus(404);
            }
        });

        ///
        /// Регистрация
        /// 
        this.router.post("/registration", checkRole([Roles.ROLE_ADMIN]), async (req : TypedRequest<RegistrationData>, res : express.Response) => {
            try {
                if (
                    req.body.login != undefined && req.body.login.length > 0
                    && req.body.password != undefined && req.body.password.length > 0
                    && req.body.firstName != undefined && req.body.firstName.length > 0
                    && req.body.secondName != undefined && req.body.secondName.length > 0
                    && req.body.thirdName != undefined && req.body.thirdName.length > 0
                    && req.body.role != undefined
                ) {
                    res.send((await userService.add(req.body.login, req.body.password, req.body.firstName, req.body.secondName, req.body.thirdName, req.body.role, req.body.email)).toString());
                }
                else
                    res.sendStatus(400);
            } catch (e) {
                console.error("Ошибка в регистрации: ", e);
                res.sendStatus(404);
            }
        });

        ///
        /// Изменение пароля пользователя
        ///
        this.router.post("/changePassword", checkPassword(userService), async (req : TypedRequest<ChangePasswordData>, res : express.Response) => {
            try {
                if (req.body.newPassword != undefined && req.body.newPassword != null && req.body.newPassword.length > 0) {
                    const user : UserData = CookieService.getActiveUser(req);
                    res.send((await userService.changePassword(user.id, req.body.newPassword)).toString());
                }
                else
                    res.sendStatus(400);
            }
            catch(e) {
                console.error("Ошибка в изменении пароля: ", e);
                res.sendStatus(404);
            }
        });
    }
}

export default AuthController;
