import express = require('express');
import DocumentService from '../services/documentService';
import UserService from '../services/userService';
import Document, { GetDocumentData, NewDocumentData, UpdateDocumentData } from '../models/document/document';
import { Roles, UserData } from '../models/user/userData';

import * as fs from 'fs';
import TypedRequest from '../models/TypedRequest';
import { checkRole } from '../middlewares/roleCheckMiddleware';
import { CookieService } from '../services/cookieService';

/**
 * Контроллер, обрабатывающий запросы документов
 */
class DocumentController {
    public router : express.Router;

    constructor(documentService : DocumentService, userService : UserService) {
        this.router = express.Router();

        ///
        /// Запрос списка документов, доступных авторизованному пользователю
        ///
        this.router.get("/allowed", async (req: express.Request, res: express.Response<Document[]>) => {
            try {
                const user : UserData = CookieService.getActiveUser(req);
                const docs: Document[] = await documentService.userDocs(user);
                res.send(docs);
            } catch(e) {
                console.error("Ошибка запроса списка доступных документов: ", e);
                res.sendStatus(404);
            }
        });
        
        ///
        /// Добавление нового документа
        ///
        this.router.post("/add", checkRole([Roles.ROLE_DIRECTOR]), async (req: TypedRequest<NewDocumentData>, res: express.Response) => {
            try {
                const curDate = new Date();
                const doc : Document = new Document();
                doc.name = req.body.name != undefined ? req.body.name : "no name";
                doc.description = req.body.description != undefined ? req.body.description : "";
                doc.filePath = curDate.getDate() + "_" + (curDate.getMonth() + 1) + "_" + curDate.getFullYear() + "-" + curDate.getTime() + ".pdf";
                doc.effectiveDate = new Date(req.body.effectiveDate);
                doc.oldVersionId = req.body.oldVersion != undefined ? parseInt(req.body.oldVersion) : null;
                res.send((await documentService.add(doc)).toString());
            } catch(e) {
                console.error("Ошибка добавления нового документа: ", e);
                res.sendStatus(404);
            }
        });

        this.router.post("/update", checkRole([Roles.ROLE_DIRECTOR]), async (req: TypedRequest<UpdateDocumentData>, res: express.Response) => {
            try {
                if (req.body.document != undefined) {
                    const docID = parseInt(req.body.document);
                    const name = req.body.name != undefined && req.body.name != '' ? req.body.name : null;
                    const description = req.body.description != undefined && req.body.name != '' ? req.body.description : null;
                    let effectiveDate = req.body.effectiveDate != undefined && req.body.name != '' ? new Date(req.body.effectiveDate) : null;
                    const oldVersionId = req.body.oldVersion != undefined && req.body.name != '' ? parseInt(req.body.oldVersion) : null;
                    if (effectiveDate != null && isNaN(effectiveDate.getTime()))
                        effectiveDate = null
                    res.send((await documentService.update(docID, name, description, effectiveDate, oldVersionId)).toString());
                }
                else
                    res.sendStatus(400);
            }
            catch (e) {
                console.log("Ошибка обновления документа: ", e);
                res.sendStatus(404);
            }
        });
        
        ///
        /// Загрузка файла, связанного с документом, на сервер
        ///
        this.router.post("/load", checkRole([Roles.ROLE_DIRECTOR]), async (req: express.Request, res: express.Response) => {
            try {
                if (!fs.existsSync(require.main.path + "/files")) {
                    fs.mkdirSync(require.main.path + "/files");
                }
                const id = parseInt(req.get("X-File-Id"));
                const document : Document = await documentService.findByID(id);
                const outStream = fs.createWriteStream(require.main.path + "/files/" + document.filePath, { flags: "a" });
                req.on("close", () => outStream.close());
                req.pipe(outStream);
                res.sendStatus(200);
            } catch(e) {
                console.error("Ошибка загрузки файла: ", e);
                res.sendStatus(404);
            }
        });
        
        ///
        /// Запрос файла, связанного с документом
        ///
        this.router.get("/read/:docID", async (req: express.Request<GetDocumentData>, res: express.Response) => {
            try {
                const user : UserData = CookieService.getActiveUser(req);
                const allowedDocs = await documentService.userDocs(user);
                const docID = parseInt(req.params.docID);
                let fileIdx  = 0;
                while (fileIdx < allowedDocs.length && allowedDocs[fileIdx].id != docID)
                    fileIdx++;
                if (fileIdx != allowedDocs.length) {
                    const path = require.main.path + "/files/" + allowedDocs[fileIdx].filePath;
                    fs.readFile(path, function (err, data) {
                        res.contentType("application/pdf");
                        res.setHeader("Content-Disposition", "inline");
                        res.send(data);
                    });
                }
                else
                    res.send("");
            }
            catch(e) {
                console.error("Ошибка запроса документа на чтение: ", e);
                res.sendStatus(404);
            }
        });
        
        ///
        /// Запрос списка всех документов в системе
        ///
        this.router.get("/list", checkRole([Roles.ROLE_DIRECTOR]), async (req: express.Request, res: express.Response) => {
            try {
                res.send(await documentService.list());
            } catch(e) {
                console.error("Ошибка запроса списка всех документов: ", e);
                res.sendStatus(404);
            }
        });
    }
}

export default DocumentController;
