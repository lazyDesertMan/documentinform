import express = require("express");
import GroupService from "../services/groupService";
import UserService from "../services/userService";
import { Group, GroupHierarchy } from "../models/organization/group";
import TypedRequest from "../models/TypedRequest";
import { checkRole } from "../middlewares/roleCheckMiddleware";
import { Roles } from "../models/user/userData";
import PositionService from "../services/positionService";
import { CookieService } from "../services/cookieService";

class GroupIdData {
    groupID : string;
}

class AddGroupData {
    parent? : number;
    name    : string;
}


class UpdateGroupData {
    public group    : number;   //!< ID группы
    public leader?  : number;   //!< ID должности руководителя группы
    public parent?  : number;   //!< ID родительской группы (если есть)
    public name?    : string;   //!< Название группы
}

class PositionIdData {
    id : string;
}

class GroupRemoveParams {
    id : number;
}

/**
 * Контроллер, обрабатывающий запросы к группам
 */
export default class GroupController {
    public router: express.Router;
    
    constructor(groupService: GroupService, positionService : PositionService, userService: UserService) {
        this.router = express.Router();

        ///
        /// Запрос списка всех групп
        ///
        this.router.get("/list", checkRole([Roles.ROLE_ADMIN, Roles.ROLE_DIRECTOR]), async (req: express.Request, res: express.Response<Group[]>) => {
            try {
                res.send(await groupService.groupList());
            } catch(e) {
                console.error("Ошибка запроса списка групп: ", e);
                res.sendStatus(404);
            }
        });

        ///
        /// Запрос иерархии всех групп
        ///
        this.router.get("/structure", checkRole([Roles.ROLE_ADMIN, Roles.ROLE_DIRECTOR]), async (req: express.Request, res: express.Response<GroupHierarchy[]>) => {
            try {
                res.send(await groupService.groupStructure());
            } catch(e) {
                console.error("Ошибка запроса структуры организации: ", e);
                res.sendStatus(404);
            }
        });

        ///
        /// Запрос сведений о группе
        ///
        this.router.get("/info/:groupID", async (req: express.Request<GroupIdData>, res: express.Response<Group>) => {
            try {
                if (typeof req.params.groupID === "string") {
                    const idx = parseInt(req.params.groupID);
                    if (!isNaN(idx))
                        res.send(await groupService.getGroup(idx));
                    else
                        res.sendStatus(400);
                }
                else
                    res.sendStatus(400);
            } catch(e) {
                console.error("Ошибка запроса сведений о группе: ", e);
                res.sendStatus(404);
            }
        });

        ///
        /// Запрос иерархии группы (группа со всеми подчинёнными подгруппами)
        ///
        this.router.get("/hierarchy/:groupID", checkRole([Roles.ROLE_ADMIN]), async (req: express.Request<GroupIdData>, res: express.Response<GroupHierarchy>) => {
            try {
                if (typeof req.params.groupID === "number") {
                    const idx = req.params.groupID;
                    res.send(await groupService.getGroupHierarchy(idx));
                }
                else
                    res.sendStatus(404);
            } catch(e) {
                console.error("Ошибка запроса иерархии группы: ", e);
                res.sendStatus(404);
            }
        });

        ///
        /// Добавление новой группы
        ///
        this.router.post("/add", checkRole([Roles.ROLE_ADMIN]), async (req: TypedRequest<AddGroupData>, res: express.Response) => {
            try {
                const parent = (typeof req.body.parent === "number") ? req.body.parent : null;
                const group: Group = new Group();
                group.leaderPositionID = null;
                group.name = req.body.name;
                res.send((await groupService.addGroup(group, parent)).toString());
            } catch(e) {
                console.error("Ошибка добавления новой группы: ", e);
                res.sendStatus(404);
            }
        });

        ///
        /// Назначение руководителя группы
        ///
        this.router.post("/update", checkRole([Roles.ROLE_ADMIN]), async (req: TypedRequest<UpdateGroupData>, res: express.Response) => {
            try {
                if (typeof req.body.group === "number") {
                    const groupID = req.body.group;
                    const leaderID = typeof req.body.leader === "number" ? req.body.leader : null;
                    const parentID = typeof req.body.parent === "number" ? req.body.parent : null;
                    const name = typeof req.body.name === "string" ? req.body.name : null;
                    res.send(await groupService.update(groupID, leaderID, parentID, name));
                }
                else
                    res.sendStatus(400);
            }
            catch (e) {
                console.error("Ошибка обновления данных группы: ", e);
                res.sendStatus(404);
            }
        });

        ///
        /// Удаление группы
        ///
        this.router.post("/remove", checkRole([Roles.ROLE_ADMIN]), async (req: TypedRequest<GroupRemoveParams>, res: express.Response) => {
            try {
                if (typeof req.body.id === "number") {
                    res.send(await groupService.remove(req.body.id));
                }
                else
                    res.sendStatus(400);
            }
            catch (e) {
                console.error("Ошибка обновления данных группы: ", e);
                res.sendStatus(404);
            }
        });

        ///
        /// Получение списка подчинённых
        ///
        this.router.get("/subordinateWorkers/:id", async (req: express.Request<PositionIdData>, res: express.Response) => {
            try {
                if (typeof req.params.id === "string") {
                    const id = parseInt(req.params.id);
                    if (!isNaN(id))
                        res.send(await groupService.getSubordinateWorkers(id));
                    else
                        res.sendStatus(400);
                }
                else
                    res.sendStatus(400);
            } catch (e) {
                console.error("Ошибка запроса списка подчинённых сотрудников: ", e);
                res.sendStatus(404);
            }
        });

        ///
        /// Получение списка подчинённых руководителей
        ///
        this.router.get("/subordinateLeaders/:id", async (req: express.Request<PositionIdData>, res: express.Response) => {
            try {
                if (typeof req.params.id === "string") {
                    const id = parseInt(req.params.id);
                    if (!isNaN(id))
                        res.send(await groupService.getSubordinateLeaders(id));
                    else
                        res.sendStatus(400);
                }
                else
                    res.sendStatus(400);
            } catch (e) {
                console.error("Ошибка запроса списка подчинённых руководителей: ", e);
                res.sendStatus(404);
            }
        });

        ///
        /// Получение списка руководителей групп, не являющихся подгруппами
        ///
        this.router.get("/topLeaders", async (req: express.Request, res: express.Response) => {
            try {
                res.send(await groupService.getTopLeaders());
            } catch (e) {
                console.error("Ошибка запроса списка руководителей групп, не являющихся подгруппами: ", e);
                res.sendStatus(404);
            }
        });

        ///
        /// Проверка, является ли авторизованный пользователь руководителем
        ///
        this.router.get("/isLeader", async (req: express.Request, res: express.Response) => {
            try {
                const user = CookieService.getActiveUser(req);
                if (user != null) {
                    const positions = (await positionService.getPositions(user.id)).map(p => p.id);
                    res.send(await groupService.isLeader(positions));
                }
                else
                    res.sendStatus(404);
            } catch (e) {
                console.error("Ошибка запроса списка руководителей групп, не являющихся подгруппами: ", e);
                res.sendStatus(404);
            }
        });
    }
}