import * as express from 'express';
import * as path from 'path';
import * as console from 'console';
import bodyParser = require('body-parser');
import * as cookieParser from 'cookie-parser';
import http = require('http');
import * as cors from 'cors';
import { Configurator, Configs } from './services/configurator';
import DocumentService from './services/documentService';
import GroupService from './services/groupService';
import PositionService from './services/positionService';
import ReportService from './services/reportService';
import TaskService from './services/taskService';
import UserService from './services/userService';
import AuthController from './controllers/authController';
import DocumentController from './controllers/documentController';
import PositionController from './controllers/positionController';
import ReportController from './controllers/reportController';
import TaskController from './controllers/taskController';
import UserController from './controllers/userController';
import GroupController from './controllers/groupController';
import FeedbackController from './controllers/feedbackController';

const app = express();
const server = http.createServer(app);

const corsOptions : cors.CorsOptions = {
    //origin: 'http://localhost:3000',
    origin: function(origin, callback) { callback(null, true) },
    credentials: true,
    optionsSuccessStatus: 200
}
app.use(cors(corsOptions));

app.use(express.static(path.join(__dirname, 'public')));

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cookieParser());

const conf    : Configs = Configurator.config();
const address : string  = process.env.DI_RUN_ADDRESS ? process.env.DI_RUN_ADDRESS : "localhost";
const port    : number  = process.env.DI_RUN_PORT ? parseInt(process.env.DI_RUN_PORT) : 1337;

const documentService : DocumentService = new DocumentService(conf.documentRepository, conf.taskRepository);
const groupService : GroupService = new GroupService(conf.groupRepository, conf.positionRepository, conf.userRepository);
const positionService : PositionService = new PositionService(conf.positionRepository, conf.userRepository);
const reportService : ReportService = new ReportService(conf.positionRepository, conf.taskRepository, conf.documentRepository);
const taskService : TaskService = new TaskService(conf.taskRepository, conf.documentRepository, conf.positionRepository, conf.userRepository);
const userService : UserService = new UserService(conf.userRepository);

const authRouter : AuthController = new AuthController(userService);
const documentRouter : DocumentController = new DocumentController(documentService, userService);
const positionRouter : PositionController = new PositionController(positionService, userService);
const groupRouter : GroupController = new GroupController(groupService, positionService, userService);
const reportRouter : ReportController = new ReportController(reportService, userService, groupService);
const taskRouter : TaskController = new TaskController(conf.mailProps, taskService);
const userRouter : UserController = new UserController(userService);
const feedbackRouter : FeedbackController = new FeedbackController(conf.mailProps);

app.use('/api/account', authRouter.router);
app.use('/api/task', taskRouter.router);
app.use('/api/document', documentRouter.router);
app.use('/api/position', positionRouter.router);
app.use('/api/group', groupRouter.router);
app.use('/api/report', reportRouter.router);
app.use('/api/user', userRouter.router);
app.use('/api/feedback', feedbackRouter.router);

server.listen(port, address, function () {
    console.info('DocumentInform run an ' + address + ':'+ port + '\n');
});
