import { NextFunction, Request, Response } from "express";
import { CookieService } from "../services/cookieService";
import UserService from "../services/userService";

class PasswordData {
    password : string;
}

export function checkPassword(userService : UserService) {
    return async (req : Request, res : Response, next : NextFunction) => {
        const reqData = req.body as PasswordData;
        const activeUser = CookieService.getActiveUser(req);
        if (activeUser && reqData.password) {
            if (await userService.authorization(activeUser.login, reqData.password))
                return next();
            else
                return res.status(400).end();
        }
        else
            return res.status(400).end();
    }
}