import express = require('express');
import { UserData } from '../models/user/userData';
import { CookieService } from '../services/cookieService';

class Cookies {
    AccessToken : string;
}

/*
 * \brief Получение токена из cookie
 */
function getToken<T>(req: express.Request<T>) : string {
    const cookie = req.cookies as Cookies;
    return cookie.AccessToken != undefined ? cookie.AccessToken : null;
}

/**
 * Получение данных пользователя из куки
 * @param req Данные запроса
 * @returns Данные пользователя
 */
function getUser<T>(req: express.Request<T>) {
    const cookie : UserData = CookieService.verify(getToken(req));
    return cookie;
}

/*
 * Проверка, что текущий пользователь находится в указанной роли
 */
export function checkRole<T>(requiredRole : string[]) {
    return (req: express.Request<T>, res: express.Response, next: express.NextFunction) => {
        try {
            const user : UserData = getUser(req);
            let access  = false;
            for (let idx = 0; !access && idx < requiredRole.length; idx++)
                if (requiredRole[idx] == user.role)
                    access = true;
            if (access)
                return next();
            else
                return res.status(403).end();  // 403 - Нет прав доступа
        } catch {
            return res.status(401).end();      // 401 - Не авторизован
        }
    }
}

export {
    getUser
}
