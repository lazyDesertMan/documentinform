import { NextFunction, Request, Response } from "express-serve-static-core";
import { Roles } from "../models/user/userData";
import { CookieService } from "../services/cookieService";
import GroupService from "../services/groupService";
import UserService from "../services/userService";

class MiddlewareParams {
    userID : string;
}

/**
 * Промежуточная функция, выполняющая проверку, является ли userID идентификатором авторизованного пользователя
 * или его подчинённого.
 * Примечание: все сотрудники считаются "подчинёнными" администраторов
 * @param userService Сервис, предоставляющий доступ к данным пользователей
 * @param groupService Сервис, предоставляющий доступ к данным о группах
 */
export function isSelfOrSubordinate(groupService : GroupService) {
    return async (req : Request<MiddlewareParams>, res : Response, next : NextFunction) => {
        const userID = parseInt(req.params.userID);
        const userData = CookieService.getActiveUser(req);
        if (userData.role === Roles.ROLE_ADMIN || userData.id === userID || (await groupService.isSubordinate(userData.id, userID)))
            return next();
        else
            res.sendStatus(404).end();
    }
}