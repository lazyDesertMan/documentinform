class NewDocumentData {
    public name:           string;              //!< Название документа (официальное)
    public description:    string;              //!< Краткое описание документа
    public effectiveDate:  string;              //!< Дата вступления документа в силу
    public oldVersion:     string | undefined;  //!< ID предыдущей версии
}

class UpdateDocumentData {
    public document:       string;              //!< ID документа
    public name:           string | undefined;  //!< Название документа (официальное)
    public description:    string | undefined;  //!< Краткое описание документа
    public filePath:       string | undefined;  //!< Имя файла (с расширением)
    public effectiveDate:  string | undefined;  //!< Дата вступления документа в силу
    public oldVersion:     string | undefined;  //!< ID предыдущей версии
}

class GetDocumentData {
    public docID : string;  //!< ID документа
}

export default class Document {
    public id:             number;  //!< ID документа
    public name:           string;  //!< Название документа (официальное)
    public description:    string;  //!< Краткое описание документа
    public filePath:       string;  //!< Имя файла (с расширением)
    public effectiveDate:  Date;    //!< Дата вступления документа в силу
    public oldVersionId:   number;  //!< ID предыдущей версии

    public init(
        id : number,
        name : string,
        description : string,
        filePath : string,
        effectiveDate : Date,
        oldVersionId : number
    ) : Document {
        this.id = id;
        this.name = name;
        this.description = description;
        this.filePath = filePath;
        this.effectiveDate = effectiveDate;
        this.oldVersionId = oldVersionId;
        return this;
    }
}

export {
    NewDocumentData, UpdateDocumentData, GetDocumentData
}
