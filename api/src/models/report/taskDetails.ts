import { ITask } from "../task/iTask";

/**
 * Перечисление возможных состояний задания
 */
export enum TaskState {
    TSTAT_COMPLETE           = 1,   //!< Задание выполнено
    TSTAT_ACTIVE             = 2,   //!< Задание не выполнено, но сроки выполнения не нарушены
    TSTAT_DEADLINE_VIOLATION = 3    //!< Задание не выполнено и нарушены сроки выполнения задания
}

/**
 * Сведения о задании
 */
export class TaskDetails {
    public readonly task   : ITask;
    public readonly state  : TaskState;

    constructor(task : ITask, state : TaskState) {
        this.task = task;
        this.state = state;
    }
}

/**
 * Дерево сведений о заданиях
 */
export class TaskDetailsTree {
    task   : TaskDetails;        //!< Информация о задании
    childs : TaskDetailsTree[];  //!< Информация о всех дочерних заданиях
}