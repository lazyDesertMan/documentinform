import { TaskDetailsTree } from "./taskDetails";

/**
 * Отчёт по заданию
 */
class TaskReport {
    public report : TaskDetailsTree;  //!< Сведения о задании
    public date   : Date;             //!< Дата формирования отчёта
}

export {
    TaskReport
}