import { TaskDetails } from "./taskDetails";

/**
 * Часть отчёта, содержащая сведения о задания по должности
 */
class PositionRecord {
    public readonly position : string;         //!< Должность пользователя
    public readonly tasks    : TaskDetails[];  //!< Состояния заданий

    constructor(position : string) {
        this.position = position;
        this.tasks = [];
    }

    public add(line : TaskDetails) : void {
        this.tasks.push(line);
    }
}

/**
 * Отчёт о заданиях сотрудника
 */
class SelfcontrolReport {
    public date         : Date;              //!< Дата формирования отчёта
    public userName     : string;            //!< ФИО пользователя
    public records      : PositionRecord[];  //!< Сведения по должностям

    constructor(userName : string) {
        this.date = new Date();
        this.userName = userName;
        this.records = [];
    }

    public add(record : PositionRecord) : void {
        this.records.push(record);
    }
}

export {
    PositionRecord,
    SelfcontrolReport
}