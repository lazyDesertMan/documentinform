import Document from "../document/document";
import { TaskDetailsTree } from "./taskDetails";

/**
 * Отчёт по заданию
 */
class DocumentReport {
    public report   : TaskDetailsTree[];  //!< Сведения о заданиях
    public document : Document;           //!< Сведения о документе, по которому сформирован отчёт
    public date     : Date;               //!< Дата формирования отчёта
}

export {
    DocumentReport
}