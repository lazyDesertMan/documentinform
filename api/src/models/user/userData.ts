/**
 * Перечень допустимых ролей
 */
 enum Roles {
    /** Работник */
    ROLE_WORKER   = "worker",
    /** Директор */
    ROLE_DIRECTOR = "director",
    /** Руководитель */
    ROLE_ADMIN    = "administrator",
}

/*
 * Данные о пользователе системы 
 */
class UserData {
    public id:         number;  //!< ID пользователя
    public login:      string;  //!< Логин пользователя
    public role:       string;  //!< Роль пользователя в системе
    public firstName:  string;  //!< Имя пользователя
    public secondName: string;  //!< Фамилия пользователя
    public thirdName:  string;  //!< Отчество пользователя
    public email?:     string;  //!< Электронная почта

    public constructor() {
        this.id = 0;
        this.login = "";
        this.firstName = "";
        this.secondName = "";
        this.thirdName = "";
        this.role = "";
        this.email = null;
    }

    public init(id : number, login : string, role : string, firstName : string, secondName : string, thirdName : string, email? : string) : UserData {
        this.id = id;
        this.login = login;
        this.role = role;
        this.firstName = firstName;
        this.secondName = secondName;
        this.thirdName = thirdName;
        this.email = email;
        return this;
    }
}

export {
    UserData,
    Roles
}
