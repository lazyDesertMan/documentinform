export default class Queue<T> {
    protected m_data : T[];

    constructor(data : T[] = []) {
        this.m_data = [...data];
    }

    public push(element : T) : void {
        this.m_data.push(element);
    }

    public pushAll(elements : T[]) : void {
        this.m_data.push(...elements);
    }

    public pop() : T {
        return this.m_data.splice(0, 1)[0];
    }

    public empty() : boolean {
        return this.m_data.length === 0;
    }
}