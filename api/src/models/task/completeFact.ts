import { ITask } from "./iTask";


/**
 * Факт выполнения заданий
 */
class CompleteFact {
    public userID        : number; //!< ID пользователя, выполнившего задание
    public completeDate  : Date;   //!< Дата выполнения задания
    public completedTask : ITask;  //!< Данные выполненого задании 

    constructor (userID : number, date : Date, compTask : ITask) {
        this.userID = userID;
        this.completeDate = date;
        this.completedTask = compTask;
    }
}

export {
    CompleteFact
}
