import { ITask, TaskType } from "./iTask";

class AddResendTaskData {
    public readonly document   : string;  //!< ID документа, связанного с заданием
    public readonly recipient  : string;  //!< ID сотрудника, который должен ознакомиться с документом
    public readonly start      : string;  //!< Дата выдачи задания
    public readonly end        : string;  //!< Крайний срок выполнения задания
    public readonly prevTask   : string;  //!< ID предыдущего задания
    public readonly type       : string;  //!< Тип задания
}

/**
 * Задание на пересылку документа
 */
class ResendTask implements ITask {
    public ID         : number;    //!< ID задания
    public document   : string;    //!< Название документа
    public documentID : number;    //!< ID документа, связанного с заданием
    public senderID   : number;    //!< ID должности сотрудника, выдавшего задание
    public recipient  : number;    //!< ID должности, с которой должна проводиться пересылка
    public startDate  : Date;      //!< Дата выдачи задания
    public deadline   : Date;      //!< Крайний срок выполнения задания
    public prevTask   : number;    //!< ID предыдущего задания
    public type       : TaskType;  //!< Тип задания

    constructor (id : number, name : string, doc : number, sender : number, recip : number, start : Date, end : Date, prevTask : number = null) {
        this.ID = id;
        this.document = name;
        this.documentID = doc;
        this.senderID = sender;
        this.recipient = recip;
        this.startDate = start;
        this.deadline = end;
        this.prevTask = prevTask;
        this.type = TaskType.RESEND_TASK_TYPE;
    }
}

export {
    AddResendTaskData, ResendTask
}
