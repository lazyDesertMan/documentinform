import express = require("express");

export default interface TypeRequest<T> extends express.Request {
    body : T;
}
