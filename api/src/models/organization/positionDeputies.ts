import { Deputy, Position, TemporaryDeputy } from "./position";

export class PositionDeputies {
    public position          : Position;          //!< Должность, для которой назначены заместители
    public deputies          : Deputy[];          //!< Постоянные заместители
    public temporaryDeputies : TemporaryDeputy[]; //!< Временные заместители
}