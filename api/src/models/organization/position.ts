/**
 * Данные должности
 */
class Position {
    id       : number;  //!< ID должности
    name     : string;  //!< Название должности
    groupID  : number;  //!< ID группы, к которой относится должность
    workerID : number;  //!< ID сотрудника, занимающего должность

    public init(id : number, name : string, groupID : number, workerID : number) : Position {
        this.id = id;
        this.name = name;
        this.groupID = groupID;
        this.workerID = workerID;
        return this;
    }
}

/**
 * Данные о замещении
 */
class Deputy {
    position  : number;     //!< ID Замещаемой должность
    replacer  : number;     //!< ID сотрудника-заместителя

    public init(position : number, replacer : number) : Deputy {
        this.position = position;
        this.replacer = replacer;
        return this;
    }
}

/**
 * Данные о временном замещении
 */
class TemporaryDeputy {
    position  : number;  //!< ID Замещаемой должность
    replacer  : number;  //!< ID сотрудника-заместителя
    startDate : Date;    //!< Дата с которой начинается замещение
    endDate   : Date;    //!< Дата завершения замещения

    public init(position : number, worker : number, startDate : Date, endDate : Date) : TemporaryDeputy {
        this.position = position;
        this.replacer = worker;
        this.startDate = startDate;
        this.endDate = endDate;
        return this;
    }
}

export { Position, Deputy, TemporaryDeputy }
