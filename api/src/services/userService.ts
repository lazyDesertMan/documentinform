import { Request, Response } from 'express';
import { Roles, UserData } from '../models/user/userData';
import IUserRepository from '../repositories/data/user/iUserRepository';
import { CookieService } from './cookieService';
import passwordService from './passwordService';

export default class UserService {
    protected userRepository : IUserRepository;

    constructor(userRepository : IUserRepository) {
        this.userRepository = userRepository;
    }

    public async authorization(user_login: string, user_password: string) {
        return this.userRepository.auth(user_login, user_password);
    }

    public async login(res : Response, user_login: string, user_password: string) : Promise<boolean> {
        const user = await this.authorization(user_login, user_password);
        if (user != null) {
            const cookie = CookieService.create(user);
            res.cookie("AccessToken", cookie, { maxAge: 10_000_000_000});
            return true;
        }
        return false;
    }

    public async findUserByID(userID : number) : Promise<UserData> {
        return this.userRepository.findByID(userID)
    }

    public async workersList() : Promise<UserData[]> {
        return this.userRepository.workersList();
    }

    public async list() : Promise<UserData[]> {
        return this.userRepository.list();
    }

    public async add(login : string, password : string, firstName : string, secondName : string, thirdName : string, role : Roles, email? : string) : Promise<number> {
        const hashPass = passwordService.createHash(password);
        return this.userRepository.add(login, hashPass.value, hashPass.salt, firstName, secondName, thirdName, role, email);
    }

    public async delete(id: number): Promise<boolean> {
        return this.userRepository.delete(id);
    }

    public async update(id: number, firstName: string, secondName: string, thirdName: string, login: string, email : string, removeEmail : boolean) : Promise<boolean> {
        return this.userRepository.update(id, firstName, secondName, thirdName, login, email, removeEmail);
    }

    public async changePassword(userID: number, password: string) : Promise<boolean>  {
        const hashPass = passwordService.createHash(password);
        return this.userRepository.changePassword(userID, hashPass.value, hashPass.salt);
    }
}