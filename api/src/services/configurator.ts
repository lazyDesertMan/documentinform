import { Pool } from "pg";
import DbgDocumentRepository from "../repositories/data/document/dbgDocumentRepository";
import IDocumentRepository from "../repositories/data/document/iDocumentRepository";
import PGDocumentRepository from "../repositories/data/document/pgDocumentRepository";
import DbgGroupRepository from "../repositories/data/group/dbgGroupRepository";
import IGroupRepository from "../repositories/data/group/iGroupRepository";
import PGGroupRepository from "../repositories/data/group/pgGroupRepository";
import DbgPositionRepository from "../repositories/data/position/dbgPositionRepository";
import IPositionRepository from "../repositories/data/position/iPositionRepository";
import PGPositionRepository from "../repositories/data/position/pgPositionRepository";
import DbgTaskRepository from "../repositories/data/task/dbgTaskRepository";
import ITaskRepository from "../repositories/data/task/iTaskRepository";
import PGTaskRepository from "../repositories/data/task/pgTaskRepository";
import DbgUserRepository from "../repositories/data/user/dbgUserRepository";
import IUserRepository from "../repositories/data/user/iUserRepository";
import PGUserRepository from "../repositories/data/user/pgUserRepository";
import { MailProps } from "./mailServeice";
import * as fs from 'fs';

class Configs {
    public userRepository     : IUserRepository;
    public taskRepository     : ITaskRepository;
    public positionRepository : IPositionRepository;
    public groupRepository    : IGroupRepository;
    public documentRepository : IDocumentRepository;
    public mailProps          : MailProps;
}

class Configurator {
    protected static readonly CONFIG_FILE_PATH : string = "/configs/api.json";

    protected static loadConfigsFromFile() {
        const path  = require.main.path + this.CONFIG_FILE_PATH;
        if (fs.existsSync(path)) {
            const data = JSON.parse(fs.readFileSync(path).toString());
            for (const [key, value] of Object.entries(data))
                process.env[key] = value.toString();
        }
    }

    public static config() : Configs {
        Configurator.loadConfigsFromFile();
        let services : Configs = new Configs;
        let source : string = process.env.DI_DATA_SOURCE;
        if (source === undefined)
            source = "debug";
        switch (source) {
            case "pg": {
                const pool : Pool = new Pool();
                services.documentRepository = new PGDocumentRepository(pool);
                services.groupRepository = new PGGroupRepository(pool);
                services.positionRepository = new PGPositionRepository(pool);
                services.taskRepository = new PGTaskRepository(pool);
                services.userRepository = new PGUserRepository(pool);
                break;
            }
            default: {
                services.documentRepository = new DbgDocumentRepository();
                services.groupRepository = new DbgGroupRepository();
                services.positionRepository = new DbgPositionRepository();
                services.taskRepository = new DbgTaskRepository();
                services.userRepository = new DbgUserRepository();
                break;
            }
        }
        services.mailProps = {
            mailHost: process.env.DI_MAIL_HOST,
            mailPort: process.env.DI_MAIL_PORT,
            senderLogin:process.env.DI_MAIL_SENDER_LOGIN,
            senderPassword:process.env.DI_MAIL_SENDER_PASSWORD,
            mailIp:process.env.DI_MAIL_IP
        };
        return services;
    }
}

export {
    Configurator, Configs
}