import { Request } from 'express-serve-static-core';
import * as jwt from 'jsonwebtoken';
import { UserData } from "../models/user/userData";

class CookieList {
    AccessToken : string;
}

class Cookie {
    user : UserData;
}

/**
 * Класс, отвечающий за создание и верификацию JWT
 */
class CookieService {
    private static readonly JWTKey = 'SecretKey';

    /**
     * Создание токена
     * @param user Данные о пользователе, помещаемые в токен
     * @returns JWT
     */
    public static create(user: UserData) : string {
        return jwt.sign({user}, CookieService.JWTKey, { expiresIn: '7d' });
    }

    /**
     * Верификация jwt
     * @param token Верифицируемый токен
     * @returns Данные токена
     */
    public static verify(token : string) : UserData {
        return (jwt.verify(token, CookieService.JWTKey) as Cookie).user;
    }

    /**
     * Получение JWT из запроса
     * @param req запрос
     * @returns Токен
     */
     protected static getToken<P, res, req> (req: Request<P, res, req>) : string {
        const cookie : CookieList = req.cookies as CookieList;
        const token = cookie.AccessToken;
        return token ? token : "";
    }
    
    /**
     * Получение данных авторизованного пользователя из запроса
     * @param req Запрос
     * @returns Данные пользователя
     */
    public static getActiveUser<P, res, req>(req : Request<P, res, req>) : UserData {
        const token : string = this.getToken(req);
        if (token != "") {
            const user : UserData = CookieService.verify(token);
            return user;
        }
        return null;
    }
}

export {
    CookieService
}
