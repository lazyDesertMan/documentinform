import * as crypto from 'crypto';

class hashedValue {
    public readonly value : string;
    public readonly salt : string;

    public constructor (val : string, slt : string) {
        this.value = val;
        this.salt = slt;
    }

    public toString() : string {
        return this.value + this.salt;
    }
}

class passwordService {

    protected static readonly Key : string = 'secreting';

    public static hash(str : string, salt : string) : string {
        const hashGenerator = crypto.createHmac('sha256', passwordService.Key);
        return hashGenerator.update(str + salt).digest('hex');
    }

    public static createHash(str : string) : hashedValue {
        const salt = crypto.randomBytes(16).toString('hex');
        return new hashedValue(passwordService.hash(str, salt), salt);
    }
}

export default passwordService;

export {
    hashedValue
}
