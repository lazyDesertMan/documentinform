import * as tls from 'tls';
import Queue from '../models/Queue';
import { ITask, TaskType } from "../models/task/iTask";

export class MailProps {
    public mailHost       : string;  //!< Адрес почтового сервиса
    public mailPort       : string;  //!< Порт почтового сервиса
    public mailIp?        : string;  //!< IP почтового сервиса, при наличии
    public senderLogin    : string;  //!< Логин отправителя
    public senderPassword : string;  //!< Пароль отправителя
}

export default class MailService {
    protected static sendMail(props : MailProps, queue : Queue<string[]>) {
        if (props.mailHost && props.mailPort) {
            const opts : tls.ConnectionOptions = {
                host: props.mailHost,
                port: parseInt(props.mailPort)
            };
            const connection = tls.connect(opts);
            connection.on('data', (resp : ArrayBuffer) => {
                const respData = resp.toString();
                if(!queue.empty() && respData.length !== 0 && respData[0] === '2' || respData[0] === '3') {
                    const line = queue.pop();
                    for (const cmd of line) {
                        connection.write(cmd + "\n");
                    }
                }
            });
        }
    }

    public static sendAlert(props : MailProps, taskData : ITask, recipients : string[]) {
        const type : string = taskData.type === TaskType.READ_TASK_TYPE ? "Получено задание на ознакомление с документом " : "Получено задание на пересылку документа ";
        const message = `Получено новое задание по документу \"${taskData.document}\".\n`
            + `Сроки выполнения: с ${taskData.startDate.toLocaleDateString()} по ${taskData.deadline.toLocaleDateString()}.\n`;
        const queue = new Queue<string[]>();
        queue.push(["EHLO " + (props.mailIp ? props.mailIp : "")]);
        queue.push(["AUTH LOGIN"]);
        queue.push([Buffer.from(props.senderLogin).toString("base64")]);
        queue.push([Buffer.from(props.senderPassword).toString("base64")]);
        queue.push(["MAIL FROM: <" + props.senderLogin + ">"]);
        for(const curRecipient of recipients)
            queue.push(["RCPT TO: <" + curRecipient + ">"]);
        queue.push(["DATA"]);
        queue.push([
            "From: DocumentInform <" + props.senderLogin + ">",
            "Subject: " + type + "\"" + taskData.document + "\"",
            "\n",
            message,
            "."
        ]);
        queue.push(["QUIT"]);
        this.sendMail(props, queue);
    }

    public static sendFeedback(props : MailProps, senderId : number, message : string, sender? : string, subject? : string) {
        subject = "Feedback: "  + (subject ? subject.replace(/(\n\r*)+/g, ";") : Date.now().toLocaleString());
        message = message.replace(/(\n\r*)+/g, "\n").replace(/(\n\.+$)/gm, "\n");
        const queue = new Queue<string[]>();
        queue.push(["EHLO " + (props.mailIp ? " " + props.mailIp : "")]);
        queue.push(["AUTH LOGIN"]);
        queue.push([Buffer.from(props.senderLogin).toString("base64")]);
        queue.push([Buffer.from(props.senderPassword).toString("base64")]);
        queue.push(["MAIL FROM: <" + props.senderLogin + ">"]);
        queue.push(["RCPT TO: <" + props.senderLogin + ">"]);
        queue.push(["DATA"]);
        queue.push([
            "From: DocumentInform <" + props.senderLogin + ">",
            "Subject: " + subject,
            "\n",
            "Отправитель: " + (sender ? sender : "No name"),
            "ID отправителя: " + senderId,
            message,
            "\n",
            "."
        ]);
        queue.push(["QUIT"]);
        this.sendMail(props, queue);
    }
}