import { PositionRecord, SelfcontrolReport } from "../models/report/selfcontrolReport";
import { TaskReport } from "../models/report/taskReport";
import { TaskDetails, TaskState } from "../models/report/taskDetails";
import { ITask } from "../models/task/iTask";
import { UserData } from "../models/user/userData";
import IPositionRepository from "../repositories/data/position/iPositionRepository";
import ITaskRepository from "../repositories/data/task/iTaskRepository";
import { DocumentReport } from "../models/report/documentReport";
import IDocumentRepository from "../repositories/data/document/iDocumentRepository";

export default class ReportService {
    protected positionRepository : IPositionRepository;
    protected taskRepository : ITaskRepository;
    protected documentRepository : IDocumentRepository;
    
    constructor(positionRepository : IPositionRepository, taskRepository : ITaskRepository, documentRepository : IDocumentRepository) {
        this.positionRepository = positionRepository;
        this.taskRepository = taskRepository;
        this.documentRepository = documentRepository;
    }

    protected static createLine (task : ITask, report : SelfcontrolReport) : TaskDetails {
        return new TaskDetails(task, (task.deadline < report.date) ? TaskState.TSTAT_DEADLINE_VIOLATION : TaskState.TSTAT_ACTIVE );
    }

    protected async loadPersonalTasks(userID : number, report : SelfcontrolReport) {
        const activeTasks = await this.taskRepository.userActiveTasks(userID)
        const completedTask = await this.taskRepository.userCompleteTasks(userID);
        const record : PositionRecord = new PositionRecord("Личные задания");
        activeTasks.forEach(task => record.add(ReportService.createLine(task, report)));
        completedTask.forEach(task => record.add(new TaskDetails(task.completedTask, TaskState.TSTAT_COMPLETE)));
        if (record.tasks.length != 0)
            report.add(record);
    }

    protected async loadPositionTasks(userID : number, report : SelfcontrolReport) {
        const positions = (await this.positionRepository.getUserPositions(userID)).map(p => p.id)
            .concat(await this.positionRepository.workerDeputies(userID));
        for (const position of positions) {
            const record : PositionRecord = new PositionRecord((await this.positionRepository.findByID(position)).name);
            (await this.taskRepository.positionActiveTasks(position)).forEach(task => record.add(ReportService.createLine(task, report)));
            (await this.taskRepository.positionCompleteTasks(position)).forEach(task => record.add(
                new TaskDetails(task.completedTask, TaskState.TSTAT_COMPLETE))
            );
            if (record.tasks.length != 0)
                report.add(record);
        }
    }
    
    public async getSelfcontrolReport(user : UserData) : Promise<SelfcontrolReport> {
        const report = new SelfcontrolReport(user.secondName + " " + user.firstName + " " + user.secondName);
        await this.loadPersonalTasks(user.id, report);
        await this.loadPositionTasks(user.id, report);
        return report;
    }

    public async getTaskReport(taskID : number) : Promise<TaskReport> {
        return this.taskRepository.taskReport(taskID);
    }

    public async getDocumentReport(docID: number) : Promise<DocumentReport> {
        const report = new DocumentReport();
        report.date = new Date();
        report.document = await this.documentRepository.findOne(docID);
        report.report = await this.taskRepository.documentTasks(docID);
        return report;
    }
}