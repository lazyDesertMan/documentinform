import { Position } from "../models/organization/position";
import { PositionDeputies } from "../models/organization/positionDeputies";
import { UserData } from "../models/user/userData";
import IPositionRepository from "../repositories/data/position/iPositionRepository";
import IUserRepository from "../repositories/data/user/iUserRepository";

export default class PositionService {
    protected positionRepository : IPositionRepository;
    protected userRepository : IUserRepository;

    constructor(positionRepository : IPositionRepository, userRepository : IUserRepository) {
        this.positionRepository = positionRepository;
        this.userRepository = userRepository;
    }

    public async getPositions(userID : number) : Promise<Position[]> {
        return this.positionRepository.getUserPositions(userID);
    }

    public async getReplaces(userID : number) : Promise<Position[]> {
        const positions : Position[] =[];
        const deputies = await this.positionRepository.workerDeputies(userID);
        for (const deputy of deputies) {
            positions.push(await this.positionRepository.findByID(deputy));
        }
        return positions;
    }

    public async addPosition(name : string, groupID : number) : Promise<number> {
        return this.positionRepository.add(new Position().init(0, name, groupID, null));
    }

    public async updatePosition(posID : number, groupID : number, workerID : number, name : string) : Promise<boolean> {
        return this.positionRepository.update(posID, groupID, workerID, name);
    }

    public async removePosition(positionID: number) : Promise<boolean> {
        return this.positionRepository.remove(positionID);
    }

    public async positionList() : Promise<Position[]> {
        return this.positionRepository.list();
    }

    public async getFreePositions(groupID : number) : Promise<Position[]> {
        return this.positionRepository.freePositions(groupID);
    }

    public async getGroupPositions(groupID : number) : Promise<Position[]> {
        return this.positionRepository.groupPositions(groupID);
    }

    public async getFreeWorkers() : Promise<UserData[]> {
        const positions = (await this.positionRepository.list()).map(p => p.workerID);
        return (await this.userRepository.workersList()).filter(r => !positions.includes(r.id));
    }

    public async addDeputy(posID : number, replacerID : number, startDate : Date, endDate : Date) : Promise<boolean> {
        return this.positionRepository.addDeputy(posID, replacerID, startDate, endDate);
    }

    public async removeDeputy(position: number, replacer: number, removeTemporary: boolean) : Promise<boolean> {
        return this.positionRepository.removeDeputy(position, replacer, removeTemporary);
    }

    public async positionDeputiers(position: number) : Promise<PositionDeputies> {
        const deputies = await this.positionRepository.positionDeputiers(position);
        const list = new PositionDeputies();
        list.position = await this.positionRepository.findByID(position);
        list.temporaryDeputies = deputies[0];
        list.deputies = deputies[1];
        return list;
    }
}
