import { CompleteFact } from "../models/task/completeFact";
import { ITask, TaskType } from "../models/task/iTask";
import { UserData } from "../models/user/userData";
import ITaskRepository from "../repositories/data/task/iTaskRepository";
import IDocumentRepository from "../repositories/data/document/iDocumentRepository";
import IPositionRepository from "../repositories/data/position/iPositionRepository";
import { Position } from "../models/organization/position";
import IUserRepository from "../repositories/data/user/iUserRepository";
import MailService, { MailProps } from "./mailServeice";
import { ReadTask } from "../models/task/readTask";
import { ResendTask } from "../models/task/resendTask";

/**
 * Контроллер заданий
 */
class TaskService {
    protected readonly taskRepository     : ITaskRepository;
    protected readonly documentRepository : IDocumentRepository;
    protected readonly positionRepository : IPositionRepository;
    protected readonly userRepository     : IUserRepository;

    constructor (
        taskRepository : ITaskRepository,
        documentRepository : IDocumentRepository,
        positionRepository : IPositionRepository,
        userRepository : IUserRepository
    ) {
        this.taskRepository = taskRepository;
        this.documentRepository = documentRepository;
        this.positionRepository = positionRepository;
        this.userRepository = userRepository;
    }

    /**
     * Получение списка активных заданий пользователя
     * @param user Пользователь, для которого ведётся формирование списка активных заданий
     * @returns Список активных заданий
     */
    public async activeTaskList (user : UserData): Promise<ITask[]> {
        const positions = await this.positionRepository.getUserPositions(user.id);
        const deputies = await this.positionRepository.workerDeputies(user.id);
        let tasks = await this.taskRepository.userActiveTasks(user.id);
        for (let idx = 0; idx < positions.length; idx++) {
            tasks = tasks.concat(await this.taskRepository.positionActiveTasks(positions[idx].id));
        }
        for (let idx = 0; idx < deputies.length; idx++) {
            tasks = tasks.concat(await this.taskRepository.positionActiveTasks(deputies[idx]));
        }
        return tasks;
    }

    /**
     * Получение списка завершённых заданий пользователя
     * @param user Пользователь, для которого ведётся формирование списка завершённых заданий
     * @returns Список завершённых заданий
     */
    public async completedTaskList (user : UserData) : Promise<CompleteFact[]> {
        const positions = await this.positionRepository.getUserPositions(user.id);
        const deputies = await this.positionRepository.workerDeputies(user.id);
        let tasks = await this.taskRepository.userCompleteTasks(user.id);
        for (let idx = 0; idx < positions.length; idx++) {
            tasks = tasks.concat(await this.taskRepository.positionCompleteTasks(positions[idx].id));
        }
        for (let idx = 0; idx < deputies.length; idx++) {
            tasks = tasks.concat(await this.taskRepository.positionCompleteTasks(deputies[idx]));
        }
        return tasks;
    }

    /**
     * Получение списка заданий на пересылку, полученных пользователем
     * @param userID ID пользователя, для которого ведётся формирование списка заданий
     * @returns Список завершённых заданий
     */
     public async resendList (userID : number, startDate : Date, endDate : Date) : Promise<ITask[]> {
        const positions = await this.positionRepository.getUserPositions(userID);
        const deputies = await this.positionRepository.workerDeputies(userID);
        const tasks : ITask[] = await this.taskRepository.resendList(positions.map(p => p.id).concat(deputies), startDate, endDate);
        return tasks;
    }

    public async addTask (mailProps : MailProps, tsk : ITask) : Promise<number> {
        const doc = await this.documentRepository.findOne(tsk.documentID);
        if (!doc)
            throw("Документ не существует");
        tsk.document = doc.name;
        const tskID = await this.taskRepository.add(tsk);
        if (tskID !== null) {
            if (tsk.type === TaskType.RESEND_TASK_TYPE) {
                const recipient : number = (tsk as ResendTask).recipient;
                const deputies = await this.positionRepository.positionDeputiers(recipient);
                const recipientsPosition = [recipient].concat(deputies[0].map(d => d.position)).concat(deputies[1].map(d => d.replacer)); 
                const positionsData = await this.positionRepository.findByIDList(recipientsPosition);
                const userMails : string[] = [];
                for (const position of positionsData) {
                    if (position.workerID != null) {
                        const user = await this.userRepository.findByID(position.workerID);
                        if (user && user.email)
                            userMails.push(user.email);
                    }
                }
                MailService.sendAlert(mailProps, await this.taskRepository.findByID(tskID), userMails);
            }
            else if (tsk.type === TaskType.READ_TASK_TYPE) {
                const recipient = await this.userRepository.findByID((tsk as ReadTask).recipient);
                if (recipient.email)
                    MailService.sendAlert(mailProps, await this.taskRepository.findByID(tskID), [recipient.email]);
            }
        }
        return tskID;
    }

    public async comleteTask(user : UserData, taskID : number) {
        await this.taskRepository.setCompleted(taskID, user.id);
    }
}

export default TaskService;
