import { Group, GroupHierarchy } from "../models/organization/group";
import { Position } from "../models/organization/position";
import { UserData } from "../models/user/userData";
import IGroupRepository from "../repositories/data/group/iGroupRepository";
import IPositionRepository from "../repositories/data/position/iPositionRepository";
import IUserRepository from "../repositories/data/user/iUserRepository";

export default class GroupService {
    protected groupRepository: IGroupRepository;
    protected positionRepository: IPositionRepository;
    protected userRepository: IUserRepository;

    constructor(groupRepository: IGroupRepository, positionRepository: IPositionRepository, userRepository: IUserRepository) {
        this.groupRepository = groupRepository;
        this.positionRepository = positionRepository;
        this.userRepository = userRepository;
    }

    public async groupList() : Promise<Group[]> {
        return this.groupRepository.groupList();
    }

    public async groupStructure() : Promise<GroupHierarchy[]> {
        return this.groupRepository.hierarchyList();
    }

    public async addGroup(group: Group, parentID: number) : Promise<number> {
        return this.groupRepository.add(group, parentID);
    }

    public async getGroup(id: number) : Promise<Group> {
        return this.groupRepository.findByID(id);
    }

    public async getGroupHierarchy(id: number) : Promise<GroupHierarchy> {
        return this.groupRepository.getHierarchy(id);
    }

    public async update(groupID: number, leaderID: number, parentID: number, name: string) : Promise<boolean> {
        return this.groupRepository.update(groupID, leaderID, parentID, name);
    }

    public async remove(id: number) : Promise<boolean> {
        return this.groupRepository.remove(id);
    }

    // TODO: Перенести часть запросов из этой части в репозитории для оптимизации
    public async getSubordinateWorkers(position: number) : Promise<UserData[]> {
        const users: Set<number> = new Set();
        const subordinateGroup = await this.groupRepository.findByLeader(position);
        if (subordinateGroup != null) {
            const subgroups = await this.groupRepository.subgroups(subordinateGroup);
            for (const group of subgroups) {
                const user = (await this.positionRepository.findByID(group.leaderPositionID)).workerID;
                if (user != null)
                    users.add(user);
            }
            const subordinates = await this.positionRepository.groupPositions(subordinateGroup);
            const leaderIdx = subordinates.findIndex(p => p.id == position);
            if (leaderIdx != -1)
                subordinates.splice(leaderIdx, 1);
            for (const position of subordinates)
                if (position.workerID != null)
                    users.add(position.workerID);
        }
        const res: UserData[] = [];
        for (const user of users)
            res.push(await this.userRepository.findByID(user));
        return res;
    }

    // TODO: Перенести часть запросов из этой части в репозитории для оптимизации
    public async getSubordinateLeaders(position: number) : Promise<Position[]> {
        const res: Position[] = [];
        const subordinateGroup = await this.groupRepository.findByLeader(position);
        if (subordinateGroup != null) {
            const subgroups = await this.groupRepository.subgroups(subordinateGroup);
            for (const group of subgroups) {
                res.push(await this.positionRepository.findByID(group.leaderPositionID))
            }
        }
        return res;
    }

    public async getTopLeaders() : Promise<Position[]> {
        const topLeaders = await this.groupRepository.topLeaders();
        return this.positionRepository.findByIDList(topLeaders);
    }

    public async subordinateList(leaderID : number) : Promise<Position[]> {
        const leaderPositions = (await this.positionRepository.getUserPositions(leaderID)).map(p => p.id); 
        const subordinateGroups = await this.groupRepository.subordinateGroupsByList(leaderPositions);
        const subordinates = await this.positionRepository.groupPositionsByList(subordinateGroups);
        return subordinates;
    }

    public async isSubordinate(leaderID : number, workerID : number) : Promise<boolean> {
        const subordinates = await this.subordinateList(leaderID);
        for (const curSubordinate of subordinates)
            if (curSubordinate.workerID === workerID)
                return true;
        return false;
    }

    public async isLeader(idList : number[]) : Promise<boolean> {
        return this.groupRepository.isLeader(idList);
    }
}
