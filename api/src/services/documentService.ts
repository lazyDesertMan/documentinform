import Document from "../models/document/document";
import { UserData } from "../models/user/userData";
import IDocumentRepository from "../repositories/data/document/iDocumentRepository";
import ITaskRepository from "../repositories/data/task/iTaskRepository";

class DocumentService {
    readonly documentRepository : IDocumentRepository;
    readonly taskRepository : ITaskRepository;

    constructor (documentRepository : IDocumentRepository, taskRepository : ITaskRepository) {
        this.documentRepository = documentRepository;
        this.taskRepository = taskRepository;
    }

    public async list() : Promise<Document[]> {
        return this.documentRepository.list();
    }

    public async userDocs(user : UserData) : Promise<Document[]> {
        const docs = await  this.taskRepository.allowedDocs(user.id);
        return this.documentRepository.find(docs);
    }

    public async add(doc : Document) : Promise<number> {
        return this.documentRepository.add(doc);
    }

    public async update(docID: number, name: string, description: string, effectiveDate: Date, oldVersionId: number) : Promise<boolean> {
        return this.documentRepository.update(docID, name, description, effectiveDate, oldVersionId);
    }

    public async findByID(id : number) : Promise<Document> {
        return this.documentRepository.findOne(id);
    }
}

export default DocumentService;
